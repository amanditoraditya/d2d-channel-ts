'use strict'
//STATICS
const Dictionary = require('../resources/statics/dictionary')
const admin = require('firebase-admin')
const Env = use('Env')

module.exports = {
	micro_default_options: {
		type : 'ERR_500',
		response_type : 'obj',
		reason : Dictionary.TR000000,
		exec_time : null
	},
	default_values : {
		page_paginate : 1,
		page_limit : 100,
		gender : {
			M : {
				initial : "M",
				capital : "MALE"
			},
			F : {
				initial : "F",
				capital : "FEMALE"
			}
		}
	}
}

let gsac = require('../' + Env.get('GOOGLE_APPLICATION_CREDENTIAL_STG'))
let fbdatabaseURL = Env.get('FIREBASE_DB_STG')
if(Env.get('NODE_ENV') == 'production'){
	gsac = require('../' + Env.get('GOOGLE_APPLICATION_CREDENTIAL_PRD'))
	fbdatabaseURL = Env.get('FIREBASE_DB_PRD')
}
let serviceAccount = gsac
admin.initializeApp({
    credential: admin.credential.cert(gsac),
    databaseURL: fbdatabaseURL
})
