'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Next = use('Adonis/Addons/Next')
const handler = Next.getRequestHandler()

Route.group(() => {
	Route.get('/', ({ request, response }) => {
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			title: 'Welcome to Empty Engine',
			description: 'Empty Engine API Services',
			version: '1.0',
			copyright: 'PT Global Urban Esensial'
		}
		return response.send(data)
	})
	
	Route.post('/login', 'AccountController.login').formats(['json'])
	Route.post('/login-by-name', 'AccountController.loginByName').formats(['json'])
	Route.post('/checklogin', 'AccountController.checklogin').formats(['json'])
	Route.post('/cleartoken', 'AccountController.clearToken').formats(['json'])
}).prefix('api/v1')

Route.group(() => {
	Route.post('/checkauth', 'AccountController.checkauth').formats(['json'])
	Route.post('/checkpass', 'AccountController.checkpass').formats(['json'])
	
	Route.get('/users/get-user', 'UserController.getUser').formats(['json'])
	Route.post('/users/datatable', 'UserController.datatable').formats(['json'])
	Route.post('/users/create', 'UserController.create').formats(['json'])
	Route.post('/users/edit', 'UserController.edit').formats(['json'])
	Route.post('/users/update/:id', 'UserController.update').formats(['json'])
	Route.post('/users/delete', 'UserController.delete').formats(['json'])
	Route.post('/users/multidelete', 'UserController.multidelete').formats(['json'])
	
	Route.get('/role/get-role', 'RoleController.getRole').formats(['json'])
	Route.get('/role/get-module-list', 'RoleController.getModuleList').formats(['json'])
	Route.post('/role/datatable', 'RoleController.datatable').formats(['json'])
	Route.post('/role/create', 'RoleController.create').formats(['json'])
	Route.post('/role/edit', 'RoleController.edit').formats(['json'])
	Route.post('/role/update/:id', 'RoleController.update').formats(['json'])
	Route.post('/role/delete', 'RoleController.delete').formats(['json'])
	Route.post('/role/multidelete', 'RoleController.multidelete').formats(['json'])
	
	Route.post('/setting/get-setting', 'SettingController.getSetting').formats(['json'])
	Route.post('/setting/get-setting-by-key', 'SettingController.getSettingByKey').formats(['json'])
	Route.post('/setting/datatable', 'SettingController.datatable').formats(['json'])
	Route.post('/setting/create', 'SettingController.create').formats(['json'])
	Route.post('/setting/edit', 'SettingController.edit').formats(['json'])
	Route.post('/setting/update/:id', 'SettingController.update').formats(['json'])
	Route.post('/setting/update-with-file/:id', 'SettingController.updateWithFile').formats(['json'])
	Route.post('/setting/delete', 'SettingController.delete').formats(['json'])
	Route.post('/setting/multidelete', 'SettingController.multidelete').formats(['json'])
	
	Route.post('/activity-log/datatable', 'SystemLogController.datatable').formats(['json'])

	// ----------------------- channel --------------------------------
	Route.get('/channel/get-channel-type', 'ChannelController.getChannelType').formats(['json'])
	Route.get('/channel/get-channels', 'ChannelController.getChannels').formats(['json'])
	Route.post('/channel/datatable', 'ChannelController.datatable').formats(['json'])
	Route.post('/channel/create', 'ChannelController.create').formats(['json'])
	Route.post('/channel/edit', 'ChannelController.edit').formats(['json'])
	Route.post('/channel/update/:id', 'ChannelController.update').formats(['json'])
	Route.post('/channel/delete', 'ChannelController.delete').formats(['json'])
	Route.post('/channel/multidelete', 'ChannelController.multidelete').formats(['json'])
	Route.post('/channel/update-status', 'ChannelController.updateStatus').formats(['json'])
	Route.get('/channel/banners/:id', 'ChannelController.channelBanner').formats(['json'])
	Route.post('/channel/banners/:id', 'ChannelController.updateBanner').formats(['json'])
	Route.get('/channel/qr/:id', 'ChannelController.channelQr').formats(['json'])
	
	// ----------------------- menu --------------------------------
	Route.get('/menu/get-menu', 'MenuController.getMenu').formats(['json'])
	Route.post('/menu/datatable', 'MenuController.datatable').formats(['json'])
	Route.post('/menu/create', 'MenuController.create').formats(['json'])
	Route.post('/menu/edit', 'MenuController.edit').formats(['json'])
	Route.post('/menu/update/:id', 'MenuController.update').formats(['json'])
	Route.post('/menu/delete', 'MenuController.delete').formats(['json'])
	Route.post('/menu/multidelete', 'MenuController.multidelete').formats(['json'])
	Route.post('/menu/update-status', 'MenuController.updateStatus').formats(['json'])
	
	// ----------------------- channel conference --------------------------------
	Route.post('/channel-conference/datatable', 'ChannelConferenceController.datatable').formats(['json'])
	Route.post('/channel-conference/create', 'ChannelConferenceController.create').formats(['json'])
	Route.post('/channel-conference/edit', 'ChannelConferenceController.edit').formats(['json'])
	Route.post('/channel-conference/view-conference', 'ChannelConferenceController.viewConference').formats(['json'])
	Route.post('/channel-conference/update/:id', 'ChannelConferenceController.update').formats(['json'])
	Route.post('/channel-conference/delete', 'ChannelConferenceController.delete').formats(['json'])
	Route.post('/channel-conference/multidelete', 'ChannelConferenceController.multidelete').formats(['json'])
	Route.get('/channel-conference/get-conference-subscribers', 'ChannelConferenceController.getConferenceSubscribers').formats(['json'])
	Route.post('/channel-conference/update-status', 'ChannelConferenceController.updateStatus').formats(['json'])
	
	// ----------------------- channel conference attendee --------------------------------
	Route.post('/channel-conference-attendee/datatable', 'ChannelConferenceAttendeeController.datatable').formats(['json'])
	Route.post('/channel-conference-attendee/update/:id', 'ChannelConferenceAttendeeController.update').formats(['json'])
	Route.post('/channel-conference-attendee/delete', 'ChannelConferenceAttendeeController.delete').formats(['json'])
	Route.post('/channel-conference-attendee/multidelete', 'ChannelConferenceAttendeeController.multidelete').formats(['json'])

	// ----------------------- user channel --------------------------------
	Route.post('/user-channel/datatable', 'UserChannelController.datatable').formats(['json'])
	Route.post('/user-channel/create', 'UserChannelController.create').formats(['json'])
	Route.post('/user-channel/edit', 'UserChannelController.edit').formats(['json'])
	Route.post('/user-channel/update/:id', 'UserChannelController.update').formats(['json'])
	Route.post('/user-channel/delete', 'UserChannelController.delete').formats(['json'])
	Route.post('/user-channel/multidelete', 'UserChannelController.multidelete').formats(['json'])
	Route.get('/user-channel/get-channel-role-list', 'UserChannelController.getChannelRoleList').formats(['json'])
	Route.post('/user-channel/update-status', 'UserChannelController.updateStatus').formats(['json'])

	// ----------------------- Forum --------------------------------
	Route.post('/forum/datatable', 'ForumController.datatable').formats(['json'])
	Route.post('/forum/create', 'ForumController.create').formats(['json'])
	Route.post('/forum/edit', 'ForumController.edit').formats(['json'])
	Route.post('/forum/update/:id', 'ForumController.update').formats(['json'])
	Route.post('/forum/delete', 'ForumController.delete').formats(['json'])
	Route.post('/forum/multidelete', 'ForumController.multidelete').formats(['json'])
	Route.post('/forum/update-status', 'ForumController.updateStatus').formats(['json'])

	// ----------------------- Forum Comment --------------------------------
	Route.post('/forum-comment/datatable', 'ForumCommentController.datatable').formats(['json'])
	Route.post('/forum-comment/create', 'ForumCommentController.create').formats(['json'])
	Route.post('/forum-comment/edit', 'ForumCommentController.edit').formats(['json'])
	Route.post('/forum-comment/update/:id', 'ForumCommentController.update').formats(['json'])
	// Route.post('/forum-comment/delete', 'ForumController.delete').formats(['json'])
	// Route.post('/forum-comment/multidelete', 'ForumController.multidelete').formats(['json'])

	// ----------------------- channel role --------------------------------

	Route.get('/channelroles/get-channel-menu', 'ChannelRolesController.getChannelMenu').formats(['json'])
	Route.get('/channelroles/get-channel', 'ChannelRolesController.getChannel').formats(['json'])
	Route.post('/channelroles/datatable', 'ChannelRolesController.datatable').formats(['json'])
	Route.post('/channelroles/create', 'ChannelRolesController.create').formats(['json'])
	Route.post('/channelroles/edit', 'ChannelRolesController.edit').formats(['json'])
	Route.post('/channelroles/update/:id', 'ChannelRolesController.update').formats(['json'])
	Route.post('/channelroles/delete', 'ChannelRolesController.delete').formats(['json'])
	Route.post('/channelroles/multidelete', 'ChannelRolesController.multidelete').formats(['json'])
	Route.post('/channelroles/update-status', 'ChannelRolesController.updateStatus').formats(['json'])

	// ----------------------- channel job ---------------------------------
	
	Route.get('/channel-job/get-channel-job-type', 'ChannelJobController.getChannelJobType').formats(['json'])
	Route.post('/channel-job/datatable', 'ChannelJobController.datatable').formats(['json'])
	Route.post('/channel-job/create', 'ChannelJobController.create').formats(['json'])
	Route.get('/channel-job/detail/:id', 'ChannelJobController.edit').formats(['json'])
	Route.post('/channel-job/update/:id', 'ChannelJobController.update').formats(['json'])
	Route.post('/channel-job/update-status', 'ChannelJobController.updateStatus').formats(['json'])
	Route.post('/channel-job/delete', 'ChannelJobController.delete').formats(['json'])
	Route.post('/channel-job/multidelete', 'ChannelJobController.multidelete').formats(['json'])
	
	// ----------------------- channel job spec ---------------------------------
	
	Route.post('/channel-job-spec/datatable', 'ChannelJobSpecController.datatable').formats(['json'])
	Route.post('/channel-job-spec/datatable-detail', 'ChannelJobSpecController.datatableDetail').formats(['json'])
	Route.post('/channel-job-spec/create/:id', 'ChannelJobSpecController.create').formats(['json'])
	Route.post('/channel-job-spec/edit', 'ChannelJobSpecController.edit').formats(['json'])
	Route.post('/channel-job-spec/update/:id', 'ChannelJobSpecController.update').formats(['json'])
	Route.post('/channel-job-spec/delete', 'ChannelJobSpecController.delete').formats(['json'])
	Route.post('/channel-job-spec/multidelete', 'ChannelJobSpecController.multidelete').formats(['json'])
	
	// ----------------------- channel job applicant ---------------------------------
	
	Route.post('/channel-job-applicant/datatable-detail', 'ChannelJobApplicantController.datatableDetail').formats(['json'])
	
	// ----------------------- channel job header ---------------------------------
	Route.get('/channel-job-header/get-header-attribute', 'ChannelJobHeaderController.getChannelJobHeaderAttribute').formats(['json'])
	Route.post('/channel-job-header/submit-data/:id', 'ChannelJobHeaderController.submitData').formats(['json'])
	Route.post('/channel-job-header/datatable', 'ChannelJobHeaderController.datatable').formats(['json'])
	Route.post('/channel-job-header/datatable-detail', 'ChannelJobHeaderController.datatableDetail').formats(['json'])
	Route.post('/channel-job-header/create/:id', 'ChannelJobHeaderController.create').formats(['json'])
	Route.post('/channel-job-header/edit', 'ChannelJobHeaderController.edit').formats(['json'])
	Route.post('/channel-job-header/update/:id', 'ChannelJobHeaderController.update').formats(['json'])
	Route.post('/channel-job-header/delete', 'ChannelJobHeaderController.delete').formats(['json'])
	Route.post('/channel-job-header/multidelete', 'ChannelJobHeaderController.multidelete').formats(['json'])
	
	// ----------------------- channel banner --------------------------------
	Route.post('/channel-banner/datatable', 'ChannelBannerController.datatable').formats(['json'])
	Route.post('/channel-banner/create', 'ChannelBannerController.create').formats(['json'])
	Route.post('/channel-banner/edit', 'ChannelBannerController.edit').formats(['json'])
	Route.post('/channel-banner/update/:id', 'ChannelBannerController.update').formats(['json'])
	Route.post('/channel-banner/delete', 'ChannelBannerController.delete').formats(['json'])
	Route.post('/channel-banner/multidelete', 'ChannelBannerController.multidelete').formats(['json'])
	Route.post('/channel-banner/update-status', 'ChannelBannerController.updateStatus').formats(['json'])
	
	//specialist
	Route.get('/specialist/list', 'SpecialistController.list').formats(['json'])
	Route.get('/history-record/datatable', 'HistoryRecordController.datatable').as('history-record.datatable')

	// -------------------------------------- header master ------------------------------------------------
	Route.post('/header-master/datatable', 'HeaderMasterController.datatable').formats(['json'])
	Route.post('/header-master/create/', 'HeaderMasterController.create').formats(['json'])
	Route.get('/header-master/detail/:id', 'HeaderMasterController.edit').formats(['json'])
	Route.post('/header-master/update/:id', 'HeaderMasterController.update').formats(['json'])
	Route.post('/header-master/delete', 'HeaderMasterController.delete').formats(['json'])
	Route.post('/header-master/multidelete', 'HeaderMasterController.multidelete').formats(['json'])
	Route.post('/header-master/update-status', 'HeaderMasterController.updateStatus').formats(['json'])

	// -------------------------------------- channel brochure ------------------------------------------------
	Route.post('/channel-brochure/datatable', 'ChannelBrochureController.datatable').formats(['json'])
	Route.post('/channel-brochure/create/', 'ChannelBrochureController.create').formats(['json'])
	Route.get('/channel-brochure/detail/:id', 'ChannelBrochureController.edit').formats(['json'])
	Route.post('/channel-brochure/update/:id', 'ChannelBrochureController.update').formats(['json'])
	Route.post('/channel-brochure/delete', 'ChannelBrochureController.delete').formats(['json'])
	Route.post('/channel-brochure/multidelete', 'ChannelBrochureController.multidelete').formats(['json'])
	Route.post('/channel-brochure/update-status', 'ChannelBrochureController.updateStatus').formats(['json'])
	Route.post('/channel-brochure/restore', 'ChannelBrochureController.restore').formats(['json'])

}).prefix('api/v1')
.middleware('auth')
.middleware('roles')
.middleware('globalparam')

Route.get('/logout', 'AccountController.logout')

Route.get('/service-worker.js', ({ request, response }) => {
	response.download('./next/.next/service-worker.js')
})

Route.get('/manifest.json', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/manifest.json')
})

Route.get('/manifest.webmanifest', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/manifest.webmanifest')
})

Route.get('/OneSignalSDKWorker.js', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/OneSignalSDKWorker.js')
})

Route.get('/OneSignalSDKUpdaterWorker.js', ({ request, response }) => {
	response.download('./next/public/static/one/vendor/OneSignalSDKUpdaterWorker.js')
})

Route.get('/downloads', ({ request, response }) => {
	let req = request.get()
	let path = req.path
	let file = req.file
	response.download('public/uploads/' + path + file)
})

Route.get('/static/uploads/images/:file', ({ params, request, response }) => {
	response.download('./next/public/static/uploads/images/' + params.file)
})

// Router for Next.js
require('./next.js')
// Router for API
require('./api.js')

Route.get('*', ({ request, response }) =>
	new Promise((resolve, reject) => {
		handler(request.request, response.response, promise => {
			promise.then(resolve).catch(reject)
		})
	})
)