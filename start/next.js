'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')
const Next = use('Adonis/Addons/Next')
const handler = Next.getRequestHandler()

Route.get('/dashboard', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/dashboard', query)
})

Route.get('/forbidden', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/forbidden', query)
})

Route.get('/users', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/users/index', query)
})
Route.get('/users/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/users/create', query)
})
Route.get('/users/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/users/edit', query)
})

Route.get('/role', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/role/index', query)
})
Route.get('/role/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/role/create', query)
})
Route.get('/role/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/role/edit', query)
})

Route.get('/setting', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/setting/index', query)
})
Route.get('/setting/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/setting/create', query)
})
Route.get('/setting/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/setting/edit', query)
})

Route.get('/activity-log', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/activity-log/index', query)
})


Route.get('/channel', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel/index', query)
})
Route.get('/channel/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel/create', query)
})
Route.get('/channel/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel/edit', query)
})

Route.get('/channelroles', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channelroles/index', query)
})
Route.get('/channelroles/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channelroles/create', query)
})
Route.get('/channelroles/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channelroles/edit', query)
})

Route.get('/menu', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/menu/index', query)
})

Route.get('/menu/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/menu/create', query)
})

Route.get('/menu/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/menu/edit', query)
})


// --------------------------------- user channel ----------------------------------------------
Route.get('/user-channel', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/user-channel/index', query)
})
Route.get('/user-channel/create', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/user-channel/create', query)
})
Route.get('/user-channel/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	console.log('query', query)
	return Next.render(request.request, response.response, '/module/user-channel/edit', query)
})

// ------------------------------------- Forum ----------------------------------------------
Route.get('/discussion', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/forum/index', query)
})
Route.get('/discussion/create', ({params, request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/forum/create', query)
})
Route.get('/discussion/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/forum/edit', query)
})
Route.get('/discussion/detail/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/forum/detail', query)
})

// ------------------------------------- Forum Comment ----------------------------------------------
// Route.get('/forum-comment', ({params, request, response }) => {
// 	const query = request.get()
// 	return Next.render(request.request, response.response, '/module/forum-comment/index', query)
// })
// Route.get('/forum-comment/create', ({params, request, response }) => {
// 	const query = request.get()
// 	return Next.render(request.request, response.response, '/module/forum-comment/create', query)
// })
// Route.get('/forum-comment/edit/:id', ({ params, request, response }) => {
// 	let qparams = {
// 		id: params.id
// 	}
// 	const query = qparams
// 	console.log('query', query)
// 	return Next.render(request.request, response.response, '/module/forum-comment/edit', query)
// })

// -------------------------- channel conference --------------------------------------------------

Route.get('/channel-conference', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-conference/index', query)
})
Route.get('/channel-conference/create', ({ request, response }) => {	
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-conference/create', query)
})
Route.get('/channel-conference/view-conference/:id', ({ params, request, response }) => {	
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-conference/view-conference', query)
})
Route.get('/channel-conference/view-attendee/:id', ({ params, request, response }) => {	
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-conference/view-attendee', query)
})
Route.get('/channel-conference/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-conference/edit', query)
})
// FOR VIDEO CONFERENCE
Route.get('/meet', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/meet', query)
})

// -------------------------- channel job --------------------------------------------------

Route.get('/channel-job', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-job/index', query)
})

Route.get('/channel-job/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-job/create', query)
})

Route.get('/channel-job/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job/edit', query)
})

Route.get('/channel-job/detail/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job/detail', query)
})

Route.get('/channel-job/copy/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id,
		copy: true
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job/edit', query)
})


// -------------------------- channel job spec ------------------------------------------------

Route.get('/channel-job-spec/index/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-spec/index', query)
})

Route.get('/channel-job-spec/create/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-spec/create', query)
})

Route.get('/channel-job-spec/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-spec/edit', query)
})

// -------------------------- channel job header ----------------------------------------------

Route.get('/channel-job-header/index/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-header/index', query)
})

Route.get('/channel-job-header/create/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-header/create', query)
})

Route.get('/channel-job-header/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-job-header/edit', query)
})

// -------------------------- header master ---------------------------------------------------

Route.get('/header-master', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/header-master/index', query)
})

Route.get('/header-master/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/header-master/create', query)
})

Route.get('/header-master/edit/:id/', ({ params, request, response }) => {
	let qparams = {
		id: params.id,
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/header-master/edit', query)
})


// -------------------------- channel banner --------------------------------------------------
Route.get('/channel-banner', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-banner/index', query)
})

Route.get('/channel-banner/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-banner/create', query)
})

Route.get('/channel-banner/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-banner/edit', query)
})

// -------------------------- channel brochure --------------------------------------------------
Route.get('/channel-brochure', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-brochure/index', query)
})

Route.get('/channel-brochure/create', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/channel-brochure/create', query)
})

Route.get('/channel-brochure/edit/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/module/channel-brochure/edit', query)
})

// -------------------------- history record --------------------------------------------------
Route.get('/history-record', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/module/history-record', query)
})