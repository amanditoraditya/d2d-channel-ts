const { hooks } = require('@adonisjs/ignitor')

hooks.before.providersBooted(() => {
	const View = use('View')
	const Logger = use('Logger')
	const moment = require('moment')
	const Env = use('Env')
	const crypto = require("crypto")
	
	View.global('yearNow', function () {
		let d = new Date()
		let n = d.getFullYear()
		return n
	})
	
	View.global('printDate', function () {
		return moment(new Date()).format('DD-MM-YYYY HH:mm')
	})

	View.global('formatDate', function (date, format = null) {
		const format_date = moment(new Date(date))
		if(format) {
			return format_date.format(format)	
		} else {
			return format_date.format('MM/DD/YYYY')
		}
	})

	View.global('formatRupiah', function (currency, sep) {
		let x = parseFloat(currency)
		if (x) {
			return sep + x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
		} else {
			return sep + '0'
		}
	})

	View.global('formatPoint', function (currency) {
		let x = parseFloat(currency)
		if (x) {
			return x.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
		} else {
			return '0'
		}
	})
	
	View.global('getCDN', function (file, type = 'large', category = 'product') {
		let imagetype

		if(file == '' || file == null) {
			imagetype = category + '-placeholder.jpg'
		} else {
			let splitfile = file.split('/')
			if (type == 'large') {
				imagetype = splitfile.slice(-1)[0]
			} else if (type == 'mediums') {
				let splitext = splitfile.slice(-1)[0].split('.')
				let splitname = splitext.slice(0, splitext.length - 1)
				let splitpop = splitext.slice(-1)[0].toLowerCase()
				imagetype = 'mediums/' + splitname.join('.') + '.' + splitpop
			} else {
				let splitext = splitfile.slice(-1)[0].split('.')
				let splitname = splitext.slice(0, splitext.length - 1)
				let splitpop = splitext.slice(-1)[0].toLowerCase()
				imagetype = 'thumbs/' + splitname.join('.') + '.' + splitpop
			}
		}
		return Env.get('CDN_URL') + imagetype
	})
	
	View.global('stripHtmlTags', function (txt) {
		if ((txt===null) || (txt==='')) {
			return false
		} else {
			txt = txt.toString()
			return txt.replace(/<[^>]*>/g, '')
		}
	})
	
	View.global('substrTexts', function (txt, start, finish) {
		if ((txt===null) || (txt==='')) {
			return false
		} else {
			txt = txt.substr(parseInt(start), parseInt(finish))
			let ltxt = txt.substr(0, Math.min(txt.length, txt.lastIndexOf(" ")))
			return ltxt
		}
	})

	View.global('encryptString', function (data) {
		if (data) {
			let cipher = crypto.createCipher('aes-256-cbc', Env.get('APP_KEY'))
	        let crypted = cipher.update(data.toString(), 'utf-8', 'hex')
	        crypted += cipher.final('hex')

	        return crypted
	    } else {
	    	return ''
	    }
	})

	View.global('decryptString', function (data) {
		if (data) {
			let decipher = crypto.createDecipher('aes-256-cbc', Env.get('APP_KEY'))
			let decrypted = decipher.update(data, 'hex', 'utf-8')
			decrypted += decipher.final('utf-8')

			return decrypted;
	    } else {
	    	return ''
	    }
	})
	
	View.global('randomString', function () {
		let text = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
		return text
	})

	View.global('replaceNewline', function(text) {
		return text.replace(/\n/g, "<br />")
	})

	View.global('baseURL', function () {
		return Env.get('BASE_URL') 
	})

	View.global('appKey', function () {
		return Env.get('APP_KEY') 
	})
	
	View.global('baseCDNUrl', function () {
		return Env.get('CDN_URL') 
	})

	View.global('formatLower', function (value) {
		let lower = value.toLowerCase()
		return lower
	})

	View.global('formatUppercaseFirstLetter', function (value) {
		if (value == '' || value == null) {
			return ''
		} else {
			let splitStr = value.toLowerCase().split(' ')
			for (let i = 0; i < splitStr.length; i++) {
				splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1)
			}
			return splitStr.join(' ')
		}
	})
})