module.exports = {
    input_types: [
        'text',
        'datetime',
        'time',
        'date'
    ],
    types:[
        'job',
    ],
    fe_input_types: [
        { value: 'text', label: 'text' },
        { value: 'datetime', label: 'datetime'},
        { value: 'time', label: 'time'},
        { value: 'date', label: 'date'}
    ],
    fe_types:[
        { value: 'job', label: 'job'},
    ]
}