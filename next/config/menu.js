module.exports = {
    listGroup: [
        {
            title: 'Dashboard',
            url: '/dashboard',
            icon: 'tachometer-alt'
        },
        {
            title: 'Admin Page',
            icon: 'user-tie',
            url: '/channel',
            type: 'admin-page',
            permissionCondition: (role, permission) => {
                return role == 'admin'
            },
            access: ['admin'],
            childs: [
                {
                    text: 'Channel',
                    slug: 'channel',
                    subMenus: {
                        list: '/channel',
                        create: '/channel/create'
                    }
                },
                {
                    text: 'Menu',
                    slug: 'menu',
                    subMenus: {
                        list: '/menu',
                        create: '/menu/create'
                    }
                },
                {
                    text: 'History Record',
                    slug: 'history-record',
                    url: '/history-record'
                },
                {
                    text: 'Header Master',
                    slug: 'header-master',
                    url: '/header-master'
                },
            ]
        },
        {
            title: 'Channel',
            icon: 'boxes',
            type: 'channel',
            url: '/channel-job',
            permissionCondition: (role, permission) => {
                return Object.keys(permission).length > 0 || role == 'admin'
            },
            childs: [
                {
                    text: 'User Channel',
                    slug: 'user-channel',
                    subMenus: {
                        list: '/user-channel',
                        create: '/user-channel/create',
                    }
                },
                {
                    text: 'Role Channel',
                    slug: 'channelroles',
                    subMenus: {
                        list: '/channelroles',
                        create: '/channelroles/create'
                    }
                },
                {
                    text: 'Conference',
                    slug: 'channel-conference',
                    subMenus: {
                        list: '/channel-conference',
                        create: '/channel-conference/create'
                    }
                },
                {
                    text: 'Job',
                    slug: 'channel-job',
                    subMenus: {
                        list: '/channel-job',
                        create: '/channel-job/create',
                    }
                },
                {
                    text: 'Banner',
                    slug: 'channel-banner',
                    subMenus: {
                        list: '/channel-banner',
                        create: '/channel-banner/create',
                    }
                },
                {
                    text: 'Discussion',
                    slug: 'forum',
                    subMenus: {
                        list: '/discussion',
                        create: '/discussion/create',
                    }
                },
                {
                    text: 'Brochure',
                    slug: 'channel-brochure',
                    subMenus: {
                        list: '/channel-brochure',
                        create: '/channel-brochure/create',
                    }
                },
            ]
        },
    ]
}