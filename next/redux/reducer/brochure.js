const SET_HEADER_BROCHURE = 'd2d/general/SET_HEADER_BROCHURE'

const initialState = {
    brochurePayload: {}
}

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_HEADER_BROCHURE :
            return {
                ...state,
                brochurePayload: action.brochurePayload,
            }
        default:
            return state
    }
}

export const setHeaderBrochure = data => ({
    type: SET_HEADER_BROCHURE,
    brochurePayload: data,
})

export default reducer