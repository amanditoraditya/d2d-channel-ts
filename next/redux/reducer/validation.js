
import { validationHelperGroup, validationHelper } from '../../helpers/validation'

const INIT_FORM = 'd2d/validation/INIT_FORM'
const CHECK_FORM = 'd2d/validation/CHECK_FORM'
const SET_FORM = 'd2d/validation/SET_FORM'
const NOT_VALID_FORM = 'd2d/validation/NOT_VALID_FORM'
const CHECK_VALID_FORM = 'd2d/validation/CHECK_VALID_FORM'
const UPDATE_STATE_FORM = 'd2d/validation/UPDATE_STATE_FORM'

const initialState = {
    formSet: {},
    isValidForm: false,
    isUpdateForm: false,
}

// const RULES = {
//   required: {
//     rules: ''
//   }
// }

const reducer = (state = initialState, action = {}) => {
    const initForm = {}
    let resultChecking = true
    switch (action.type) {
        case INIT_FORM:
            for (let indexParam = 0; indexParam < action.payload.length; indexParam += 1) {
                initForm[action.payload[indexParam].param] = {
                    value: typeof action.payload[indexParam].value !== 'undefined' ? action.payload[indexParam] : '',
                    errorMsg: '',
                    rule: action.payload[indexParam].rule,
                    aliasParam: typeof action.payload[indexParam].alias !== 'undefined' ? action.payload[indexParam].alias : action.payload[indexParam].param,
                }
            }
            return {
                ...state,
                formSet: initForm,
            }
        case SET_FORM:
            return {
                ...state,
                formSet: Object.assign(state.formSet, {
                    [`${action.payload.param}`]: action.payload,
                }),
            }
        case NOT_VALID_FORM:
            return {
                ...state,
                isValidForm: false,
            }
        case CHECK_VALID_FORM:
            Object.keys(action.oldState.validation.formSet).map((item) => {
                if (action.oldState.validation.formSet[item].errorMsg !== '' && resultChecking) {
                    resultChecking = false
                }
                return resultChecking
            })
            return {
                ...state,
                isValidForm: resultChecking,
            }
        case CHECK_FORM:
            // Object.keys(action.oldState.validation.formSet).map(async (item) => {
            //   const {
            //     conditionValidForm,
            //     newFormObj,
            //   } = await validationHelper(action.oldState.validation.formSet[item])
            //   if (resultChecking && !conditionValidForm) {
            //     resultChecking = false
            //   }

            //   Object.assign(updateFormObj, { [item]: newFormObj })
            //   return updateFormObj
            // })
            return {
                ...state,
                isValidForm: action.payload.isValidForm,
                formSet: action.payload.formSet,
            }
        case UPDATE_STATE_FORM:
            return {
                ...state,
                isUpdateForm: true,
            }
        default:
            return state
    }
}

export const initFormRedux = objInit => dispatch => (
    dispatch({ type: INIT_FORM, payload: objInit })
)

export const setFormRedux = (formObj, initEvent = false) => async (dispatch, getState) => {
    const { conditionValidForm, newFormObj } = await validationHelper(formObj)

    if (!conditionValidForm) {
        dispatch({ type: NOT_VALID_FORM })
    } else if (!initEvent) {
        dispatch({ type: CHECK_VALID_FORM, oldState: getState() })
    }
    dispatch({ type: SET_FORM, payload: newFormObj })
}

export const checkForm = () => async (dispatch, getState) => {
    const data = await validationHelperGroup(getState().validation.formSet)
    const keyFormSet = Object.keys(getState().validation.formSet)
    let validAllForm = true
    const formSet = {}
    for (let indexForm = 0; indexForm < data.length; indexForm += 1) {
        Object.assign(formSet, {
            [keyFormSet[indexForm]]: data[indexForm].newFormObj,
        })
        if (!data[indexForm].conditionValidForm && validAllForm) {
            validAllForm = false
        }
    }
    dispatch({
        type: CHECK_FORM,
        payload: {
            isValidForm: validAllForm,
            formSet,
        },
    })
}

export const updateStateForm = () => (dispatch) => {
    dispatch({ type: UPDATE_STATE_FORM })
}

export default reducer
