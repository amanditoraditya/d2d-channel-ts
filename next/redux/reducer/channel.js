const SET_BANNER = 'd2d/channel/SET_BANNER'

const initialState = {
    banners: []
}

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_BANNER:
            return {
                ...state,
                banners: action.payload,
            }
        default:
            return state
    }
}

export const setBanner = data => ({
    type: SET_BANNER,
    payload: data,
})


export default reducer
