// import axios from 'axios'

const OPEN_MODAL = 'd2d/general/OPEN_MODAL'
const CLOSE_MODAL = 'd2d/general/CLOSE_MODAL'
const TOGGLE_MODAL = 'd2d/general/TOGGLE_MODAL'
const SUCCESS_TOAST = 'd2d/general/SUCCESS_TOAST'
const ERROR_TOAST = 'd2d/general/ERROR_TOAST'
const HIDE_TOAST = 'd2d/general/HIDE_TOAST'

const initialState = {
    modalOpen: false,
    modal: {
        header: '',
        text: ''
    },
    toast: {
        message: '',
        type: ''
    }
}

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case OPEN_MODAL:
            return {
                ...state,
                modalOpen: true,
            }
        case CLOSE_MODAL:
            return {
                ...state,
                modalOpen: false,
            }
        case TOGGLE_MODAL:
            return {
                ...state,
                modalOpen: !state.modalOpen,
                modal: {
                    ...action.modal
                }
            }
        case SUCCESS_TOAST:
            return {
                ...state,
                toast: {
                    message: action.message,
                    type: 'success'
                }
            }
        case ERROR_TOAST:
            return {
                ...state,
                toast: {
                    message: action.message,
                    type: 'danger'
                }
            }
        case HIDE_TOAST:
            return {
                ...state,
                toast: {
                    message: '',
                    type: ''
                }
            }
        default:
            return state
    }
}

export const openSignInUpModal = (modalType = 'GENERAL') => {
    return {
        type: OPEN_MODAL
    }
}

export const closeSignInUpModal = () => ({
    type: CLOSE_MODAL,
})

export const toggleModal = (modalType = 'GENERAL') => {
    let modalOption = {
        header: 'Confirmation',
        text: 'Are u sure to run this action?'
    }
    switch (modalType) {
        case 'PUBLISH':
            modalOption = {
                header: 'Publish Confirmation',
                text: 'Are u sure to publish this record?'
            }
            break;
        case 'UNPUBLISH':
            modalOption = {
                header: 'Unpublish Confirmation',
                text: 'Are u sure to unpublish this record?'
            }
            break;

        default:
            break;
    }
    return {
        type: TOGGLE_MODAL,
        modal: {
            ...modalOption
        }
    }
}

const showToast = (type, message, dispatch) => {
    setTimeout(() => {
        dispatch({ type: HIDE_TOAST })
    }, 4000);
    dispatch({
        type,
        message
    })
}

export const successToast = message => (dispatch) => {
    showToast(SUCCESS_TOAST, message, dispatch)
}

export const errorToast = message => (dispatch) => {
    showToast(ERROR_TOAST, message, dispatch)
}

export const hideToast = () => ({
    type: HIDE_TOAST
})

export default reducer
