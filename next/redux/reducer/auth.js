import AuthServices from '../../components/auth'

const Auth = new AuthServices()
// import axios from 'axios'

// const SET_HEADER_JOB = 'd2d/general/SET_HEADER_JOB'

let initialState = {
    login: false,
    is_admin: false,
    profile: {}
}

if (typeof window !== 'undefined') {
    initialState = {
        login: Auth.loggedIn(),
        is_admin: Auth.isAdmin(),
        profile: Auth.getProfile()
    }
}

const reducer = (state = initialState, action = {}) => {
    return state
}


export default reducer
