// import axios from 'axios'

const SET_HEADER_JOB = 'd2d/general/SET_HEADER_JOB'

const initialState = {
    jobPayload: {
        job: {},
        specs: [],
        headers: []
    }
}

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_HEADER_JOB:
            return {
                ...state,
                jobPayload: action.jobPayload,
            }
        default:
            return state
    }
}

export const setHeaderJob = data => ({
    type: SET_HEADER_JOB,
    jobPayload: data,
})


export default reducer
