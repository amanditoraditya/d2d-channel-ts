// import axios from 'axios'

const SET_HEADER_MASTER = 'd2d/general/SET_HEADER_MASTER'

const initialState = {
    header: {}
}

const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_BANNER:
            return {
                ...state,
                header: action.headerPayLoad,
            }
        default:
            return state
    }
}

export const setHeaderMaster = data => ({
    type: SET_HEADER_MASTER,
    headerPayLoad: data,
})


export default reducer
