import { combineReducers } from 'redux'

import job from './job'
import auth from './auth'
import generalReducer from './general'
import validation from './validation'
import channel from './channel'

const rootReducer = combineReducers({
    job,
    auth,
    general: generalReducer,
    validation,
    channel
})

export default rootReducer
