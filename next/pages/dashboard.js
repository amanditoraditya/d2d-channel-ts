import React, { useContext } from 'react'
import Link from 'next/link'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
const moment = require('moment')
import Wrapper from '../components/wrapper'
import AuthServices from '../components/auth'
import Dashboard from '../components/dashboard/index'
import loadjs from 'loadjs'
import QRCode from '../components/channel/qrcode'
import { getService, postService } from '../services/base-service'
import { useRouter } from 'next/router'

const goaConfig = require('../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        return { query }
    }

    constructor(props) {
        super(props)

        this.state = {
            login: 0,
            user: null,
            qr: null,
            linkChecker: 0,
            url: ''
        }

        this.installPWA = this.installPWA.bind(this)
        this.cancelInstallPWA = this.cancelInstallPWA.bind(this)
        this.modalShowQR = this.modalShowQR.bind(this)
        this.downloadQR = this.downloadQR.bind(this)
        this.invitationLink = this.invitationLink.bind(this)
        this.capitalizeTheFirstLetterOfEachWord = this.capitalizeTheFirstLetterOfEachWord.bind(this)
    }

    async componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            let user = Auth.getProfile()
            this.setState({
                login: 1,
                user,
                is_medical_rep: user.is_medical_rep,
                user_channel_id: user.user.channel_id
            }, () => {
                this.modalShowQR()
            })
        }
        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
            '../../../../static/one/plugins/custom/fullcalendar/fullcalendar.bundle.js',
        ])
    }
    installPWA() {
        let self = this
        let deferredPrompt = this.state.deferredPrompt

        if (deferredPrompt) {
            deferredPrompt.prompt()
            deferredPrompt.userChoice.then((choiceResult) => {
                if (choiceResult.outcome === 'accepted') {
                    localStorage.setItem('install', false)
                    console.log('User accepted the A2HS prompt')
                } else {
                    console.log('User dismissed the A2HS prompt')
                }

                self.setState({
                    deferredPrompt: null
                })
            })
        }
    }

    cancelInstallPWA() {
        localStorage.setItem('install', false)

        this.setState({
            install: false
        })
    }
    modalShowQR(id) {
        getService({
            url: `/channel/qr/${this.state.user_channel_id}`,
            success: ({ data }) => {

                let channelId = data.xid
                let userId = this.state.user.user.xid
                let urlContentAccesed = ''
                if (typeof window !== 'undefined') {
                    urlContentAccesed = window.location.hostname;
                }
                let url = `https://${urlContentAccesed}/?type=channel-invitation&id=${channelId}&referral=${userId}&invitationType=qr`

                data.url = url
                this.setState({
                    qr: data,
                    selectedId: id,
                })
            }
        })
    }

    downloadQR() {
        const canvas = document.getElementById("qrcode")
        const pngUrl = canvas
            .toDataURL("image/png")
            .replace("image/png", "image/octet-stream")
        let downloadLink = document.createElement("a")
        downloadLink.href = pngUrl
        downloadLink.download = this.state.qr.channel_name + " - Channel - QRCode.png"
        document.body.appendChild(downloadLink)
        downloadLink.click()
        document.body.removeChild(downloadLink)
    }

    invitationLink() {
        this.setState({ linkChecker: 1 })
        setTimeout(() => { this.setState({ linkChecker: 0 }) }, 1500)
        var textField = document.createElement('textarea')

        textField.innerText = "Youre invited to join Channel '" + this.state.qr.channel_name + "' in D2D Apps. - Invitation Link : " + this.state.qr.url
        document.body.appendChild(textField)
        textField.select()
        document.execCommand('copy')
        textField.remove()
    }
    capitalizeTheFirstLetterOfEachWord(words) {
        var separateWord = words.toLowerCase().split(' ');
        for (var i = 0; i < separateWord.length; i++) {
           separateWord[i] = separateWord[i].charAt(0).toUpperCase() +
           separateWord[i].substring(1);
        }
        return separateWord.join(' ');
     }
     
    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Dashboard" selected="dashboard" module="Home" permission="read">
                    {this.state.login == 1 &&
                        <React.Fragment>
                            {/* <Dashboard /> */}
                            <div className="d-flex flex-column-fluid">
                                <div className="container-fluid">
                                    <div className="card card-custom gutter-b">
                                        <div className="card-body">
                                            <h2>Dashboard landing page</h2>

                                            {this.state.is_medical_rep &&
                                                <div className="row form-group">
                                                    {this.state.qr &&
                                                        <div className="col-sm-12 text-center">
                                                            <div className="row form-group">
                                                                <div className="col-sm-12 text-center">
                                                                    <h3>{this.capitalizeTheFirstLetterOfEachWord(this.state.qr.channel_name)}</h3>
                                                                </div>
                                                            </div>
                                                            <div className="row form-group">
                                                                <div className="col-sm-12 text-center">
                                                                    <QRCode qr={this.state.qr} />
                                                                </div>
                                                            </div>
                                                            <div className="row">
                                                                <div className="col-sm-12 text-center mt-3">
                                                                    <button type="button" className="btn btn-primary px-15 mr-5" onClick={this.downloadQR}><i className="fas fa-download"></i>Download QR Code</button>
                                                                    <button type="button" className={this.state.linkChecker == 0 ? "btn btn-primary px-15" : "btn btn-success px-15"} onClick={this.invitationLink}>{this.state.linkChecker == 0 ? <i className="fas fa-copy"></i> : <i className="fas fa-check"></i>}Copy Invitation Link</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>



                            {this.state.install &&
                                <React.Fragment>
                                    <div id="a2hs-area" className="modal show" style={{ display: 'block' }}>
                                        <div className="modal-dialog modal-dialog-centered">
                                            <div className="modal-content">
                                                <div className="modal-body text-center py-10 px-10">
                                                    <i className="flaticon2-download-1 icon-5x text-success d-inline-block"></i>
                                                    <h4 className="text-success mb-10">Tambahkan Empty Engine Next ke layar desktop?</h4>
                                                    <p className="mb-10 mx-10">Buka Empty Engine Next dengan cepat dan mudah dengan cara menambahkannya ke layar desktop Anda.</p>
                                                    <button type="button" className="btn btn-success px-15 mr-10" onClick={() => this.installPWA()}>Tambah</button>{' '}
                                                    <button type="button" className="btn btn-secondary px-15" onClick={() => this.cancelInstallPWA()}>Tidak</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-backdrop show"></div>
                                </React.Fragment>
                            }
                        </React.Fragment>
                    }
                </Wrapper>
            </div >
        )
    }
}