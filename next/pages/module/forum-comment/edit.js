import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
// import Schema from '../../../scheme/forum-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			publishEnum: [
				{
					label: 'Not Published',
					value: 0
				},
				{
					label: 'Published',
					value: 1
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
			init_roles: [],
		}
		
		this.handleSubmit 		= this.handleSubmit.bind(this)
		this.changeChannelState = this.changeChannelState.bind(this)
		this.changeIsPublished 	= this.changeIsPublished.bind(this)
		this.flashInfo 			= this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			await this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}
		// console.log('this.props.channel_id', this.props.channel_id)
		// let currentChannel = Auth.getCurrentChannel();
		// console.log('currentChannel', currentChannel)

		// if(_lo.isEmpty(currentChannel) == false){
		// 	await this.setState({
		// 		channel_id: currentChannel
		// 	})
		// }
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/forum/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let forum = response.data.data
				self.setState({
					forum,
					channel_id: forum.channel_id,
					is_published: forum.is_published,
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		data.channel_id 	= self.state.channel_id;
		data.is_published 	= self.state.is_published;
		
		const data_form 	= new FormData()
		data_form.append('data', JSON.stringify(data))
		console.log("edit",data,data_form)
		
		axios({
			url: goaConfig.BASE_API_URL + '/forum/update/' + self.state.id,
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				Router.push('/module/forum/index', '/forum')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}

	changeChannelState(e) {
		this.setState({
			channel_id: e.value
		})
	}

	changeIsPublished(e) {
		this.setState({
			is_published: e
		})
	}

	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Channel" parent="channel" selected="forum" module="UserChannel" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update User Channel</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/forum/index"} 
												as={"/forum/"} passHref>
												<a href={"/forum/"} className="btn btn-sm btn-dark">
												<i className="fa fa-arrow-left"></i> Back</a>
											</Link>							
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.forum &&
											<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													{ this.state.is_admin &&
														<div className="col-md-12">
															<AutoField name="channel_id" url={"/channel/get-channels"} isSearchable={true} 
															options={this.state.forum.channels}
															defaultValue={this.state.forum.channels} 
															placeholder="Select a Channel" 
															onChange={(e) => this.changeChannelState(e)} />
														</div>
													}
													<div className="col-md-12">
														<AutoField name="title" initialValue={ this.state.forum.title } />
													</div>
													<div className="col-md-12">
														<AutoField name="description" initialValue={ this.state.forum.description } />
													</div>
													<div className="col-md-12">
														<SelectField name="is_published" value={ this.state.is_published } options={this.state.publishEnum} placeholder="Select Published Status" 
														onChange={e => this.changeIsPublished(e)} />
													</div>
												</div>
												<div className="form-layout-footer float-right">
													<SubmitField value="Submit" />
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}