import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
// import Schema from '../../../scheme/forum-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			channel_id: null,
			is_published: 1,
			login: 0,
			publishEnum: [
				{
					label: 'Not Published',
					value: 0
				},
				{
					label: 'Published',
					value: 1
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
			roleEnum: [],
		}

		this.changeIsPublished = this.changeIsPublished.bind(this)
		this.changeChannelState = this.changeChannelState.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		let is_admin = Auth.isAdmin()
		let channel_id
		let login = 0
		if(Auth.loggedIn()) {
			login = 1
		}
		if (!is_admin) {
			let profile = Auth.getProfile()
			channel_id = profile.user.channel_id
		} else {
			channel_id = null
		}

		await this.setState({
			login,
			channel_id,
			is_admin
		})

		// axios.get(goaConfig.BASE_API_URL + '/forum/get-channel-role-list', {params: {channel_id: channel_id}})
		// .then(response => {
		// 	this.setState({roleEnum: response.data})
		// });

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		data.channel_id 		= self.state.channel_id;
		data.is_published 		= self.state.is_published;

		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		
		console.log("create",data,data_form)		

		axios({
			url: goaConfig.BASE_API_URL + '/forum/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/forum/index', '/forum')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeIsPublished(e) {
		this.setState({
			is_published: e
		})
	}
	
	changeChannelState(e) {
		this.setState({
			channel_id: e.value
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah Forum Post" parent="channel" selected="forum" module="Forum" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Forum Post</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/forum/index"} 
												as={"/forum/"} passHref>
												<a href={"/forum/"} className="btn btn-sm btn-dark">
												<i className="fa fa-arrow-left"></i> Back</a>
											</Link>							
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												{ this.state.is_admin &&
													<div className="col-md-12">
														<AutoField name="channel_id" url={"/channel/get-channels"} isSearchable={true} 
														options={[{label: '', value: ''}]} placeholder="Select a Channel" 
														onChange={(e) => this.changeChannelState(e)} />
													</div>
												}
												<div className="col-md-12">
													<AutoField name="title" />
												</div>
												<div className="col-md-12">
													<AutoField name="description" />
												</div>
												<div className="col-md-12">
													<SelectField name="is_published" options={this.state.publishEnum} placeholder="Select Published Status" 
													onChange={(e) => this.changeIsPublished(e)}/>
												</div>
											</div>
											<div className="form-layout-footer float-right">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}