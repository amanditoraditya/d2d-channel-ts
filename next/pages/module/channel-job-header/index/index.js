import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import Wrapper from '../../../../components/wrapper'
import AuthServices from '../../../../components/auth'
import Table from '../table'
import loadjs from 'loadjs'
import _lo from 'lodash'

const goaConfig = require('../../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}

	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			info: false,
			infoStatus: '',
			infoMessage: '',
			is_archived: 1,
		}
		
		this.handleSubmit  = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {

			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
    }

	handleSubmit() {
		let self = this
		$('.error-bar').delay(1000).show()
		
		axios({
			url: goaConfig.BASE_API_URL + '/channel-job-header/submit-data/' + this.state.id,
			method: 'POST',
			data: [],
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/channel-job/index', '/channel-job')
			} else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
				self.flashInfoErrorMultiLine(response.data.data._errorMessages)
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Channel Job Header" parent="channel" selected="channel-job" module="ChannelJobHeader" permission="read">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Job Header</h3>
										</div>
										<div className="card-toolbar">
											<Link href={'/channel-job-header/create?id=' + this.state.id} as={'/channel-job-header/create/' + this.state.id} passHref>
												<a href={'/channel-job-header/create?id=' + this.state.id}  className="btn btn-sm btn-dark"><i className="fa fa-plus"></i> Add New</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<Table flashInfo={this.flashInfo} id={this.state.id}/>
									</div>

									<div className="card-footer flex-wrap py-3">
										<div className="card-toolbar float-right">
											<a onClick={this.handleSubmit} className="btn btn-sm btn-primary">Submit</a>
										</div>
										<div className="card-toolbar float-right mr-2">
											<Link href={'/channel-job-spec/index?id=' + this.state.id} as={'/channel-job-spec/index/' + this.state.id} passHref>
												<a href={'/channel-job-spec/index?id=' + this.state.id}  className="btn btn-sm btn-primary">Back</a>
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}