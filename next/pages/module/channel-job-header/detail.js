import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import _lo from 'lodash'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import DataTable from 'datatables.net'
import 'datatables.net-bs4'
import Select2 from 'select2'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {

	constructor(props) {
		super(props)
		
		this.state = {
			jobid:this.props.id,
			totalData: 0,
			isAdmin: false
		}

		this.flashInfo = this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		let columns = [
			{
				data: "id",
				searchable: false,
				orderable: false,
			},
			{
				data: "value",
				orderable: true,
				render: (data) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:450px">${data}</div>`
				}
			},
			{
				data: "name",
				orderable: true,
				searchable: false,
				render: (data) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:500px">${data}</div>`
				}
			},
			{
				data: "icon",
				searchable: false,
				orderable: false,
				render: (data) => {
					let image = ''
					if (data != null || data != '') {
						image = `<img class="pull-left img-responsive img-thumbnail" src="${data}" style="width:50px; height:50px"/>`
					}
					return image
				}
			},
		]
	
		$('#table-channel-job-header').DataTable({
			order: [[0, 'DESC']],
			autoWidth: false,
			columnDefs: [{
				targets: [ 0 ],
				className: "hide_column"
			}],
			columns,
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 5,
			lengthMenu: [
				[5, 10, 15, 20, -1],
				[5, 10, 15, 20, 'All']
			],
			ajax: {
				type: 'post',
				url: goaConfig.BASE_API_URL + '/channel-job-header/datatable-detail',
				data: {
					id: self.state.jobid
				},
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
			"drawCallback": function(settings) {
				$("#titleCheck").click(function() {
					let checkedStatus = this.checked;
					$("table tbody tr td div:first-child input[type=checkbox]").each(function() {
						this.checked = checkedStatus
						if (checkedStatus == this.checked) {
							$(this).closest('table tbody tr').removeClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
						if (this.checked) {
							$(this).closest('table tbody tr').addClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
					})
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
					let checkedStatus = this.checked
					this.checked = checkedStatus
					
					if (checkedStatus == this.checked) {
						$(this).closest('table tbody tr').removeClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
					
					if (this.checked) {
						$(this).closest('table tbody tr').addClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
				})
				
				$('table tbody tr td div:first-child input[type=checkbox]').change(function() {
					$(this).closest('tr').toggleClass("table-select", this.checked)
				})
			}
		})
		
		let windowWidth = $(window).width()
		if (windowWidth < 767) {
			$('.table-wrapper').addClass('table-responsive')
		} else {
			$('.table-wrapper').removeClass('table-responsive')
		}
    }
	
	componentWillUnmount() {
		$('.data-table-wrapper').find('table').DataTable().destroy(true)
    }

	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<div className="table-responsive">
					<table id="table-channel-job-header" className="table table-bordered responsive nowrap">
						<thead>
							<tr>
								<th>Id</th>
								<th>Detail</th>
								<th>Name</th>
								<th>Icon</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
					</table>
				</div>
				<style>{`
					.hide_column {
						display : none;
					}
      			`}</style>
			</div>
		)
	}
}