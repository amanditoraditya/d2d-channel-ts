import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/userchannel-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)

        this.state = {
            login: 0,
            id: this.props.id,
            block: null,
            statusEnum: [
                {
                    label: 'Active',
                    value: 0
                }, {
                    label: 'Disabled / Blocked',
                    value: 1
                }
            ],
            roleEnum: [],
            info: false,
            infoStatus: '',
            infoMessage: '',
            channel_id: null,
            init_channel_id: null,
            channel_roles: null,
            init_roles: null,
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.changeChannelRoles = this.changeChannelRoles.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
    }

    async componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            await this.setState({
                login: 1,
                is_admin: Auth.isAdmin()
            })
        }

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])

        axios({
            url: goaConfig.BASE_API_URL + '/user-channel/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let userChannel = response.data.data
                self.setState({
                    userChannel,
                    block: userChannel.block,
                    channel_id: userChannel.channel_id,
                    init_channel_id: userChannel.channel_id,
                    channel_roles: userChannel.channel_roles,
                    init_roles: userChannel.channel_roles
                })
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleSubmit(data) {
        let self = this

        data.cid = data.channel_id.value

        let block = ''

        if (self.state.userChannel.block != null) {
            block = self.state.userChannel.block ? 1 : 0
        } else {
            block = 0
        }

        data.block = block

        data.channel_roles = self.state.channel_roles;

        const data_form = new FormData()
        data_form.append('data', JSON.stringify(data))
        axios({
            url: goaConfig.BASE_API_URL + '/user-channel/update/' + self.state.id,
            method: 'POST',
            data: data_form,
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                await self.flashInfo('success', response.data.message)
                Router.push('/module/user-channel/index', '/user-channel')
            } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                await self.flashInfoErrorMultiLine(response.data.data._errorMessages)
            } else {
                await awaitself.flashInfo('error', response.data.message)
            }
        })
    }

    flashInfoErrorMultiLine(err) {
        console.log(err)
        $('.error-bar').delay(1000).show()
        this.setState({
            info: true,
            infoStatus: 'alert alert-solid alert-danger error-bar',
            infoMessage: err.map(e => e.message).join(", ")
        })
        $('.error-bar').delay(2000).fadeOut()
    }

    changeChannelRoles(e) {
        this.setState({
            channel_roles: e
        })
    }

    changeChannelState(e) {
        let id = null
        if (e.value != undefined) {
            id = e.value
        } else if (this.state.channel_id != undefined) {
            id = this.state.channel_id
        }

        if (id != null) {
            let init_roles = this.state.init_roles
            this.setState({
                channel_id: e.id
            })

            if (id == this.state.init_channel_id) {
                this.setState({ channel_roles: init_roles })
            } else {
                this.setState({ channel_roles: [] })
            }

            axios.get(goaConfig.BASE_API_URL + '/user-channel/get-channel-role-list', { params: { channel_id: id } })
                .then(response => {
                    this.setState({ roleEnum: response.data })
                });
        }
    }

    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)
        $('.error-bar').delay(500).show()

        await $('.error-bar').delay(1000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Update Channel" parent="channel" selected="user-channel" module="UserChannel" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Update User Channel</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href={"/module/user-channel/index"}
                                                as={"/user-channel/"} passHref>
                                                <a href={"/user-channel/"} className="btn btn-sm btn-dark">
                                                    <i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        {this.state.userChannel &&
                                            <AutoForm schema={Schema} onSubmit={this.handleSubmit}
                                                onChange={(key, value) => {
                                                    if (key == 'channel_id') { this.changeChannelState(value) }
                                                    // if (key == 'block') { this.changeStatus(value) }
                                                }}
                                            >
                                                <div className="row mg-b-15">
                                                    <div className="col-md-12 text-capitalize">
                                                        <ErrorsField />
                                                    </div>
                                                    <div className={this.state.is_admin ? 'col-md-12' : 'd-none'}>
                                                        <AutoField name="channel_id"
                                                            url={"/channel/get-channels"}
                                                            isSearchable={true}
                                                            value={this.state.userChannel.channels}
                                                            placeholder="Select a Channel"
                                                        />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="first_name" initialValue={this.state.userChannel.first_name} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="last_name" initialValue={this.state.userChannel.last_name} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="email" initialValue={this.state.userChannel.email} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <TextField type="password" name="password" />
                                                    </div>
													
                                                    <div className="col-md-6">
                                                        <AutoField name="channel_roles"
                                                            isSearchable={true}
                                                            options={this.state.roleEnum}
                                                            value={this.state.channel_roles}
                                                            placeholder="Select Channel Role"
                                                            disabled={(_lo.isEmpty(this.state.roleEnum))}
                                                            onChange={(e) => this.changeChannelRoles(e)}
                                                        />
                                                    </div>
                                                    <div className="col-md-6">
														<AutoField name="block" value={ this.state.userChannel.block }
														onChange={e => this.setState(prevState => ({userChannel: { ...prevState.userChannel, block: e}}))}
													/>
                                                    </div>	
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
                                            </AutoForm>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
            </div>
        )
    }
}