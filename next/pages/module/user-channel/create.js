import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/userchannel-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			channel_id: null,
			channel_role_id: [],
			login: 0,
			statusEnum: [
				{
					label: 'Active',
					value: 0
				}, {
					label: 'Disabled / Blocked',
					value: 1
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
			roleEnum: [],
		}

		this.changeChannelRole = this.changeChannelRole.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}

	async componentDidMount() {
		let self = this
		let is_admin = Auth.isAdmin()
		let channel_id
		let login = 0
		if (Auth.loggedIn()) {
			login = 1
		}
		if (!is_admin) {
			let profile = Auth.getProfile()
			channel_id = {
				value: profile.user.channel_id
			}
		} else {
			channel_id = undefined
		}

		await this.setState({
			login,
			channel_id,
			is_admin
		})
		// axios.get(goaConfig.BASE_API_URL + '/user-channel/get-channel-role-list', { params: { channel_id } })
		// 	.then(response => {
		// 		this.setState({ roleEnum: response.data })
		// 	});

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
	}

	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		// console.log("create userchannel before", data)
		data.channel_role_id = this.state.channel_role_id;

		data.cid = data.channel_id.value

        let block = ''

        if (data.block != undefined) {
            block = data.block ? 1 : 0
        } else {
            block = 0
        }

        data.block = block

		console.log("create userchannel after", data)
		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))

		axios({
			url: goaConfig.BASE_API_URL + '/user-channel/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				await self.flashInfo('success', response.data.message)
				Router.push('/module/user-channel/index', '/user-channel')
			} else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
				await self.flashInfoErrorMultiLine(response.data.data._errorMessages)
			} else {
				await self.flashInfo('error', response.data.message)
			}
		})
	}

	flashInfoErrorMultiLine(err) {
		console.log(err)
		$('.error-bar').delay(500).show()
		this.setState({
			info: true,
			infoStatus: 'alert alert-solid alert-danger error-bar',
			infoMessage: err.map(e => e.message).join(", ")
		})
		$('.error-bar').delay(1000).fadeOut()
	}

	changeChannelState(e) {

		this.setState({ channel_role_id: [] })

		let id = null
		if (e.value != undefined) {
			id = e.value
		} else if (this.state.channel_id != undefined) {
			id = this.state.channel_id
		}

		if (id != null) {
			axios.get(goaConfig.BASE_API_URL + '/user-channel/get-channel-role-list', { params: { channel_id: id } })
				.then(response => {
					this.setState({ roleEnum: response.data })
				});
		}
	}
	changeChannelRole(e) {
		this.setState({
			channel_role_id: e
		})
	}

	async flashInfo(status, message) {
		let data = {
			info: true,
			infoStatus: '',
			infoMessage: message
		}
		$('.error-bar').delay(1000).show()

		if (status == 'success') {
			data.infoStatus = 'alert alert-solid alert-info error-bar'
		} else {
			data.infoStatus = 'alert alert-solid alert-danger error-bar'
		}
		this.setState(data)

		await $('.error-bar').delay(2000).fadeOut().promise()

		// clear message
		data = {
			info: false,
			infoStatus: '',
			infoMessage: ''
		}
		this.setState(data)
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah User Channel" parent="channel" selected="user-channel" module="UserChannel" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add User Channel</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/user-channel/index"}
												as={"/user-channel/"} passHref>
												<a href={"/user-channel/"} className="btn btn-sm btn-dark">
													<i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>

									<div className="card-body">
										{this.state.info &&
											<div className={this.state.infoStatus}>{this.state.infoMessage}</div>
										}

										<AutoForm schema={Schema} onSubmit={this.handleSubmit}
											onChange={(key, value) => {
												if (key == 'channel_id') { this.changeChannelState(value) }
												// if (key == 'block') { this.changeStatusState(value) }
											}}
										>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												<div className={this.state.is_admin ? 'col-md-12' : 'd-none'}>
													<AutoField
														name="channel_id"
														url={"/channel/get-channels"}
														initialValue={this.state.channel_id}
														placeholder="Select a Channel"
													/>
												</div>
												<div className="col-md-6">
													<AutoField name="first_name" />
												</div>
												<div className="col-md-6">
													<AutoField name="last_name" />
												</div>
												<div className="col-md-6">
													<AutoField name="email" />
												</div>
												<div className="col-md-6">
													<TextField type="password" name="password" />
												</div>
												<div className="col-md-6">
													<AutoField name="channel_role_id"
														isSearchable={true}
														options={this.state.roleEnum}
														placeholder="Select Channel Role"
														disabled={(_lo.isEmpty(this.state.roleEnum))}
														onChange={(e) => this.changeChannelRole(e)} />
												</div>
												<div className="col-md-6">
													<AutoField name="block" />	
												</div>
											</div>
											<div className="form-layout-footer float-right">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}