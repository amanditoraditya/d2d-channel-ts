import React from 'react'
import { withRouter } from 'next/router'
import { connect } from 'react-redux'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import HeaderForm from '../../../components/header-master/form'
import loadjs from 'loadjs'
import _lo from 'lodash'

const Auth = new AuthServices()

class EditHeaderForm extends React.Component {
	render() {
		return (
			<div>
				<Wrapper {...this.props} onCard={true} title="Header Master" parent="admin-page" selected="header-master" parent="admin-page" module="HeaderMaster" permission="edit" headerCard="Update Header">
					<HeaderForm id={this.props.router.query.id} />
				</Wrapper>
			</div>
		)
	}
}

export default withRouter(EditHeaderForm)
