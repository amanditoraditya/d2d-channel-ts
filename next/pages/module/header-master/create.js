import React from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import HeaderForm from '../../../components/header-master/form'
import loadjs from 'loadjs'
import _lo from 'lodash'

const Auth = new AuthServices()

class CreateHeaderForm extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			login: 0,
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
	}

	componentDidMount() {
		let self = this

		if (Auth.loggedIn()) {

			this.setState({
				login: 1
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Header Master" parent="admin-page" selected="header-master" module="HeaderMaster" permission="read">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								{
									typeof this.props.toast.message != 'undefined' && this.props.toast.message != '' && (
										<div className={`alert alert-solid alert-${this.props.toast.type} error-bar`}>{this.props.toast.message}</div>
									)
								}
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Create Header</h3>
										</div>
										<div className="card-toolbar">
                                            <Link href="/module/header-master/index" as="/header-master" passHref>
                                                <a href="/header-master" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
									</div>

									<div className="card-body">
										<HeaderForm id={null} />
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}

const mapStateToProps = state => ({
	toast: state.general.toast
})

export default connect(mapStateToProps, {})(CreateHeaderForm)
