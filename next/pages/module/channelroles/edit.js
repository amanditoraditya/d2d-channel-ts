import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import _lo from 'lodash'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-roles-edit'
import loadjs from 'loadjs'
import { listGroup } from '../../../config/menu'
import { typeGroup } from '../../../config/role'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)
        const menus = []
        listGroup.map((parentRow) => {
            if (parentRow.type == 'channel') {
                parentRow.childs.map((childRow) => {
                    menus.push({
                        label: childRow.text,
                        value: childRow.slug
                    })
                })
            }
        })
        const typeEnum = typeGroup
        this.state = {
            login: 0,
            id: this.props.id,
            status: null,
            menuEnum: [],
            channelroles: null,
            statusEnum: [
                {
                    label: 'Active',
                    value: 1
                }, {
                    label: 'Inactive',
                    value: 0
                }
            ],
            typeEnum,
            type: null,
            info: false,
            infoStatus: '',
            infoMessage: '',
            roleName: '',
            menus,
            menuSelected: null,
            menusTable: []
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.changeStatusState = this.changeStatusState.bind(this)
        this.changeName = this.changeName.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
        this.handleBtnAddMenu = this.handleBtnAddMenu.bind(this)
        this.handleMenuChange = this.handleMenuChange.bind(this)
        this.handleDetailAccess = this.handleDetailAccess.bind(this)
        this.changeTypeState = this.changeTypeState.bind(this)
        this.handleDeleteDetail = this.handleDeleteDetail.bind(this)
    }

    componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            this.setState({
                login: 1,
                is_admin: Auth.isAdmin()
            })
        }

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])

        axios({
            url: goaConfig.BASE_API_URL + '/channelroles/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let channelroles = response.data.data
                let typeName = self.state.typeEnum.map(function(item){
                    return channelroles.type == item.value ? item.label : null
                })
                self.setState({
                    channelroles,
                    channel_id: channelroles.channel_id,
                    roleName: channelroles.channel_roles_name,
                    status: channelroles.status,
                    type: channelroles.type != null || "" ? [{label: typeName, value: channelroles.type}] : null,
                    channel_role_id: channelroles.channel_role_id,
                    menusTable: channelroles.permissions != null ? JSON.parse(channelroles.permissions) : []
                })
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleSubmit(data) {
        let self = this
        $('.error-bar').delay(1000).show()

        data.type = this.state.type
        data.status = self.state.status;
        data.channel_role_id = self.state.channel_role_id
        data.cid = self.state.channel_id
        data.permissions = self.state.menusTable

        const data_form = new FormData()
        data_form.append('data', JSON.stringify(data))

        axios({
            url: goaConfig.BASE_API_URL + '/channelroles/update/' + self.state.id,
            method: 'POST',
            data: data_form,
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                self.flashInfo('success', response.data.message)

                Router.push('/module/channelroles/index', '/channelroles')
            } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                self.flashInfoErrorMultiLine(response.data.data._errorMessages)
                if (Auth.isAdmin()) {
                    self.setState({
                        menuEnum: [],
                        channel_id: null
                    })
                }
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    changeChannelType(e) {
        this.setState({
            channel_id: e.value
        })		
    }

    changeStatusState(e) {
		let value = e == false ? e=0 : e=1;
		this.setState({
		    status: value
		});
	}

    changeTypeState(e) {
        this.setState({
            type: e
        })
    }

    changeName(e) {
        this.setState({
            roleName: e
        })
    }

    handleMenuChange(event) {
        this.setState({
            menuSelected: event,
        })
    }

    handleBtnAddMenu() {
        if (this.state.menuSelected != null && this.state.menuSelected != '' && typeof this.state.menuSelected.value != 'undefined' && this.state.menuSelected.value != '' && this.state.menuSelected.value != null) {
            const existingMenu = this.state.menusTable
            // validate unique
            let isValid = true
            existingMenu.map((menu) => {
                if (menu.menu_id == this.state.menuSelected.value) {
                    isValid = false
                }
            })
            if (isValid) {
                existingMenu.push({
                    menu_id: this.state.menuSelected.value,
                    menu_name: this.state.menuSelected.label,
                    create: true,
                    read: true,
                    update: true,
                    delete: true,
                })
                this.setState({
                    menusTable: existingMenu,
                    menuSelected: {
                        value: '',
                        label: ''
                    }
                })
            } else {
                this.flashInfo('error', 'Menu already add to list')
            }
        } else {
            this.flashInfo('error', 'Menu is required')
        }
    }

    flashInfoErrorMultiLine(err) {
        $('.error-bar').delay(1000).show()
        this.setState({
            info: true,
            infoStatus: 'alert alert-solid alert-danger error-bar',
            infoMessage: err.map(e => e.message).join(", ")
        })
        $('.error-bar').delay(2000).fadeOut()
    }

    flashInfo(status, message) {
        $('.error-bar').delay(1000).show()

        this.setState({
            info: true,
            infoStatus: `alert alert-solid ${status == 'success' ? 'alert-info' : 'alert-danger'} error-bar`,
            infoMessage: message
        }, () => {
            $('.error-bar').delay(2000).fadeOut()
        })
    }

    handleDetailAccess(index, type) {
        const allMenuSelected = this.state.menusTable
        if (typeof allMenuSelected[index] != 'undefined') {
            allMenuSelected[index][type] = !allMenuSelected[index][type]
            this.setState({
                menusTable: allMenuSelected
            })
        }
    }

    handleDeleteDetail(index) {
        if (typeof this.state.menusTable[index] != 'undefined') {
            const existingMenu = this.state.menusTable
            existingMenu.splice(index, 1)
            this.setState({
                menusTable: existingMenu
            })
        }
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Update Channel Roles" parent="channel" selected="channelroles" module="ChannelRoles" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Update Roles</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/channelroles/index" as="/channelroles" passHref>
                                                <a href="/channelroles" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        {this.state.channelroles &&
                                            <AutoForm schema={Schema} onSubmit={this.handleSubmit}
                                                onChange={(key, value) => {
                                                    if (key == 'channel_id') { this.changeChannelType(value) }
                                                    if (key == 'status') { this.changeStatusState(value) }
                                                    if (key == 'name') { this.changeName(value) }
                                                }}
                                            >
                                                <div className="row mg-b-15">
                                                    <div className="col-md-12 text-capitalize">
                                                        <ErrorsField />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="name" value={this.state.roleName} />
                                                    </div>
                                                    <div className="col-md-6">
													    <AutoField 
                                                            name="status" 
                                                            value={this.state.status}
													    />	
													</div>
                                                    {this.state.is_admin &&
                                                        <div className="col-md-6"><AutoField
                                                            name="channel_id"
                                                            url={"/channel/get-channels"}
                                                            isSearchable={true}
                                                            initialValue={{ label: this.state.channelroles.channel_name, value: this.state.channelroles.channel_id }}
                                                            placeholder="Select Channel"
                                                        />
                                                        </div>
                                                    }
                                                    {!this.state.is_admin &&
                                                        <AutoField hidden="hidden" name="channel_id" initialValue={{ label: '', value: this.state.channelroles.channel_id }} />
                                                    }
                                                    <div className="col-md-6">
                                                        <AutoField
                                                            name="type"
                                                            value={this.state.type}
                                                            onChange={this.changeTypeState}
                                                            options={this.state.typeEnum}
                                                            placeholder="Select Type"
                                                            isClearable={true}
                                                        />	
                                                    </div>
                                                </div>
                                                <div className="row mg-b-15">
                                                    <div className="col-md-6">
                                                        <AutoField
                                                            name="menu_id"
                                                            ref={ref => {
                                                                this.selectRef = ref
                                                            }}
                                                            isClearable={true}
                                                            onChange={this.handleMenuChange}
                                                            onClickBtn={this.handleBtnAddMenu}
                                                            value={this.state.menuSelected}
                                                            options={this.state.menus}
                                                            placeholder="Select Menu"
                                                        />
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group required">
                                                            <label className="form-label">Access</label>
                                                            <div className="table-responsive">
                                                                <table id="table-role" className="table table-bordered" width="100%" cellSpacing="0">
                                                                    <thead>
                                                                        <tr>
                                                                            <th className="text-center">Menu</th>
                                                                            <th className="text-center">Read</th>
                                                                            <th className="text-center">Write</th>
                                                                            <th className="text-center">Update</th>
                                                                            <th className="text-center">Delete</th>
                                                                            <th className="text-center">Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {
                                                                            this.state.menusTable.length == 0 ? <tr><td className="text-center" colSpan={6}>Data not selected yet.</td></tr> : this.state.menusTable.map((menu, indexMenu) => (
                                                                                <tr key={indexMenu}>
                                                                                    <td>{menu.menu_name}</td>
                                                                                    <td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="read" onChange={() => { this.handleDetailAccess(indexMenu, 'read') }} checked={menu.read} type="checkbox" /><span></span></label></div></td>
                                                                                    <td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="create" onChange={() => { this.handleDetailAccess(indexMenu, 'create') }} checked={menu.create} type="checkbox" /><span></span></label></div></td>
                                                                                    <td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="update" onChange={() => { this.handleDetailAccess(indexMenu, 'update') }} checked={menu.update} type="checkbox" /><span></span></label></div></td>
                                                                                    <td className="text-center"><div className="d-flex justify-content-center"><label className="checkbox checkbox-lg checkbox-inline"><input name="delete" onChange={() => { this.handleDetailAccess(indexMenu, 'delete') }} checked={menu.delete} type="checkbox" /><span></span></label></div></td>
                                                                                    <td className="text-center"><button onClick={() => { this.handleDeleteDetail(indexMenu) }} type="button" className="btn btn-danger btn-xs"><i className="fa fa-trash"></i></button></td>
                                                                                </tr>
                                                                            ))
                                                                        }
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
                                            </AutoForm>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
            </div>
        )
    }
}

