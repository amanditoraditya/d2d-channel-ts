import React from 'react'
import { withRouter } from 'next/router'
import { connect } from 'react-redux'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import JobForm from '../../../components/job/form'
import loadjs from 'loadjs'
import _lo from 'lodash'

const Auth = new AuthServices()

class CreateJobForm extends React.Component {

	componentDidMount() {
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} onCard={true} title="Channel Job" parent="channel" selected="channel-job" parent="channel" module="ChannelJob" permission="edit" headerCard={this.props.router.query.copy == true ? "Create Channel Job" : "Update Channel Job"}>
					<JobForm id={this.props.router.query.id} copy={this.props.router.query.copy == true ? true : false} />
				</Wrapper>
			</div>
		)
	}
}

export default withRouter(CreateJobForm)
