import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import { connect } from 'react-redux'
import Reactdom from 'react-dom'
import _lo from 'lodash'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import 'datatables.net-bs4'
import SwitchTableForm from '../../../components/forms/general-form/switch-table-form'
import { errorToast } from '../../../redux/reducer/general'

const goaConfig = require('../../../config')

class TableJob extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			channelid: this.props.channelid,
			totalData: 0,
			idDel: 0,
			idCopy: 0,
			modalDel: false,
			modalDelAll: false,
			modalCopy: false
		}

		this.actionEdit = this.actionEdit.bind(this)
		this.toggleDel = this.toggleDel.bind(this)
		this.toggleDelAll = this.toggleDelAll.bind(this)
		this.toggleCopy = this.toggleCopy.bind(this)
		this.actionDelete = this.actionDelete.bind(this)
		this.actionDeleteAll = this.actionDeleteAll.bind(this)
	}

	async componentDidMount() {
		let self = this
		let is_admin = this.props.isLoginUser;
		let indexPublished = 5
		let columns = [
			{
				data: null,
				orderable: false,
				searchable: false,
				render: (data) => {
					return `
					<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`
				}
			},
			{
				data: "id",
				searchable: false,
				orderable: false,
			},
			{
				data: "title",
				orderable: true,
				render: (data) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:200px">${data}</div>`
				}
			},
			{
				data: "channel_job_type_name",
				searchable: false,
				orderable: true,
				render: (data, row, rowData) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:200px">${data}</div>`
				}
			},
			{
				data: "channel_name",
				searchable: false,
				orderable: true,
				render: (data) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:200px">${data}</div>`
				}
			},
			{
				data: "is_published",
				searchable: false,
				orderable: true,
				render: (data) => {
					return data == 1 ? 'Published' : 'Not Published'
				}
			},
			{
				data: "created_at",
				searchable: false,
				orderable: true,
				render: (data) => {
					return moment(data).format('D MMMM YYYY')
				}
			},
			{
				data: "due_date",
				searchable: false,
				orderable: true,
				render: (data) => {
					return moment(data).format('D MMMM YYYY')
				}
			},
			{
				data: null,
				orderable: false,
				searchable: false,
				className: 'text-center',
				render: (data) => {
					return `
					<div class='text-center'>
						<div class='btn-group btn-group-sm'>
							<a href='#' data-href='/module/channel-job/detail?id=${data.encrypted}' data-as='/channel-job/detail/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='Detail'><i class='fas fa-eye'></i></a>
							<a href='#' data-href='/module/channel-job/edit?id=${data.encrypted}' data-as='/channel-job/edit/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>
							<a href='#' class='btn btn-sm btn-danger alertdel' id='${data.encrypted}' title='Delete'><i class='far fa-trash-alt'></i></a>
							<a href='#' class='btn btn-sm btn-success alertcopy' id='${data.encrypted}' title='Copy'><i class='far fa-copy'></i></a>
						</div>
					</div>`
				}
			}
		]

		if (!is_admin) {
			columns = columns.filter(function (row) {
				return row.data != 'channel_name';
			});
			indexPublished = 4
		}

		await self.setState({
			is_admin
		})

		$('#table-channel-job').DataTable({
			order: [[1, 'DESC']],
			autoWidth: false,
			columnDefs: [
				{
					targets: [1],
					className: "d-none"
				},
				{
					targets: indexPublished,
					createdCell: (td, cellData, rowData, row, col) => Reactdom.render(
						<SwitchTableForm
							onFailed={(message) => { this.props.errorToast(message) }}
							checked={cellData == 1 ? true : false}
							id={rowData.encrypted}
							url="/channel-job/update-status"
						/>,
						td)
				}
			],
			width: '10px',
			columns,
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 10,
			lengthMenu: [
				[10, 30, 50, 100, -1],
				[10, 30, 50, 100, 'All']
			],
			ajax: {
				type: 'post',
				url: goaConfig.BASE_API_URL + '/channel-job/datatable',
				data: {
					channelid: this.props.channelid
				},
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
			"drawCallback": function (settings) {
				$("#titleCheck").click(function () {
					let checkedStatus = this.checked;
					$("table tbody tr td div:first-child input[type=checkbox]").each(function () {
						this.checked = checkedStatus
						if (checkedStatus == this.checked) {
							$(this).closest('table tbody tr').removeClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
						if (this.checked) {
							$(this).closest('table tbody tr').addClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
					})
				})

				$('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
					let checkedStatus = this.checked
					this.checked = checkedStatus

					if (checkedStatus == this.checked) {
						$(this).closest('table tbody tr').removeClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}

					if (this.checked) {
						$(this).closest('table tbody tr').addClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
				})

				$('table tbody tr td div:first-child input[type=checkbox]').change(function () {
					$(this).closest('tr').toggleClass("table-select", this.checked)
				})

				$(".alertdel").click(function () {
					let id = $(this).attr("id")
					self.toggleDel()
					self.setState({
						idDel: id
					})
				})

				$(".alertcopy").click(function () {
					let id = $(this).attr("id")
					self.toggleCopy()
					self.setState({
						idCopy: id
					})
				})

				$('.btn-edit').on('click', function () {
					let href = $(this).attr('data-href')
					let as = $(this).attr('data-as')

					self.actionEdit(href, as)
				})
			}
		})
	}

	componentWillUnmount() {
		$('#table-channel-job').DataTable().destroy(true)
	}

	actionEdit(href, as) {
		Router.push(href, as)
	}

	toggleDel() {
		this.setState({
			modalDel: !this.state.modalDel
		})
	}

	toggleDelAll() {
		this.setState({
			modalDelAll: !this.state.modalDelAll
		})
	}

	toggleCopy() {
		this.setState({
			modalCopy: !this.state.modalCopy
		})
	}

	actionDelete() {
		let self = this

		this.toggleDel()

		axios({
			url: goaConfig.BASE_API_URL + '/channel-job/delete',
			method: 'POST',
			data: {
				id: self.state.idDel
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('#table-channel-job')
					.DataTable()
				table.clear().draw()
				table.state.clear()

				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}

	actionDeleteAll() {
		let self = this

		let deldata = []
		let values = $('.table-body input[type=checkbox]:checked').map(function () {
			deldata = $(this).attr("id")
			return deldata
		}).get()

		this.toggleDelAll()

		axios({
			url: goaConfig.BASE_API_URL + '/channel-job/multidelete',
			method: 'POST',
			data: {
				totaldata: self.state.totalData,
				item: JSON.stringify(values)
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('#table-channel-job')
					.DataTable()
				table.clear().draw()
				table.state.clear()

				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}

	render() {
		return (
			<div>
				<div className="table-responsive">
					<table id="table-channel-job" className="table table-bordered responsive nowrap">
						<thead>
							<tr>
								<th className="no-sort" style={{ width: "10px" }}></th>
								<th style={{ width: "30px" }}>Id</th>
								<th>Title</th>
								<th>Job Type</th>
								{this.state.is_admin &&
									<th>Channel</th>
								}
								<th>Status</th>
								<th>Start Date</th>
								<th>Due Date</th>
								<th className="no-sort" style={{ width: "100px" }}>Action</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
						<tfoot>
							<tr>
								<td style={{ width: "10px" }} className="text-center">
									<label className="checkbox checkbox-lg checkbox-inline">
										<input type="checkbox" id="titleCheck" />
										<span></span>
									</label>
								</td>
								<td colSpan={this.state.is_admin ? "7" : "6"} className="d-table-cell">
									<button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>

				<Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure delete this data? Please confirm your choose.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>

				<Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure delete all this data? Please confirm your choose.</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>

				<Modal isOpen={this.state.modalCopy} toggle={this.toggleCopy} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon2-copy icon-5x text-success d-inline-block"></i>
						<h4 className="text-success mb-10">Copy Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure copy this data? Please confirm your choose.</p>
						<Link href={"/module/channel-job/edit/" + this.state.idCopy} as={"/channel-job/copy/" + this.state.idCopy} passHref>
							<button type="button" className="btn btn-success px-15 mr-10"><i className="fas fa-check"></i> Yes</button>
                        </Link>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleCopy}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
			</div>
		)
	}
}
const mapDispatchToProps = dispatch => ({
	errorToast: data => dispatch(errorToast(data)),
})

const mapStateToProps = state => ({
	isLoginUser: state.auth.login
})

export default connect(mapStateToProps, mapDispatchToProps)(TableJob)