import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-job-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
    
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
            id: this.props.id,
			publishedEnum: [
				{
					label: 'Published',
					value: 1
				},{
					label: 'Not Published',
					value: 0
				}
			],
			role: '',
            info: false,
			infoStatus: '',
			infoMessage: '',
            image: '',
			image_preview: '',
            due_date: undefined,
			due_date_raw: undefined,
			init_due_date: true,
			activeTab: '1'
		}
		
		this.handleSubmit 				 = this.handleSubmit.bind(this)
		this.flashInfo 					 = this.flashInfo.bind(this)
		this.changeChannelState 		 = this.changeChannelState.bind(this)
		this.changeChannelJobTypeState   = this.changeChannelJobTypeState.bind(this)
        this.changeIsPublished 			 = this.changeIsPublished.bind(this) 
		this.changeDueDate			     = this.changeDueDate.bind(this)
        this.changeImages                = this.changeImages.bind(this)
		this.removeImage                 = this.removeImage.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])

        axios({
			url: goaConfig.BASE_API_URL + '/channel-job/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let channel_job = response.data.data
				let image_preview = channel_job.image == null || channel_job.image == '' ? '' : channel_job.image

				self.setState({
					channel_job,
                    channel_id: channel_job.channel_id,
                    specialist: channel_job.specialist,
                    job_type_id: channel_job.job_type_id,
                    due_date: moment(channel_job.due_date).format('YYYY-MM-DD HH:mm'), 
					due_date_raw: moment(channel_job.due_date).toDate(),
                    image_preview
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		// let due_date = null
		// if (this.state.due_date != null && this.state.end_time != null) {
		// 	due_date = moment(this.state.due_date).format('YYYY-MM-DD')
		// 	due_date += ' ' + this.state.end_time
		// }
		
		data.specialist         = self.state.specialist
		// data.channel_id         = self.state.channel_id
		data.cid         		= self.state.channel_id
        // data.job_type_id        = self.state.job_type_id
        data.jid        		= self.state.job_type_id
        data.is_published		= self.state.is_published
		data.due_date 			= self.state.due_date
		data.image				= self.state.image
		data.remove_image		= self.state.remove_image

        const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
        data_form.append('image', self.state.image)
        
		axios({
			url: goaConfig.BASE_API_URL + '/channel-job/update/' + self.state.id,
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				Router.push('/module/channel-job-spec/index?id=' + response.data.data, '/channel-job-spec/index/' + response.data.data )
			} else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
				self.flashInfoErrorMultiLine(response.data.data._errorMessages) 
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}

	async flashInfoErrorMultiLine(err) {
		$('.error-bar').delay(1000).show()
		this.setState({
			info: true,
			infoStatus: 'alert alert-solid alert-danger error-bar',
			infoMessage: err.map(e => e.message).join(", ")
		})
		$('.error-bar').delay(6000).fadeOut()
	}
	
	changeChannelState(e) {
		let value = e;

		this.setState({
			channel_id: value
		})
	}
	
	changeIsPublished(e) {
		this.setState({
			is_published: e
		})
	}

    changeChannelJobTypeState(e) {
		let value = e;

        this.setState({
			job_type_id: value
		})
    }

	changeDueDate(e) {
		if (this.state.init_due_date) {
			this.setState({
				init_due_date: false
			})
		} else {
			this.setState({
				due_date: moment(e).format('YYYY-MM-DD HH:mm'),
				due_date_raw: e
			})
		}
	}

    changeImages(e) {
		this.setState({ 
			image: e.target.files[0], 
			image_preview: URL.createObjectURL(e.target.files[0])
		})
	}

	changeJobSpecialist(e) {
		// let value = _lo.isArray(e.value)? e.value[0].value : e.value;
		let value = e;

		this.setState({
			specialist: value
		})
	}

	removeImage(e) {
		e.preventDefault()
		this.setState({ 
			remove_image: true,
			image_preview: ''
		})
	}
	
	async flashInfo(status, message){
		let data = {
			info: true,
			infoStatus: '',
			infoMessage: message
		}
		$('.error-bar').delay(1000).show()

		if (status == 'success') {
			data.infoStatus = 'alert alert-solid alert-info error-bar'
		} else {
			data.infoStatus = 'alert alert-solid alert-danger error-bar'
		}
		this.setState(data)

		await $('.error-bar').delay(2000).fadeOut().promise()
		
		// clear message
		data = {
			info: false,
			infoStatus: '',
			infoMessage: ''
		}
		this.setState(data)
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Channel Job" parent="channel" selected="channel-job" module="ChannelJob" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Channel Job</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channel-job/index" as="/channel-job" passHref>
												<a href="/channel-job" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}

                                        { this.state.channel_job &&
											<AutoForm schema={Schema} onSubmit={this.handleSubmit}
											onChange={(key, value) => {
												if (key == 'channel_id') {this.changeChannelState(value)}
												if (key == 'job_type_id') {this.changeChannelJobTypeState(value)}
												if (key == 'is_published') {this.changeIsPublished(value)}
												if (key == 'due_date') {this.changeDueDate(value)}
												if (key == 'job_specialist') {this.changeJobSpecialist(value)}
												}}
												>
												<div className="mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
														<div className="row mt-5">
															{ this.state.is_admin &&
																<div className="col-md-6">
																	<AutoField name="channel_id" url={"/channel/get-channels"} 
																	isSearchable={true} 
																	options={[{label: this.state.channel_job.channel_name, value: this.state.channel_job.channel_id}]} 
																	initialValue={{label: this.state.channel_job.channel_name, value: this.state.channel_job.channel_id}}
																	defaultValue={{label: this.state.channel_job.channel_name, value: this.state.channel_job.channel_id}} 
																	placeholder="Select a Channel" />
																</div>
															}
															{!this.state.is_admin &&
																<AutoField hidden="hidden" name="channel_id" initialValue={{label: '', value: this.state.channel_job.channel_id}}/>
															}
															<div className={this.state.is_admin ? "col-md-6" : "col-md-12"}>
																	<AutoField name="job_type_id" url={"/channel-job/get-channel-job-type"} isSearchable={true} 
																	options={[{label: this.state.channel_job.job_type_name, value: this.state.channel_job.job_type_id}]} 
																	initialValue={{label: this.state.channel_job.job_type_name, value: this.state.channel_job.job_type_id}} 
																	defaultValue={{label: this.state.channel_job.job_type_name, value: this.state.channel_job.job_type_id}}  
																	placeholder="Select a Job Type" />
															</div>
															<div className="col-md-12">
																	<AutoField name="job_specialist" url={"/specialist/list"} isSearchable={true} 
																	options={this.state.channel_job.specialist} 
																	initialValue={this.state.channel_job.specialist} 
																	defaultValue={this.state.channel_job.specialist}  
																	placeholder="Select a Job Specialist" />
															</div>
															<div className="col-md-12">
																<AutoField name="title" initialValue={this.state.channel_job.title}/>
															</div>
															<div className="col-md-12">
																<SelectField name="is_published" initialValue={this.state.channel_job.is_published} options={this.state.publishedEnum} placeholder="Select Published"/>
															</div>
															<div className="col-md-12">
																<AutoField name="due_date" value={this.state.due_date} selected={this.state.due_date_raw} required={true} />
															</div>
															<div className="col-md-12">
																<div className="row form-group" key="0">
																	<div className="col-md-2 pr-0">
																			<img src={this.state.image_preview} className="img-fluid img-thumbnail" />
																		</div>
																		<div className="col-md-1">
																			<button className="btn py-0 px-0" onClick={this.removeImage}>
																				<span className="navi-icon">
																					<i className="flaticon2-cross icon-nm"></i>
																				</span>
																			</button>
																		</div>
																	<div className="col-md-9">
																		<label className="text-capitalize">Image</label>
																		<div className="custom-file">
																			<input type="file" name="image" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
																			<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.image == null ? 'Choose File...' : this.state.image.name }</label>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div className="form-layout-footer float-right">
															<SubmitField value="Next" />
														</div>
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
				<style>{`
      			  	.react-date-picker__wrapper, .react-time-picker__wrapper {
						border-style: none
					}
					.react-date-picker__button, 
					.react-time-picker__clock-button, 
					.react-time-picker__button {
						padding:0 0 0 6px !important;
					}	
      			`}</style>
			</div>
		)
	}
}