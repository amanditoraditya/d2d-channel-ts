import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-job-edit'
import loadjs from 'loadjs'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import DetailSpec from '../channel-job-spec/detail'
import DetailApplicant from '../channel-job-applicant/detail'
import DetailHeader from '../channel-job-header/detail'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
    
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
            id: this.props.id,
			role: '',
            publishedEnum: [
				{
					label: 'Published',
					value: 1
				},{
					label: 'Not Published',
					value: 0
				}
			],
            info: false,
			infoStatus: '',
			infoMessage: '',
            image: null,
			due_date: undefined,
			due_date_raw: undefined,
			activeTab: '1'
		}

		this.flashInfo 					 = this.flashInfo.bind(this)
		this.setActiveTab 				 = this.setActiveTab.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])

        axios({
			url: goaConfig.BASE_API_URL + '/channel-job/detail/' + this.props.id,
			method: 'GET',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let channel_job = response.data.data
                let image_image = channel_job.job.image == null || channel_job.job.image == '' ? '' : channel_job.job.image

				self.setState({
					channel_job,
                    channel_id: channel_job.job.channel_id,
                    job_type_id: channel_job.job.job_type_id,
					due_date: moment(channel_job.job.due_date).format('YYYY-MM-DD HH:mm'), 
					due_date_raw: moment(channel_job.job.due_date).toDate(),
					image_image
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	setActiveTab(tab) {
		this.setState({
			activeTab: tab
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Detail Channel Job" parent="channel" selected="channel-job" module="ChannelJob" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Detail Channel Job</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channel-job/index" as="/channel-job" passHref>
												<a href="/channel-job" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}

                                        { this.state.channel_job &&
											<AutoForm schema={Schema}>
												<div className="mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													<Nav tabs className="nav-justified">
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.setActiveTab('1') }}>Channel Job</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.setActiveTab('2'); }}>Spec</NavLink>
                                                        </NavItem>
														<NavItem>
															<NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.setActiveTab('3'); }}>Header</NavLink>
														</NavItem>
														<NavItem>
															<NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.setActiveTab('4'); }}>Applicant</NavLink>
														</NavItem>
                                                    </Nav>
													<TabContent activeTab={this.state.activeTab}>
														<TabPane tabId="1">
															<div className="row mt-5">
                                                                { this.state.is_admin &&
                                                                    <div className="col-md-12">
																		<AutoField name="channel_id" url={"/channel/get-channels"} 
																		initialValue={{label: this.state.channel_job.job.channel_name, value: this.state.channel_job.job.channel_id}} 
																		placeholder="Select a Channel" 
																		disabled
																		/>
                                                                    </div>
                                                                }
																{!this.state.is_admin &&
																	<AutoField hidden="hidden" name="channel_id" initialValue={{label: '', value: this.state.channel_job.job.job.channel_id}}
                                                                    disabled/>
																}
                                                                <div className="col-md-12">
                                                                        <AutoField name="job_type_id" url={"/channel-job/get-channel-job-type"} isSearchable={true} 
                                                                        options={[{label: this.state.channel_job.job.job_type_name, value: this.state.channel_job.job.job_type_id}]} 
																		initialValue={{label: this.state.channel_job.job.job_type_name, value: this.state.channel_job.job.job_type_id}} 
                                                                        defaultValue={{label: this.state.channel_job.job.job_type_name, value: this.state.channel_job.job.job_type_id}}  
                                                                        disabled/>
                                                                </div>
                                                                <div className="col-md-12">
																    <AutoField name="title" initialValue={this.state.channel_job.job.title} disabled/>
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <SelectField name="is_published" initialValue={this.state.channel_job.job.is_published} options={this.state.publishedEnum} disabled/>
                                                                </div>
                                                                <div className="col-md-12">
                                                                    <AutoField name="due_date" value={this.state.due_date} selected={this.state.due_date_raw} required={true} disabled/>
                                                                </div>
																<div className="col-md-12">
																	<AutoField name="job_specialist" 
																		url={"/specialist/list"} 
																		isSearchable={true} 
																		value={this.state.channel_job.job.specialist} 
																		placeholder="Select a Job Specialist" 
																		disabled/>
																</div>
                                                                <div className="col-md-12">
                                                                    <div className="row form-group" key="0">
                                                                        <div className="col-md-2 pr-0">
                                                                            <label className="text-capitalize">Image</label>
                                                                            <img src={this.state.image_image} className="img-fluid img-thumbnail" />
                                                                        </div>
                                                                    </div>
                                                                </div>
															</div>
														</TabPane>
														<TabPane tabId="2">
															<div className="row mt-5">
																<div className="table">
																	<hr class="rounded"></hr>
																		{/* SPEC */}
																		<DetailSpec flashInfo={this.flashInfo} id={this.state.id} />	
																</div>
															</div>
														</TabPane>
														<TabPane tabId="3">
															<div className="row mt-5">
																<div className="table">
																	<hr class="rounded"></hr>
																		{/* HEADER */}
																		<DetailHeader flashInfo={this.flashInfo} id={this.state.id} />	
																</div>
															</div>
														</TabPane>
														<TabPane tabId="4">
															<div className="row mt-5">
																<div className="table">
																	<hr class="rounded"></hr>
																		{/* APPLICANT */}
																		<DetailApplicant flashInfo={this.flashInfo} id={this.state.id} />	
																</div>
															</div>
														</TabPane>
													</TabContent>
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
				<style>{`
      			  	.react-date-picker__wrapper, .react-time-picker__wrapper {
						border-style: none
					}
					.react-date-picker__button, 
					.react-time-picker__clock-button, 
					.react-time-picker__button {
						padding:0 0 0 6px !important;
					}
					input[type=text]:disabled {
						background: #f2f2f2;
					}
					[disabled]{
						color: #999999;
					}
      			`}</style>
			</div>
		)
	}
}