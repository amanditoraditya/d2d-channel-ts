import React from 'react'
import { withRouter } from 'next/router'
import Wrapper from '../../../components/wrapper'
import DetailJobForm from '../../../components/job/detail'
import loadjs from 'loadjs'
import _lo from 'lodash'

class DetailJob extends React.Component {

	componentDidMount() {
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} onCard={true} title="Channel Job" parent="channel" selected="channel-job" parent="channel" module="ChannelJob" permission="edit" headerCard="Detail Channel Job">
					<DetailJobForm id={this.props.router.query.id} />
				</Wrapper>
			</div>
		)
	}
}

export default withRouter(DetailJob)