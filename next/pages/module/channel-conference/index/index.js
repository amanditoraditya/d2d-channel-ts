import React from 'react'
import Link from 'next/link'
import $ from 'jquery'
import Wrapper from '../../../../components/wrapper'
import AuthServices from '../../../../components/auth'
import Table from '../table'
import loadjs from 'loadjs'
import _lo from 'lodash'

const goaConfig = require('../../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.flashInfo = this.flashInfo.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {

			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
    }
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Channel Conference" parent="channel" selected="channel-conference" module="ChannelConference" permission="read">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Channel Conference</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channel-conference/create" as="/channel-conference/create" passHref>
												<a href="/channel-conference/create" className="btn btn-sm btn-dark"><i className="fa fa-plus"></i> Add New</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<Table flashInfo={this.flashInfo} />
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}