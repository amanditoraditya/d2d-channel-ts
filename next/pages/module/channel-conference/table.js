import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import DataTable from 'datatables.net'
import 'datatables.net-bs4'
import Select2 from 'select2'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import Reactdom from 'react-dom'
import SwitchTableForm from '../../../components/forms/general-form/switch-table-form'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            totalData: 0,
            idDel: 0,
            modalDel: false,
            modalDelAll: false,
            is_admin: false,
        }

        this.actionEdit = this.actionEdit.bind(this)
        this.toggleDel = this.toggleDel.bind(this)
        this.toggleDelAll = this.toggleDelAll.bind(this)
        this.actionDelete = this.actionDelete.bind(this)
        this.actionDeleteAll = this.actionDeleteAll.bind(this)
    }

    async componentDidMount() {
        let self = this
        let is_admin = Auth.isAdmin();
        let columns = [
            {
                data: null,
                orderable: false,
                searchable: false,
                render: (data) => {
                    return `
					<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`
                }
            },
            // {
            // 	data: "id",
            // 	searchable: false,
            // 	orderable: true,
            // },
            {
                data: "created_at",
                searchable: false,
                orderable: true,
                className: 'd-none',
            },
            {
                data: "title",
                searchable: true,
                render: (data) => {
                    return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:450px">${data}</div>`
                }
            },
            {
                data: "channel_name",
                searchable: false,
                orderable: true,
            },
            {
                data: "gmt_tz",
                searchable: false,
                orderable: true,
            },
            {
                data: "start_date",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return moment(data).format('D MMMM YYYY')
                }
            },
            {
                data: "end_date",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return moment(data).format('D MMMM YYYY')
                }
            },
            {
                data: "is_published",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return data == '1' ? 'Published' : 'Unpublished'
                }
            },
            {
                data: null,
                orderable: false,
                searchable: false,
                className: 'text-center',
                render: (data) => {
                    return `
					<div class='text-center'>
						<div class='btn-group btn-group-sm'>
                            <a href='#' data-href='/module/channel-conference/view-attendee?id=${data.encrypted}' data-as='/channel-conference/view-attendee/${data.encrypted}' class='btn btn-sm btn-info btn-edit' title='View Attendee'>
                                <i class='fas fa-users'></i>
                            </a>
							<a href='#' data-href='/module/channel-conference/view-conference?id=${data.encrypted}' data-as='/channel-conference/view-conference/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='View Conference'>
                                <i class='fas fa-video'></i>
                            </a>
							<a href='#' data-href='/module/channel-conference/edit?id=${data.encrypted}' data-as='/channel-conference/edit/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='Edit'>
                                <i class='fas fa-pencil-alt'></i>
                            </a>
							<a href='#' class='btn btn-sm btn-danger alertdel' id='${data.encrypted}' title='Delete'>
                                <i class='far fa-trash-alt'></i>
                            </a>
						</div>
					</div>`
                }
            }
        ]
		let indexPublished = 7

        if (!is_admin) {
            columns = columns.filter(function (row) {
                return row.data != 'channel_name';
            });
            indexPublished = 6
        }

        self.setState({
            is_admin
        })
        $('#table-channel-conference').DataTable({
            order: [[1, 'DESC']],
            columnDefs: [
                {
                    targets: 'no-sort',
                    orderable: false
                },
                {
                    targets: indexPublished,
                    createdCell: (td, cellData, rowData, row, col) => Reactdom.render(
                        <SwitchTableForm
                            onFailed={(message) => { self.props.flashInfo('error', message) }}
                            checked={cellData == 1 ? true : false}
                            id={rowData.encrypted}
                            url="/channel-conference/update-status"
                        />,
                        td)
                }
            ],
            columns,
            stateSave: true,
            serverSide: true,
            processing: true,
            pageLength: 10,
            autoWidth: false,
            lengthMenu: [
                [10, 30, 50, 100, -1],
                [10, 30, 50, 100, 'All']
            ],
            ajax: {
                type: 'post',
                url: goaConfig.BASE_API_URL + '/channel-conference/datatable',
            },
            responsive: true,
            language: {
                paginate: {
                    first: '<i class="ki ki-double-arrow-back"></i>',
                    last: '<i class="ki ki-double-arrow-next"></i>',
                    next: '<i class="ki ki-arrow-next"></i>',
                    previous: '<i class="ki ki-arrow-back"></i>'
                }
            },
            "drawCallback": function (settings) {
                $("#titleCheck").click(function () {
                    let checkedStatus = this.checked;
                    $("table tbody tr td div:first-child input[type=checkbox]").each(function () {
                        this.checked = checkedStatus
                        if (checkedStatus == this.checked) {
                            $(this).closest('table tbody tr').removeClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                        if (this.checked) {
                            $(this).closest('table tbody tr').addClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                    })
                })

                $('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
                    let checkedStatus = this.checked
                    this.checked = checkedStatus

                    if (checkedStatus == this.checked) {
                        $(this).closest('table tbody tr').removeClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }

                    if (this.checked) {
                        $(this).closest('table tbody tr').addClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }
                })

                $('table tbody tr td div:first-child input[type=checkbox]').change(function () {
                    $(this).closest('tr').toggleClass("table-select", this.checked)
                })

                $(".alertdel").click(function () {
                    let id = $(this).attr("id")
                    self.toggleDel()
                    self.setState({
                        idDel: id
                    })
                })

                $('.btn-edit').on('click', function () {
                    let href = $(this).attr('data-href')
                    let as = $(this).attr('data-as')

                    self.actionEdit(href, as)
                })
            }
        })
    }

    componentWillUnmount() {
        $('#table-channel-conference').DataTable().destroy(true)
    }

    actionEdit(href, as) {
        Router.push(href, as)
    }

    toggleDel() {
        this.setState({
            modalDel: !this.state.modalDel
        })
    }

    toggleDelAll() {
        this.setState({
            modalDelAll: !this.state.modalDelAll
        })
    }

    actionDelete() {
        let self = this

        this.toggleDel()

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference/delete',
            method: 'POST',
            data: {
                id: self.state.idDel
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let table = $('#table-channel-conference').DataTable()

                table.clear().draw()
                table.state.clear()

                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }

    actionDeleteAll() {
        let self = this

        let deldata = []
        let values = $('.table-body input[type=checkbox]:checked').map(function () {
            deldata = $(this).attr("id")
            return deldata
        }).get()

        this.toggleDelAll()

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference/multidelete',
            method: 'POST',
            data: {
                totaldata: self.state.totalData,
                item: JSON.stringify(values)
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let table = $('#table-channel-conference').DataTable()
                table.clear().draw()
                table.state.clear()

                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }

    render() {
        return (
            <div>
                <div className="table-responsive">
                    <table id="table-channel-conference" className="table table-bordered responsive nowrap" width="100%">
                        <thead>
                            <tr>
                                <th className="no-sort" style={{ width: "10px" }}></th>
                                {/* <th style={{ width: "30px" }}>Id</th> */}
                                <th>Created</th>
                                <th>Title</th>
                                {/* <th>URL</th> */}
                                {this.state.is_admin &&
                                    <th>Channel</th>
                                }
                                <th>Time Zone</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Status</th>
                                <th className="no-sort" style={{ width: "100px" }}>Action</th>
                            </tr>
                        </thead>
                        <tbody className="table-body"></tbody>
                        <tfoot>
                            <tr>
                                <td style={{ width: "10px" }} className="text-center">
                                    <label className="checkbox checkbox-lg checkbox-inline">
                                        <input type="checkbox" id="titleCheck" />
                                        <span></span>
                                    </label>
                                </td>
                                <td colSpan="6" className="d-table-cell">
                                    <button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>

                <Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete all this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>
            </div>
        )
    }
}