import React from 'react'
import Link from 'next/link'
import $ from 'jquery'
import 'datatables.net-bs4'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import AttendeeTable from './components/attendee'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import _lo from 'lodash'
import Schema from '../../../scheme/channel-conference-attendee-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.id,
            totalData: 0,
            idDel: 0,
            modalDel: false,
            modalDelAll: false,
            is_admin: false,
            info: false,
            infoStatus: '',
            infoMessage: '',
            activeTab: '1',
        }

        this.flashInfo = this.flashInfo.bind(this)
        this.setActiveTab = this.setActiveTab.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

        this.child = React.createRef();
    }

    async componentDidMount() {
        let self = this
        let is_admin = Auth.isAdmin()
        let login = 0

        if (Auth.loggedIn()) {
            login = 1
        }

        self.setState({
            login,
            is_admin
        })

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                console.log('response.data.data', response.data.data)
                let channel_conference = response.data.data
                let channel_id = channel_conference.channel_id
                console.log('channel_id', channel_id)
                let resSub = await axios.get(goaConfig.BASE_API_URL + '/channel-conference/get-conference-subscribers', { params: { channel_id } })
                console.log('resSub', resSub)

                let subscriberEnum = !_lo.isEmpty(resSub.data) ? resSub.data : []

                self.setState({
                    channel_conference,
                    channel_subscribers: channel_conference.channel_subscribers,
                    init_subscribers: channel_conference.channel_subscribers,
                    channel_id,
                    init_channel_id: channel_id,
                    subscriberEnum
                })

            } else {
                self.flashInfo('error', response.data.message)
            }
        })

        loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
    handleSubmit(data) {
        let self = this
        data.channel_subscribers = this.state.channel_subscribers

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference-attendee/update/' + self.state.id,
            method: 'POST',
            data: data,
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                self.child.current.refreshTable()
                await self.flashInfo('success', response.data.message)
                // Router.push('/module/channel-conference/index', '/channel-conference')
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }
    changeSubscriberID(e) {
        this.setState({
            channel_subscribers: e
        })
    }
    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)
        $('.error-bar').delay(1000).show()

        await $('.error-bar').delay(2000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    setActiveTab(tab) {
        this.setState({
            activeTab: tab
        })
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Tambah Channel" parent="channel" selected="channel-conference" module="ChannelConference" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">View Channel Conference Attendee</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/channel-conference/index" as="/channel-conference" passHref>
                                                <a href="/channel-conference" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">

                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }
                                        <Nav tabs className="nav-justified">
                                            <NavItem>
                                                <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.setActiveTab('1'); }}>Attendance</NavLink>
                                            </NavItem>
                                            {this.state.is_admin &&
                                                <NavItem>
                                                    <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.setActiveTab('2'); }}>Edit Attendance</NavLink>
                                                </NavItem>
                                            }

                                        </Nav>
                                        <TabContent activeTab={this.state.activeTab}>
                                            <TabPane tabId="1">
                                                <div className="row mt-5">
                                                    <div className="table px-5">
                                                        <hr className="rounded"></hr>
                                                        <AttendeeTable ref={this.child} flashInfo={this.flashInfo} id={this.state.id} />
                                                    </div>
                                                </div>
                                            </TabPane>
                                            {this.state.is_admin &&
                                                <TabPane tabId="2">
                                                    <AutoForm onSubmit={this.handleSubmit} schema={Schema}>
                                                        <div className="row mt-5">
                                                            <div className="col-md-12">
                                                                <AutoField
                                                                    name="channel_subscriber_id"
                                                                    isSearchable={true}
                                                                    value={this.state.channel_subscribers}
                                                                    options={this.state.subscriberEnum}
                                                                    disabled={_lo.isEmpty(this.state.subscriberEnum)}
                                                                    placeholder="Select Subscribers" onChange={(e) => this.changeSubscriberID(e)} />
                                                            </div>
                                                        </div>
                                                        <div className="form-layout-footer float-right">
                                                            <SubmitField value="Submit" />
                                                        </div>
                                                    </AutoForm>
                                                </TabPane>
                                            }
                                        </TabContent>

                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
            </div>
        )
    }
}