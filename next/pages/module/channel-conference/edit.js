import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/channel-conference'
import loadjs from 'loadjs'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import _lo from 'lodash'
// import AttendeeTable from './attendee'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.id,
            login: 0,
            channel: null,
            statusEnum: [
                {
                    label: 'Active',
                    value: '1'
                }, {
                    label: 'Disabled',
                    value: '0'
                }
            ],
            tfEnum: [
                {
                    label: 'Published',
                    value: 1
                }, {
                    label: 'Not Published',
                    value: 0
                }
            ],
            // role: '',
            saved_tz: '+07:00',
            info: false,
            infoStatus: '',
            infoMessage: '',
            channel_id: null,
            startDate: undefined,
            endDate: undefined,
            startDateRaw: undefined,
            endDateRaw: undefined,
            registrationUntil: undefined,
            registrationUntilRaw: undefined,
            activeTab: '1',
            channel_subscribers: null,
            subscriberEnum: [],
            init_subscribers: [],
            init_channel_id: null,
            init_start: true,
            init_end: true,
            init_regis: true
        }

        // this.changeRole = this.changeRole.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
        this.encryptString = this.encryptString.bind(this)
        this.changeIsPublished = this.changeIsPublished.bind(this)
        this.changeIsInstant = this.changeIsInstant.bind(this)
        this.changeStartDate = this.changeStartDate.bind(this)
        this.changeEndDate = this.changeEndDate.bind(this)
        this.changeRegistrationUntil = this.changeRegistrationUntil.bind(this)
        this.setActiveTab = this.setActiveTab.bind(this)
        this.changeSubscriberID = this.changeSubscriberID.bind(this)
    }

    componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            this.setState({
                login: 1,
                is_admin: Auth.isAdmin()
            })
        }

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                console.log('response.data.data', response.data.data)
                let channel_conference = response.data.data
                let channel_id = channel_conference.channel_id
                let resSub = await axios.get(goaConfig.BASE_API_URL + '/channel-conference/get-conference-subscribers', { params: { channel_id } })

                let subscriberEnum = !_lo.isEmpty(resSub.data) ? resSub.data : []

                self.setState({
                    channel_conference,
                    saved_tz: channel_conference.gmt_tz,
                    curr_tz: moment().format('Z'),
                    startDateRaw: moment(channel_conference.start_date).toDate(),
                    startDate: moment(channel_conference.start_date).format('YYYY-MM-DD HH:mm'),
                    endDateRaw: moment(channel_conference.end_date).toDate(),
                    endDate: moment(channel_conference.end_date).format('YYYY-MM-DD HH:mm'),
                    registrationUntilRaw: moment(channel_conference.registration_until).toDate(),
                    registrationUntil: moment(channel_conference.registration_until).format('YYYY-MM-DD HH:mm'),
                    is_published: channel_conference.is_published,
                    is_instant: channel_conference.is_instant,
                    channel_subscribers: channel_conference.channel_subscribers,
                    init_subscribers: channel_conference.channel_subscribers,
                    channel_id,
                    init_channel_id: channel_id,
                    subscriberEnum
                })

            } else {
                self.flashInfo('error', response.data.message)
            }
        })

    }

    handleSubmit(data) {
        let self = this
        $('.error-bar').delay(1000).show()
        let valid = false
        let err_msg = ''

        data.start_date = self.state.startDate
        data.end_date = self.state.endDate
        data.registration_until = self.state.registrationUntil

        data.gmt_tz = moment(self.state.startDateRaw).format('Z')

        data.is_published = self.state.is_published
        data.is_instant = self.state.is_instant

        // data.description = self.state.channel_conference.description

        // if (_lo.isArray(data.channel_id)) {
        // 	data.channel_id = data.channel_id[0].value
        // } else {
        // 	data.channel_id = self.state.channel_id
        // }

        data.cid = data.channel_id.value

        data.channel_subscribers = self.state.channel_subscribers

        let start = moment(self.state.startDateRaw)
        let end = moment(self.state.endDateRaw)
        var duration = moment.duration(end.diff(start));
        var minutes = duration.asMinutes();
        minutes = Math.round(minutes)

        if (minutes < 0) {
            err_msg = 'Start Date cannot be greater than End Date'
        } else if (minutes > 60 * 24) {
            err_msg = 'Conference duration cannot be greater than a day'
        } else {
            valid = true
        }

        if (valid) {
            axios({
                url: goaConfig.BASE_API_URL + '/channel-conference/update/' + self.state.id,
                method: 'POST',
                data: data,
                timeout: goaConfig.TIMEOUT
            }).then(async function (response) {
                if (response.data.code == '2000') {
                    await self.flashInfo('success', response.data.message)
                    Router.push('/module/channel-conference/index', '/channel-conference')
                } else if (response.data.code == '4004') {
                    self.flashInfoErrorMultiLine(response.data.data._errorMessages)
                } else {
                    self.flashInfo('error', response.data.message)
                }
            })
        } else {
            self.flashInfo('error', err_msg)
        }

    }
    flashInfoErrorMultiLine(err) {
        $('.error-bar').delay(1000).show()
        this.setState({
            info: true,
            infoStatus: 'alert alert-solid alert-danger error-bar',
            infoMessage: err.map(e => e.message).join(", ")
        })
        $('.error-bar').delay(2000).fadeOut()
    }
    setActiveTab(tab) {
        this.setState({
            activeTab: tab
        })
    }
    async changeChannelState(e) {
        let init_subscribers = this.state.init_subscribers

        if (e.value == this.state.init_channel_id) {
            this.setState({ channel_subscribers: init_subscribers })
        } else {
            this.setState({ channel_subscribers: [] })
        }

        let id = null
        if (e.value != undefined) {
            id = e.value
        } else if (this.state.channel_id != undefined) {
            id = this.state.channel_id
        }

        if (id != null) {
            let resSub = await axios.get(goaConfig.BASE_API_URL + '/channel-conference/get-conference-subscribers', { params: { channel_id: id } })
            let subscriberEnum = !_lo.isEmpty(resSub.data) ? resSub.data : []
            this.setState({ subscriberEnum, channel_id: e.value })
        }
    }
    changeSubscriberID(e) {
        this.setState({
            channel_subscribers: e
        })
    }
    changeIsPublished(e) {
        this.setState({
            is_published: e
        })
    }
    changeIsInstant(e) {
        this.setState({
            is_instant: e
        })
    }
    changeStartDate(e) {
        if (this.state.init_start) {
            this.setState({
                init_start: false
            })
        } else {
            this.setState({
                startDate: moment(e).format('YYYY-MM-DD HH:mm'),
                startDateRaw: e
            })
        }
    }
    changeEndDate(e) {
        if (this.state.init_end) {
            this.setState({
                init_end: false
            })
        } else {
            this.setState({
                endDate: moment(e).format('YYYY-MM-DD HH:mm'),
                endDateRaw: e
            })
        }
    }

    changeRegistrationUntil(e) {
        if (this.state.init_regis) {
            this.setState({
                init_regis: false
            })
        } else {
            this.setState({
                registrationUntil: moment(e).format('YYYY-MM-DD HH:mm'),
                registrationUntilRaw: e
            })
        }
    }

    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }
        $('.error-bar').delay(1000).show()

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)

        await $('.error-bar').delay(2000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    encryptString(string) {
        let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
        let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
        crypted += cipher.final('hex')

        return crypted
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Update Channel Conference" parent="channel" selected="channel-conference" module="ChannelConference" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Update Channel Conference</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/channel-conference/index" as="/channel-conference" passHref>
                                                <a href="/channel-conference" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        {this.state.channel_conference &&
                                            <AutoForm schema={Schema} onSubmit={this.handleSubmit}
                                                onChange={(key, value) => {
                                                    console.log('onchange')
                                                    if (key == 'channel_id') { this.changeChannelState(value) }
                                                    if (key == 'start_date') { this.changeStartDate(value) }
                                                    if (key == 'end_date') { this.changeEndDate(value) }
                                                    if (key == 'registration_until') { this.changeRegistrationUntil(value) }
                                                }}>
                                                <div className="mg-b-15">
                                                    <div className="col-md-12 text-capitalize">
                                                        <ErrorsField />
                                                    </div>
                                                    <Nav tabs className="nav-justified">
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.setActiveTab('1') }}>General</NavLink>
                                                        </NavItem>
                                                        <NavItem>
                                                            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.setActiveTab('2'); }}>Date</NavLink>
                                                        </NavItem>

                                                    </Nav>
                                                    <TabContent activeTab={this.state.activeTab}>
                                                        <TabPane tabId="1">
                                                            <div className="row mt-5">
                                                                <div className={this.state.is_admin ? 'col-md-6' : 'd-none'}>
                                                                    <AutoField name="channel_id"
                                                                        url={"/channel/get-channels"}
                                                                        initialValue={this.state.channel_conference.channels}
                                                                        placeholder="Select a Channel"
                                                                    />
                                                                </div>
                                                                <div className={this.state.is_admin ? "col-md-6" : "col-md-12"}>
                                                                    <AutoField name="title" initialValue={this.state.channel_conference.title} />
                                                                </div>
                                                                {/* <div className="col-md-12">
																	<AutoField name="description" value={this.state.channel_conference.description} onChange={e => this.setState(prevState => ({ channel_conference: { ...prevState.channel_conference, description: e } }))} />
																</div> */}
                                                                <div className="col-md-6">
                                                                    <AutoField name="is_published" value={this.state.is_published} options={this.state.tfEnum} placeholder="Select Published" onChange={(e) => this.changeIsPublished(e)} />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <AutoField name="max_attendees" initialValue={this.state.channel_conference.max_attendees} />
                                                                </div>
                                                            </div>
                                                        </TabPane>
                                                        <TabPane tabId="2">
                                                            <div className="row mt-5">
                                                                <div className="col-md-6">
                                                                    <AutoField name="saved_tz" value={this.state.saved_tz} disabled={true} />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <AutoField name="curr_tz" value={this.state.curr_tz} disabled={true} />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <AutoField name="start_date" value={this.state.startDate} selected={this.state.startDateRaw} required={true} />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <AutoField name="end_date" value={this.state.endDate} selected={this.state.endDateRaw} required={true} />
                                                                </div>
                                                                <div className="col-md-6">
                                                                    <AutoField name="registration_until" value={this.state.registrationUntil} selected={this.state.registrationUntilRaw} required={true} />
                                                                </div>
                                                            </div>
                                                        </TabPane>
                                                        {/* <TabPane tabId="3">
															<div className="row mt-5">
																<div className="table px-5">
																	<hr className="rounded"></hr>
																	<AttendeeTable flashInfo={this.flashInfo} id={this.state.id} />
																</div>
															</div>
														</TabPane>
														{this.state.is_admin &&
															<TabPane tabId="4">
																<div className="row mt-5">
																	<div className="col-md-12">
																		<AutoField name="channel_subscriber_id"
																			isSearchable={true}
																			value={this.state.channel_subscribers}
																			options={this.state.subscriberEnum}
																			disabled={_lo.isEmpty(this.state.subscriberEnum)}
																			placeholder="Select Subscribers" onChange={(e) => this.changeSubscriberID(e)} />
																	</div>
																</div>
															</TabPane>
														} */}
                                                    </TabContent>
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
                                            </AutoForm>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
                {/* override picker style */}
                <style>{`
      			  	.react-date-picker__wrapper, .react-time-picker__wrapper {
						border-style: none
					}
					.react-date-picker__button, 
					.react-time-picker__clock-button, 
					.react-time-picker__button {
						padding:0 0 0 6px !important;
					}
					
      			`}</style>
            </div>
        )
    }
}