import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import loadjs from 'loadjs'
import _lo from 'lodash'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import Iframe from 'react-iframe'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}

	constructor(props) {
		super(props)

		this.state = {
			login: 0,
			status: 1,
			id: this.props.id,
			tfEnum: [
				{
					label: 'Yes',
					value: 1
				}, {
					label: 'No',
					value: 0
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
	}

	async componentDidMount() {
		let self = this

		let is_admin = Auth.isAdmin()
		let login = 0

		if (Auth.loggedIn()) {
			login = 1
		}

		await this.setState({
			login,
			is_admin
		})

		axios({
			url: goaConfig.BASE_API_URL + '/channel-conference/view-conference',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let conference_url = []
				if (!_lo.isEmpty(response.data.data.dmeet)) {
					conference_url = response.data.data.dmeet
				}
				self.setState({
					conference_url,
				})

			} else {
				self.flashInfo('error', response.data.message)
			}
		})

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Tambah Channel" parent="channel" selected="channel-conference" module="ChannelConference" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">View Channel Conference</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/channel-conference/index" as="/channel-conference" passHref>
												<a href="/channel-conference" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>

									<div className="card-body">
										<Iframe url={this.state.conference_url}
											height="500px"
											width="100%"
											id="myId"
											className="myClassname"
											display="initial"
											allowFullScreen
										/>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}