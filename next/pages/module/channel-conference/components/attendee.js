import React from 'react'
import _lo from 'lodash'
import $ from 'jquery'
import 'datatables.net-bs4'
import AuthServices from '../../../../components/auth'
import { Modal } from 'reactstrap'

const goaConfig = require('../../../../config')
const Auth = new AuthServices()

export default class extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            conference_id: this.props.id,
            totalData: 0,
            is_admin: false,
            modalDel: false,
            modalDelAll: false
        }

        this.actionEdit = this.actionEdit.bind(this)
        this.toggleDel = this.toggleDel.bind(this)
        this.toggleDelAll = this.toggleDelAll.bind(this)
        this.actionDelete = this.actionDelete.bind(this)
        this.actionDeleteAll = this.actionDeleteAll.bind(this)
        this.refreshTable = this.refreshTable.bind(this)
    }

    async componentDidMount() {
        let self = this
        let columns = [
            {
                data: null,
                orderable: false,
                searchable: false,
                render: (data) => {
                    return `
					<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`
                }
            },
            {
                data: "created_at",
                searchable: false,
                orderable: true,
                className: 'd-none',
            },
            {
                data: "name",
                searchable: true,
                orderable: true,
            },
            {
                data: "email",
                searchable: true,
                orderable: true,
            },
            {
                data: "spesialization",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return data == null || data == '' ? '-' : data
                }
            },
            {
                data: "nonspesialization",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return data == null || data == '' ? '-' : data
                }
            },
            {
                data: "display_picture",
                searchable: false,
                orderable: false,
                render: (data) => {
                    let image = ''
                    if (data != null || data != '') {
                        image = `<img class="pull-left img-responsive img-thumbnail" src="${data}" style="width:100px; height:100px;"/>`
                    }
                    return image
                }
            },
            {
                data: null,
                orderable: false,
                searchable: false,
                className: 'text-center',
                render: (data) => {
                    return `
					<div class='text-center'>
						<div class='btn-group btn-group-sm'>
							<a href='#' class='btn btn-sm btn-danger alertdel' id='${data.encrypted}' title='Delete'><i class='far fa-trash-alt'></i></a>
						</div>
					</div>`
                }
            }
        ]
        $('#table-channel-conference-attendee').DataTable({
            order: [[1, 'ASC']],
            autoWidth: false,
            columnDefs: [{
                targets: 'no-sort',
                orderable: false,
            }],
            columns,
            stateSave: true,
            serverSide: true,
            processing: true,
            pageLength: 5,
            lengthMenu: [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, 'All']
            ],
            ajax: {
                type: 'post',
                url: goaConfig.BASE_API_URL + '/channel-conference-attendee/datatable',
                data: {
                    id: self.state.conference_id
                },
            },
            responsive: true,
            language: {
                paginate: {
                    first: '<i class="ki ki-double-arrow-back"></i>',
                    last: '<i class="ki ki-double-arrow-next"></i>',
                    next: '<i class="ki ki-arrow-next"></i>',
                    previous: '<i class="ki ki-arrow-back"></i>'
                }
            },
            "drawCallback": function (settings) {
                $("#titleCheck").click(function () {
                    let checkedStatus = this.checked;
                    $("table tbody tr td div:first-child input[type=checkbox]").each(function () {
                        this.checked = checkedStatus
                        if (checkedStatus == this.checked) {
                            $(this).closest('table tbody tr').removeClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                        if (this.checked) {
                            $(this).closest('table tbody tr').addClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                    })
                })

                $('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
                    let checkedStatus = this.checked
                    this.checked = checkedStatus

                    if (checkedStatus == this.checked) {
                        $(this).closest('table tbody tr').removeClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }

                    if (this.checked) {
                        $(this).closest('table tbody tr').addClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }
                })

                $('table tbody tr td div:first-child input[type=checkbox]').change(function () {
                    $(this).closest('tr').toggleClass("table-select", this.checked)
                })

                $(".alertdel").click(function () {
                    let id = $(this).attr("id")
                    self.toggleDel()
                    self.setState({
                        idDel: id
                    })
                })
            }
        })

        let windowWidth = $(window).width()
        if (windowWidth < 767) {
            $('.table-wrapper').addClass('table-responsive')
        } else {
            $('.table-wrapper').removeClass('table-responsive')
        }
    }

    componentWillUnmount() {
        $('#table-channel-conference-attendee').DataTable().destroy(true)
    }

    actionEdit(href, as) {
        Router.push(href, as)
    }

    toggleDel() {
        console.log('toggledel')
        this.setState({
            modalDel: !this.state.modalDel
        })
    }

    toggleDelAll() {
        console.log('toggleDelAll')
        this.setState({
            modalDelAll: !this.state.modalDelAll
        })
    }

    actionDelete() {
        let self = this

        this.toggleDel()

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference-attendee/delete',
            method: 'POST',
            data: {
                id: self.state.idDel
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                self.refreshTable()
                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }

    actionDeleteAll() {
        let self = this

        let deldata = []
        let values = $('.table-body input[type=checkbox]:checked').map(function () {
            deldata = $(this).attr("id")
            return deldata
        }).get()

        this.toggleDelAll()

        axios({
            url: goaConfig.BASE_API_URL + '/channel-conference-attendee/multidelete',
            method: 'POST',
            data: {
                totaldata: self.state.totalData,
                item: JSON.stringify(values)
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                self.refreshTable()
                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }
    refreshTable() {
        let table = $('#table-channel-conference-attendee').DataTable()
        table.clear().draw()
        table.state.clear()
    }

    render() {
        return (
            <div>
                <div className="table-responsive">
                    <table id="table-channel-conference-attendee" className="table table-bordered responsive nowrap" width="100%">
                        <thead>
                            <tr>
                                <th className="no-sort" style={{ width: "10px" }}></th>
                                {/* <th style={{ width: "30px" }}>Id</th> */}
                                <th>Created</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Spesialization</th>
                                <th>Non Spesialization</th>
                                <th>Display Picture</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody className="table-body"></tbody>
                        <tfoot>
                            <tr>
                                <td style={{ width: "10px" }} className="text-center">
                                    <label className="checkbox checkbox-lg checkbox-inline">
                                        <input type="checkbox" id="titleCheck" />
                                        <span></span>
                                    </label>
                                </td>
                                <td colSpan="6" className="d-table-cell">
                                    <button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>

                <Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete all this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>
            </div>
        )
    }
}