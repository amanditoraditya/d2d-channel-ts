import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/channel-banner'
import loadjs from 'loadjs'
import _lo from 'lodash'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            login: 0,
            statusEnum: [
                {
                    label: 'Active',
                    value: 1
                }, {
                    label: 'Disabled',
                    value: 0
                }
            ],
            tfEnum: [
                {
                    label: 'Published',
                    value: 1
                }, {
                    label: 'Not Published',
                    value: 0
                }
            ],
            info: false,
            infoStatus: '',
            infoMessage: '',
            status: 1,
            is_active: 1,
            image: '',
            image_preview: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
        this.changeImages = this.changeImages.bind(this)
        this.removeImage = this.removeImage.bind(this)
        this.changeIsActive = this.changeIsActive.bind(this)
    }

    async componentDidMount() {
        let self = this
        let is_admin = Auth.isAdmin()
        let channel_id
        let login = 0

        if (Auth.loggedIn()) {
            login = 1
        }

        if (!is_admin) {
            let profile = Auth.getProfile()
            channel_id = {
                value: profile.user.channel_id
            }
        } else {
            channel_id = undefined
        }

        await this.setState({
            login,
            channel_id,
            is_admin
        })

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])
    }

    handleSubmit(data) {
        let self = this

        data.remove_image = self.state.remove_image
        data.cid = data.channel_id.value
        data.is_active = self.state.is_active

        const data_form = new FormData()
        data_form.append('data', JSON.stringify(data))
        data_form.append('image', self.state.image)
        axios({
            url: goaConfig.BASE_API_URL + '/channel-banner/create',
            method: 'POST',
            data: data_form,
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                await self.flashInfo('success', response.data.message)
                Router.push('/module/channel-banner/index', '/channel-banner')
            } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                self.flashInfoErrorMultiLine(response.data.data._errorMessages)
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleKeyDown (e) {
        if (e.key === 'Enter') {
          e.preventDefault();
          e.stopPropagation();
        }
    }

    flashInfoErrorMultiLine(err) {
        $('.error-bar').delay(1000).show()
        this.setState({
            info: true,
            infoStatus: 'alert alert-solid alert-danger error-bar',
            infoMessage: err.map(e => e.message).join(", ")
        })
        $('.error-bar').delay(2000).fadeOut()
    }

    changeIsActive(e) {
        this.setState({
            is_active: e
        })
    }

    changeImages(e) {
        let self = this

        let image = this.state.image
        self.setState({
            image: e.target.files[0]
        })

        this.setState({
            image: e.target.files[0],
            image_preview: URL.createObjectURL(e.target.files[0])
        })
    }

    removeImage(e) {
        e.preventDefault()

        $('#customFile').val(null)
        this.setState({
            remove_image: true,
            image_preview: '',
            image: null
        })
    }

    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)
        $('.error-bar').delay(1000).show()
        await $('.error-bar').delay(2000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Tambah Banner" parent="channel" selected="channel-banner" module="ChannelBanner" permission="create">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Add Banner</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/channel-banner/index" as="/channel-banner" passHref>
                                                <a href="/channel-banner" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        <AutoForm
                                            schema={Schema}
                                            onSubmit={this.handleSubmit}
                                            onKeyDown={this.handleKeyDown}
                                            onChange={(key, value) => {
                                                if (key == 'is_active') { this.changeIsActive(value) }
                                            }}
                                        >
                                            <div className="row mg-b-15">
                                                <div className="col-md-12 text-capitalize">
                                                    <ErrorsField />
                                                </div>
                                                <div className={this.state.is_admin ? 'col-md-6 input-to-front-1' : 'd-none'}>
                                                    <AutoField name="channel_id"
                                                        url={"/channel/get-channels"}
                                                        isSearchable={true}
                                                        options={[{ label: '', value: '' }]}
                                                        initialValue={this.state.channel_id}
                                                        placeholder="Select a Channel"
                                                    />
                                                </div>
                                                <div className="col-md-6">
                                                    <AutoField name="name" />
                                                </div>
                                                <div className="col-md-6">
                                                    <AutoField name="position" />
                                                </div>
                                                <div className="col-md-6">
                                                    <AutoField name="link" />
                                                </div>
                                                <div className="col-md-6 input-to-front-2">
                                                    <AutoField name="is_active" value={this.state.is_active} options={this.state.tfEnum} placeholder="Select Active" />
                                                </div>
                                                <div className="col-md-12">
                                                    <div className="row form-group" key="0">
                                                        <div className="col-md-2 pr-0">
                                                            <img src={this.state.image_preview} className="img-fluid img-thumbnail" />
                                                        </div>
                                                        <div className="col-md-1">
                                                            <button className="btn py-0 px-0" onClick={this.removeImage}>
                                                                <span className="navi-icon">
                                                                    <i className="flaticon2-cross icon-nm"></i>
                                                                </span>
                                                            </button>
                                                        </div>

                                                        <div className="col-md-9">
                                                            <label className="text-capitalize">Banner Image</label>
                                                            <div className="custom-file">
                                                                <input type="file" name="image" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
                                                                <label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.image == null ? 'Choose File...' : this.state.image.name}</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-layout-footer float-right">
                                                <SubmitField value="Submit" />
                                            </div>
                                        </AutoForm>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
                <style>{`
      			  	.input-to-front-1 {
						position: relative;
						z-index: 9000;
					} 
					.input-to-front-2 {
						position: relative;
						z-index: 8999;
					}
      			`}</style>
            </div>
        )
    }
}