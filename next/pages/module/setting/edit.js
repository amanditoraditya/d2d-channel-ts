import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, SelectField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/setting-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			settingpos: null,
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.encryptString = this.encryptString.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.setState({
					settingpos: response.data.data
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		data.value = this.state.settingpos.value
		
		axios({
			url: goaConfig.BASE_API_URL + '/setting/update/' + self.state.id,
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/setting/index', '/setting')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')
		
		return crypted
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Setting" selected="setup" module="Setting" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update Setting</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/setting/index" as="/setting" passHref>
												<a href="/setting" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
											</div>
										</div>
									
										<div className="card-body">
											{ this.state.info &&
												<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
											}
											
											{ this.state.settingpos &&
												<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
													<div className="row mg-b-15">
														<div className="col-md-12 text-capitalize">
															<ErrorsField />
														</div>
														<div className="col-md-4">
															<AutoField name="groups" initialValue={ this.state.settingpos.groups } />
														</div>
														<div className="col-md-8">
															<AutoField name="options" initialValue={ this.state.settingpos.options } />
														</div>
														<div className="col-md-12">
															<AutoField name="value" value={ this.state.settingpos.value } onChange={e => this.setState(prevState => ({settingpos: { ...prevState.settingpos, value: e}}))} />
														</div>
													</div>
													<div className="form-layout-footer float-right">
														<SubmitField value="Submit" />
													</div>
												</AutoForm>
											}
										</div>
									</div>
								</div>
							</div>
					}
				</Wrapper>
			</div>
		)
	}
}