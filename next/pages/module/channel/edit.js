import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/channel'
import loadjs from 'loadjs'
import _lo from 'lodash'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)

        this.state = {
            login: 0,
            id: this.props.id,
            channel: null,
            logo: null,
            statusEnum: [
                {
                    label: 'Active',
                    value: '1'
                }, {
                    label: 'Disabled',
                    value: '0'
                }
            ],
            countryEnum: [
                {
                    label: 'Indonesia',
                    value: 'ID'
                },
                {
                    label: 'Cambodia',
                    value: 'KH'
                },
                {
                    label: 'Myanmar',
                    value: 'MM'
                },
                {
                    label: 'Philippines',
                    value: 'PH'
                },
            ],
            info: false,
            infoStatus: '',
            infoMessage: '',
            logo: '',
            logo_image: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
        this.encryptString = this.encryptString.bind(this)
        this.changeImages = this.changeImages.bind(this)
        this.removeImage = this.removeImage.bind(this)
    }

    componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            this.setState({
                login: 1
            })
        }

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])

        axios({
            url: goaConfig.BASE_API_URL + '/channel/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let channel = response.data.data
                let logo_image = channel.logo == null || channel.logo == '' ? '' : channel.logo

                self.setState({
                    channel,
                    channel_type_id: channel.channel_type_id,
                    channel_menus: channel.channel_menus,
                    logo_image
                })
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleSubmit(data) {
        let self = this

        data.admin = self.state.channel.admin
        data.status = self.state.channel.status

        data.ctid = self.state.channel_type_id
        data.channel_menus = self.state.channel_menus
        data.logo = self.state.logo
        data.remove_image = self.state.remove_image
        data.description = self.state.channel.description != null ? self.state.channel.description.replace("<p>", "<p style='margin-bottom:16px;'>") : ''

        if (!_lo.isEmpty(data.ctid.value)) {
            data.ctid = data.ctid.value
        } else {
            data.ctid = self.state.channel_type_id
        }

        const data_form = new FormData()
        data_form.append('data', JSON.stringify(data))
        data_form.append('logo', self.state.logo)
        axios({
            url: goaConfig.BASE_API_URL + '/channel/update/' + self.state.id,
            method: 'POST',
            data: data_form,
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                await self.flashInfo('success', response.data.message)
                Router.push('/module/channel/index', '/channel')
            } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                self.flashInfoErrorMultiLine(response.data.data._errorMessages)
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleKeyDown(e) {
        if (e.key === 'Enter') {
            e.preventDefault();
            e.stopPropagation();
        }
    }

    flashInfoErrorMultiLine(err) {
        $('.error-bar').delay(1000).show()
        this.setState({
            info: true,
            infoStatus: 'alert alert-solid alert-danger error-bar',
            infoMessage: err.map(e => e.message).join(", ")
        })
        $('.error-bar').delay(2000).fadeOut()
    }

    changeChannelType(e) {
        this.setState({
            channel_type_id: e.value
        })
    }

    changeMenuID(e) {
        this.setState({
            channel_menus: e
        })
    }

    changeImages(e) {
        let self = this

        let logo = this.state.logo
        self.setState({
            logo: e.target.files[0]
        })

        this.setState({
            logo: e.target.files[0],
            logo_image: URL.createObjectURL(e.target.files[0])
        })
    }

    removeImage(e) {
        e.preventDefault()

        $('#customFile').val(null)
        this.setState({
            remove_image: true,
            logo_image: '',
            logo: null
        })
    }

    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }
        $('.error-bar').delay(1000).show()

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)

        await $('.error-bar').delay(2000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    encryptString(string) {
        let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
        let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
        crypted += cipher.final('hex')

        return crypted
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Update Channel" selected="channel" parent="admin-page" module="Channel" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Update Channel</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/channel/index" as="/channel" passHref>
                                                <a href="/channel" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        {this.state.channel &&
                                            <AutoForm
                                                schema={Schema}
                                                onSubmit={this.handleSubmit}
                                                onKeyDown={this.handleKeyDown}
                                                onChange={(key, value) => {
                                                    if (key == 'channel_type_id') { this.changeChannelType(value) }
                                                }}
                                            >
                                                <div className="row mg-b-15">
                                                    <div className="col-md-12 text-capitalize">
                                                        <ErrorsField />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField
                                                            name="channel_type_id" url="/channel/get-channel-type"
                                                            isSearchable={true}
                                                            options={[{ label: this.state.channel.channel_type_name, value: this.state.channel.channel_type_id }]}
                                                            defaultValue={{ label: this.state.channel.channel_type_name, value: this.state.channel.channel_type_id }}
                                                            initialValue={{ label: this.state.channel.channel_type_name, value: this.state.channel.channel_type_id }}
                                                            placeholder={'Selected ' + this.state.channel.channel_type_name}
                                                        />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="country_code" options={this.state.countryEnum} initialValue={this.state.channel.country_code} placeholder="Select Country" />
                                                    </div>
                                                    <div className="col-md-12">
                                                        <AutoField name="channel_name" initialValue={this.state.channel.channel_name} />
                                                    </div>
                                                    <div className="col-md-12">
                                                        <AutoField name="description" value={this.state.channel.description} onChange={e => this.setState(prevState => ({ channel: { ...prevState.channel, description: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="status" value={this.state.channel.status} options={this.state.statusEnum} placeholder="Select Status" onChange={e => this.setState(prevState => ({ channel: { ...prevState.channel, status: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField
                                                            className="menu-dropdown"
                                                            name="menu_id" url="/menu/get-menu"
                                                            isSearchable={true}
                                                            options={this.state.channel.channel_menus}
                                                            defaultValue={this.state.channel.channel_menus}
                                                            placeholder={'Selected Menus'}
                                                            onChange={(e) => this.changeMenuID(e)}
                                                        />
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="row form-group" key="0">
                                                            <div className="col-md-2 pr-0">
                                                                <img src={this.state.logo_image} className="img-fluid img-thumbnail" />
                                                            </div>
                                                            <div className="col-md-1">
                                                                <button className="btn py-0 px-0" onClick={this.removeImage}>
                                                                    <span className="navi-icon">
                                                                        <i className="flaticon2-cross icon-nm"></i>
                                                                    </span>
                                                                </button>
                                                            </div>

                                                            <div className="col-md-9">
                                                                <label className="text-capitalize">Logo</label>
                                                                <div className="custom-file">
                                                                    <input type="file" name="logo" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
                                                                    <label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.logo == null ? 'Choose File...' : this.state.logo.name}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
                                            </AutoForm>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
                {/* override picker style */}
                <style>{`
      			  	.menu-dropdown {
						position: relative;
						z-index: 10000;
					}
      			`}</style>
            </div>
        )
    }
}