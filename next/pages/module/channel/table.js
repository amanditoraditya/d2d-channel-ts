import React from 'react'
import Router from 'next/router'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import SortableList from '../../../components/channel/sortable'
import QRCode from '../../../components/channel/qrcode'
import Reactdom from 'react-dom'
import 'datatables.net-bs4'
import AuthServices from '../../../components/auth'
import SwitchTableForm from '../../../components/forms/general-form/switch-table-form'
import { getService, postService } from '../../../services/base-service'
import axios from 'axios'
import { connect } from 'react-redux'
import { successToast } from '../../../redux/reducer/general'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

class TableChannel extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            totalData: 0,
            idDel: 0,
            modalDel: false,
            modalDelAll: false,
            modalBanner: false,
            selectedId: null,
            banners: [],
            modalQRCode: false,
            qr: [],
            linkChecker: 0,
        }

        this.actionEdit = this.actionEdit.bind(this)
        this.toggleDel = this.toggleDel.bind(this)
        this.toggleDelAll = this.toggleDelAll.bind(this)
        this.actionDelete = this.actionDelete.bind(this)
        this.actionDeleteAll = this.actionDeleteAll.bind(this)
        this.modalOrderBanner = this.modalOrderBanner.bind(this)
        this.toggleModalBanner = this.toggleModalBanner.bind(this)
        this.submitBanner = this.submitBanner.bind(this)
        this.modalShowQR = this.modalShowQR.bind(this)
        this.toggleModalQR = this.toggleModalQR.bind(this)
        this.downloadQR = this.downloadQR.bind(this)
        this.invitationLink = this.invitationLink.bind(this)
    }

    componentDidMount() {
        let self = this
        let is_admin = Auth.isAdmin();
        if (Auth.loggedIn()) {
            let user = Auth.getProfile()
            this.setState({
                login: 1,
                user,
            })
        }

        let columns = [
            {
                data: null,
                orderable: false,
                searchable: false,
                render: (data) => {
                    return `
					<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`
                }
            },
            {
                data: "created_at",
                searchable: false,
                orderable: true,
                className: 'd-none',
            },
            {
                data: "country_code",
                searchable: true,
                orderable: true,
            },
            {
                data: "channel_name",
                searchable: true,
                orderable: true,
            },
            {
                data: "channel_type_name",
                searchable: false,
                orderable: false,
            },
            {
                data: "status",
                searchable: false,
                orderable: true,
                render: (data) => {
                    return data == 1 ? 'Active' : 'Inactive'
                }
            },
            {
                data: null,
                orderable: false,
                searchable: false,
                className: 'text-center',
                render: (data) => {
                    let actionHtml = ''
                    const sortBanner = `<button class="btn btn-success btn-sort-banner" type="button" data-id="${data.encrypted}" title='Banner Position'><i class="fa fa-list"></i></button>`
                    const qrCode = `<button class='btn btn-sm btn-warning btn-qr' type="button" data-id="${data.encrypted}" title='Barcode'><i class='fas fa-qrcode'></i></button>`
                    if (!is_admin) {
                        actionHtml = `<div class='text-center'>\
							<div class='btn-group btn-group-sm'>\
								<a href='#' data-href='/module/user-channel/index' data-as='/user-channel' class='btn btn-sm btn-primary btn-edit' title='Manage'><i class='fas fa-compass'></i></a>\
                                ${qrCode}
                                ${sortBanner}
							</div>\
						</div>\n`
                    } else {
                        actionHtml = `<div class='text-center'>
							<div class='btn-group btn-group-sm'>
								<a href='#' data-href='/module/channel/edit?id=${data.encrypted}' data-as='/channel/edit/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>
                                ${qrCode}
                                ${sortBanner}
								<a href='#' class='btn btn-sm btn-danger alertdel' id='${data.encrypted}' title='Delete'><i class='far fa-trash-alt'></i></a>
							</div>\
						</div>`
                    }

                    return actionHtml
                }
            }
        ]
        $('#table-channel').DataTable({
            order: [[1, 'DESC']],
            columnDefs: [
                {
                    targets: 'no-sort',
                    orderable: false
                },
                {
                    targets: 5,
                    createdCell: (td, cellData, rowData, row, col) => Reactdom.render(
                        <SwitchTableForm
                            onFailed={(message) => { self.props.flashInfo('error', message) }}
                            checked={cellData == 1 ? true : false}
                            trueSwitchLabel='Active'
                            falseSwitchLabel='Disabled'
                            id={rowData.encrypted}
                            url="/channel/update-status"
                        />,
                        td)
                }
            ],
            columns,
            stateSave: true,
            serverSide: true,
            processing: true,
            pageLength: 10,
            autoWidth: false,
            lengthMenu: [
                [10, 30, 50, 100, -1],
                [10, 30, 50, 100, 'All']
            ],
            ajax: {
                type: 'post',
                url: goaConfig.BASE_API_URL + '/channel/datatable',
            },
            responsive: true,
            language: {
                paginate: {
                    first: '<i class="ki ki-double-arrow-back"></i>',
                    last: '<i class="ki ki-double-arrow-next"></i>',
                    next: '<i class="ki ki-arrow-next"></i>',
                    previous: '<i class="ki ki-arrow-back"></i>'
                }
            },
            "drawCallback": function (settings) {
                $("#titleCheck").click(function () {
                    let checkedStatus = this.checked;
                    $("table tbody tr td div:first-child input[type=checkbox]").each(function () {
                        this.checked = checkedStatus
                        if (checkedStatus == this.checked) {
                            $(this).closest('table tbody tr').removeClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                        if (this.checked) {
                            $(this).closest('table tbody tr').addClass('table-select')
                            $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                            self.setState({
                                totalData: $('.table-body input[type=checkbox]:checked').length
                            })
                        }
                    })
                })

                $('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
                    let checkedStatus = this.checked
                    this.checked = checkedStatus

                    if (checkedStatus == this.checked) {
                        $(this).closest('table tbody tr').removeClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }

                    if (this.checked) {
                        $(this).closest('table tbody tr').addClass('table-select')
                        $(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
                        self.setState({
                            totalData: $('.table-body input[type=checkbox]:checked').length
                        })
                    }
                })

                $('table tbody tr td div:first-child input[type=checkbox]').change(function () {
                    $(this).closest('tr').toggleClass("table-select", this.checked)
                })

                $(".alertdel").click(function () {
                    let id = $(this).attr("id")
                    self.toggleDel()
                    self.setState({
                        idDel: id
                    })
                })

                $('.btn-edit').on('click', function () {
                    let href = $(this).attr('data-href')
                    let as = $(this).attr('data-as')

                    self.actionEdit(href, as)
                })
                $(".btn-sort-banner").bind('click', ({ currentTarget }) => {
                    self.modalOrderBanner($(currentTarget).data('id'))
                })
                $(".btn-qr").bind('click', ({ currentTarget }) => {
                    self.modalShowQR($(currentTarget).data('id'))
                })
            }
        })
    }

    modalOrderBanner(id) {
        getService({
            url: `/channel/banners/${id}`,
            success: ({ data }) => {
                this.setState({
                    banners: data,
                    selectedId: id
                })
                this.toggleModalBanner()
            }
        })
    }

    toggleModalBanner() {
        const { modalBanner } = this.state
        this.setState({
            modalBanner: !modalBanner
        })
    }

    modalShowQR(id) {
        getService({
            url: `/channel/qr/${id}`,
            success: ({ data }) => {
                try {
                    let channelId = data.xid
                    let userId = this.state.user.user.xid
                    let urlContentAccesed = ''
                    if (typeof window !== 'undefined') {
                        urlContentAccesed = window.location.hostname;
                    }
                    let url = `https://${urlContentAccesed}/?type=channel-invitation&id=${channelId}&referral=${userId}&invitationType=qr`

                    data.url = url
                    this.setState({
                        qr: data,
                        selectedId: id
                    })
                    this.toggleModalQR()
                } catch (error) {
                    console.log(error)
                }
            }
        })
    }

    toggleModalQR() {

        const { modalQRCode } = this.state
        this.setState({
            modalQRCode: !modalQRCode
        })
    }

    downloadQR() {
        const canvas = document.getElementById("qrcode")
        const pngUrl = canvas
            .toDataURL("image/png")
            .replace("image/png", "image/octet-stream")
        let downloadLink = document.createElement("a")
        downloadLink.href = pngUrl
        downloadLink.download = this.state.qr.channel_name + " - Channel - QRCode.png"
        document.body.appendChild(downloadLink)
        downloadLink.click()
        document.body.removeChild(downloadLink)
    }

    invitationLink() {
        this.setState({ linkChecker: 1 })
        setTimeout(() => { this.setState({ linkChecker: 0 }) }, 1500)
        var textField = document.createElement('textarea')
        textField.innerText = "Youre invited to join Channel '" + this.state.qr.channel_name + "' in D2D Apps. - Invitation Link : " + this.state.qr.url
        document.body.appendChild(textField)
        textField.select()
        document.execCommand('copy')
        textField.remove()
    }

    componentWillUnmount() {
        $('#table-channel').DataTable().destroy(true)
    }

    actionEdit(href, as) {
        Router.push(href, as)
    }

    toggleDel() {
        this.setState({
            modalDel: !this.state.modalDel
        })
    }

    toggleDelAll() {
        this.setState({
            modalDelAll: !this.state.modalDelAll
        })
    }

    actionDelete() {
        let self = this

        this.toggleDel()

        axios({
            url: goaConfig.BASE_API_URL + '/channel/delete',
            method: 'POST',
            data: {
                id: self.state.idDel
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let table = $('#table-channel')
                    .DataTable()
                table.clear().draw()
                table.state.clear()

                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }

    actionDeleteAll() {
        let self = this

        let deldata = []
        let values = $('.table-body input[type=checkbox]:checked').map(function () {
            deldata = $(this).attr("id")
            return deldata
        }).get()

        this.toggleDelAll()

        axios({
            url: goaConfig.BASE_API_URL + '/channel/multidelete',
            method: 'POST',
            data: {
                totaldata: self.state.totalData,
                item: JSON.stringify(values)
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {
                let table = $('#table-channel')
                    .DataTable()
                table.clear().draw()
                table.state.clear()

                self.props.flashInfo('success', response.data.message)
            } else {
                self.props.flashInfo('error', response.data.message)
            }
        })
    }

    submitBanner() {
        console.log(this.props.bannerOrder, "BANNER_ORDER")
        postService({
            url: `/channel/banners/${this.state.selectedId}`,
            data: {
                banners: this.props.bannerOrder
            },
            success: () => {
                this.props.successToast('Banner position is updated.')
                this.setState({
                    modalBanner: false
                })
            }
        })
    }

    render() {
        return (
            <div>
                <div className="table-responsive">
                    <table id="table-channel" className="table table-bordered responsive nowrap" width="100%">
                        <thead>
                            <tr>
                                <th className="no-sort" style={{ width: "10px" }}></th>
                                <th>Created</th>
                                <th>Country</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Status</th>
                                <th className="no-sort" style={{ width: "100px" }}>Action</th>
                            </tr>
                        </thead>
                        <tbody className="table-body"></tbody>
                        <tfoot>
                            <tr>
                                <td style={{ width: "10px" }} className="text-center">
                                    <label className="checkbox checkbox-lg checkbox-inline">
                                        <input type="checkbox" id="titleCheck" />
                                        <span></span>
                                    </label>
                                </td>
                                <td colSpan="5" className="d-table-cell">
                                    <button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>

                <Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
                    <div className="modal-body text-center py-10 px-10">
                        <i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
                        <h4 className="text-danger mb-10">Delete Confirmation</h4>
                        <p className="mb-10 mx-10">Are you sure you want to delete all this data? Please confirm your choice.</p>
                        <button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
                        <button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
                    </div>
                </Modal>
                <Modal style={{ maxWidth: "1200px" }} isOpen={this.state.modalBanner} toggle={this.toggleModalBanner} fade={true} centered={true}>
                    <div className="modal-body py-10 px-10">
                        <div className="row">
                            <div className="col-sm-12">
                                <SortableList banners={this.state.banners} />
                            </div>
                        </div>
                        {
                            this.state.banners.length > 0 ? (
                                <div className="row">
                                    <div className="col-sm-6">
                                        <button type="button" className="btn btn-default px-15" onClick={this.toggleModalBanner}><i className="fas fa-times"></i> Cancel</button>
                                    </div>
                                    <div className="col-sm-6 text-right">
                                        <button type="button" onClick={this.submitBanner} className="btn btn-success px-15"><i className="far fa-save"></i> Save</button>
                                    </div>
                                </div>
                            ) : (
                                <div className="col-sm-12 text-center">
                                    <button type="button" className="btn btn-default px-15" onClick={this.toggleModalBanner}><i className="fas fa-times"></i> Close</button>
                                </div>
                            )
                        }
                    </div>
                </Modal>
                <Modal style={{ maxWidth: "600px" }} isOpen={this.state.modalQRCode} toggle={this.toggleModalQR} fade={true} centered={true}>
                    <div className="modal-body py-10 px-10">
                        <div className="row justify-content-end form-group">
                            <button type="button" className="btn btn-default px-3 py-2" onClick={this.toggleModalQR} style={{ marginTop: "-15px" }}><i className="fas fa-times" style={{ verticalAlign: "middle" }}></i></button>
                            <div className="col-sm-12 text-center">
                                <QRCode qr={this.state.qr} />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 text-center mt-3">
                                <button type="button" className="btn btn-primary px-15 mr-5" onClick={this.downloadQR}><i className="fas fa-download"></i>Download QR Code</button>
                                <button type="button" className={this.state.linkChecker == 0 ? "btn btn-primary px-15" : "btn btn-success px-15"} onClick={this.invitationLink}>{this.state.linkChecker == 0 ? <i className="fas fa-copy"></i> : <i className="fas fa-check"></i>}Copy Invitation Link</button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    bannerOrder: state.channel.banners
})

const mapDispatchToProps = (dispatch) => ({
    successToast: message => dispatch(successToast(message))
})
export default connect(mapStateToProps, mapDispatchToProps)(TableChannel)