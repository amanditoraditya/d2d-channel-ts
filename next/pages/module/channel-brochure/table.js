import React from 'react'
import Router from 'next/router'
import { connect } from 'react-redux'
import Reactdom from 'react-dom'
import _lo from 'lodash'
import $ from 'jquery'
import { Modal } from 'reactstrap'
import 'datatables.net-bs4'
import SwitchTableForm from '../../../components/forms/general-form/switch-table-form'
import { errorToast } from '../../../redux/reducer/general'

const goaConfig = require('../../../config')

class TableBrochure extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			channelid: this.props.channelid,
			totalData: 0,
			idDel: 0,
			modalDel: false,
			modalRestore: false,
			modalDelAll: false,
		}

		this.actionEdit = this.actionEdit.bind(this)
		this.toggleRestore = this.toggleRestore.bind(this)
		this.toggleDel = this.toggleDel.bind(this)
		this.toggleDelAll = this.toggleDelAll.bind(this)
		this.actionRestore = this.actionRestore.bind(this)
		this.actionDelete = this.actionDelete.bind(this)
		this.actionDeleteAll = this.actionDeleteAll.bind(this)
	}

	async componentDidMount() {
		let self = this
		let is_admin = this.props.isAdmin
		let columns = [
			{
				data: null,
				orderable: false,
				searchable: false,
				render: (data) => {
					return data.is_archived == 0 ?
					`<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`: 
					`<div class='text-center'>
					<label class='checkbox checkbox-lg checkbox-inline'>
						<input disabled type='checkbox' id='${data.encrypted}' />
						<span></span>
					</label>
					</div>`
				}
			},
			{
				data: "id",
				searchable: false,
				orderable: false,
			},
			{
				data: "title",
				orderable: true,
				render: (data, row, rowData) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:200px;
					${rowData.is_archived == 1 && 'color:white !important'}">${data}</div>`
				}
			},
			{
				data: "channel_name",
				searchable: false,
				orderable: true,
				render: (data, row, rowData) => {
					return `<div style="overflow:hidden;text-overflow:ellipsis;max-width:200px;
					${rowData.is_archived == 1 && 'color:white !important'}">${data}</div>`
				}
			},
			{
				data: "cover",
				searchable: false,
				orderable: false,
				render: (data, row, rowData) => {
					let image = ''
                    if (data != null || data != '') {
                        image = `<img class="img-responsive img-thumbnail" src="${rowData.cover != '' ? data : '../../../static/one/media/svg/icons/Files/Pictures1.svg' }" 
						style="width:100px;height:100px;${rowData.is_archived == 1 && 'color:white !important'}"/>`
                    } 
                    return image
				}
			},
			{
				data: "is_published",
				searchable: false,
				orderable: true,
				render: (data) => {
					return data == 1 ? 'Published' : 'Unpublished'
				}
			},
			{
				data: null,
				orderable: false,
				searchable: false,
				className: 'text-center',
				render: (data) => {
					return data.is_archived == 0 ?
					`<div class='text-center'>
						<div class='btn-group btn-group-sm'>
							<a href='#' data-href='/module/channel-brochure/edit?id=${data.encrypted}' data-as='/channel-brochure/edit/${data.encrypted}' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>
							<a href='#' class='btn btn-sm btn-danger alertdel' id='${data.encrypted}' title='Delete'><i class='far fa-trash-alt'></i></a>
						</div>
					</div>` :
					`<div class='text-center'>
						<div class='btn-group btn-group-sm'>
							<a href='#' class='btn btn-sm btn-secondary alertrestore' id='${data.encrypted}' title='Restore'><i class='fa fa-undo'></i></a>
						</div>
					</div>`
				}
			}
		]

		let indexPublished = 5
		if (!is_admin) {
			columns = columns.filter(function (row) {
				return row.data != 'channel_name';
			});
            indexPublished = 4
		}

		await self.setState({
			is_admin
		})

		$('#table-channel-brochure').DataTable({
			order: [[1, 'DESC']],
			autoWidth: false,
			columnDefs: [
				{
					targets: [1],
					className: "d-none"
				},
				{
					targets: indexPublished,
					createdCell: (td, cellData, rowData, row, col) => Reactdom.render(
						<SwitchTableForm
							onFailed={(message) => { this.props.errorToast(message) }}
							checked={cellData == 1 ? true : false}
							disabled={rowData.is_archived == 1 ? true : false}
							id={rowData.encrypted}
							url="/channel-brochure/update-status"
							trueSwitchLabel='Published'
							falseSwitchLabel='Unpublished'
						/>,
						td)
				}
			],
			width: '10px',
			columns,
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 10,
			lengthMenu: [
				[10, 30, 50, 100, -1],
				[10, 30, 50, 100, 'All']
			],
			ajax: {
				type: 'post',
				url: goaConfig.BASE_API_URL + '/channel-brochure/datatable',
				data: {
					channelid: this.props.channelid
				},
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
			"drawCallback": function (settings) {
				$("#titleCheck").click(function () {
					let checkedStatus = this.checked;
					$("table tbody tr td div:first-child input[type=checkbox]").each(function () {
						this.checked = checkedStatus
						if (checkedStatus == this.checked) {
							$(this).closest('table tbody tr').removeClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
						if (this.checked) {
							$(this).closest('table tbody tr').addClass('table-select')
							$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
							self.setState({
								totalData: $('.table-body input[type=checkbox]:checked').length
							})
						}
					})
				})

				$('table tbody tr td div:first-child input[type=checkbox]').on('click', function () {
					let checkedStatus = this.checked
					this.checked = checkedStatus

					if (checkedStatus == this.checked) {
						$(this).closest('table tbody tr').removeClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}

					if (this.checked) {
						$(this).closest('table tbody tr').addClass('table-select')
						$(this).closest('table tbody tr').find('input:hidden').attr('disabled', !this.checked)
						self.setState({
							totalData: $('.table-body input[type=checkbox]:checked').length
						})
					}
				})

				$('table tbody tr td div:first-child input[type=checkbox]').change(function () {
					$(this).closest('tr').toggleClass("table-select", this.checked)
				})

				$(".alertdel").click(function () {
					let id = $(this).attr("id")
					self.toggleDel()
					self.setState({
						idDel: id
					})
				})

				$(".alertrestore").click(function () {
					let id = $(this).attr("id")
					self.toggleRestore()
					self.setState({
						idRestore: id
					})
				})

				$('.btn-edit').on('click', function () {
					let href = $(this).attr('data-href')
					let as = $(this).attr('data-as')

					self.actionEdit(href, as)
				})
			},
			rowCallback: (row, data) => {
				if (data.is_archived == 1) {
					$(row).addClass('deleted-row')
				}
			}
		})
	}

	componentWillUnmount() {
		$('#table-channel-brochure').DataTable().destroy(true)
	}

	actionEdit(href, as) {
		Router.push(href, as)
	}

	toggleDel() {
		this.setState({
			modalDel: !this.state.modalDel
		})
	}

	toggleRestore() {
		this.setState({
			modalRestore: !this.state.modalRestore
		})
	}

	toggleDelAll() {
		this.setState({
			modalDelAll: !this.state.modalDelAll
		})
	}

	actionDelete() {
		let self = this

		this.toggleDel()

		axios({
			url: goaConfig.BASE_API_URL + '/channel-brochure/delete',
			method: 'POST',
			data: {
				id: self.state.idDel
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('#table-channel-brochure')
					.DataTable()
				table.clear().draw()
				table.state.clear()

				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}

	actionRestore() {
		let self = this

		this.toggleRestore()

		axios({
			url: goaConfig.BASE_API_URL + '/channel-brochure/restore',
			method: 'POST',
			data: {
				id: self.state.idRestore
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '200') {
				let table = $('#table-channel-brochure')
					.DataTable()
				table.clear().draw()
				table.state.clear()

				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}

	actionDeleteAll() {
		let self = this

		let deldata = []
		let values = $('.table-body input[type=checkbox]:checked').map(function () {
			deldata = $(this).attr("id")
			return deldata
		}).get()

		this.toggleDelAll()

		axios({
			url: goaConfig.BASE_API_URL + '/channel-brochure/multidelete',
			method: 'POST',
			data: {
				totaldata: self.state.totalData,
				item: JSON.stringify(values)
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let table = $('#table-channel-brochure')
					.DataTable()
				table.clear().draw()
				table.state.clear()

				self.props.flashInfo('success', response.data.message)
			} else {
				self.props.flashInfo('error', response.data.message)
			}
		})
	}

	render() {
		return (
			<div>
				<div className="table-responsive">
					<table id="table-channel-brochure" className="table table-bordered responsive nowrap">
						<thead>
							<tr>
								<th className="no-sort" style={{ width: "10px" }}></th>
								<th style={{ width: "30px" }}>Id</th>
								<th>Title</th>
								{ this.state.is_admin && <th>Channel</th> }
								<th style={{ width: "150px" }}>Cover</th>
								<th style={{ width: "225px" }}>Status</th>
								<th className="no-sort" style={{ width: "100px" }}>Action</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
						<tfoot>
							<tr>
								<td style={{ width: "10px" }} className="text-center">
									<label className="checkbox checkbox-lg checkbox-inline">
										<input type="checkbox" id="titleCheck" />
										<span></span>
									</label>
								</td>
								<td colSpan={this.state.is_admin ? "5" : "4"}  className="d-table-cell">
									<button className="btn btn-sm btn-danger" type="button" onClick={this.toggleDelAll}><i className="far fa-trash-alt"></i> Delete Selected</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>

				<Modal isOpen={this.state.modalDel} toggle={this.toggleDel} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure want to delete this data ?</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDelete}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDel}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>

				<Modal isOpen={this.state.modalRestore} toggle={this.toggleRestore} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Restore Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure want to restore this data ?</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionRestore}><i className="fa fa-undo"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleRestore}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>

				<Modal isOpen={this.state.modalDelAll} toggle={this.toggleDelAll} fade={true} centered={true}>
					<div className="modal-body text-center py-10 px-10">
						<i className="flaticon-warning-sign icon-5x text-danger d-inline-block"></i>
						<h4 className="text-danger mb-10">Delete Confirmation</h4>
						<p className="mb-10 mx-10">Are you sure want to delete this data ?</p>
						<button type="button" className="btn btn-danger px-15 mr-10" onClick={this.actionDeleteAll}><i className="far fa-trash-alt"></i> Yes</button>
						<button type="button" className="btn btn-default px-15" onClick={this.toggleDelAll}><i className="fas fa-sign-out-alt"></i> No</button>
					</div>
				</Modal>
			</div>
		)
	}
}
const mapDispatchToProps = dispatch => ({
	errorToast: data => dispatch(errorToast(data)),
})

const mapStateToProps = state => ({
    isAdmin: state.auth.is_admin,
})

export default connect(mapStateToProps, mapDispatchToProps)(TableBrochure)