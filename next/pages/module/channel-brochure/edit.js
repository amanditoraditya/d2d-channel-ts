import React from 'react'
import { withRouter } from 'next/router'
import Wrapper from '../../../components/wrapper'
import BrochureForm from '../../../components/brochure/form'
import loadjs from 'loadjs'
import _lo from 'lodash'

class CreateBrochureForm extends React.Component {

	componentDidMount() {
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} onCard={true} title="Channel Brochure" parent="channel" selected="channel-brochure" module="ChannelBrochure" permission="edit" headerCard="Update Brochure">
					<BrochureForm id={this.props.router.query.id} />
				</Wrapper>
			</div>
		)
	}
}

export default withRouter(CreateBrochureForm)