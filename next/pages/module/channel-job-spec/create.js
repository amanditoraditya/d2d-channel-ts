import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField, LongTextField} from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/channel-job-spec-create'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
    
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			role: '',
            info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.handleSubmit 				 = this.handleSubmit.bind(this)
		this.flashInfo 					 = this.flashInfo.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
        const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
        
		axios({
			url: goaConfig.BASE_API_URL + '/channel-job-spec/create/' + self.state.id,
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/channel-job-spec/index?id=' + response.data.data, '/channel-job-spec/index/' + response.data.data )
			} else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
				self.flashInfoErrorMultiLine(response.data.data._errorMessages)
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Channel Job" parent="channel" selected="channel-job" module="ChannelJobSpec" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Job Spec</h3>
										</div>
										<div className="card-toolbar">
											<Link href={'/channel-job-spec/index?id=' + this.state.id} as={'/channel-job-spec/index/' + this.state.id} passHref>
												<a href={'/channel-job-spec/index?id=' + this.state.id} className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										<AutoForm schema={Schema} onSubmit={this.handleSubmit} onChange={(key, value) => {}}>
											<div className="mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
                                                <div className="row mt-5">
                                                    <div className="col-md-12">
                                                        <AutoField name="title" />
                                                    </div>
                                                    <div className="col-md-12">
														<AutoField name="description" />
													</div>
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
				<style>{`
      			  	.react-date-picker__wrapper, .react-time-picker__wrapper {
						border-style: none
					}
					.react-date-picker__button, 
					.react-time-picker__clock-button, 
					.react-time-picker__button {
						padding:0 0 0 6px !important;
					}
					.table {
						position: relative;
						z-index: 10000;
					}
					.menu-dropdown {
						position: relative;
						z-index: 10000;
					}
      			`}</style>
			</div>
		)
	}
}