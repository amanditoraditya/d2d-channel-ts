import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/menu'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
    static async getInitialProps({ req, res, query }) {
        let id = query ? query.id : '0'

        return { id }
    }

    constructor(props) {
        super(props)

        this.state = {
            login: 0,
            id: this.props.id,
            menu: null,
            statusEnum: [
                {
                    label: 'Active',
                    value: 1
                }, {
                    label: 'Disabled',
                    value: 0
                }
            ],
            tfEnum: [
                {
                    label: 'Yes',
                    value: 1
                }, {
                    label: 'No',
                    value: 0
                }
            ],
            info: false,
            infoStatus: '',
            infoMessage: '',
            icon: '',
            icon_image: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.flashInfo = this.flashInfo.bind(this)
        this.encryptString = this.encryptString.bind(this)
        this.changeImages = this.changeImages.bind(this)
        this.removeImage = this.removeImage.bind(this)
    }

    componentDidMount() {
        let self = this

        if (Auth.loggedIn()) {
            this.setState({
                login: 1
            })
        }

        loadjs([
            '../../../../static/one/js/scripts.bundle.js',
            '../../../../static/one/js/pages/widgets.js',
        ])

        axios({
            url: goaConfig.BASE_API_URL + '/menu/edit',
            method: 'POST',
            data: {
                id: self.state.id
            },
            timeout: goaConfig.TIMEOUT
        }).then(function (response) {
            if (response.data.code == '2000') {

                let menu = response.data.data
                let icon_image = menu.icon == null || menu.icon == '' ? '' : menu.icon

                self.setState({
                    menu: response.data.data,
                    icon_image
                })
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleSubmit(data) {
        let self = this
        $('.error-bar').delay(1000).show()

        data.is_published = self.state.menu.is_published
        data.description = self.state.menu.description
        data.status = self.state.menu.status

        data.icon = self.state.icon
        data.remove_image = self.state.remove_image

        const data_form = new FormData()
        data_form.append('data', JSON.stringify(data))
        data_form.append('icon', self.state.icon)

        axios({
            url: goaConfig.BASE_API_URL + '/menu/update/' + self.state.id,
            method: 'POST',
            data: data_form,
            timeout: goaConfig.TIMEOUT
        }).then(async function (response) {
            if (response.data.code == '2000') {
                await self.flashInfo('success', response.data.message)
                Router.push('/module/menu/index', '/menu')
            } else {
                self.flashInfo('error', response.data.message)
            }
        })
    }

    handleKeyDown (e) {
        if (e.key === 'Enter') {
          e.preventDefault();
          e.stopPropagation();
        }
    }

    changeImages(e) {
        let self = this

        let icon = this.state.icon
        self.setState({
            icon: e.target.files[0]
        })

        this.setState({
            icon: e.target.files[0],
            icon_image: URL.createObjectURL(e.target.files[0])
        })
    }

    removeImage(e) {
        e.preventDefault()
        $('#customFile').val(null)
        this.setState({
            remove_image: true,
            icon_image: '',
            icon: null
        })
    }

    async flashInfo(status, message) {
        let data = {
            info: true,
            infoStatus: '',
            infoMessage: message
        }
        $('.error-bar').delay(1000).show()

        if (status == 'success') {
            data.infoStatus = 'alert alert-solid alert-info error-bar'
        } else {
            data.infoStatus = 'alert alert-solid alert-danger error-bar'
        }
        this.setState(data)

        await $('.error-bar').delay(2000).fadeOut().promise()

        // clear message
        data = {
            info: false,
            infoStatus: '',
            infoMessage: ''
        }
        this.setState(data)
    }

    encryptString(string) {
        let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
        let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
        crypted += cipher.final('hex')

        return crypted
    }

    render() {
        return (
            <div>
                <Wrapper {...this.props} title="Update Menu" parent="admin-page" selected="menu" module="Menu" permission="update">
                    {this.state.login == 1 &&
                        <div className="d-flex flex-column-fluid">
                            <div className="container-fluid">
                                <div className="card card-custom gutter-b">
                                    <div className="card-header flex-wrap py-3">
                                        <div className="card-title">
                                            <h3 className="card-label">Update Menu</h3>
                                        </div>
                                        <div className="card-toolbar">
                                            <Link href="/module/menu/index" as="/menu" passHref>
                                                <a href="/menu" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                                            </Link>
                                        </div>
                                    </div>

                                    <div className="card-body">
                                        {this.state.info &&
                                            <div className={this.state.infoStatus}>{this.state.infoMessage}</div>
                                        }

                                        {this.state.menu &&
                                            <AutoForm 
                                                schema={Schema} 
                                                onSubmit={this.handleSubmit}
                                                onKeyDown={this.handleKeyDown}
                                            >
                                                <div className="row mg-b-15">
                                                    <div className="col-md-12 text-capitalize">
                                                        <ErrorsField />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="name" initialValue={this.state.menu.name} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="description" value={this.state.menu.description} onChange={e => this.setState(prevState => ({ menu: { ...prevState.menu, description: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="link" initialValue={this.state.menu.link} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="target" initialValue={this.state.menu.target} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="parent_id" value={this.state.menu.parent_id} onChange={e => this.setState(prevState => ({ menu: { ...prevState.menu, parent_id: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="position" value={this.state.menu.position} onChange={e => this.setState(prevState => ({ menu: { ...prevState.menu, position: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="status" value={this.state.menu.status} options={this.state.statusEnum} placeholder="Select Status" onChange={e => this.setState(prevState => ({ menu: { ...prevState.menu, status: e } }))} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <AutoField name="is_published" value={this.state.menu.is_published} options={this.state.tfEnum} placeholder="Select Status" onChange={e => this.setState(prevState => ({ menu: { ...prevState.menu, is_published: e } }))} />
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="row form-group" key="0">
                                                            <div className="col-md-2 pr-0">
                                                                <img src={this.state.icon_image} className="img-fluid img-thumbnail" />
                                                            </div>
                                                            <div className="col-md-1">
                                                                <button className="btn py-0 px-0" onClick={this.removeImage}>
                                                                    <span className="navi-icon">
                                                                        <i className="flaticon2-cross icon-nm"></i>
                                                                    </span>
                                                                </button>
                                                            </div>

                                                            <div className="col-md-9">
                                                                <label className="text-capitalize">Icon</label>
                                                                <div className="custom-file">
                                                                    <input type="file" name="icon" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeImages(e)} />
                                                                    <label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.icon == null ? 'Choose File...' : this.state.icon.name}</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="form-layout-footer float-right">
                                                    <SubmitField value="Submit" />
                                                </div>
                                            </AutoForm>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </Wrapper>
            </div>
        )
    }
}