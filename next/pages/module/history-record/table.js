import React from 'react'
import $ from 'jquery'
import 'datatables.net-bs4'

const goaConfig = require('../../../config')
let table
export default class extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		table = $('#table-record-log').DataTable({
			order: [[0, 'DESC']],
			columns: [
				{
					data: "created_at",
					searchable: false,
					render: (data) => {
						return moment(data).format('DD-MMMM-YYYY HH:mm')
					}
				},
				{
					data: "table_name",
					orderable: false,
					render: (data) => {
						return data.replace(/_/g, ' ').toUpperCase()
					}
				},
				{
					data: "full_name",
					name: "user_channels.email",
					orderable: false,
					render: (data, row, rowData) => {
						return `${data} (${rowData.email})`
					}
				},
				{
					data: "transaction_type",
					orderable: false,
					searchable: false
				},
				{
					data: null,
					orderable: false,
					searchable: false,
					className: 'text-center',
					render: (data) => {
						return `
							<div class="btn-group btn-group-xs">
								<button class="btn btn-xs btn-primary btn-detail"><i class="fa fa-chevron-up pr-0"></i></button>
							</div>
						`
					}
				}
			],
			stateSave: true,
			serverSide: true,
			processing: true,
			pageLength: 10,
			lengthMenu: [
				[10, 30, 50, 100, -1],
				[10, 30, 50, 100, 'All']
			],
			ajax: {
				url: `${goaConfig.BASE_API_URL}/history-record/datatable`,
			},
			responsive: true,
			language: {
				paginate: {
					first: '<i class="ki ki-double-arrow-back"></i>',
					last: '<i class="ki ki-double-arrow-next"></i>',
					next: '<i class="ki ki-arrow-next"></i>',
					previous: '<i class="ki ki-arrow-back"></i>'
				}
			},
		})
		$(document).on('click', '.btn-detail', ({ currentTarget }) => {
			const rowElement = $(currentTarget).closest('tr')
			const rowDatatable = table.row(rowElement)
			if (rowDatatable.child.isShown()) {
				$(currentTarget).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
				$(currentTarget).removeClass('btn-danger').addClass('btn-primary')
				rowDatatable.child.hide()
				rowElement.removeClass('shown')
			} else {
				const rowData = rowDatatable.data()
				$(currentTarget).find('i').addClass('fa-chevron-down').removeClass('fa-chevron-up')
				// $(currentTarget).addClass('btn-danger').removeClass('btn-primary')
				// rowDatatable.child(`
				// 	<div class="row">
				// 		<div class="col-sm-6">
				// 			<label>Last Payload</label>
				// 			<pre>${JSON.stringify(JSON.parse(rowData.last_payload), undefined, 4)}</pre>
				// 		</div>
				// 		<div class="col-sm-6">
				// 			<label>Latest Payload</label>
				// 			<pre>${JSON.stringify(JSON.parse(rowData.latest_payload), undefined, 4)}</pre>
				// 		</div>
				// 	</div>
				// `).show()

				//#region last payload reformat
				let last_payload = JSON.parse(rowData.last_payload)
				let content_last = ''
				for (const property in last_payload) {
					// change underscore into space
					let label = property.replace(/_/g, ' ');
					// capitalize each word
					let words = label.split(" ");
					for (let i = 0; i < words.length; i++) {
						words[i] = words[i][0].toUpperCase() + words[i].substr(1);
					}
					words = words.join(" ");					

					content_last += `
						<div class="row my-2">
							<div class="col-md-3">
							${words}
							</div>
							<div class="col-md-9 text-wrap">
							: ${last_payload[property]}
							</div>
						</div>
					`
				}
				//#endregion

				//#region latest payload reformat
				let latest_payload = JSON.parse(rowData.latest_payload)
				let content_latest = ''
				for (const property in latest_payload) {
					// change underscore into space
					let label = property.replace(/_/g, ' ');
					// capitalize each word
					let words = label.split(" ");
					for (let i = 0; i < words.length; i++) {
						words[i] = words[i][0].toUpperCase() + words[i].substr(1);
					}
					words = words.join(" ");					

					content_latest += `
						<div class="row my-2">
							<div class="col-md-3">
							${words}
							</div>
							<div class="col-md-9 text-wrap">
							: ${latest_payload[property]}
							</div>
						</div>
					`
				}
				//#endregion

				rowDatatable.child(`
					<div class="d-flex justify-content-center row mt-3 px-2">
						<div class="col-md-5">
							<label>Last Payload</label>
							<div class="border rounded px-5 py-4 bg-light">${content_last}</div>
						</div>
						<div class="col-md-2 d-flex justify-content-center align-items-center"><i class="fas fa-arrow-right fa-5x"></i></div>
						<div class="col-md-5">
							<label>Latest Payload</label>
							<div class="border rounded px-5 py-4 bg-light">${content_latest}</div>
						</div>
					</div>
				`).show()
				rowElement.addClass('shown')
			}
		})
	}

	componentWillUnmount() {
		$('.data-table-wrapper').find('table').DataTable().destroy(true)
	}
	render() {
		return (
			<div>
				<div className="table-responsive">
					<table id="table-record-log" className="table table-bordered responsive nowrap" width="100%">
						<thead>
							<tr>
								<th className="text-center">Date</th>
								<th className="text-center">Table Name</th>
								<th className="text-center">User</th>
								<th className="text-center">Transaction Type</th>
								<th className="text-center">Action</th>
							</tr>
						</thead>
						<tbody className="table-body"></tbody>
					</table>
				</div>
			</div>
		)
	}
}