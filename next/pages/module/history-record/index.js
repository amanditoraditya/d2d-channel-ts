import React from 'react'
import $ from 'jquery'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import Table from './table'
import loadjs from 'loadjs'

const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			login: 0
		}
	}

	componentDidMount() {
		if (Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
			'../../../../static/one/plugins/custom/datatables/datatables.bundle.js',
		])
	}

	render() {
		return (
			<Wrapper {...this.props} title="History Record" parent="admin-page" selected="history-record" module="HistoryRecord" permission="read">
				{this.state.login == 1 &&
					<div className="d-flex flex-column-fluid">
						<div className="container-fluid">
							<div className="card card-custom gutter-b">
								<div className="card-header flex-wrap py-3">
									<div className="card-title">
										<h3 className="card-label">History Record</h3>
									</div>
								</div>

								<div className="card-body">
									{this.state.info &&
										<div className={this.state.infoStatus}>{this.state.infoMessage}</div>
									}

									<Table />
								</div>
							</div>
						</div>
					</div>
				}
			</Wrapper>
		)
	}
}