import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/forum'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}

	constructor(props) {
		super(props)

		this.state = {
			login: 0,
			id: this.props.id,
			channel: null,
			description: null,
			publishEnum: [
				{
					label: 'Not Published',
					value: 0
				},
				{
					label: 'Published',
					value: 1
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
			init_channel_id: null,
			is_published: null,
			init_is_published: null,
			attachment: null,
			init_attachment: null,
			attachment_url: '',
			remove_Attachment: false
		}

		this.changeDescription = this.changeDescription.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleKeyDown = this.handleKeyDown.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}

	async componentDidMount() {
		let self = this

		if (Auth.loggedIn()) {
			await this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])

		axios({
			url: goaConfig.BASE_API_URL + '/forum/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let forum = response.data.data
				self.setState({
					forum,
					description: forum.description,
					channel_id: forum.channel_id,
					is_published: forum.is_published,
					attachment: forum.attachment,
					init_channel_id: forum.channel_id,
					init_is_published: forum.is_published,
					init_attachment: forum.attachment,
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}

	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		data.attachment = self.state.attachment
		data.remove_Attachment = self.state.remove_Attachment

		data.is_published = self.state.forum.is_published;
		data.description = self.state.description != null ? self.state.description.replace("<p>", "<p style='margin-bottom:16px;'>") : ''
		data.channel_id_value = data.channel_id.value

		if (self.state.remove_Attachment == true) {
			data.old_attachment = self.state.init_attachment;
		}

		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		data_form.append('attachment', self.state.attachment)

		axios({
			url: goaConfig.BASE_API_URL + '/forum/update/' + self.state.id,
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				await self.flashInfo('success', response.data.message)
				Router.push('/module/forum/index', '/discussion')
			} else {
				await self.flashInfo('error', response.data.message)
			}
		})
	}

	handleKeyDown(e) {
		if (e.key === 'Enter') {
			e.preventDefault();
			e.stopPropagation();
		}
	}

	changeAttachment(e) {
		let self = this

		self.setState({
			remove_Attachment: true,
			attachment: e.target.files[0]
		})

		let url = '';
		if (_lo.isEmpty(e.target.files[0]) == false) {
			url = URL.createObjectURL(e.target.files[0]);
		}

		this.setState({
			remove_Attachment: true,
			attachment: e.target.files[0],
			attachment_url: url
		})
	}

	changeDescription(e) {
		this.setState({
			description: e
		})
	}

	removeAttachment(e) {
		let self = this

		e.preventDefault()
		self.setState({
			attachment: null,
			remove_Attachment: true,
			attachment_url: ''
		})
	}

	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')

		return crypted
	}

	async flashInfo(status, message) {
		let data = {
			info: true,
			infoStatus: '',
			infoMessage: message
		}
		$('.error-bar').delay(500).show()

		if (status == 'success') {
			data.infoStatus = 'alert alert-solid alert-info error-bar'
		} else {
			data.infoStatus = 'alert alert-solid alert-danger error-bar'
		}
		this.setState(data)

		await $('.error-bar').delay(1000).fadeOut().promise()

		// clear message
		data = {
			info: false,
			infoStatus: '',
			infoMessage: ''
		}
		this.setState(data)
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Discussion" parent="channel" selected="forum" module="Forum" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update Discussion</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/forum/index"}
												as={"/discussion/"} passHref>
												<a href={"/discussion/"} className="btn btn-sm btn-dark">
													<i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>

									<div className="card-body">
										{this.state.info &&
											<div className={this.state.infoStatus}>{this.state.infoMessage}</div>
										}

										{this.state.forum &&
											<AutoForm
												schema={Schema}
												onSubmit={this.handleSubmit}
												onKeyDown={this.handleKeyDown}
												onChange={(key, value) => {
													if (key == 'description') { this.changeDescription(value) }
												}}
											>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													<div className={this.state.is_admin ? 'col-md-12 input-to-front-1' : 'd-none'}>
														<AutoField
															name="channel_id"
															url={"/channel/get-channels"}
															isSearchable={true}
															initialValue={this.state.forum.channels}
															placeholder="Select a Channel"
														/>
													</div>
													<div className="col-md-9">
														<AutoField name="title" initialValue={this.state.forum.title} />
													</div>
													<div className="col-md-3">
														<AutoField name="is_published" value={this.state.forum.is_published}
															onChange={e => this.setState(prevState => ({ forum: { ...prevState.forum, is_published: e } }))}
														/>
													</div>
													<div className="col-md-12">
														<AutoField name="short_description" initialValue={this.state.forum.short_description} />
													</div>
													<div className="col-md-12">
														<AutoField name="description" value={this.state.description}
														/>
													</div>
													<div className="col-md-12">
														<div className="row form-group" key="0">
															<div className="col-md-2 pr-0">
																<label className="card-label">Attachment</label>
															</div>
															<div className="col-md-9">
																<div className="custom-file">
																	<input type="file" name="attachment" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeAttachment(e)} />
																	<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.attachment == null ? 'Choose File...' : this.state.attachment.name}</label>
																</div>
															</div>
															<div className="col-md-1">
																<button className="btn py-0 px-0" onClick={(e) => this.removeAttachment(e)}>
																	<span className="navi-icon">
																		<i className="flaticon2-cross icon-nm"></i>
																	</span>
																</button>
															</div>
														</div>
													</div>
												</div>
												<div className="form-layout-footer float-right">
													<SubmitField value="Submit" />
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}