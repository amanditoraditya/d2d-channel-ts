import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Table from '../forum-comment/table'
import Schema from '../../../scheme/forum'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			publishEnum: [
				{
					label: 'Not Published',
					value: 0
				},
				{
					label: 'Published',
					value: 1
				}
			],
			info: false,
			infoStatus: '',
			infoMessage: '',
			attachment: '',
			attachment_url: '',
		}

        this.changeChannelState = this.changeChannelState.bind(this)
		this.changeIsPublished 	= this.changeIsPublished.bind(this)
		this.flashInfo 			= this.flashInfo.bind(this)
	}
	
	async componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			await this.setState({
				login: 1,
				is_admin: Auth.isAdmin()
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])

        // get detail forum post
		axios({
			url: goaConfig.BASE_API_URL + '/forum/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				let forum = response.data.data
				self.setState({
					forum,
					channel_id: forum.channel_id,
					is_published: forum.is_published,
					attachment: forum.attachment,
					init_channel_id: forum.channel_id,
					init_is_published: forum.is_published,
					init_attachment: forum.attachment,
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }

    changeChannelState(e) {
		this.setState({
			channel_id: e.value
		})
	}

	changeIsPublished(e) {
		this.setState({
			is_published: e
		})
	}

	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Detail Discussion" parent="channel" selected="forum" module="Forum" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Detail Discussion</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/forum/index"} 
												as={"/discussion/"} passHref>
												<a href={"/discussion/"} className="btn btn-sm btn-dark">
												<i className="fa fa-arrow-left"></i> Back</a>
											</Link>							
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.forum &&
											<AutoForm schema={Schema}>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													{ this.state.is_admin &&
														<div className="col-md-12">
                                                            <AutoField 
															name="channel_id" 
															url={"/channel/get-channels"} 
                                                        	value={this.state.forum.channels}
                                                            disabled/>
                                                        </div>
													}
													<div className="col-md-12">
														<AutoField name="title" initialValue={ this.state.forum.title } disabled/>
													</div>
													<div className="col-md-12">
														<AutoField name="short_description" initialValue={ this.state.forum.short_description } disabled/>
													</div>
													<div className="col-md-12">
														<AutoField name="description" value={ this.state.forum.description } 
														onChange={e => this.setState(prevState => ({forum: { ...prevState.forum, description: e}}))}
														disabled	
													/>
													</div>
													<div className="col-md-12">
														<AutoField name="is_published" value={ this.state.forum.is_published }
														onChange={e => this.setState(prevState => ({forum: { ...prevState.forum, is_published: e}}))}
														disabled
													/>	
													</div>
													<div className="col-md-12">
														<div className="row form-group" key="0">
															<div className="col-md-2 pr-0">
																<label className="card-label">Attachment</label>
															</div>
															<div className="col-md-10">
																<div className="custom-file">
																	<input type="file" name="attachment" className="custom-file-input" id={'customFile'} onChange={(e) => this.changeAttachment(e)} disabled />
																	<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.attachment == null ? 'No File Uploaded' : this.state.attachment.name }</label>
																</div>
															</div>
														</div>
													</div>
												</div>
											</AutoForm>
										}
                                        <hr class="rounded"></hr>
                                        {   this.state.forum &&
                                            <Table flashInfo={this.flashInfo} forumpostid={this.state.id} />
                                        }
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}