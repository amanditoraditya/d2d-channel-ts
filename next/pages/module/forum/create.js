import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import _lo from 'lodash'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Schema from '../../../scheme/forum'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			is_published: 1,
			login: 0,
			publishEnum: [
				{
					label: 'Not Published',
					value: 0
				},
				{
					label: 'Published',
					value: 1
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
			attachment: '',
			attachment_url: '',
		}

		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleKeyDown = this.handleKeyDown.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
	}

	async componentDidMount() {
		let self = this
		let is_admin = Auth.isAdmin()
		let channel_id
		let login = 0
		if (Auth.loggedIn()) {
			login = 1
		}
		if (!is_admin) {
			let profile = Auth.getProfile()
			channel_id = {
				value: profile.user.channel_id
			}
		} else {
			channel_id = undefined
		}

		await this.setState({
			login,
			channel_id,
			is_admin
		})

		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
	}

	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()

		data.channel_id_value = data.channel_id.value
		data.attachment = self.state.attachment
		data.description = data.description != null ? data.description.replace("<p>", "<p style='margin-bottom:16px;'>") : ''

		const data_form = new FormData()
		data_form.append('data', JSON.stringify(data))
		data_form.append('attachment', self.state.attachment)

		axios({
			url: goaConfig.BASE_API_URL + '/forum/create',
			method: 'POST',
			data: data_form,
			timeout: goaConfig.TIMEOUT
		}).then(async function (response) {
			if (response.data.code == '2000') {
				await self.flashInfo('success', response.data.message)
				Router.push('/module/forum/index', '/discussion')
			} else {
				await self.flashInfo('error', response.data.message)
			}
		})
	}

	handleKeyDown(e) {
		if (e.key === 'Enter') {
			e.preventDefault();
			e.stopPropagation();
		}
	}

	changeAttachment(e) {
		let self = this

		self.setState({
			attachment: e.target.files[0]
		})

		let url = '';
		if (_lo.isEmpty(e.target.files[0]) == false) {
			url = URL.createObjectURL(e.target.files[0]);
		}

		this.setState({
			attachment: e.target.files[0],
			attachment_url: url
		})
	}

	removeAttachment(e) {
		let self = this

		e.preventDefault()
		self.setState({
			attachment: null,
			remove_Attachment: true,
			attachment_url: ''
		})
	}

	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')

		return crypted
	}

	async flashInfo(status, message) {
		let data = {
			info: true,
			infoStatus: '',
			infoMessage: message
		}
		$('.error-bar').delay(500).show()

		if (status == 'success') {
			data.infoStatus = 'alert alert-solid alert-info error-bar'
		} else {
			data.infoStatus = 'alert alert-solid alert-danger error-bar'
		}
		this.setState(data)

		await $('.error-bar').delay(1000).fadeOut().promise()

		// clear message
		data = {
			info: false,
			infoStatus: '',
			infoMessage: ''
		}
		this.setState(data)
	}

	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Add Discussion" parent="channel" selected="forum" module="Forum" permission="create">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Add Discussion</h3>
										</div>
										<div className="card-toolbar">
											<Link href={"/module/forum/index"}
												as={"/discussion/"} passHref>
												<a href={"/discussion/"} className="btn btn-sm btn-dark">
													<i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>

									<div className="card-body">
										{this.state.info &&
											<div className={this.state.infoStatus}>{this.state.infoMessage}</div>
										}

										<AutoForm
											schema={Schema}
											onSubmit={this.handleSubmit}
											onKeyDown={this.handleKeyDown}
											onChange={(key, value) => { }}
										>
											<div className="row mg-b-15">
												<div className="col-md-12 text-capitalize">
													<ErrorsField />
												</div>
												<div className={this.state.is_admin ? 'col-md-12 input-to-front-1' : 'd-none'}>
													<AutoField
														name="channel_id"
														url={"/channel/get-channels"}
														isSearchable={true}
														options={[{ label: '', value: '' }]}
														initialValue={this.state.channel_id}
														placeholder="Select a Channel"
													/>
												</div>
												{!this.state.is_admin &&
													<AutoField
														hidden="hidden"
														name="channel_id"
														initialValue={{ label: '', value: this.state.channel_id }}
													/>
												}
												<div className="col-md-9">
													<AutoField name="title" />
												</div>
												<div className="col-md-3">
													<AutoField name="is_published" />
												</div>
												<div className="col-md-12">
													<AutoField name="short_description" />
												</div>
												<div className="col-md-12">
													<AutoField name="description" />
												</div>
												<div className="col-md-12">
													<div className="row form-group col-md-12" key="0">
														<div className="col-md-1.5">
															<h4 className="card-label">Attachment</h4>
														</div>
														<div className="col-md-9">
															<div className="custom-file">
																<input type="file" enctype="form-data"
																	name="attachment" className="custom-file-input" id={'customFile'}
																	accept=".doc,.docx,.pdf,.jpg,.jpeg,.png"
																	onChange={(e) => this.changeAttachment(e)}
																/>
																<label className="custom-file-label custom-file-label-primary" htmlFor={'customFile'}>{this.state.attachment == null ? 'Choose File...' : this.state.attachment.name}</label>
															</div>
														</div>
														<div className="col-md-1.5">
															<button className="btn py-0 px-0" onClick={(e) => this.removeAttachment(e)}>
																<span className="navi-icon">
																	<i className="flaticon2-cross icon-nm"></i>
																</span>
															</button>
														</div>
													</div>
												</div>
											</div>
											<div className="form-layout-footer float-right">
												<SubmitField value="Submit" />
											</div>
										</AutoForm>
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}