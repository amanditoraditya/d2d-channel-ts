import React from 'react'
import Link from 'next/link'
import Router from 'next/router'
import $ from 'jquery'
import axios from 'axios'
import crypto from 'crypto'
import Wrapper from '../../../components/wrapper'
import AuthServices from '../../../components/auth'
import { AutoForm, AutoField, ErrorsField, SubmitField, BaseField, TextField, SelectField } from 'uniforms-bootstrap4'
import Select2Field from '../../../components/forms/select'
import Schema from '../../../scheme/user-edit'
import loadjs from 'loadjs'

const goaConfig = require('../../../config')
const Auth = new AuthServices()

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let id = query ? query.id : '0'

		return { id }
	}
	
	constructor(props) {
		super(props)
		
		this.state = {
			login: 0,
			id: this.props.id,
			user: null,
			userType: [
				{
					label: 'Pegawai',
					value: '1'
				},{
					label: 'Management',
					value: '2'
				}
			],
			role: '',
			info: false,
			infoStatus: '',
			infoMessage: '',
		}
		
		this.changeRole = this.changeRole.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.flashInfo = this.flashInfo.bind(this)
		this.encryptString = this.encryptString.bind(this)
	}
	
	componentDidMount() {
		let self = this
		
		if(Auth.loggedIn()) {
			this.setState({
				login: 1
			})
		}
		
		loadjs([
			'../../../../static/one/js/scripts.bundle.js',
			'../../../../static/one/js/pages/widgets.js',
		])
		
		axios({
			url: goaConfig.BASE_API_URL + '/users/edit',
			method: 'POST',
			data: {
				id: self.state.id
			},
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.setState({
					user: response.data.data,
					role: response.data.data.user_role
				})
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
    }
	
	handleSubmit(data) {
		let self = this
		$('.error-bar').delay(1000).show()
		
		data.user_role = this.state.role
		
		axios({
			url: goaConfig.BASE_API_URL + '/users/update/' + self.state.id,
			method: 'POST',
			data: data,
			timeout: goaConfig.TIMEOUT
		}).then(function (response) {
			if (response.data.code == '2000') {
				self.flashInfo('success', response.data.message)
				
				Router.push('/module/users/index', '/users')
			} else {
				self.flashInfo('error', response.data.message)
			}
		})
	}
	
	changeRole(e) {
		this.setState({
			role: e.value
		})
	}
	
	flashInfo(status, message){
		$('.error-bar').delay(1000).show()
		
		if(status == 'success') {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-info error-bar',
				infoMessage: message
			})
		} else {
			this.setState({
				info: true,
				infoStatus: 'alert alert-solid alert-danger error-bar',
				infoMessage: message
			})
		}
		
		$('.error-bar').delay(2000).fadeOut()
	}
	
	encryptString(string) {
		let cipher = crypto.createCipher('aes-256-cbc', goaConfig.APP_KEY)
		let crypted = cipher.update(string.toString(), 'utf-8', 'hex')
		crypted += cipher.final('hex')
		
		return crypted
	}
	
	render() {
		return (
			<div>
				<Wrapper {...this.props} title="Update Pegawai" parent="admin-page" selected="users" module="User" permission="update">
					{this.state.login == 1 &&
						<div className="d-flex flex-column-fluid">
							<div className="container-fluid">
								<div className="card card-custom gutter-b">
									<div className="card-header flex-wrap py-3">
										<div className="card-title">
											<h3 className="card-label">Update User</h3>
										</div>
										<div className="card-toolbar">
											<Link href="/module/users/index" as="/users" passHref>
												<a href="/users" className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
											</Link>
										</div>
									</div>
									
									<div className="card-body">
										{ this.state.info &&
											<div className={ this.state.infoStatus }>{ this.state.infoMessage }</div>
										}
										
										{ this.state.user &&
											<AutoForm schema={Schema} onSubmit={this.handleSubmit}>
												<div className="row mg-b-15">
													<div className="col-md-12 text-capitalize">
														<ErrorsField />
													</div>
													<div className="col-md-6">
														<AutoField name="fullname" initialValue={ this.state.user.fullname } />
													</div>
													<div className="col-md-6">
														<AutoField name="username" initialValue={ this.state.user.username } disabled={true} help="* Username tidak dapat diubah." />
													</div>
													<div className="col-md-6">
														<AutoField name="email" initialValue={ this.state.user.email } />
													</div>
													<div className="col-md-6">
														<TextField type="password" name="password" help="* Kosongkan saja jika tidak diubah." />
													</div>
													<div className="col-md-6">
														<SelectField name="user_type" options={this.state.userType} placeholder={ 'Selected ' + this.state.userType[this.state.user.user_type-1]['label'] } />
													</div>
													<div className="col-md-6">
														<AutoField name="user_role" url="/role/get-role" isSearchable={true} options={[{label: this.state.user.role_title, value: this.state.user.user_role}]} defaultValue={{label: this.state.user.role_title, value: this.state.user.user_role}} placeholder={ 'Selected ' + this.state.user.role_title } onChange={(e) => this.changeRole(e)} />
													</div>
												</div>
												<div className="form-layout-footer float-right">
													<SubmitField value="Submit" />
												</div>
											</AutoForm>
										}
									</div>
								</div>
							</div>
						</div>
					}
				</Wrapper>
			</div>
		)
	}
}