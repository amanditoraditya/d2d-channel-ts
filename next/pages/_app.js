import React from 'react'
import App from 'next/app'
import NextNprogress from 'nextjs-progressbar'
import 'pikaday/css/pikaday.css'
import 'lightpick/css/lightpick.css'
import '../../node_modules/react-checkbox-tree/lib/react-checkbox-tree.css'
import '../public/static/one/css/pages/login/classic/login-4.css'
import "../../node_modules/react-datepicker/dist/react-datepicker.css";
import 'react-confirm-alert/src/react-confirm-alert.css'
import { Provider } from 'react-redux'

import withReduxStore from '../redux/store/with-store'
import './styles/App.css'

class MyApp extends App {
	render() {
		const { Component, pageProps, reduxStore } = this.props
		return (
			<React.Fragment>
				<NextNprogress height="2" options={{ easing: 'ease', speed: 500 }} />
				<Provider store={reduxStore}>
					<Component {...pageProps} />
				</Provider>
			</React.Fragment>
		)
	}
}

// export default MyApp
export default withReduxStore(MyApp)