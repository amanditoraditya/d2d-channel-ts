import React from "react";
import MeetCall from "../components/meetcall";
import Router from 'next/router'
import Crypto from "crypto-js"

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		let meet_conf = null
		try{
			let _knock = query.knock
			let buff = Buffer.from(_knock, 'base64') 
			let text = buff.toString('ascii')
			let _key = text.substring(0, 5)
			let _fknock = text.substring(5)
			const bytes = Crypto.AES.decrypt(_fknock, _key)
			meet_conf = JSON.parse(bytes.toString(Crypto.enc.Utf8))
		}catch(e){}
		return {meet_conf}
	}

	constructor(props) {
		super(props)
	}

	componentDidMount() {
		if(!this.props.meet_conf){
			Router.push({
				pathname: '/unknown'
			})
		}
	}

	render(){
		return <MeetCall {...this.props} />;
	}
}


 