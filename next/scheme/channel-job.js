import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'
import DateTimePickerField from '../components/forms/datetimepicker'

const schema = {
	title: 'Channel Job',
	type: 'object',
	properties: {
		channel_id: { label: 'Channel', uniforms: { component: Select2PaginateField } },
		title: { type: 'string', label: 'Title' },
		registration_url: { type: 'string', label: 'Registration URL' },
		registration_button_caption: { type: 'string', label: 'Registration Button Caption' },
		job_type_id: { label: 'Job Type', uniforms: { component: Select2Field } },
		specialist: { label: 'Job Specialist', uniforms: { component: Select2PaginateField, isMulti: true } },
		is_published: {
			label: 'Status',
			uniforms: {
				component: ToggleSwitchField,
				switchLabel: {
					true: "Published",
					false: "Not Published"
				},
			}
		},
		due_date: { label: 'Due Date', uniforms: { component: DateTimePickerField } },
	},

	required: ['channel_id', 'title', 'job_type_id', 'specialist', 'due_date']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge