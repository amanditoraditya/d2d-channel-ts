import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import Select2FieldNoSearch from '../components/forms/selectnosearch'
import ToggleSwitchField from '../components/forms/toggleswitch'
import Select2PaginateField from '../components/forms/select-paginate'

const schema = {
    title: 'User Channel',
    type: 'object',
    properties: {
        channel_id: {
            label: 'Channel',
            description: 'Channel',
            uniforms: { component: Select2PaginateField }
        },
        first_name: { type: 'string', label: 'First Name' },
        last_name: { type: 'string', label: 'Last Name' },
        email: { type: 'string', format: 'email', label: 'Email' },
        password: { type: 'string', label: 'New Password' },
        channel_roles: {
            label: 'Channel Role',
            isMulti: true,
            uniforms: { component: Select2FieldNoSearch }
        },
        block: {
			label: 'Status', 
			defaultValue: false, 
			uniforms: {component: ToggleSwitchField},
			switchLabel:{
				true : "Active",
				false : "Blocked"
			},
			setting:{
				onColor:"#86d3ff",
                onHandleColor:"#2693e6",
                handleDiameter:30,
                uncheckedIcon:false,
                checkedIcon:false,
                boxShadow:"0px 1px 5px rgba(0, 0, 0, 0.6)",
                activeBoxShadow:"0px 0px 1px 10px rgba(0, 0, 0, 0.2)",
                height:25,
                width:55,
			}
		},
    },
    required: ['first_name', 'last_name', 'email', 'channel_id']
}

function createValidator(schema) {
    const validator = ajv.compile(schema)
    return model => {
        validator(model)
        if (validator.errors && validator.errors.length) {
            throw { details: validator.errors }
        }
    }
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge