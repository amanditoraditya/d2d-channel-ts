import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'Banner',
	type: 'object',
	properties: {
		channel_id: {
			label: 'Channel',
			description: 'Channel',
			uniforms: { component: Select2PaginateField }
		},
		name: { label: 'Title', type: 'string'},
		link: { type: 'string', label: 'Link' },
        is_active: {
			label: 'Published',
			uniforms: {
				component: ToggleSwitchField,
				switchLabel: {
					true: "Published",
					false: "Unpublished"
				},
			}
		},
		position: {
			description: 'Position Starts from 0',
			type: 'integer',
			minimum: 0,
			maximum: 1000
		},
	},
	required: ['channel_id', 'name', 'is_active', 'position']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge