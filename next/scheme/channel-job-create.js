import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import DateTimePickerField from '../components/forms/datetimepicker'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'Channel Job',
	type: 'object',
	properties: {
		channel_id: { label: 'Channel', uniforms: { component: Select2PaginateField } },
		title: { minLength: 1, type: 'string', label: 'Title' },
		job_type_id: { label: 'Job Type', uniforms: { component: Select2Field } },
		job_specialist: { label: 'Job Specialist', uniforms: { component: Select2Field } },
		is_published: { defaultValue: 1, label: 'Status' },
		due_date: { label: 'Due Date', uniforms: { component: DateTimePickerField } },
	},

	required: ['channel_id', 'title', 'job_type_id', 'job_specialist', 'due_date']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge