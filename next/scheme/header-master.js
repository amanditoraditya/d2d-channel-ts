import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'
import DateTimePickerField from '../components/forms/datetimepicker'

const schema = {
	title: 'Header Master',
	type: 'object',
	properties: {
		name: { type: 'string', label: 'Name' },
		type: { label: 'Type', uniforms: { component: Select2Field }},
		input_type: { label: 'Input Type', uniforms: { component: Select2Field }},
		is_archived: {
			label: 'Status',
			uniforms: {
				component: ToggleSwitchField,
				switchLabel: {
					true: "Active",
					false: "Disabled"
				},
			}
		},
	},

	required: ['name', 'is_archived']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge