import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2FieldNoSearch from '../components/forms/selectnosearch'

const schema = {
    title: 'Channel Conference Attendee',
    type: 'object',
    properties: {
        channel_subscriber_id: {
            type: 'integer',
            defaultValue: 0,
            description: 'Channel',
            isMulti: true,
            uniforms: { component: Select2FieldNoSearch }
        },
    },
    required: []
}

function createValidator(schema) {
    const validator = ajv.compile(schema)
    return model => {
        validator(model)
        if (validator.errors && validator.errors.length) {
            throw { details: validator.errors }
        }
    }
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge