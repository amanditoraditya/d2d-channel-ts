import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import DateTimePickerField from '../components/forms/datetimepicker'
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'Channel Job',
	type: 'object',
	properties: {
		channel_id: { label: 'Channel', uniforms: { component: Select2PaginateField }},
		title: {  minLength: 1, type: 'string', label: 'Title'},
		job_type_id: { label: 'Job Type', uniforms: { component: Select2Field }},
		job_specialist: { label: 'Job Specialist', uniforms: { component: Select2Field }},
		is_published: {
			label: 'Status', 
			defaultValue: 1, 
			uniforms: {component: ToggleSwitchField},
			switchLabel:{
				true : "Active",
				false : "Inactive"
			},
			setting:{
				onColor:"#1BC5BD",
				offColor:"#F54A5D",
                handleDiameter:20,
                uncheckedIcon: false,
                checkedIcon: false,
                boxShadow:"0px 1px 5px rgba(0, 0, 0, 0.6)",
                activeBoxShadow:"0px 0px 1px 10px rgba(0, 0, 0, 0.2)",
                height:25,
                width:55,
			}
		},
		due_date: { label: 'Due Date', uniforms: { component: DateTimePickerField }},	
	},
	required: ['channel_id', 'title', 'job_type_id', 'job_specialist', 'due_date']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge