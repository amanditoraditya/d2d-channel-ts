import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2PaginateField from '../components/forms/select-paginate'
import TextEditorField from '../components/forms/texteditor'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'Brochure',
	type: 'object',
	properties: {
		channel_id: {
			label: 'Channel',
			description: 'Channel',
			uniforms: { component: Select2PaginateField }
		},
		title: { 
			label: 'Title', 
			type: 'string'
		},
		description: { 
			type: 'string', 
			label: 'Description', 
			uniforms: {component: TextEditorField}
		},
        is_published: {
			label: 'Status',
			uniforms: {
				component: ToggleSwitchField,
				switchLabel: {
					true: "Published",
					false: "Unpublished"
				},
			}
		},
	},
	required: ['channel_id', 'title', 'description']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge