import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2FieldNoSearch from '../components/forms/selectnosearch'
import DateTimePickerField from '../components/forms/datetimepicker'
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'Channel Conference',
	type: 'object',
	properties: {
		channel_id: { 
			label: 'Channel',
			description: 'Channel', 
			uniforms: { component: Select2PaginateField }
		},
		title: { maxLength: 255, type: 'string', label: 'Title' },
        is_published: {
			label: 'Published',
			uniforms: {
				component: ToggleSwitchField,
				switchLabel: {
					true: "Published",
					false: "Unpublished"
				},
			}
		},
		is_instant: { 
			type: 'integer', 
			defaultValue: 0, 
			description: 'Instant' 
		},
		max_attendees: { 
			label: 'Max Attendance',
			type: 'integer', 
			description: 'Max Attendees',
		},
		saved_tz: { 
			type: 'string', 
			label: 'Default Time Zone',
		},
		curr_tz: { 
			type: 'string', 
			label: 'Current Time Zone',
		},
		start_date: { 
			description: 'Start Date',
			uniforms: { component: DateTimePickerField }
		},
		end_date: { 
			description: 'End Date',
			uniforms: { component: DateTimePickerField }
		},
		registration_until: { 
			description: 'Registration Until',
			uniforms: { component: DateTimePickerField }
		},
		channel_subscriber_id: { 
			type: 'integer', 
			defaultValue: 0, 
			description: 'Channel', 
			isMulti: true, 
			uniforms: { component: Select2FieldNoSearch }
		},
	},
	required: [
		'title', 
		'max_attendees', 
		'channel_id', 
		'start_date', 
		'end_date', 
		'registration_until',
	]
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge