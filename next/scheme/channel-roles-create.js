import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'
import Select2PaginateField from '../components/forms/select-paginate'
import ToggleSwitchField from '../components/forms/toggleswitch'

const schema = {
	title: 'ChannelRoles',
	type: 'object',
	properties: {
		xid: { type: 'string', label: 'xid' },
        name: { type: 'string', minLength: 1, defaultValue: "", label: 'Roles Name' },
		read: { defaultValue: 1, label: 'Read' },
		write: { defaultValue: 1, label: 'Write' },
		update: { defaultValue: 1, label: 'Update' },
		delete2: { defaultValue: 1, label: 'Delete' },
		channel_id: { label: 'Channel', uniforms: { component: Select2PaginateField }},
		type: { label: 'Type', uniforms: { component: Select2Field }},
		menu_id: { label: 'Menu', uniforms: { component: Select2Field }},
		status: {
			label: 'Status', 
			uniforms: {component: ToggleSwitchField},
			switchLabel:{
				true : "Active",
				false : "Disabled"
			},
		},
	},
	required: ['name', 'channel_id']
}

function createValidator(schema) {	
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge