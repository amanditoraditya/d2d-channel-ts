import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import TextEditorField from '../components/forms/texteditor'

const schema = {
	title: 'Channel Job Spec',
	type: 'object',
	properties: {
		title: {  minLength: 1, type: 'string', label: 'Title'},
		description: {  minLength: 1, type: 'string', label: 'Description', uniforms: {component: TextEditorField}},	
	},
	required: ['title','description']
}


function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge