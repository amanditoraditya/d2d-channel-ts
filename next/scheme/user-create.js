import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import Select2Field from '../components/forms/select'

const schema = {
	title: 'User',
	type: 'object',
	properties: {
		fullname: { type: 'string', label: 'Fullname' },
		username: { type: 'string', label: 'Username' },
		email: { type: 'string', label: 'Email' },
		password: { type: 'string', label: 'Password' },
		user_type: { type: 'string', defaultValue: '1', label: 'User Type' },
		user_role: { type: 'string', label: 'User Role', uniforms: { component: Select2Field } }
	},
	required: ['fullname', 'username', 'email', 'password']
}

function createValidator(schema) {
	const validator = ajv.compile(schema)
	return model => {
		validator(model)
		if (validator.errors && validator.errors.length) {
			throw { details: validator.errors }
		}
	}
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge