import Ajv from 'ajv'
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema'
const ajv = new Ajv({ allErrors: true, useDefaults: true })
import TextEditorField from '../components/forms/texteditor'
import ToggleSwitchField from '../components/forms/toggleswitch'
import Select2PaginateField from '../components/forms/select-paginate'

const schema = {
    title: 'Channel',
    type: 'object',
    properties: {
        channel_id: {
            label: 'Channel',
            description: 'Channel',
            uniforms: { component: Select2PaginateField }
        },
        title: { maxLength: 255, type: 'string', label: 'Title' },
        short_description: { type: 'string', label: 'Short Description' },
        description: { type: 'string', label: 'Description', uniforms: { component: TextEditorField } },
        is_published: {
            label: 'Status',
            defaultValue: false,
            uniforms: { component: ToggleSwitchField },
            switchLabel: {
                true: "Published",
                false: "Not Published"
            },
        },
    },
    required: ['channel_id', 'title', 'short_description', 'description']
}

function createValidator(schema) {
    const validator = ajv.compile(schema)
    return model => {
        validator(model)
        if (validator.errors && validator.errors.length) {
            throw { details: validator.errors }
        }
    }
}

const schemaValidator = createValidator(schema)
const bridge = new JSONSchemaBridge(schema, schemaValidator)

export default bridge