export const validationHelper = formObj => (
    new Promise((resolve) => {
        let conditionValidForm = true
        const newFormObj = formObj
        const rules = newFormObj.rule.split('|')
        newFormObj.errorMsg = ''
        for (let indexRules = 0; indexRules < rules.length; indexRules += 1) {
            if (conditionValidForm) {
                switch (rules[indexRules]) {
                    case 'required':
                        conditionValidForm = formObj.value !== ''
                        if (!conditionValidForm) {
                            newFormObj.errorMsg = `${formObj.aliasParam} is required`
                        }
                        break
                    case 'number':
                        conditionValidForm = /^\d+$/.test(formObj.value)
                        if (!conditionValidForm) {
                            newFormObj.errorMsg = `${formObj.aliasParam} must be number`
                        }
                        break
                    case 'min-length':
                        conditionValidForm = formObj.value >= (typeof rules[indexRules].split(':')[1] !== 'undefined' ? parseInt(rules[indexRules].split(':')[1], 10) : 0)
                        if (!conditionValidForm) {
                            newFormObj.errorMsg = `${formObj.aliasParam} must greater than ${typeof rules[indexRules].split(':')[1] !== 'undefined' ? parseInt(rules[indexRules].split(':')[1], 10) : 0}`
                        }
                        break
                    case 'max-length':
                        conditionValidForm = formObj.value.toString().length <= (typeof rules[indexRules].split(':')[1] !== 'undefined' ? parseInt(rules[indexRules].split(':')[1], 10) : 0)
                        if (!conditionValidForm) {
                            newFormObj.errorMsg = `${formObj.aliasParam} must less than ${typeof rules[indexRules].split(':')[1] !== 'undefined' ? parseInt(rules[indexRules].split(':')[1], 10) : 0}`
                        }
                        break
                    default:
                        break
                }
            } else {
                indexRules = rules.length
            }

            if ((indexRules + 1) >= rules.length) {
                resolve({ conditionValidForm, newFormObj })
            }
        }
    }).then(data => (data))
)

export const validationHelperGroup = async (objFormSet) => {
    const result = []
    const keyObject = Object.keys(objFormSet)
    for (let indexObj = 0; indexObj < keyObject.length; indexObj += 1) {
        result.push(validationHelper(objFormSet[keyObject[indexObj]]))
    }
    return Promise.all(result)
}

export const regexList = {
    email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
}
