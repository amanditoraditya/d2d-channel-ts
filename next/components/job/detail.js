import React from 'react'
import { connect } from 'react-redux'
import { AutoForm, AutoField, ErrorsField } from 'uniforms-bootstrap4'
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import DetailApplicant from '../../pages/module/channel-job-applicant/detail'
import classnames from 'classnames'
import axios from 'axios'
import { setHeaderJob } from '../../redux/reducer/job'
import { successToast, errorToast } from '../../redux/reducer/general'
import { BASE_API_URL } from '../../config'
import Schema from '../../scheme/channel-job'
import moment from 'moment'

class DetailJobForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                due_date: null,
                is_published: true,
                specialist: []
            },
            selected: {
                channel_id: { label: '', value: '' },
                job_type_id: { label: '', value: '' }
            },
            jobPayload: null,
            image: '',
            image_preview: '',
            remove_image: false,
            due_date_raw: undefined,
            activeTab: '1'
        }
        this.setActiveTab = this.setActiveTab.bind(this)
    }

    componentDidMount() {
        if (this.props.id != null) {
            axios.get(`${BASE_API_URL}/channel-job/detail/${this.props.id}`)
                .then((res) => {
                    const { job, header, spec } = res.data.data
                    const existingJobPayload = this.props.jobPayload
                    header.map((eachHeader, indexHeader) => {
                        switch (eachHeader.type) {
                            case 'date':
                                header[indexHeader]['detail'] = moment(eachHeader.detail).format('DD MMMM YYYY')
                                break;
                            case 'time':
                                header[indexHeader]['detail'] = moment(eachHeader.detail).format('HH:mm')
                                break;
                            default:
                                break;
                        }
                    })
                    existingJobPayload.headers = header
                    existingJobPayload.specs = spec
                    this.props.setHeaderJob(existingJobPayload)

                    job.is_published = job.is_published == 1 ? true : false
                    const specialistPayload = []
                    job.specialist.map((specialist) => {
                        specialistPayload.push({
                            value: specialist.id,
                            label: specialist.title,
                        })
                    })
                    const originDueDate = _.cloneDeep(job.due_date)
                    job.due_date = moment(job.due_date).format('YYYY-MM-DD HH:mm')
                    job.specialist = specialistPayload
                    const imagePreview = _.cloneDeep(job.image)
                    delete job.image
                    this.setState({
                        data: job,
                        due_date_raw: moment(originDueDate).toDate(),
                        image_preview: imagePreview,
                        jobPayload: existingJobPayload,
                        selected: {
                            channel_id: { label: job.channel_name, value: job.channel_id },
                            job_type_id: { label: job.job_type_name, value: job.job_type_id }
                        }
                    })
                })
        }
    }

    setActiveTab(tab) {
		this.setState({
			activeTab: tab
		})
	}

    render() {
        const {
            jobPayload,
            isAdmin
        } = this.props
        return (
            <AutoForm schema={Schema}>
                <div className="mg-b-15">
                    <Nav tabs className="nav-justified">
                        <NavItem>
                            <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.setActiveTab('1') }}>Job Detail</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.setActiveTab('2'); }}>Applicant</NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                            <div className="row mt-5">
                                {isAdmin &&
                                    <div className="col-md-12">
                                        <AutoField
                                            name="channel_id"
                                            url={"/channel/get-channels"}
                                            value={this.state.selected.channel_id}
                                            disabled
                                        />
                                    </div>
                                }
                                <div className="col-md-12">
                                    <AutoField
                                        name="job_type_id"
                                        url={"/channel-job/get-channel-job-type"}
                                        value={this.state.selected.job_type_id}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-12">
                                    <AutoField
                                        name="specialist"
                                        url={"/specialist/list"}
                                        value={this.state.data.specialist}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-12">
                                    <AutoField 
                                        name="title"
                                        default={this.state.data.title || ''} 
                                        value={this.state.data.title || ''}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-12">
                                    <AutoField 
                                        name="registration_url" 
                                        default={this.state.data.registration_url || ''} 
                                        value={this.state.data.registration_url || ''}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-12">
                                    <AutoField 
                                        name="registration_button_caption" 
                                        default={this.state.data.registration_button_caption || ''} 
                                        value={this.state.data.registration_button_caption || ''}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-12">
                                    <AutoField
                                        name="due_date"
                                        value= {this.state.data.due_date || undefined}
                                        selected={this.state.due_date_raw}
                                        required={true}
                                        disabled
                                    />
                                </div>
                                <div className="col-md-6">
                                    <div className="row form-group" key="0">
                                        <div className="col-md-3 pr-0">
                                            <label className="text-capitalize">Image</label>
                                            <img src={this.state.image_preview} className="img-fluid img-thumbnail" />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <AutoField 
                                        name="is_published" 
                                        value={this.state.data.is_published} 
                                        disabled
                                    />
                                </div>
                            </div>
                            <div className="col-sm-12"><hr /></div>
                                <div className="row">
                                    <div className="col-sm-12">
                                        <label className="text-capitalize">Header</label>
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th className="text-center">Header Info</th>
                                                    <th className="text-center">Detail</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.props.jobPayload.headers.length == 0 ? (
                                                        <tr className="text-center"><td colSpan="3">Header is Empty.</td></tr>
                                                    ) : this.props.jobPayload.headers.map((header, index) => (
                                                        <tr key={index}>
                                                            <td>{typeof header.name != 'undefined' ? header.name : header.header.label}</td>
                                                            <td>{typeof header.value != 'undefined' ? header.value : header.detail.toString()}</td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <div className="col-sm-12"><hr /></div>
                                <div className="row">
                                    <div className="col-sm-12">
                                        <label className="text-capitalize">Spec</label>
                                        <table className="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th className="text-center">Title</th>
                                                    <th className="text-center">Description</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.props.jobPayload.specs.length == 0 ? (
                                                        <tr className="text-center"><td colSpan="3">Spec is Empty.</td></tr>
                                                    ) : this.props.jobPayload.specs.map((spec, index) => (
                                                        <tr key={index}>
                                                            <td>{spec.title}</td>
                                                            <td><div dangerouslySetInnerHTML={{ __html: spec.description }}></div></td>
                                                        </tr>
                                                    ))
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        </TabPane>
                        <TabPane tabId="2"> 
                            <div className="row mt-5">
                                <div className="table">
                                    <hr class="rounded"></hr>
                                    <DetailApplicant id={this.props.id} />	
                                </div>
                            </div>
                        </TabPane>
                    </TabContent>
                </div>
            </AutoForm>
        )
    }
}

DetailJobForm.defaultProps = {
    id: null,
    jobPayload: {
        specs: [],
        headers: [],
    }
}

const mapStateToProps = state => ({
    jobPayload: state.job.jobPayload,
    isAdmin: state.auth.is_admin
})

const mapDispatchToProps = dispatch => ({
    setHeaderJob: data => dispatch(setHeaderJob(data)),
    successToast: message => dispatch(successToast(message)),
    errorToast: message => dispatch(errorToast(message)),
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailJobForm)