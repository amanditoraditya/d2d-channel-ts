import React from 'react'

import axios from 'axios'
import { connect } from 'react-redux'
import { setHeaderJob } from '../../redux/reducer/job'
import TextInput from '../forms/general-form/textinput'
import ReactSelect from 'react-select'
import enUs from 'date-fns/locale/en-US'
import DatePicker, { registerLocale } from "react-datepicker"

class HeaderJobForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headerAttributes: [],
            data: {
                header_id: '',
                header: null,
                detail: ''
            },
            errors: {
                header_id: '',
                detail: '',
            },
            addedHeaderIds: [],
            valueHeaders: []
        }
        this.handleChange = this.handleChange.bind(this)
        this.addHeader = this.addHeader.bind(this)
        this.removeHeader = this.removeHeader.bind(this)
    }
    componentDidMount() {
        const valueHeaders = []
        this.props.headers.map((header) => {
            valueHeaders.push(header.value)
        })
        this.setState({
            valueHeaders
        })
        registerLocale("en-US", enUs)
    }

    handleChange(index, event) {
        const { valueHeaders } = this.state
        valueHeaders[index] = typeof event.target != 'undefined' ? event.target.value : event
        const existingJobPayload = this.props.jobPayload
        existingJobPayload.headers[index].detail = valueHeaders[index]
        if (valueHeaders[index] == '') {
            existingJobPayload.headers[index].error = `${existingJobPayload.headers[index].name} is required.`
        } else {
            delete existingJobPayload.headers[index].error
        }
        this.props.setHeaderJob(existingJobPayload)
        this.setState({
            valueHeaders,
        })
    }

    addHeader() {
        const { data, errors } = this.state
        let isValid = true

        const addedHeaderIds = []
        const existingJobPayload = this.props.jobPayload
        existingJobPayload.headers.map((header) => {
            addedHeaderIds.push(header.job_header_attribute_id)
        })

        if (data.header_id == '' || (data.header_id != '' && addedHeaderIds.indexOf(data.header_id) >= 0)) {
            errors['header_id'] = data.header_id == '' ? 'Header Info is required' : 'The type of Header info is already added'
            isValid = false
        }
        if (data.detail == '') {
            errors['detail'] = 'Detail is required'
            isValid = false
        }
        if (isValid) {
            existingJobPayload.headers.push({
                job_header_attribute_id: data.header_id,
                header: data.header,
                detail: data.detail
            })
            this.props.setHeaderJob(existingJobPayload)
            // addedHeaderIds.push(data.header_id)
            this.setState({
                addedHeaderIds,
                headers: existingJobPayload.headers,
                data: {
                    header: null,
                    header_id: '',
                    detail: ''
                }
            })
        } else {
            this.setState({
                errors
            })
        }
    }
    removeHeader(index) {
        const existingJobPayload = this.props.jobPayload
        if (typeof existingJobPayload.headers[index] != 'undefined') {
            const { addedHeaderIds } = this.state
            addedHeaderIds.splice(addedHeaderIds.indexOf(existingJobPayload.headers[index].header_id), 1)
            existingJobPayload.headers.splice(index, 1)
            this.props.setHeaderJob(existingJobPayload)
            this.setState({
                addedHeaderIds,
                headers: existingJobPayload.headers
            })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-12">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th className="text-center">Header Info</th>
                                <th className="text-center">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.headers.length == 0 ? (
                                    <tr className="text-center"><td colSpan="3">Header is not added yet.</td></tr>
                                ) : this.props.headers.map((header, index) => (
                                    <tr key={index}>
                                        <td>{typeof header.name != 'undefined' ? header.name : header.header.label}</td>
                                        <td className="custom-datepicker">

                                            <TextInput
                                                onChange={(event) => {
                                                    this.handleChange(index, event)
                                                }}
                                                value={header.detail}
                                                headerValid={this.props.headerValid}
                                                error={typeof header.error != 'undefined' && header.error}
                                                {
                                                ...header.type != 'text' ? {
                                                    component: propChild => {
                                                        let childComponent = null
                                                        let detail = propChild.value != null && propChild.value instanceof Date ? propChild.value : ''
                                                        switch (header.type) {
                                                            case 'date':
                                                                childComponent = (
                                                                    <DatePicker
                                                                        {...propChild}
                                                                        selected={detail}
                                                                        className="form-control"
                                                                        locale="en-US"
                                                                        dateFormat="yyyy-MM-dd"
                                                                    />
                                                                )
                                                                break;
                                                            case 'datetime':
                                                                childComponent = (
                                                                    <DatePicker
                                                                        {...propChild}
                                                                        selected={detail}
                                                                        className="form-control"
                                                                        locale="en-US"
                                                                        showTimeSelect
                                                                        timeIntervals={15}
                                                                        timeCaption="Time"
                                                                        dateFormat="yyyy-MM-dd HH:mm"
                                                                        timeFormat="HH:mm"
                                                                    />
                                                                )
                                                                break;
                                                            case 'time':
                                                                childComponent = (
                                                                    <DatePicker
                                                                        {...propChild}
                                                                        selected={detail}
                                                                        className="form-control"
                                                                        locale="en-US"
                                                                        showTimeSelect
                                                                        showTimeSelectOnly
                                                                        timeIntervals={15}
                                                                        timeCaption="Time"
                                                                        dateFormat="HH:mm"
                                                                        timeFormat="HH:mm"
                                                                    />
                                                                )
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        return childComponent
                                                    }
                                                } : <div></div>
                                                }
                                            />
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

HeaderJobForm.defaultProps = {
    setHeaderJob: () => { },
    jobPayload: {
        job: {},
        specs: [],
        headers: []
    },
    headers: [],
    headerValid: true
}

const mapStateToProps = state => ({
    jobPayload: state.job.jobPayload
})

const mapDispatchToProps = dispatch => ({
    setHeaderJob: data => dispatch(setHeaderJob(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(HeaderJobForm)
