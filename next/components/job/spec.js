import React from 'react'

import { connect } from 'react-redux'
import { setHeaderJob } from '../../redux/reducer/job'
import TextInput from '../forms/general-form/textinput'
import { InputTextEditor } from '../forms/jodit-editor'

class SpecJobForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            specs: [],
            data: {
                title: '',
                description: ''
            },
            errors: {
                title: '',
                description: '',
            },
        }
        this.handleChange = this.handleChange.bind(this)
        this.addSpec = this.addSpec.bind(this)
        this.removeSpec = this.removeSpec.bind(this)
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                specs: this.props.jobPayload.specs
            })
        }, 1000);
    }

    handleChange(event) {
        const { data, errors } = this.state
        if (typeof event.target == 'undefined') {
            data['description'] = event
            errors['description'] = ''
        } else {
            data[event.target.name] = event.target.value
            errors[event.target.name] = ''
        }
        this.setState({
            data,
            errors
        })
    }

    addSpec() {
        const { data, errors } = this.state
        let isValid = true
        if (typeof data.title == 'undefined' || data.title == '') {
            errors['title'] = 'Title is required'
            isValid = false
        }
        if (typeof data.description == 'undefined' || data.description == '') {
            errors['description'] = 'Description is required'
            isValid = false
        }
        if (isValid) {
            const existingJobPayload = this.props.jobPayload
            existingJobPayload.specs.push({
                title: data.title,
                description: data.description,
            })
            this.props.setHeaderJob(existingJobPayload)
            this.setState({
                specs: existingJobPayload.specs,
                data: {
                    title: '',
                    description: ''
                }
            })
        } else {
            this.setState({
                errors
            })
        }
    }
    removeSpec(index) {
        const existingJobPayload = this.props.jobPayload
        if (typeof existingJobPayload.specs[index] != 'undefined') {
            existingJobPayload.specs.splice(index, 1)
            this.props.setHeaderJob(existingJobPayload)
            this.setState({
                specs: existingJobPayload.specs
            })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <TextInput
                        label="Title"
                        name="title"
                        error={this.state.errors.title}
                        value={this.state.data.title}
                        onChange={this.handleChange}
                    />
                </div>
                <div className="col-md-6">
                    <TextInput
                        label="Description"
                        error={this.state.errors.description}
                        component={propChild =>
                            <InputTextEditor
                                {...propChild}
                                name="detail"
                                onChange={this.handleChange}
                                value={this.state.data.description}
                            />
                        }
                    />
                </div>
                <div className="col-sm-12 text-right mb-8">
                    <button type="button" onClick={this.addSpec} className="btn btn-success"><i className="fa fa-plus"></i> Add Spec</button>
                </div>
                <div className="col-sm-12">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th className="text-center">Title</th>
                                <th className="text-center">Description</th>
                                <th className="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.jobPayload.specs.length == 0 ? (
                                    <tr className="text-center"><td colSpan="3">Spec is not added yet.</td></tr>
                                ) : this.props.specs.map((spec, index) => (
                                    <tr key={index}>
                                        <td>{spec.title}</td>
                                        <td><div dangerouslySetInnerHTML={{ __html: spec.description }}></div></td>
                                        <td className="text-center"><button type="button" className="btn btn-danger btn-xs" onClick={() => { this.removeSpec(index) }}><i className="fa fa-trash"></i></button></td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

SpecJobForm.defaultProps = {
    setHeaderJob: () => { },
    specs: []
    // jobPayload: {
    //     job: {},
    //     specs: [],
    //     headers: []
    // }
}

const mapStateToProps = state => ({
    jobPayload: state.job.jobPayload
})

const mapDispatchToProps = dispatch => ({
    setHeaderJob: data => dispatch(setHeaderJob(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(SpecJobForm)
