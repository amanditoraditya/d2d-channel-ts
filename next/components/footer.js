import { string } from 'prop-types'
import Link from 'next/link'

const Footer = (props) => (
	<div className="footer py-2 py-lg-0 my-5 d-flex flex-lg-column" id="kt_footer">
		<div className="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
			<div className="text-dark order-2 order-md-1">
				<span className="text-muted font-weight-bold mr-2">2020 ©</span>
				<a href="https://www.goapotik.com" target="_blank" className="text-dark-75 text-hover-primary">PT. Global Urban Esensial</a>
			</div>
			
			{/* <div className="nav nav-dark order-1 order-md-2">
				<a href="#" target="_blank" className="nav-link pr-3 pl-0">About</a>
				<a href="#" target="_blank" className="nav-link px-3">Team</a>
				<a href="#" target="_blank" className="nav-link pl-3 pr-0">Contact</a>
			</div> */}
		</div>
	</div>
)

Footer.propTypes = {
	selectedMenu: string
}

export default Footer