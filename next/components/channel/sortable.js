import React, { Component } from 'react';
import { connect } from 'react-redux'
import { setBanner } from '../../redux/reducer/channel'
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import arrayMove from 'array-move';

const listStyling = {
    display: "flex",
    alignItems: "center",
    width: "100%",
    padding: "0 20px",
    backgroundColor: "#fff",
    borderBottom: "1px solid #efefef",
    boxSizing: "border-box",
    userSelect: "none",
    color: "#333",
    fontWeight: "400",
    float: "left",
    padding: "8px",
    background: "transparent",
    border: "0",
    width: "350px",
    height: "200px",
    cursor: "grab",
    zIndex: 1050
}

const wrapperStyling = {
    display: "block",
    width: "100%",
    height: "75vh",
    border: "0",
    position: "relative",
    backgroundColor: "#f3f3f3",
    border: "1px solid #efefef",
    borderRadius: "3px",
    outline: "none",
    overflow: "auto",
    border: "1px solid #999",
}

const imageStyle = {
    height: "100%",
    width: "100%",
    objectFit: "cover",
    borderRadius: "8px",
}

const wrapperBoxStyling = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%",
    background: "#fff",
    border: "1px solid #efefef",
    whiteSpace: "break-spaces",
    boxShadow: "0 0px 20px -10px",
    borderRadius: "8px",
    position: "relative"
}

const numberIndexStyling = {
    position: "absolute",
    top: "0px",
    right: "0px",
    width: "30px",
    height: "30px",
    background: "#1BC5BD",
    color: "#fff",
    textAlign: "center",
    fontSize: "20px",
    fontWeight: "bold",
    borderRadius: "0px 8px"
}
const SortableItem = SortableElement(({ value, sortIndex }) => {
    return (
        <li style={listStyling}>
            <div style={wrapperBoxStyling}>
                <span style={numberIndexStyling}>{sortIndex + 1}</span>
                <img style={imageStyle} src={value.image} alt="image-sortable" />
            </div>
        </li>
    )
})

const SortableList = SortableContainer(({ items }) => {
    return items.length > 0 ? (
        <ul className="sorting-list__custom" style={wrapperStyling}>
            {items.map((value, index) => (
                <SortableItem key={`item-${index}`} index={index} sortIndex={index} value={value} />
            ))}
        </ul>
    ) : (
        <div style={{ display: "flex", width: "100%", height: "75vh", fontSize: "26px", fontWeight: "bold", justifyContent: "center", alignItems: "center" }}>Banner is not already add yet.</div>
    )
})

class Sortable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            items: props.banners
        }
        this.props.setBanner(props.banners)
    }
    onSortEnd = ({ oldIndex, newIndex }) => {
        const { items } = this.state
        const newItems = arrayMove(items, oldIndex, newIndex)
        this.props.setBanner(newItems)
        this.setState({
            items: newItems
        })
    }
    render() {
        return <SortableList items={this.state.items} onSortEnd={this.onSortEnd} axis="xy" />;
    }
}

Sortable.defaultProps = {
    setBanner: () => { }
}

const mapDispatchToProps = dispatch => ({
    setBanner: data => dispatch(setBanner(data))
})

export default connect(null, mapDispatchToProps)(Sortable)