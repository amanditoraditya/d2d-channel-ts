import React from 'react'

import Router from 'next/router'
import { connect } from 'react-redux'
import { AutoForm, AutoField, SubmitField, SelectField, ErrorsField } from 'uniforms-bootstrap4'
import HeaderJobForm from './header'
import SpecJobForm from './spec'
import axios from 'axios'
import { create as createJob, detail as detailJob } from '../../services/job'
import { postService } from '../../services/base-service'
import { setHeaderJob } from '../../redux/reducer/job'
import { successToast, errorToast } from '../../redux/reducer/general'
import { BASE_API_URL } from '../../config'
// import { useForm } from 'uniforms'
import Schema from '../../scheme/channel-job'

class JobForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                due_date: 1,
                is_published: true,
                specialist: []
            },
            selected: {
                channel_id: { label: '', value: '' },
                job_type_id: { label: '', value: '' }
            },
            jobPayload: null,
            image: '',
            image_preview: '',
            remove_image: false,
            due_date_raw: undefined
        }
        this.handleChange = this.handleChange.bind(this)
        this.submitJob = this.submitJob.bind(this)
        this.removeImage = this.removeImage.bind(this)
    }

    componentDidMount() {
        if (this.props.id != null) {
            axios.get(`${BASE_API_URL}/channel-job/detail/${this.props.id}`)
                .then((res) => {
                    console.log(res.data.data)
                    const { job, header, spec } = res.data.data
                    const existingJobPayload = this.props.jobPayload
                    existingJobPayload.headers = header
                    existingJobPayload.specs = spec
                    this.props.setHeaderJob(existingJobPayload)

                    job.is_published = job.is_published == 1 ? true : false
                    const specialistPayload = []
                    job.specialist.map((specialist) => {
                        specialistPayload.push({
                            value: specialist.id,
                            label: specialist.title,
                        })
                    })
                    const originDueDate = _.cloneDeep(job.due_date)
                    job.due_date = moment(job.due_date).format('YYYY-MM-DD HH:mm')
                    job.specialist = specialistPayload
                    const imagePreview = _.cloneDeep(job.image)
                    delete job.image
                    this.setState({
                        data: job,
                        due_date_raw: moment(originDueDate).toDate(),
                        image_preview: imagePreview,
                        jobPayload: existingJobPayload,
                        selected: {
                            channel_id: { label: job.channel_name, value: job.channel_id },
                            job_type_id: { label: job.job_type_name, value: job.job_type_id }
                        }
                    })
                })
        }
        else {
            const existingJobPayload = this.props.jobPayload
            existingJobPayload.headers = []
            existingJobPayload.specs = []
            this.props.setHeaderJob(existingJobPayload)
        }
    }

    handleChange(event, valueObj) {
        const { data } = this.state
        if (typeof event.target != 'undefined' && event.target.name == 'image') {
            data[event.target.name] = event.target.files[0]
            this.setState({
                data,
                remove_image: true,
                image_preview: URL.createObjectURL(event.target.files[0])
            })
        } else if (event == 'due_date') {
            data['due_date'] = moment(valueObj).format('YYYY-MM-DD HH:mm')
            this.setState({
                data,
                due_date_raw: valueObj
            })
        } else {
            if (event == 'channel_id' || event == 'job_type_id') {
                const { selected } = this.state
                selected[event] = valueObj
                this.setState({
                    selected
                })
            }
            data[event] = valueObj != null && typeof valueObj.value != 'undefined' ? valueObj.value : valueObj
            this.setState({
                data
            })
        }
    }

    removeImage(e) {
        e.preventDefault()
        const { data } = this.state
        delete data.image
        this.setState({
            remove_image: true,
            data,
            image_preview: ''
        })
    }

    submitJob() {
        // try {
            const { data } = this.state
            data.remove_image = this.state.remove_image
            // data.spec = this.props.jobPayload.specs
            // data.header = this.props.jobPayload.headers
            const originPayload = {
                job: _.cloneDeep(data),
                spec: this.props.jobPayload.specs,
                header: this.props.jobPayload.headers
            }
            delete originPayload.job.image
            originPayload.job.specialist = data.specialist

            const payload = new FormData()
            payload.append('data', JSON.stringify(originPayload))
            if (typeof data.image != 'undefined' && data.image != null) {
                payload.append('image', data.image)
            }
            postService({
                url: this.props.id != null ? `/channel-job/update/${this.props.id}` : '/channel-job/create',
                data: payload,
                callback: (response) => {
                    if (response.data.code == '2000') {
                        Router.push('/module/channel-job/index')
                    } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                        this.props.errorToast(response.data.data._errorMessages.map(e => e.message).join(", "))
                    } else {
                        this.props.errorToast(response.data.message)
                    }
                }
            })
        // } catch (err) {
        //     console.error(`Message : ${e.message} || File : ${e.file} || Line: ${e.line}`)
        // }
    }

    render() {
        const {
            jobPayload,
            isAdmin
        } = this.props
        return (
            <AutoForm
                schema={Schema}
                onSubmit={this.submitJob}
                onChange={this.handleChange}
            >
                <div className="mg-b-15">
                    <div className="row mt-5">
                        <div className="col-sm-12 text-capitalize">
                            <ErrorsField />
                        </div>
                        {isAdmin &&
                            <div className="col-md-6">
                                <AutoField
                                    name="channel_id"
                                    url={"/channel/get-channels"}
                                    isSearchable={true}
                                    value={this.state.selected.channel_id}
                                    // options={this.state.defaultChannels}
                                    placeholder="Select a Channel"
                                />
                            </div>
                        }
                        <div className={isAdmin ? "col-md-6" : "col-md-12"}>
                            <AutoField
                                name="job_type_id"
                                url={"/channel-job/get-channel-job-type"}
                                isSearchable={true}
                                value={this.state.selected.job_type_id}
                                placeholder="Select a Job Type"
                            />
                        </div>
                        <div className="col-md-12">
                            <AutoField
                                name="specialist"
                                url={"/specialist/list"}
                                isSearchable={true}
                                value={this.state.data.specialist}
                                placeholder="Select a Job Specialist"
                            />
                        </div>
                        <div className="col-md-12">
                            <AutoField name="title" default={this.state.data.title || ''} value={this.state.data.title || ''} />
                        </div>
                        <div className="col-md-12">
                            <AutoField name="is_published" value={this.state.data.is_published} placeholder="Select Published" />
                        </div>
                        <div className="col-md-12">
                            <AutoField
                                name="due_date"
                                value= {this.state.data.due_date || undefined}
                                selected={this.state.due_date_raw}
                                required={true}
                            />
                        </div>

                        <div className="col-md-12">
                            <div className="row form-group" key="0">
                                <div className="col-md-2 pr-0">
                                    <img src={this.state.image_preview} className="img-fluid img-thumbnail" />
                                </div>
                                <div className="col-md-1">
                                    <button className="btn py-0 px-0" onClick={this.removeImage}>
                                        <span className="navi-icon">
                                            <i className="flaticon2-cross icon-nm"></i>
                                        </span>
                                    </button>
                                </div>
                                <div className="col-md-9">
                                    <label className="text-capitalize">Image</label>
                                    <div className="custom-file">
                                        <input type="file" name="image" className="custom-file-input" id="customFile" onChange={this.handleChange} />
                                        <label className="custom-file-label custom-file-label-primary" htmlFor="customFile">{typeof this.state.data.image == 'undefined' || (typeof this.state.data.image != 'undefined' && typeof this.state.data.image.name == 'undefined') ? 'Choose File...' : this.state.data.image.name}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-12"><hr /></div>
                    <HeaderJobForm headers={this.props.jobPayload.headers} />
                    <div className="col-sm-12"><hr /></div>
                    <SpecJobForm specs={this.props.jobPayload.specs}/>
                    <div className="form-layout-footer float-right">
                        <SubmitField value="Save" />
                    </div>
                </div>
            </AutoForm>
        )
    }
}

JobForm.defaultProps = {
    id: null,
    jobPayload: {
        specs: [],
        headers: [],
    }
}

const mapStateToProps = state => ({
    jobPayload: state.job.jobPayload
})

const mapDispatchToProps = dispatch => ({
    setHeaderJob: data => dispatch(setHeaderJob(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(JobForm)
