import React from "react"
import QRCodeGenerator from 'qrcode.react'

class QRCode extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            url: this.props.qr.url,
            // channel_name: this.props.qr.channel_name,
        }
    }

    render() {
        return (
            <QRCodeGenerator
                id= "qrcode"
                value= {this.state.url}
                size= {360}
                fgColor= "#000000"
                bgColor= "#ffffff"
                level= "H"
                renderAs= "canvas"
                includeMargin= {false}
                imageSettings={{
                    src: "/static/one/images/logo-512x512.png",
                    x: null,
                    y: null,
                    height: 80,
                    width: 80,
                    excavate: false,
                }}
            />
        )
    }
}

export default QRCode