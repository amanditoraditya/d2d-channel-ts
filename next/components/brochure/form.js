import React from 'react'

import Router from 'next/router'
import { connect } from 'react-redux'
import { AutoForm, AutoField, SubmitField, ErrorsField } from 'uniforms-bootstrap4'
import axios from 'axios'
import { postService } from '../../services/base-service'
import { setHeaderBrochure } from '../../redux/reducer/brochure'
import { successToast, errorToast } from '../../redux/reducer/general'
import { BASE_API_URL } from '../../config'
import Schema from '../../scheme/channel-brochure'

class BrochureForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                is_published: true,
                channel_id: undefined,
            },
            brochurePayLoad: null,
            cover: '',
            cover_preview: '',
            remove_cover: false,
            file_preview: '',
            remove_file: false,
            required_objects: Schema.schema.required
        }
        this.handleChange = this.handleChange.bind(this)
        this.submitHeader = this.submitHeader.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.removeCover = this.removeCover.bind(this)
        this.removeFile = this.removeFile.bind(this)
    }

    async componentDidMount() {
        if (this.props.id != null) {
            axios.get(`${BASE_API_URL}/channel-brochure/detail/${this.props.id}`)
                .then((res) => {          
                    const brochure = res.data.data;
                    const existingBrochurePayLoad = this.props.brochurePayLoad
                    this.props.setHeaderBrochure(existingBrochurePayLoad)

                    brochure.is_published = brochure.is_published == 1 ? true : false
                    brochure.channel_id = [{
                        label: brochure.channel_name,
                        value: brochure.channel_id
                    }]

                    const coverPreview = _.cloneDeep(brochure.cover)
                    const filePreview = "/static/one/media/svg/icons/Files/File.svg"
                    delete brochure.cover
                    delete brochure.filename
    
                    this.setState({
                        data: brochure,
                        cover_preview: coverPreview,
                        file_preview: filePreview,
                        brochurePayLoad: existingBrochurePayLoad,
                    })
                })
        } else {
            const existingBrochurePayLoad = this.props.brochurePayLoad
            existingBrochurePayLoad.brochurePayLoad = {} 
            this.props.setHeaderBrochure(existingBrochurePayLoad)
            this.setState({
                brochurePayLoad: existingBrochurePayLoad
            })

            if (!this.props.isAdmin) {
                this.setState({
                    data: { channel_id: this.props.profile.user.channel_id, is_published: true}
                })
            }
        }
    }

    handleChange(event, valueObj) {
        const { data } = this.state
        if (typeof event.target != 'undefined' && event.target.name == 'cover') {
            data[event.target.name] = event.target.files[0]
            this.setState({
                data,
                remove_cover: true,
                cover_preview: URL.createObjectURL(event.target.files[0])
            })
        }
        else if (typeof event.target != 'undefined' && event.target.name == 'filename'){
            data[event.target.name] = event.target.files[0]
            this.setState({
                data,
                remove_file: true,
                file_preview: "/static/one/media/svg/icons/Files/File.svg"
            })
        }
        else{
            if (event == 'channel_id') {
                data[event] = valueObj != null && typeof valueObj.value != 'undefined' ? [valueObj] : undefined
            } else {
                data[event] = valueObj
            }
            this.setState({
                data
            })
        }
    }

    removeCover(e) {
        e.preventDefault()
        const { data } = this.state
        delete data.cover
        this.setState({
            remove_cover: true,
            data,
            cover_preview: ''
        })
    }

    removeFile(e) {
        e.preventDefault()
        const { data } = this.state
        delete data.filename
        this.setState({
            remove_file: true,
            data,
            file_preview: ''
        })
    }

    submitHeader() {
        const { data } = this.state

        if (!this.props.isAdmin) {
            data.channel_id_value = this.props.profile.user.channel_id
        } else {
            data.channel_id_value = data.channel_id[0].value
        }

        data.remove_cover = this.state.remove_cover
        data.remove_file = this.state.remove_file

        const originPayload = {
            brochurePayLoad: _.cloneDeep(data)
        }
        delete originPayload.cover
        delete originPayload.filename

        const payload = new FormData()
        payload.append('data', JSON.stringify(originPayload))
        if (typeof data.cover != 'undefined' && data.cover != null) {
            payload.append('cover', data.cover)
        }

        if (typeof data.filename != 'undefined' && data.filename != null) {
            payload.append('filename', data.filename)
        }

        console.log(data)

        postService({
            url: this.props.id != null ? `/channel-brochure/update/${this.props.id}` : '/channel-brochure/create',
            data: payload,
            callback: (response) => {
                if (response.data.code == '2000') {
                    Router.push('/module/channel-brochure/index')
                } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                    this.props.errorToast(response.data.data._errorMessages.map(e => e.message).join(", "))
                } else {
                    this.props.errorToast(response.data.message)
                }
            }
        })
    }

    handleKeyDown (e) {
        if (e.key === 'Enter') {
          e.preventDefault();
          e.stopPropagation();
        }
    }

    render() {
        const {
            isAdmin
        } = this.props
        return (
            <AutoForm
                schema={Schema}
                onSubmit={this.submitHeader}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
                model={this.state.data}
                modelTransform={(mode, model) => {
                    if (mode == 'form') {
                        const { required_objects } = this.state
                        for (let row of required_objects) {
                            model[row] = model[row] == null || model[row] == '' ? undefined : model[row]
                        }
                    }
                    return model;
                }}
            >
                <div className="mg-b-15">
                    <div className="row">
                        <div className="col-sm-12 text-capitalize">
                            <ErrorsField />
                        </div>
                        <div className={isAdmin ? 'col-md-12 input-to-front-1' : 'd-none'}>
                            <AutoField
                                name="channel_id"
                                url={"/channel/get-channels"}
                                isSearchable={true}
                                value={this.state.data.channel_id}
                                initialValue={this.state.data.channel_id}
                                placeholder="Select a Channel"
                            />
                        </div>
                        <div className="col-md-9">
                            <AutoField name="title" default={this.state.data.title} value={this.state.data.title} />
                        </div>
                        <div className="col-md-3">
                            <AutoField name="is_published" value={this.state.data.is_published} placeholder="Select Status" />
                        </div>
                        <div className="col-md-12">
                            <AutoField name="description" />
                        </div>
                        <div className="col-md-6">
                            <div className="row form-group" key="0">
                                <div className="col-md-3 pr-0">
                                    <img src={this.state.cover_preview} className="img-fluid img-thumbnail" style={{ width: "125px", height: "100px"}}/>
                                </div>
                                <div className="col-md-1">
                                    <button className="btn py-0 px-0" onClick={this.removeCover}>
                                        <span className="navi-icon">
                                            <i className="flaticon2-cross icon-nm"></i>
                                        </span>
                                    </button>
                                </div>
                                <div className="col-md-8">
                                    <label className="text-capitalize">Cover</label>
                                    <div className="custom-file">
                                        <input type="file" name="cover" className="custom-file-input" id="customFile" onChange={this.handleChange} />
                                        <label className="custom-file-label custom-file-label-primary" htmlFor="customFile">{typeof this.state.data.cover == 'undefined' || (typeof this.state.data.cover != 'undefined' && typeof this.state.data.cover.name == 'undefined') ? 'Choose Cover...' : this.state.data.cover.name}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="row form-group" key="1">
                                <div className="col-md-3 pr-0">
                                    <img src={this.state.file_preview} className="img-fluid img-thumbnail" style={{ width: "125px", height: "100px"}}/>
                                </div>
                                <div className="col-md-1">
                                    <button className="btn py-0 px-0" onClick={this.removeFile}>
                                        <span className="navi-icon">
                                            <i className="flaticon2-cross icon-nm"></i>
                                        </span>
                                    </button>
                                </div>
                                <div className="col-md-8">
                                    <label className="text-capitalize">File</label>
                                    <div className="custom-file">
                                        <input type="file" name="filename" className="custom-file-input" id="customFile" onChange={this.handleChange} />
                                        <label className="custom-file-label custom-file-label-primary" htmlFor="customFile">{typeof this.state.data.filename == 'undefined' || (typeof this.state.data.filename != 'undefined' && typeof this.state.data.filename.name == 'undefined') ? 'Choose File...' : this.state.data.filename.name}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-layout-footer float-right">
                        <SubmitField value="Submit" />
                    </div>
                </div>
            </AutoForm>
        )
    }
}

BrochureForm.defaultProps = {
    id: null,
    brochurePayLoad: {}
}

const mapStateToProps = state => ({
    brochurePayLoad: state.brochurePayLoad,
    isAdmin: state.auth.is_admin,
    profile: state.auth.profile
})

const mapDispatchToProps = dispatch => ({
    setHeaderBrochure: data => dispatch(setHeaderBrochure(data)),
    successToast: message => dispatch(successToast(message)),
    errorToast: message => dispatch(errorToast(message)),
})

export default connect(mapStateToProps, mapDispatchToProps)(BrochureForm)