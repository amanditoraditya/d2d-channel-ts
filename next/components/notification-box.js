import React from 'react'

import { connect } from 'react-redux'

class NotificationBox extends React.Component {
    render() {
        return (
            <div>
                {
                    typeof this.props.toast.message != 'undefined' && this.props.toast.message != '' && (
                        <div className={`alert alert-solid alert-${this.props.toast.type} error-bar`}>{this.props.toast.message}</div>
                    )
                }
            </div>
        )
    }
}

NotificationBox.defaultProps = {
    toast: {
        message: '',
        type: '',
    }
}

const mapStateToProps = state => ({
    toast: state.general.toast
})

export default connect(mapStateToProps, {})(NotificationBox)
