import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import ReactSelect from 'react-select'
import AsyncSelect from 'react-select/async'
import axios from 'axios'
import _ from 'lodash'

const goaConfig = require('../../config')
const Select2 = AsyncSelect

const filterDatas = (url, inputValue, callback) => {
    axios({
        url: goaConfig.BASE_API_URL + url + '?phrase=' + inputValue,
        method: 'GET',
        timeout: goaConfig.TIMEOUT
    }).then(function (response) {
        const items = response.data
        let options = items.map(function (item) {
            return {
                value: item.value,
                label: item.label,
            }
        })
        callback(options)
    })
}

const promiseOptions = (url, inputValue, callback) => (
    new Promise(resolve => {
        setTimeout(() => {
            resolve(filterDatas(url, inputValue, callback))
        }, 1000)
    })
)

const generalStyling = {
    menu: (styles) => ({
        ...styles,
        zIndex: 3,
    }),
}

const groupStyling = {
    container: (styles) => ({
        ...styles,
        width: '90%'
    }),
    menu: (styles) => ({
        ...styles,
        zIndex: 3,
    }),
}

const notOriginProp = ['onClickBtn']
const notOriginPropForm = ['onClickBtn', 'isSearchable', 'isClearable', 'ref', 'initialValue']

const renderSelect = (props) => {
    if (typeof props.url != 'undefined') {
        const beforePromiseOptions = (e, callback) => {
            promiseOptions(props.url, e, callback)
        }

        return (
            <Select2
                className={props.className}
                clearValue={props.clearValue}
                getValue={props.getValue}
                isMulti={props.isMulti}
                selectOption={props.selectOption}
                selectProps={props.selectProps}
                setValue={props.value}
                options={props.options}
                hidden={props.hidden}
                defaultInputValue={props.defaultInputValue}
                inputValue={props.inputValue}
                defaultValue={props.defaultValue}
                value={props.value || (props.fieldType === Array ? [] : undefined)}
                isDisabled={props.disabled}
                isSearchable={props.isSearchable}
                isClearable={props.isClearable}
                name={props.name}
                tabIndex={props.tabIndex}
                autoFocus={props.autoFocus}
                placeholder={props.placeholder}
                ref={props.inputRef}
                id='select-custom'
                inputValueId={props.id}
                onChange={(option, action) =>
                    props.onChange(option)
                }
                defaultOptions
                loadOptions={(query, callback) => beforePromiseOptions(query, callback)}
                theme={{
                    borderRadius: 0,
                    spacing: {
                        baseUnit: 5.5,
                    }
                }}
                height={43}
                noOptionsMessage={() => "Type to search option data..."}
                styles={generalStyling}
            />
        )
    } else {
        if (typeof props.onClickBtn != 'undefined') {
            const propOrigin = _.cloneDeep(props)
            notOriginProp.map((param) => {
                delete propOrigin[param]
            })
            return (
                <div className="input-group mb-3">
                    <ReactSelect
                        styles={groupStyling}
                        {...props}
                    />
                    <div className="input-group-append" style={{ width: '10%' }}>
                        <button onClick={props.onClickBtn} className="btn btn-success" style={{ width: '100%' }} type="button"><i className="fa fa-plus"></i></button>
                    </div>
                </div>
            )
        }
        return (
            <ReactSelect
                styles={generalStyling}
                {...props}
            />
        )
    }
}

function renderAddIdToSelect(props) {
    let input = document.getElementsByName(props.name)
    if (input[0]) {
        input[0].setAttribute('id', props.id)
    }
}

const Select = (props) => {
    const propsWrapField = _.cloneDeep(props)
    notOriginPropForm.map((param) => {
        if (typeof propsWrapField[param] != 'undefined') {
            delete propsWrapField[param]
        }
    })
    return wrapField(
        propsWrapField,
        renderSelect(props),
        renderAddIdToSelect(props)
    )
}

export default connectField(Select, { ensureValue: false })