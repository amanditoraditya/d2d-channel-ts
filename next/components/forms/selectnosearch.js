import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import ReactSelect from 'react-select'
import axios from 'axios'

const goaConfig = require('../../config')
const Select2 = ReactSelect

const renderSelect = (props) => {
	return (
		<Select2
			className={props.className}
			clearValue={props.clearValue}
			getValue={props.getValue}
			isMulti={props.isMulti}
			selectOption={props.selectOption}
			selectProps={props.selectProps}
			setValue={props.value}
			options={props.options}
			defaultInputValue={props.defaultInputValue}
			inputValue={props.inputValue}
			defaultValue={props.defaultValue}
			value={props.value || (props.fieldType === Array ? [] : undefined)}
			isDisabled={props.disabled}
			isSearchable={props.isSearchable}
			isClearable={props.isClearable}
			name={props.name}
			tabIndex={props.tabIndex}
			autoFocus={props.autoFocus}
			placeholder={props.placeholder}
			ref={props.inputRef}
			id='select-custom'
			inputValueId={props.id}
			onChange={(option, action) =>
				props.onChange(option)
			}
			theme={{
				borderRadius: 0,
				spacing: {
					baseUnit: 5.5,
				}
			}}
			height={43}
			noOptionsMessage={() => "Type to search option data..." }
		/>
	)
}

function renderAddIdToSelect(props) {
	let input = document.getElementsByName(props.name)
	if(input[0]) {
		input[0].setAttribute('id', props.id)
	}
}

const Select = props =>
	wrapField(
		props,
		renderSelect(props),
		renderAddIdToSelect(props)
	)

export default connectField(Select, { ensureValue: false })