const importJodit = () => import('jodit-react');

import dynamic from 'next/dynamic';

export const InputTextEditor = dynamic(importJodit, {
    ssr: false,
})