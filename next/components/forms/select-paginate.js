import React, { useEffect, useState } from "react";
import { AsyncPaginate } from "react-select-async-paginate";
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import _ from 'lodash'
const goaConfig = require('../../config')
const notOriginProp = ['onClickBtn']
const notOriginPropForm = ['onClickBtn', 'isSearchable', 'isClearable', 'ref', 'initialValue']

const generalStyling = {
    menu: (styles) => ({
        ...styles,
        zIndex: 3,
    }),
}

const renderSelect = (props) => {
    async function loadOptions(search, loadedOptions) {
        let response = await axios({
            url: `${goaConfig.BASE_API_URL}${props.url}?phrase=${search}&offset=${loadedOptions.length}`,
            method: 'GET',
            timeout: goaConfig.TIMEOUT
        })
        const items = response.data.result
        let options = items.map(function (item) {
            return {
                value: item.value,
                label: item.label,
            }
        })

        return {
            options: options,
            hasMore: response.data.has_more,
        };
    }

    return (
        <AsyncPaginate
            value={props.value}
            loadOptions={loadOptions}
            onChange={(option, action) =>
                props.onChange(option)
            }
            className={props.className}
            hidden={props.hidden}
            isDisabled={props.disabled}
            placeholder={props.placeholder}
            styles={generalStyling}
            isMulti={props.isMulti}
        />
    );
}

const SelectAsyncPaginate = (props) => {
    const propsWrapField = _.cloneDeep(props)
    notOriginPropForm.map((param) => {
        if (typeof propsWrapField[param] != 'undefined') {
            delete propsWrapField[param]
        }
    })
    return wrapField(
        propsWrapField,
        renderSelect(props),
    )
}

export default connectField(SelectAsyncPaginate, { ensureValue: false })
