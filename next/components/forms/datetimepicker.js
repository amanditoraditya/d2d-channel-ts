import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
import InputDateTimePicker from "react-datepicker";
const DateTimePicker = props =>
	wrapField(
		props,
		<InputDateTimePicker
		wrapperClassName="form-control"
		className="form-control"
		disabled={props.disabled}
		format={props.format}
		onChange={date => {props.onChange(date)}}
		name={props.name}
		selected={props.selected}
		value={props.value}
		required={props.required}
		clearIcon={props.clearIcon}
		showTimeSelect={true}
		timeFormat="HH:mm"
		timeIntervals={15}
		/>,
	)

DateTimePicker.defaultProps = {
	disabled: false,
	name: 'datetime',
	required: false,
}

export default connectField(DateTimePicker)