import React from 'react'

import { postService } from '../../../services/base-service'
import SwitchForm from './switch-form'
import { confirmAlert } from 'react-confirm-alert'

class SwitchTableForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            checkedStatus: false,
            confirmationAttr: {
                header: 'Are u sure?',
                text: 'You want to run this action?',
                noButton: 'default',
                yesButton: 'success'
            }
        }
        this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount() {
        this.setState({
            checkedStatus: this.props.checked
        })
    }

    handleChange(value) {
        this.setState({
            checkedStatus: value
        })
        confirmAlert({
            closeOnEscape: false,
            closeOnClickOutside: false,
            customUI: ({ onClose }) => {
                return (
                    <div className='custom-confirmation-box'>
                        <h1>{this.state.confirmationAttr.header}</h1>
                        <p>{this.state.confirmationAttr.text}</p>
                        <button
                            className={`btn btn-xs btn-cancel btn-${this.state.confirmationAttr.noButton}`}
                            onClick={() => {
                                onClose()
                                this.setState({
                                    checkedStatus: !value
                                })
                            }}
                        >
                            No
                        </button>
                        <button
                            className={`btn btn-xs btn-${this.state.confirmationAttr.yesButton}`}
                            onClick={() => {
                                if (this.props.url != null) {
                                    postService({
                                        url: this.props.url,
                                        data: {
                                            id: this.props.id
                                        },
                                        success: ({ data }) => {
                                            this.props.onSuccess(data)
                                        },
                                        failed: (message, response) => {
                                            this.props.onFailed(message, response)
                                            this.setState({
                                                checkedStatus: !value
                                            })
                                        }
                                    })
                                } else {
                                    this.setState({
                                        checkedStatus: value
                                    })
                                }
                                onClose()
                            }}
                        >
                            Yes
                        </button>
                    </div>
                );
            }
        })
    }

    render() {
        return (
            <SwitchForm
                value={this.state.checkedStatus}
                onChange={this.handleChange}
                disabled={this.props.disabled}
                trueSwitchLabel={this.props.trueSwitchLabel}
                falseSwitchLabel={this.props.falseSwitchLabel}
            />
        )
    }
}

SwitchTableForm.defaultProps = {
    errorToast: () => { },
    onFailed: () => { },
    onSuccess: () => { },
    id: null,
    url: null,
    type: 'GENERAL',
    trueSwitchLabel: 'Published',
    falseSwitchLabel: 'Unpublished',
    checked: false,
    disabled: false
}

export default SwitchTableForm
// export default connect(mapStateToProps, mapDispatchToProps)(SwitchTableForm)