import React from 'react'

const BaseForm = props => {
    const { component } = props
    return (
        <div className={`form-group ${typeof props.error != 'undefined' && props.error != '' && 'is-invalid'}`}>
            {typeof props.label != 'undefined' && (<label className="control-label">{props.label}</label>)}
            {typeof component != 'undefined' ? component(props) : <input type="text" name={props.name} value={props.value} className={`form-control ${typeof props.error != 'undefined' && props.error != '' && 'is-invalid'}`} onChange={props.onChange} />}
            {typeof props.error != 'undefined' && props.error != '' && <p className="help-block error">{props.error}</p>}
        </div>
    )
}

BaseForm.defaultProps = {
    disabled: false,
    error: '',
    name: '',
    value: '',
    required: false,
    onChange: () => { }
}

export default BaseForm