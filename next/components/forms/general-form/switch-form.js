import Switch from 'react-switch'

const styleSetting = {
    onColor: "#86d3ff",
    onHandleColor: "#2693e6",
    handleDiameter: 30,
    uncheckedIcon: false,
    checkedIcon: false,
    boxShadow: "0px 1px 5px rgba(0, 0, 0, 0.6)",
    activeBoxShadow: "0px 0px 1px 10px rgba(0, 0, 0, 0.2)",
    height: 25,
    width: 55,
}

function getValueSetting(props, setting) {
    let settingValue = false;
    try {
        // settingValue = props.setting[setting];
        if (typeof props.setting[setting] != "undefined" && props.setting[setting] != null) {
            settingValue = props.setting[setting];
        } else {
            settingValue = styleSetting[setting];
        }
    } catch (e) {
        settingValue = styleSetting[setting];
    }
    return settingValue;
}

const SwitchForm = props => (
    <div className="row col-md-12 align-items-center">
        <Switch
            className="m-0 mr-5"
            disabled={props.disabled}
            onChange={(value) => {
                props.onChange(value)
            }}
            checked={props.value}
            onColor={getValueSetting(props, 'onColor')}
            onHandleColor={getValueSetting(props, 'onHandleColor')}
            handleDiameter={getValueSetting(props, 'handleDiameter')}
            uncheckedIcon={getValueSetting(props, 'uncheckedIcon')}
            checkedIcon={getValueSetting(props, 'checkedIcon')}
            boxShadow={getValueSetting(props, 'boxShadow')}
            activeBoxShadow={getValueSetting(props, 'activeBoxShadow')}
            height={getValueSetting(props, 'height')}
            width={getValueSetting(props, 'width')}
        />
        <label className="m-0" style={{ fontSize: getValueSetting(props, 'textFontSize'), color: props.disabled == true ? 'white' : 'black' }}>
            {props.value == true ? props.trueSwitchLabel : props.falseSwitchLabel}
        </label>
    </div>
)

SwitchForm.defaultProps = {
    trueSwitchLabel: 'On',
    onChange: () => { },
    falseSwitchLabel: 'Off'
}
export default SwitchForm