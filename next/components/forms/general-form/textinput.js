import React from 'react'
import BaseForm from './base-form'

const TextInput = props => (
    <BaseForm
        label={props.label}
        value={props.value}
        name={props.name}
        error={props.error}
        onChange={props.onChange}
        {...(typeof props.component != 'undefined') ? {component: props.component} : {}}
    />
)

TextInput.defaultProps = {
	disabled: false,
	format: 'y-MM-dd',
	name: 'date',
    required: false,
    onChange: () => {}
}

export default TextInput