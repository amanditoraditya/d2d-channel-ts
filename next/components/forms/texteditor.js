import React from 'react'
import { connectField, filterDOMProps } from 'uniforms'
import { wrapField } from 'uniforms-bootstrap4'
const importJodit = () => import('jodit-react');

import dynamic from 'next/dynamic';

const InputTextEditor = dynamic(importJodit, {
    ssr: false,
});
  
const TextEditor = props =>
	wrapField(
		props,
		<InputTextEditor
			className="form-control"
			disabled={props.disabled}
			onChange={props.onChange}
			name={props.name}
			value={props.value}
			required={props.required}
			config={config}
		/>,
	)

const config = {
	language: 'en',
	minHeight: 300
}

TextEditor.defaultProps = {
	disabled: false,
	name: 'texteditor',
	required: false,
}

export default connectField(TextEditor)