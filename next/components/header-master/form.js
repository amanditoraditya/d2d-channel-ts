import React from 'react'

import Router from 'next/router'
import { connect } from 'react-redux'
import { AutoForm, AutoField, SubmitField, SelectField, ErrorsField } from 'uniforms-bootstrap4'
import axios from 'axios'
import { postService } from '../../services/base-service'
import { setHeaderMaster } from '../../redux/reducer/headermaster'
import { successToast, errorToast } from '../../redux/reducer/general'
import { BASE_API_URL } from '../../config'
import { fe_input_types, fe_types } from '../../config/header'
import Schema from '../../scheme/header-master'

class JobForm extends React.Component {
    selectType = null;
    selectInputType = null;
    constructor(props) {
        super(props)
        this.state = {
            data: {
                type: '',
                input_type: '',
                is_archived: false,
            },
            selected: {
                type: 'job',
                input_type: ''
            },
            type: [],
            input_type: [],
            headerPayLoad: null,
            icon: '',
            image_preview: '',
            remove_image: false,
        }
        this.handleChange = this.handleChange.bind(this)
        this.submitHeader = this.submitHeader.bind(this)
        this.handleKeyDown = this.handleKeyDown.bind(this)
        this.removeImage = this.removeImage.bind(this)
        this.handleTypeChange = this.handleTypeChange.bind(this)
        this.handleInputTypeChange = this.handleInputTypeChange.bind(this)
    }

    async componentDidMount() {
        this.setState({
            type: fe_types,
            input_type: fe_input_types
        });
        if (this.props.id != null) {
            axios.get(`${BASE_API_URL}/header-master/detail/${this.props.id}`)
                .then((res) => {
                    
                    const { header } = res.data.data;
                    const existingHeaderPayLoad = this.props.headerPayLoad
                    this.props.setHeaderMaster(existingHeaderPayLoad)

                    header.is_archived = header.is_archived == 1 ? true : false
                    const imagePreview = _.cloneDeep(header.icon)
                    delete header.icon
                    this.setState({
                        data: header,
                        image_preview: imagePreview,
                        headerPayLoad: existingHeaderPayLoad,
                        selected: {
                            type: { label: header.type, value: header.type },
                            input_type: { label: header.input_type, value: header.input_type }
                        }
                    })
                })
        } else {
            const existingHeaderPayLoad = this.props.headerPayLoad
            existingHeaderPayLoad.header = {} 
            this.props.setHeaderMaster(existingHeaderPayLoad)
            this.setState({
                headerPayLoad: existingHeaderPayLoad
            })
        }
    }

    handleChange(event, valueObj) {
        const { data } = this.state
        if (typeof event.target != 'undefined' && event.target.name == 'icon') {
            data[event.target.name] = event.target.files[0]
            this.setState({
                data,
                remove_image: true,
                image_preview: URL.createObjectURL(event.target.files[0])
            })
        }
        else{
            data[event] = valueObj != null && typeof valueObj.value != 'undefined' ? valueObj.value : valueObj
            this.setState({
                data
            })
        }
        // else {
        //     if (event == 'type' || event == 'input_type') {
        //         const { selected } = this.state
        //         selected[event] = valueObj
        //         this.setState({
        //             selected
        //         })
        //     }
        //     data[event] = valueObj != null && typeof valueObj.value != 'undefined' ? valueObj.value : valueObj
        //     this.setState({
        //         data
        //     })
        // }
    }

    handleTypeChange(valueObj) {
        const { data, selected } = this.state;
        selected['type'] = valueObj
        this.setState({
            selected
        })
        data['type'] = valueObj != null && typeof valueObj.value != 'undefined' ? valueObj.value : valueObj;
        this.setState({
            data
        })
    }

    handleInputTypeChange(valueObj) {
        const { data, selected } = this.state;
        selected['input_type'] = valueObj
        this.setState({
            selected
        })
        data['input_type'] = valueObj != null && typeof valueObj.value != 'undefined' ? valueObj.value : valueObj;
        this.setState({
            data
        })
    }

    removeImage(e) {
        e.preventDefault()
        const { data } = this.state
        delete data.icon
        this.setState({
            remove_image: true,
            data,
            image_preview: ''
        })
    }

    submitHeader() {
        const { data } = this.state
        data.remove_image = this.state.remove_image

        // const { selected } = this.state
        // data['type'] = selected.type != null && typeof selected.type.value != 'undefined' ? selected.type.value : selected.type;
        // data['input_type'] = selected.input_type != null && typeof selected.input_type.value != 'undefined' ? selected.input_type.value : selected.input_type;

        const originPayload = {
            header: _.cloneDeep(data)
        }
        delete originPayload.header.icon

        const payload = new FormData()
        payload.append('data', JSON.stringify(originPayload))
        if (typeof data.icon != 'undefined' && data.icon != null) {
            payload.append('icon', data.icon)
        }
        postService({
            url: this.props.id != null ? `/header-master/update/${this.props.id}` : '/header-master/create',
            data: payload,
            callback: (response) => {
                if (response.data.code == '2000') {
                    Router.push('/module/header-master/index')
                } else if (response.data.code == '4004' && response.data.message == 'Please fill required marker field!') {
                    this.props.errorToast(response.data.data._errorMessages.map(e => e.message).join(", "))
                } else {
                    this.props.errorToast(response.data.message)
                }
            }
        })
    }

    handleKeyDown (e) {
        if (e.key === 'Enter') {
          e.preventDefault();
          e.stopPropagation();
        }
    }

    render() {
        return (
            <AutoForm
                schema={Schema}
                onSubmit={this.submitHeader}
                onChange={this.handleChange}
                onKeyDown={this.handleKeyDown}
            >
                <div className="mg-b-15">
                    <div className="row mt-5">
                        <div className="col-sm-12 text-capitalize">
                            <ErrorsField />
                        </div>
                        <div className="col-md-12">
                            <AutoField name="name" default={this.state.data.name || ''} value={this.state.data.name || ''} />
                        </div>
                        <div className="col-md-12">
                            <AutoField
                                name="type"
                                ref={ref => {
                                    this.selectType = ref
                                }}
                                isClearable={true}
                                isSearchable={true}
                                onChange={this.handleTypeChange}
                                value={this.state.selected.type}
                                options={this.state.type}
                                placeholder="Select Header Type"
                            />
                        </div>
                        <div className="col-md-12">
                            <AutoField
                                name="input_type"
                                ref={ref => {
                                    this.selectInputType = ref
                                }}
                                isClearable={true}
                                isSearchable={true}
                                onChange={this.handleInputTypeChange}
                                value={this.state.selected.input_type}
                                options={this.state.input_type}
                                placeholder="Select Header Input Type"
                            />
                        </div>
                        <div className="col-md-12">
                            <AutoField name="is_archived" value={this.state.data.is_archived} placeholder="Select Status" />
                        </div>
                        <div className="col-md-12">
                            <div className="row form-group" key="0">
                                <div className="col-md-2 pr-0">
                                    <img src={this.state.image_preview} className="img-fluid img-thumbnail" />
                                </div>
                                <div className="col-md-1">
                                    <button className="btn py-0 px-0" onClick={this.removeImage}>
                                        <span className="navi-icon">
                                            <i className="flaticon2-cross icon-nm"></i>
                                        </span>
                                    </button>
                                </div>
                                <div className="col-md-9">
                                    <label className="text-capitalize">icon</label>
                                    <div className="custom-file">
                                        <input type="file" name="icon" className="custom-file-input" id="customFile" onChange={this.handleChange} />
                                        <label className="custom-file-label custom-file-label-primary" htmlFor="customFile">{typeof this.state.data.icon == 'undefined' || (typeof this.state.data.icon != 'undefined' && typeof this.state.data.icon.name == 'undefined') ? 'Choose File...' : this.state.data.icon.name}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-layout-footer float-right">
                        <SubmitField value="Save" />
                    </div>
                </div>
            </AutoForm>
        )
    }
}

JobForm.defaultProps = {
    id: null,
    headerPayLoad: {
        specs: [],
        headers: [],
    }
}

const mapStateToProps = state => ({
    headerPayLoad: state.job.headerPayLoad,
    isAdmin: state.auth.is_admin
})

const mapDispatchToProps = dispatch => ({
    setHeaderMaster: data => dispatch(setHeaderMaster(data)),
    successToast: message => dispatch(successToast(message)),
    errorToast: message => dispatch(errorToast(message)),
})

export default connect(mapStateToProps, mapDispatchToProps)(JobForm)
