import { string, array, object } from 'prop-types'
import Menu from './menu/index'
import Link from 'next/link'

const Aside = (props) => (
	<div className="aside aside-left d-flex flex-column" id="kt_aside">
		<div className="aside-brand d-flex flex-column align-items-center flex-column-auto pt-5 pt-lg-18 pb-10">
			<div className="btn p-0 symbol symbol-60 symbol-light-primary" href="./" id="kt_quick_user_toggle">
				<div className="symbol-label">
					<img alt="Logo" src="/static/one/media/svg/avatars/001-boy.svg" className="h-75 align-self-end" />
				</div>
			</div>
		</div>

		<div className="aside-nav d-flex flex-column align-items-center flex-column-fluid pb-10">
			<ul className="nav flex-column">
				{
					props.lists.map((menu, index) => (
						<Menu {...menu} key={index} active={menu.type == props.activeMenu} showMenu={(typeof menu.permissionCondition != 'undefined' && menu.permissionCondition(props.roleAccessed, props.permissions)) || typeof menu.permissionCondition == 'undefined'} />
					))
				}
			</ul>
		</div>

		{/* <div className="aside-footer d-flex flex-column align-items-center flex-column-auto py-8">
			<div className="dropdown" data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window" title="Languages">
				<a href="#" className="btn btn-icon btn-hover-text-primary btn-lg" data-toggle="dropdown" data-offset="0px,0px">
					<img className="w-20px h-20px rounded" src="/static/one/media/svg/flags/226-united-states.svg" alt="image" />
				</a>

				<div className="dropdown-menu p-0 m-0 dropdown-menu-anim-up dropdown-menu-sm dropdown-menu-left">
					<ul className="navi navi-hover py-4">
						<li className="navi-item">
							<a href="#" className="navi-link">
								<span className="symbol symbol-20 mr-3">
									<img src="/static/one/media/svg/flags/226-united-states.svg" alt="" />
								</span>
								<span className="navi-text">English</span>
							</a>
						</li>

						<li className="navi-item active">
							<a href="#" className="navi-link">
								<span className="symbol symbol-20 mr-3">
									<img src="/static/one/media/svg/flags/209-indonesia.svg" alt="" />
								</span>
								<span className="navi-text">Indonesia</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div> */}
	</div>
)

Aside.propTypes = {
	selectedMenu: string,
	activeMenu: string,
	roleAccessed: string,
	permissions: object,
	lists: array
}

export default Aside