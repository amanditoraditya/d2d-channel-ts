import React from "react";

const MeetCall = (props) => {

  const jitsiContainerId = "jitsi-container-id";
  const [jitsi, setJitsi] = React.useState({});

  const loadJitsiScript = () => {
    let resolveLoadJitsiScriptPromise = null;

    const loadJitsiScriptPromise = new Promise(resolve => {
      resolveLoadJitsiScriptPromise = resolve;
    });

    var meta = document.createElement('meta');
    meta.httpEquiv = "viewport";
    meta.content = "initial-scale=1.0, maximum-scale=1.0";
    document.getElementsByTagName('head')[0].appendChild(meta);

    const script = document.createElement("script");
    script.src = "https://meetd.d2d.co.id/external_api.js";
    script.async = true;
    script.onload = () => resolveLoadJitsiScriptPromise(true);
    document.body.appendChild(script);

    return loadJitsiScriptPromise;
  };

  const initialiseJitsi = async () => {
    if (!window.JitsiMeetExternalAPI) {
      await loadJitsiScript();
    }
    const _domain = props.meet_conf.domain
    let _options = props.meet_conf.option
    _options.parentNode = document.getElementById(jitsiContainerId)

    const _jitsi = new window.JitsiMeetExternalAPI(_domain, _options);
    setJitsi(_jitsi);
  };

  React.useEffect(() => {
    initialiseJitsi();

    return () => jitsi?.dispose?.();
  }, []);

  return <div id={jitsiContainerId} style={{ height: "100%", width: "100%", position: "absolute", top: "0", left: "0"}} />;
};

export default MeetCall;
