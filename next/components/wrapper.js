import React from 'react'
import Link from 'next/link'
import { connect } from 'react-redux'
import Router from 'next/router'
import axios from 'axios'
import crypto from 'crypto'
import Head from '../components/head'
import Aside from '../components/aside'
import Footer from '../components/footer'
import UserPanel from '../components/panels/user-panel'
import Notification from '../components/panels/notification'
import QuickAction from '../components/panels/quick-action'
import QuickPanel from '../components/panels/quick-panel'
import HeaderMenu from './menu/headermenu'
import { listGroup } from '../config/menu'

const goaConfig = require('../config')

class Wrapper extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user: null,
      role: null,
      login: false,
      listGroup: listGroup,
      currentSubMenu: []
    }
    this.checkAuth = this.checkAuth.bind(this)
  }

  componentDidMount() {
    let self = this
    axios({
      url: goaConfig.BASE_API_URL + '/checklogin',
      method: 'POST',
      timeout: goaConfig.TIMEOUT
    }).then(function (response) {
      if (response.data.code == '2000') {
        const user = window.localStorage.getItem('user')

        if (user != null && user != '') {
          try {
            let decipher = crypto.createDecipher('aes-256-cbc', goaConfig.APP_KEY)
            let decrypted = decipher.update(user, 'hex', 'utf-8')
            decrypted += decipher.final('utf-8')
            const userData = JSON.parse(decrypted)
            userData.role = userData.is_admin ? 'admin' : 'role'
            const listSubMenu = {}
            const userSlugPermissions = Object.keys(userData.permissions)
            const accessAssigned = []
            let urlSideMenu = ''
            self.state.listGroup.map((list, indexListGroup) => {
              if (typeof list.type != 'undefined' && (typeof list.access == 'undefined' || (typeof list.access != 'undefined' && list.access.indexOf(userData.role) >= 0))) {
                list.childs.map((child) => {
                  if (userData.is_admin || (!userData.is_admin && userSlugPermissions.indexOf(child.slug) >= 0)) {
                    accessAssigned.push(child.slug)
                    if (list.type == 'channel' && urlSideMenu == '') {
                      urlSideMenu = child.subMenus[Object.keys(child.subMenus)[0]]
                    }
                  }
                })
                listSubMenu[list.type] = list.childs
                if (list.type == 'channel') {
                  self.state.listGroup[indexListGroup].url = urlSideMenu != '' ? urlSideMenu : '/dashboard'
                }
              }
            })
            if (self.props.selected != 'dashboard' && accessAssigned.indexOf(self.props.selected) < 0) {
              Router.push({
                pathname: '/dashboard'
              })
            }

            self.setState({
              user: userData,
              login: self.props.isLoginUser,
              listGroup: self.state.listGroup,
              role: userData.role,
              currentSubMenu: typeof listSubMenu[self.props.parent] != 'undefined' ? listSubMenu[self.props.parent] : [],
            })

          } catch (error) {
            console.error("ERROR : ", error)
          }
        }

        self.checkAuth()
      } else {
        window.localStorage.removeItem('user')

        Router.push({
          pathname: '/'
        })
      }
    })
  }

  checkAuth() {
    let self = this
    axios({
      url: goaConfig.BASE_API_URL + '/checkauth',
      method: 'POST',
      data: {
        admin: this.props.admin,
        module: this.props.module,
        permission: this.props.permission
      },
      timeout: goaConfig.TIMEOUT
    }).then(function (response) {
      if (response.data.code == '4003') {
        Router.push({
          pathname: '/forbidden'
        })
      }
    })
  }

  render() {
    return (
      <React.Fragment>
        <Head title={this.props.title} chat={true} />

        <div id="kt_header_mobile" className="header-mobile header-mobile-fixed">
          <a href="./">
            <img alt="Logo" src="/static/one/images/logo-512x512.png" className="logo-default max-h-30px" />
          </a>

          <div className="d-flex align-items-center">
            <button className="btn rounded-0 p-0 burger-icon burger-icon-left" id="kt_header_mobile_toggle">
              <span></span>
            </button>
            {/* <button className="btn btn-hover-icon-primary p-0 ml-5" id="kt_sidebar_mobile_toggle">
              <span className="svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <rect x="0" y="0" width="24" height="24" />
                    <path d="M6,9 L6,15 C6,16.6568542 7.34314575,18 9,18 L15,18 L15,18.8181818 C15,20.2324881 14.2324881,21 12.8181818,21 L5.18181818,21 C3.76751186,21 3,20.2324881 3,18.8181818 L3,11.1818182 C3,9.76751186 3.76751186,9 5.18181818,9 L6,9 Z" fill="#000000" fillRule="nonzero" />
                    <path d="M10.1818182,4 L17.8181818,4 C19.2324881,4 20,4.76751186 20,6.18181818 L20,13.8181818 C20,15.2324881 19.2324881,16 17.8181818,16 L10.1818182,16 C8.76751186,16 8,15.2324881 8,13.8181818 L8,6.18181818 C8,4.76751186 8.76751186,4 10.1818182,4 Z" fill="#000000" opacity="0.3" />
                  </g>
                </svg>
              </span>
            </button> */}
            <button className="btn btn-hover-icon-primary p-0 ml-2" id="kt_aside_mobile_toggle">
              <span className="svg-icon svg-icon-xl">
                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                  <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <polygon points="0 0 24 0 24 24 0 24" />
                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fillRule="nonzero" opacity="0.3" />
                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fillRule="nonzero" />
                  </g>
                </svg>
              </span>
            </button>
          </div>
        </div>

        <div className="d-flex flex-column flex-root">
          <div className="d-flex flex-row flex-column-fluid page">
            <Aside
              permissions={this.state.user != null && typeof this.state.user.permissions != 'undefined' ? this.state.user.permissions : {}}
              roleAccessed={this.state.role}
              lists={this.state.listGroup}
              activeMenu={this.props.parent}
            />

            <div className="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
              <HeaderMenu
                permissions={this.state.user != null && typeof this.state.user.permissions != 'undefined' ? (this.state.role == 'admin' ? true : this.state.user.permissions) : false}
                lists={this.state.currentSubMenu}
                selected={this.props.selected}
              />
              {/* <Header selected={ this.props.selected } /> */}

              {this.props.onCard ? (
                this.state.login == 1 &&
                (<div className="d-flex flex-column-fluid">
                  <div className="container-fluid">
                    {
                      typeof this.props.toast.message != 'undefined' && this.props.toast.message != '' && (
                        <div className={`alert alert-solid alert-${this.props.toast.type} error-bar`}>{this.props.toast.message}</div>
                      )
                    }
                    <div className="card card-custom gutter-b">
                      <div className="card-header flex-wrap py-3">
                        <div className="card-title">
                          <h3 className="card-label">{this.props.headerCard}</h3>
                        </div>
                        <div className="card-toolbar">
                          <Link href={`/module/${this.props.selected}/index`} as={`/${this.props.selected}`} passHref>
                            <a href={`/module/${this.props.selected}`} className="btn btn-sm btn-dark"><i className="fa fa-arrow-left"></i> Back</a>
                          </Link>
                        </div>
                      </div>

                      <div className="card-body">
                        {this.props.children}
                      </div>
                    </div>
                  </div>
                </div>)
              ) : this.props.children}

              <Footer />
            </div>

            {/* <Sidebar /> */}
          </div>
        </div>

        <UserPanel />
        <Notification />
        <QuickAction />
        <QuickPanel />


        <div id="kt_scrolltop" className="scrolltop">
          <span className="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
              <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <polygon points="0 0 24 0 24 24 0 24" />
                <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1" />
                <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fillRule="nonzero" />
              </g>
            </svg>
          </span>
        </div>
      </React.Fragment>
    )
  }
}

Wrapper.defaultProps = {
  onCard: false
}

const mapStateToProps = state => ({
  toast: state.general.toast,
  isLoginUser: state.auth.login
})

export default connect(mapStateToProps, {})(Wrapper)