import { string, object } from 'prop-types'

const SubHeaderMenu = (props) => (
    <li className={`menu-item menu-item-submenu menu-item-rel ${props.selected == props.slug ? " menu-item-active" : ""} ${!((typeof props.permissions == 'object' && typeof props.permissions[props.slug] != 'undefined') || (typeof props.permissions == 'boolean' && props.permissions)) && 'hidden'}`} data-menu-toggle="click" aria-haspopup="true">
        <a href={typeof props.url != 'undefined' ? props.url : '#'} className={`menu-link${typeof props.url != 'undefined' ? '' : ' menu-toggle'}`}>
            <span className="menu-text">{props.text}</span>
            <i className="menu-arrow"></i>
        </a>
        {typeof props.subMenus != 'undefined' &&
            <div className="menu-submenu menu-submenu-classNameic menu-submenu-left">
                <ul className="menu-subnav">
                    {
                        Object.keys(props.subMenus).map((key, index) => {
                            let payloadList = {
                                text: '',
                                icon: ''
                            }
                            switch (key) {
                                case 'list':
                                    payloadList = {
                                        text: `List ${props.text}`,
                                        icon: 'list'
                                    }
                                    break;
                                case 'create':
                                    payloadList = {
                                        text: `Add New ${props.text}`,
                                        icon: 'plus'
                                    }
                                    break;
                            }
                            return (
                                <li className="menu-item" data-menu-toggle="hover" aria-haspopup="true" key={index}>
                                    <a href={props.subMenus[key]} className="menu-link">
                                        <i className={`fa fa-${payloadList.icon}`}></i>
                                        <span className="menu-text">{payloadList.text}</span>
                                    </a>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        }
    </li>
)

SubHeaderMenu.propTypes = {
    text: string,
    slug: string,
    url: string,
    subMenus: object,
    // permissions: object,
}

export default SubHeaderMenu