import { array, object } from 'prop-types'
import SubHeaderMenu from './subheadermenu'

const HeaderMenu = (props) => (
    <div id="kt_header" className="header header-fixed">
        <div className="header-wrapper rounded-top-xl d-flex flex-grow-1 align-items-center">
            <div className="container-fluid d-flex align-items-center justify-content-end justify-content-lg-between flex-wrap">
                <div className="header-menu-wrapper header-menu-wrapper-left py-lg-2" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu" className="header-menu header-menu-mobile header-menu-layout-default">
                        <ul className="menu-nav">
                            {
                                typeof props.lists != 'undefined' && props.lists.length > 0 ? props.lists.map((list, index) => (
                                    <SubHeaderMenu selected={props.selected} {...list} key={index} permissions={props.permissions} />
                                )) : ''
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

HeaderMenu.propTypes = {
    lists: array,
    // permissions: object
}
export default HeaderMenu