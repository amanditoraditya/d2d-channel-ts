import { string, bool } from 'prop-types'

const Menu = (props) => (
    <li className={`nav-item mb-2 ${!props.showMenu && 'hidden'}`} data-toggle="tooltip" data-placement="right" data-container="body" data-boundary="window" title={props.title}>
        <a href={props.url} className={`nav-link btn btn-icon btn-hover-text-primary btn-lg ${props.active ? 'active' : ''}`}>
            <span className="svg-icon svg-icon-xxl">
                <i className={`fa fa-${props.icon}`}></i>
            </span>
        </a>
    </li>
)

Menu.propTypes = {
	title: string,
	url: string,
	active: bool,
	showMenu: bool,
	icon: string,
}
export default Menu