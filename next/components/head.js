import NextHead from 'next/head'
import { string } from 'prop-types'
import crypto from 'crypto'

const goaConfig = require('../config')

const defaultTitle = 'D2D'
const defaultDescription = 'D2D'
const defaultKeyword = ''

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			user: null
		}
	}
	
	static propTypes = {
		title: string,
		description: string,
		keyword: string
	}
	
	componentDidMount() {
		let user = localStorage.getItem('user')
		
		if(user) {
			let decipher = crypto.createDecipher('aes-256-cbc', goaConfig.APP_KEY)
			let decrypted = decipher.update(user, 'hex', 'utf-8')
			decrypted += decipher.final('utf-8')
			
			this.setState({
				user: JSON.parse(decrypted)
			})
		}
	}
	
	render() {
		const props = this.props
		
		return (
			<NextHead>
				<meta charSet="UTF-8" />
				<title>{props.title || defaultTitle} - D2D</title>
				<meta name="description" content={props.description || props.title ? props.title + ' - ' + defaultDescription : defaultDescription} />
				<meta name="keywords" content={props.keywords || defaultKeyword} />
				<meta name="author" content="PT. GUE" />
				<meta name="HandheldFriendly" content="true"/>
				<meta name="language" content="ID" />
				<meta name="revisit-after" content="7" />
				<meta name="webcrawlers" content="all" />
				<meta name="rating" content="general" />
				<meta name="spiders" content="all" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<meta name="google-site-verification" content="" />
				<meta name="msapplication-tap-highlight" content="no" />
				<meta name="mobile-web-app-capable" content="yes" />
				<meta name="application-name" content="Empty Engine Next" />
				<meta name="theme-color" content="#ffffff" />
				<meta name="apple-mobile-web-app-capable" content="yes" />
				<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
				<meta name="apple-mobile-web-app-title" content="Empty Engine Next" />
				<meta name="apple-mobile-web-app-status-bar-style" content="#ffffff" />
				<meta property="og:title" content={props.title || defaultTitle} />
				<meta property="og:description" content={props.description || props.title ? props.title + ' - ' + defaultDescription : defaultDescription} />
				<meta property="og:site_name" content="Empty Engine Next" />
				
				<link rel="apple-touch-icon" href="/static/one/media/logo-129x129.png" />
				<link rel="apple-touch-icon" sizes="129x129" href="/static/one/media/logo-129x129.png" />
				<link rel="apple-touch-icon" sizes="114x114" href="/static/one/media/logo-114x114.png" />
				<link rel="apple-touch-icon" sizes="120x120" href="/static/one/media/logo-120x120.png" />		
				<link rel="apple-touch-icon" sizes="152x152" href="/static/one/media/logo-192x192.png" />
				<link rel="shortcut icon" href="/static/one/media/logos/D2D.ico" />
				
				<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
				<link type="text/css" rel="stylesheet" href="/static/one/plugins/custom/datatables/datatables.bundle.css" />
				<link type="text/css" rel="stylesheet" href="/static/one/plugins/custom/fullcalendar/fullcalendar.bundle.css" />
				<link type="text/css" rel="stylesheet" href="/static/one/plugins/global/plugins.bundle.css" />
				<link type="text/css" rel="stylesheet" href="/static/one/plugins/custom/prismjs/prismjs.bundle.css" />
				<link type="text/css" rel="stylesheet" href="/static/one/css/style.bundle.css" />
				
				<link rel="dns-prefetch" href="//goapotik.oss-ap-southeast-5.aliyuncs.com" />
				<link rel="dns-prefetch" href="//www.google-analytics.com" />
				<link rel="dns-prefetch" href="//www.googletagmanager.com" />
				<link rel="manifest" href="/manifest.json" />
				<link rel="manifest" href="/manifest.webmanifest" />
				
				<script dangerouslySetInnerHTML={{__html: `
					var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };`
				}}></script>
				
				<script src="https://unpkg.com/axios/dist/axios.min.js" async=""></script>
				<script type="text/javascript" src="/static/one/plugins/global/plugins.bundle.js"></script>
				<script type="text/javascript" src="/static/one/plugins/custom/prismjs/prismjs.bundle.js"></script>
				
				<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"></script>
				
				{this.state.user != null && goaConfig.ONESIGNAL_APPID &&
					<script dangerouslySetInnerHTML={{__html: `
						var OneSignal = window.OneSignal || [];
						
						if (OneSignal.installServiceWorker) {
							OneSignal.installServiceWorker();
						} else {
							if (navigator.serviceWorker) {
								navigator.serviceWorker.register("` + goaConfig.BASE_URL + `/OneSignalSDKWorker.js?appId=` + goaConfig.ONESIGNAL_APPID + `").then(function (registration) {
									//console.log(registration);
								});
							}
						}
						
						OneSignal.push(function() {
							OneSignal.init({
								appId: "` + goaConfig.ONESIGNAL_APPID + `",
								allowLocalhostAsSecureOrigin: true,
								autoResubscribe: true,
								notifyButton: {
									enable: true,
								},
							});
							
							OneSignal.push(["getNotificationPermission", function(permission) {
								console.log("Site Notification Permission:", permission);
								OneSignal.showNativePrompt();
								OneSignal.setSubscription(true);
							}]);
							
							OneSignal.sendTags({
								email: '` + this.state.user.user.email + `',
							}).then(function(tagsSent) {
								//console.log("tagsSent: ", tagsSent);
							});
							
							OneSignal.on('subscriptionChange', function (isSubscribed) {
								if (isSubscribed) {
									console.log(isSubscribed);
									OneSignal.getUserId(function(playerId) {
										//console.log("OneSignal Player ID:", playerId);
									});
								} else {
									//console.log('isSubscribed: false');
								}
							});
							
							OneSignal.setExternalUserId(` + this.state.user.user.id + `);
						});`
					}}></script>
				}
			</NextHead>
		)
	}
}