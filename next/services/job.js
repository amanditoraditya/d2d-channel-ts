import { postService, getService } from './base-service'

export const create = ({ data, callback }) => {
    postService({
        url: '/channel-job/create',
        data,
        callback
    })
}