import apiConfig from '../config'
import axios from 'axios'

export const getService = (option) => {
    const { url, failed, success } = option
    return axios({
        method: 'GET',
        url: `${apiConfig.BASE_API_URL}${url}`,
        timeout: apiConfig.TIMEOUT
    }).then(({ data }) => {
        if (typeof success != 'undefined') {
            success(data)
        }
    }).catch((error) => {
        const { response } = error
        if (typeof failed != 'undefined') {
            const { data } = response
            failed(response.status > 500 ? 'Something went wrong' : data.message, data)
        }
    })
}

export const postService = (option) => {
    axios({
        method: 'POST',
        url: `${apiConfig.BASE_API_URL}${option.url}`,
        data: typeof option.data != 'undefined' ? option.data : {},
        contentType: 'application/json',
        timeout: apiConfig.TIMEOUT
    }).then((res) => {
        if (typeof option.callback != 'undefined') {
            option.callback(res)
        }
        if (typeof option.success != 'undefined') {
            option.success(res)
        }
    }).catch((error) => {
        const { response } = error
        if (typeof option.failed != 'undefined') {
            const { data } = response
            option.failed(response.status > 500 ? 'Something went wrong' : data.message, data)
        }
    })
}