'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelMenuSchema extends Schema {
    up() {
        this.table('channel_menus', (table) => {
            // alter table
            table.dateTime("deleted_at").defaultTo(null);
        })
    }

    down() {
        this.table('channel_menus', (table) => {
            // reverse alternations
            table.dropColumn('deleted_at')
        })
    }
}

module.exports = ChannelMenuSchema
