'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobsSchema extends Schema {
  up () {
    this.table('channel_jobs', (table) => {
      // alter table
      table.integer('job_type_id').unsigned().references('id').inTable('channel_job_types').after('title')
    })
  }

  down () {
    this.table('channel_jobs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelJobsSchema
