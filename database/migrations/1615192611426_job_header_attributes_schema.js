'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JobHeaderAttributesSchema extends Schema {
  up () {
    this.create('job_header_attributes', (table) => {
      table.increments()
      table.string('name', 100).notNullable()
      table.string('icon', 255).notNullable()
      table.string('input_type', 100).nullable()
      table.integer('position', 5).defaultTo(0)
      table.boolean('is_archived').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('job_header_attributes')
  }
}

module.exports = JobHeaderAttributesSchema
