'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MenusSchema extends Schema {
  up () {
    this.table('menus', (table) => {
      // alter table
      table.string('disabled_icon', 255).nullable().after('icon')
    })
  }

  down () {
    this.table('menus', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MenusSchema
