'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobsSchema extends Schema {
  up () {
    this.table('channel_jobs', (table) => {
      // alter table
      table.string('gmt_tz', 10).notNullable().defaultTo('+07:00').after('due_date')
    })
  }

  down () {
    this.table('channel_jobs', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelJobsSchema
