'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelsSchema extends Schema {
  up () {
    this.table('channels', (table) => {
       // alter table
       table.string('disabled_logo', 255).nullable().after('logo')
    })
  }

  down () {
    this.table('channels', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelsSchema
