'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BrochuresSchema extends Schema {
  up () {
    this.table('brochures', (table) => {
      // alter table
      table.dropUnique('title');
    })
  }

  down () {
    this.table('brochures', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BrochuresSchema
