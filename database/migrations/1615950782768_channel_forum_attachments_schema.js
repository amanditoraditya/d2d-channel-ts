'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumAttachmentsSchema extends Schema {
  up () {
    this.table('channel_forum_attachments', (table) => {
      // alter table
      table.boolean('is_archived').defaultTo(false).after('attachment_url')
    })
  }

  down () {
    this.table('channel_forum_attachments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelForumAttachmentsSchema
