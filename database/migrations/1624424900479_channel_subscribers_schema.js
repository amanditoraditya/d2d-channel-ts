'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelSubscribersSchema extends Schema {
  up () {
    this.table('channel_subscribers', (table) => {
      table.enum('referral_type', ['qr', 'link']).nullable()
      table.integer('referral_admin_id').unsigned().references('id').inTable('user_channels').nullable()
    })
  }

  down () {
    this.table('channel_subscribers', (table) => {
      table.dropForeign('referral_admin_id', 'channel_subscribers_referral_admin_id_foreign')
      table.dropColumn('referral_type')
      table.dropColumn('referral_admin_id')
    })
  }
}

module.exports = ChannelSubscribersSchema
