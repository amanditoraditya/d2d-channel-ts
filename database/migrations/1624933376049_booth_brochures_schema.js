'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BoothBrochuresSchema extends Schema {
  up () {
    this.create('brochures', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.string('title', 255).unique()
      table.text('description', 'longtext').nullable()
      table.string('filename', 255).nullable()
      table.string('cover', 255).nullable()
      table.boolean('is_published').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.dateTime("deleted_at").defaultTo(null);
      table.timestamps()
    })
  }

  down () {
    this.drop('brochures')
  }
}

module.exports = BoothBrochuresSchema
