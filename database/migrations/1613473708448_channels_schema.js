'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelsSchema extends Schema {
  up () {
    this.create('channels', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_type_id').unsigned().references('id').inTable('channel_types')
      table.string('country_code', 5).notNullable()
      table.string('channel_name', 255).notNullable()
      table.string('logo', 255).nullable()
      table.text('description', 'longtext').nullable()
      table.boolean('is_admin').defaultTo(false)
      table.boolean('status').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channels')
  }
}

module.exports = ChannelsSchema
