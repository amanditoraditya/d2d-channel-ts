'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelRolesSchema extends Schema {
  up() {
    this.table('channel_roles', (table) => {
      table.longText('permissions').nullable()
    })
  }

  down() {
    this.table('channel_roles', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelRolesSchema
