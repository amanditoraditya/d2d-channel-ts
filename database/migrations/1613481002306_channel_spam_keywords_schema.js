'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelSpamKeywordsSchema extends Schema {
  up () {
    this.create('channel_spam_keywords', (table) => {
      table.increments()
      table.string('keyword', 255).notNullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_spam_keywords')
  }
}

module.exports = ChannelSpamKeywordsSchema
