'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumCommentsSchema extends Schema {
  up () {
    this.table('channel_forum_comments', (table) => {
      table.integer('reply_to', 100).defaultTo(0).after('parent_id')
      // alter table
    })
  }

  down () {
    this.table('channel_forum_comments', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelForumCommentsSchema
