'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JobHeaderAttributesSchema extends Schema {
  up () {
    this.table('job_header_attributes', (table) => {
      // alter table
      table.string('type', 255).defaultTo('job').notNullable().after('name')
    })
  }

  down () {
    this.table('job_header_attributes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = JobHeaderAttributesSchema
