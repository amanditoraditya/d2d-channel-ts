'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MenusSchema extends Schema {
  up () {
    this.create('menus', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.string('name', 255).unique()
      table.string('icon', 255).notNullable()
      table.text('description', 'longtext').nullable()
      table.string('link', 255).nullable()
      table.string('target', 255).nullable()
      table.boolean('status').defaultTo(true)
      table.boolean('is_published').defaultTo(false)
      table.integer('parent_id', 100).defaultTo(0)
      table.integer('position', 5).defaultTo(0)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('menus')
  }
}

module.exports = MenusSchema
