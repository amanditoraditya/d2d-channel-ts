'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumPostsSchema extends Schema {
  up () {
    this.table('channel_forum_posts', (table) => {
      // alter table
      table.text('short_description', 'longtext').nullable().after('description')
    })
  }

  down () {
    this.table('channel_forum_posts', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelForumPostsSchema
