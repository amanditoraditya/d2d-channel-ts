'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobHeaderAttributesSchema extends Schema {
  up () {
    this.table('channel_job_header_attributes', (table) => {
      // alter table
      table.boolean('is_archived').defaultTo(false).after('value')
    })
  }

  down () {
    this.table('channel_job_header_attributes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChannelJobHeaderAttributesSchema
