'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelsSchema extends Schema {
    up() {
        this.table('channels', (table) => {
            table.dateTime("deleted_at").defaultTo(null);
        })
    }

    down() {
        this.table('channels', (table) => {
            table.dropColumn('deleted_at')
        })
    }
}

module.exports = ChannelsSchema
