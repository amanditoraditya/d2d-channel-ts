'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobHeaderAttributesSchema extends Schema {
  up () {
    this.create('channel_job_header_attributes', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_id').unsigned().references('id').inTable('channel_jobs')
      table.integer('job_header_attribute_id').unsigned().references('id').inTable('job_header_attributes')
      table.string('value', 255).notNullable()
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['channel_job_id', 'job_header_attribute_id'], 'cchanneljob_header_attributes_unique')
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_header_attributes')
  }
}

module.exports = ChannelJobHeaderAttributesSchema
