'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserChannelsSchema extends Schema {
  up () {
    this.create('user_channels', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.string('email', 255).unique()
      table.string('password', 60).notNullable()
      table.boolean('block').defaultTo(false)
      table.string('first_name', 255).notNullable()
      table.string('last_name', 255).nullable()
      table.boolean('is_archived').defaultTo(false)
      table.string('current_token', 255).nullable()
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_channels')
  }
}

module.exports = UserChannelsSchema
