'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobsSchema extends Schema {
  up () {
    this.table('channel_jobs', (table) => {
      // alter table
        table.string('registration_url').nullable()
        table.string('registration_button_caption').nullable()
    })
  }

  down () {
    this.table('channel_jobs', (table) => {
      // reverse alternations
      table.dropColumn('registration_url')
      table.dropColumn('registration_button_caption')
    })
  }
}

module.exports = ChannelJobsSchema
