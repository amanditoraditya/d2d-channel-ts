'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelSubscribersSchema extends Schema {
  up () {
    this.create('channel_subscribers', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.uuid('uid').notNullable()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.boolean('blocked').defaultTo(false)
      table.string('source', 10).notNullable()
      table.boolean('is_unsubscribed').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['uid', 'channel_id'])
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_subscribers')
  }
}

module.exports = ChannelSubscribersSchema
