'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TemporaryMembersSchema extends Schema {
  up () {
    this.table('temporary_members', (table) => {
      // alter table
      table.string('nonspesialization', 255).nullable().after('spesialization')
    })
  }

  down () {
    this.table('temporary_members', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TemporaryMembersSchema
