'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumAttachmentsSchema extends Schema {
  up () {
    this.create('channel_forum_attachments', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('ref_id', 100).defaultTo(0)
      table.integer('channel_forum_content_type_id').unsigned().references('id').inTable('channel_forum_content_types')
      table.string('attachment_name', 255).notNullable()
      table.string('attachment_type', 100).notNullable()
      table.string('attachment_url', 255).notNullable()
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_forum_attachments')
  }
}

module.exports = ChannelForumAttachmentsSchema
