'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelRolesSchema extends Schema {
  up () {
    this.create('channel_roles', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.string('name', 255).notNullable()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.boolean('status').defaultTo(true)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_roles')
  }
}

module.exports = ChannelRolesSchema
