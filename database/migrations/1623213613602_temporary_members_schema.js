'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TemporaryMembersSchema extends Schema {
  up () {
    this.table('temporary_members', (table) => {
      // alter table
        table.string('phone').nullable()
        table.string('home_location').nullable()
    })
  }

  down () {
    this.table('temporary_members', (table) => {
      // reverse alternations
      table.dropColumn('phone')
      table.dropColumn('home_location')
    })
  }
}

module.exports = TemporaryMembersSchema
