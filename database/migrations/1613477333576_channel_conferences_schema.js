'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferencesSchema extends Schema {
  up () {
    this.create('channel_conferences', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_id').unsigned().references('id').inTable('channels')
      table.string('title', 255).notNullable()
      table.text('description', 'longtext').nullable()
      table.boolean('is_published').defaultTo(false)
      table.boolean('is_instant').defaultTo(false)
      table.string('url', 255).nullable()
      table.string('meet_key', 100).nullable()
      table.string('meet_id', 100).nullable()
      table.datetime('start_date').nullable()
      table.datetime('end_date').nullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_conferences')
  }
}

module.exports = ChannelConferencesSchema
