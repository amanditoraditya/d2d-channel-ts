'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecordLogSchema extends Schema {
  up () {
    this.create('record_logs', (table) => {
      table.increments()
      table.integer('user_channel_id').unsigned().references('id').inTable('user_channels')
      table.integer('foreign_id')
      table.string('table_name')
      table.enum('transaction_type', ['CREATE', 'UPDATE', 'DELETE', 'RESTORE'])
      table.longText('last_payload').nullable()
      table.longText('latest_payload').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('record_logs')
  }
}

module.exports = RecordLogSchema
