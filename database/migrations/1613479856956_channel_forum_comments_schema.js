'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelForumCommentsSchema extends Schema {
  up () {
    this.create('channel_forum_comments', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('parent_id', 100).defaultTo(0)
      table.integer('channel_forum_post_id').unsigned().references('id').inTable('channel_forum_posts')
      table.integer('channel_subscriber_id').unsigned().references('id').inTable('channel_subscribers')
      table.text('comment', 'longtext').notNullable()
      table.boolean('is_spammed').defaultTo(false)
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_forum_comments')
  }
}

module.exports = ChannelForumCommentsSchema
