'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TemporaryMembersSchema extends Schema {
  up () {
    this.table('temporary_members', (table) => {
      // alter table
      table.string('spesialization', 255).nullable().after('name')
    })
  }

  down () {
    this.table('temporary_members', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TemporaryMembersSchema
