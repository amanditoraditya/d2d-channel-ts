'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SpecialistsSchema extends Schema {
  up () {
    this.create('specialists', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.string('title', 100).notNullable()
      table.text('meta_title', 'longtext').nullable()
      table.integer('parent_id', 100).defaultTo(0)
      table.boolean('is_archived').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('specialists')
  }
}

module.exports = SpecialistsSchema
