'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobApplicantStatusesSchema extends Schema {
  up () {
    this.create('channel_job_applicant_statuses', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_applicant_id').unsigned().references('id').inTable('channel_job_applicants')
      table.integer('status_step', 50).notNullable()
      table.boolean('is_final').defaultTo(true)
      table.string('status', 25).notNullable()
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['channel_job_applicant_id', 'status_step'], 'cjob_applicant_statuses_cjob_applicant_id_status_step_unique')
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_applicant_statuses')
  }
}

module.exports = ChannelJobApplicantStatusesSchema
