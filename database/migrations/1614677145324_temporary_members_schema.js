'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TemporaryMembersSchema extends Schema {
  up () {
    this.create('temporary_members', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.string('email', 255).nullable()
      table.string('name', 255).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('temporary_members')
  }
}

module.exports = TemporaryMembersSchema
