'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobSpecsSchema extends Schema {
  up () {
    this.create('channel_job_specs', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_id').unsigned().references('id').inTable('channel_jobs')
      table.string('title', 255).notNullable()
      table.string('icon', 255).nullable()
      table.boolean('is_doc_needed').defaultTo(false)
      table.text('description', 'longtext').nullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_specs')
  }
}

module.exports = ChannelJobSpecsSchema
