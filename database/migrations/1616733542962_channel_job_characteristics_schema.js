'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelJobCharacteristicsSchema extends Schema {
  up () {
    this.create('channel_job_characteristics', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('channel_job_id').unsigned().references('id').inTable('channel_jobs')
      table.integer('ref_id', 100).defaultTo(0)
      table.string('ref_type', 255).notNullable() // [SPECIALIST]
      table.string('value', 255).nullable()
      table.boolean('is_archived').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_job_characteristics')
  }
}

module.exports = ChannelJobCharacteristicsSchema
