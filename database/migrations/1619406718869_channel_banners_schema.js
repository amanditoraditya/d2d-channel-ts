'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelBannersSchema extends Schema {
    up() {
        this.table('channel_banners', (table) => {
            // alter table
            table.dateTime("deleted_at").defaultTo(null);
        })
    }

    down() {
        this.table('channel_banners', (table) => {
            // reverse alternations
            table.dropColumn('deleted_at')
        })
    }
}

module.exports = ChannelBannersSchema
