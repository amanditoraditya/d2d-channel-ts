'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class JobHeaderAttributesSchema extends Schema {
  up () {
    this.table('job_header_attributes', (table) => {
      // alter table
      table.text('icon', 'longtext').notNullable().alter()
    })
  }

  down () {
    this.table('job_header_attributes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = JobHeaderAttributesSchema
