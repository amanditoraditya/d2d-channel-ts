'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferenceAttendeesSchema extends Schema {
  up () {
    this.create('channel_conference_attendees', (table) => {
      table.increments()
      table.uuid('xid').unique()
      table.integer('conference_id').unsigned().references('id').inTable('channel_conferences')
      table.integer('channel_subscriber_id').unsigned().references('id').inTable('channel_subscribers')
      table.string('registered_from', 50).notNullable()
      table.boolean('is_notify').defaultTo(false)
      table.boolean('blocked').defaultTo(false)
      table.boolean('is_removed').defaultTo(false)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.unique(['conference_id', 'channel_subscriber_id'], 'cconference_attendees_conference_id_csubscriber_id_unique')
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_conference_attendees')
  }
}

module.exports = ChannelConferenceAttendeesSchema
