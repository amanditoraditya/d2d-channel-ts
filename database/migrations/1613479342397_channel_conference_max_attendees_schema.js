'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelConferenceMaxAttendeesSchema extends Schema {
  up () {
    this.create('channel_conference_max_attendees', (table) => {
      table.increments()
      table.integer('channel_id').unsigned().references('id').inTable('channels').unique()
      table.integer('max', 100).defaultTo(0)
      table.integer('created_by', 100).defaultTo(1)
			table.integer('updated_by', 100).defaultTo(1)
      table.timestamps()
    })
  }

  down () {
    this.drop('channel_conference_max_attendees')
  }
}

module.exports = ChannelConferenceMaxAttendeesSchema
