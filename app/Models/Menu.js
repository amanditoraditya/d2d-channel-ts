'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class Menu extends GueBaseModel {
	channels() {
		return this
			.belongsToMany('App/Models/Channel')
			.pivotModel('App/Models/ChannelMenu')
	}

	_v1_channel_menus() {
		return this
			.hasOne('App/Models/ChannelMenu')
	}
}

module.exports = Menu