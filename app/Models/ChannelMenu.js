'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class ChannelMenu extends GueBaseModel {
    static boot() {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
        // this.addHook('beforeDelete', async (bannerInstance) => {
        //     console.log('hook delete')
        //     console.log(bannerInstance)
        // })
    }
}

module.exports = ChannelMenu