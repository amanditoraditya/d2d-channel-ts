'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const RecordLog = use('App/Models/RecordLog')

class GueBaseModel extends Model {

    static _bootIfNotBooted() {
        if (this.name !== 'GueBaseModel') {
            super._bootIfNotBooted()
        }
    }

    static boot() {
        super.boot()

        this.addHook('afterUpdate', (modelInstance) => {
            this.addHistoryRecord(modelInstance, 'UPDATE')
        })
    }

    static async addHistoryRecord(lucidRecord, type) {
        let dataToInsert = {
            foreign_id: lucidRecord.id,
            table_name: this.table,
            transaction_type: type
        }
        const latestAttributes = typeof lucidRecord.$attributes != 'undefined' ? lucidRecord.$attributes : {}
        const originalAttributes = typeof lucidRecord.$originalAttributes != 'undefined' ? lucidRecord.$originalAttributes : {}
        Object.keys(originalAttributes).map((keyOriginal) => {
            if (typeof latestAttributes[keyOriginal] == 'undefined') {
                latestAttributes[keyOriginal] = originalAttributes[keyOriginal]
            }
        })
        switch (type) {
            case 'UPDATE':
                if (typeof lucidRecord.updated_by != 'undefined') {
                    Object.assign(dataToInsert, {
                        user_channel_id: lucidRecord.updated_by,
                        last_payload: JSON.stringify(originalAttributes),
                        latest_payload: JSON.stringify(latestAttributes)
                    })
                } else {
                    dataToInsert = null
                }
                break;
            case 'CREATE':
                Object.assign(dataToInsert, {
                    latest_payload: latestAttributes
                })
                break;
            default:
                break;
        }
        if (dataToInsert == null) {
            return false
        } else {
            await RecordLog.create(dataToInsert)
            return true
        }
    }
}

module.exports = GueBaseModel
