'use strict'

const Model = use('Model')

class Specialist extends Model {
	static get rules() {
		return {
			title: 'required',
			description: 'required'
		}
	}

	_channel_job_characteristics(){
		return this
			.hasMany('App/Models/ChannelJobCharacteristic')
	}

}

module.exports = Specialist