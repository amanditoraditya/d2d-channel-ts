'use strict'

const Model = use('Model')

class ChannelForumAttachment extends Model {
    _v1_channel_forum_content_type(){
		return this
		.hasOne('App/Models/ChannelForumContentType', 'channel_forum_content_type_id', 'id')
	}
}

module.exports = ChannelForumAttachment