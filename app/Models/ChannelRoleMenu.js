'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class ChannelRoleMenu extends GueBaseModel  {		
	users() {
		return this
			.belongsToMany('App/Models/UserChannel')
			.pivotModel('App/Models/UserChannelRole')
	}
}

module.exports = ChannelRoleMenu
