'use strict'
const Database = use('Database')
const Model = use('Model')

class ChannelForumComment extends Model {

    _v1_channel_forum_like(){
		return this
		.hasOne('App/Models/ChannelForumLike', 'id', 'ref_id')
	}

	_v1_channel_forum_post(){
		return this
		.belongsTo('App/Models/ChannelForumPost')
	}

	_v1_channel_subscriber(){
		return this
		.belongsTo('App/Models/ChannelSubscriber')
		.with('_v1_member')
	}

	_v1_forum_comment_children_preview(){
		return this
		.hasMany('App/Models/ChannelForumComment', 'id', 'parent_id')
		.with('_v1_channel_subscriber')
		.where('is_spammed', 0)
		.where('is_archived', 0)
		.where('id', Database.raw('(SELECT max(id) FROM channel_forum_comments child_group where child_group.parent_id=channel_forum_comments.parent_id GROUP BY parent_id)'))
		.groupBy('parent_id')
		.orderBy('created_at', 'desc')
	}

	_v1_forum_comment_parent(){
		return this
		.belongsTo('App/Models/ChannelForumComment', 'parent_id', 'id')
		.where('is_spammed', 0)
		.where('is_archived', 0)
		.where('parent_id', 0)
	}

}

module.exports = ChannelForumComment