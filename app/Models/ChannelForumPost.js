'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class ChannelForumPost extends GueBaseModel {

    _v1_channel(){
		return this
		.belongsTo('App/Models/Channel')
	}

    _v1_channel_forum_attachments(){
		return this
		.hasMany('App/Models/ChannelForumAttachment', 'id', 'ref_id')
		.orderBy('id', 'desc')
	}

	_v1_channel_forum_like(){
		return this
		.hasOne('App/Models/ChannelForumLike', 'id', 'ref_id')
	}

}

module.exports = ChannelForumPost