'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class ChannelJob extends GueBaseModel {
	static castDates (key, value) {
		return value.toISOString()
	}

    _v1_channel_job_type(){
		return this
			.belongsTo('App/Models/ChannelJobType', 'job_type_id', 'id')
	}

	_v1_channel_job_specs(){
		return this
			.hasMany('App/Models/ChannelJobSpec')
	}

	_v1_channel_job_applicant(){
		return this
			.hasOne('App/Models/ChannelJobApplicant')
	}

	_v1_channel_job_characteristic(){
		return this
			.hasMany('App/Models/ChannelJobCharacteristic')
	}

	_v1_channel_job_header_attributes(){
		return this
		.hasMany('App/Models/ChannelJobHeaderAttribute')
		.with('_v1_job_header_attribute')
	}

	_v1_channel(){
		return this
		.belongsTo('App/Models/Channel')
	}

}

module.exports = ChannelJob