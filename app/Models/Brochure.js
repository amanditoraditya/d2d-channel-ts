'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class Brochure extends GueBaseModel {
    static boot() {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
    }

    channels() {
        return this
            .belongsTo('App/Models/Channel')
    }
}

module.exports = Brochure