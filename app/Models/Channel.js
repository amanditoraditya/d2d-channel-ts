'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class Channel extends GueBaseModel {
		
	// static get rules() {
	// 	return {
	// 		role_id: 'required',
	// 		user_id: 'required'
	// 	}
	// }
    static boot() {
        super.boot()
        this.addTrait('@provider:Lucid/SoftDeletes')
        // this.addHook('beforeDelete', async (bannerInstance) => {
        //     console.log('hook delete')
        //     console.log(bannerInstance)
        // })
    }
    channel_menus() {
		return this
		.hasMany('App/Models/ChannelMenu')
	}
    
	menus() {
		return this
			.belongsToMany('App/Models/Menu')
			.pivotModel('App/Models/ChannelMenu')
	}
	_v1_channel_user(){
		return this
		.belongsTo('App/Models/UserChannel')
	}

	_v1_channelsubscribers(){
		return this
		.hasMany('App/Models/ChannelSubscriber')
	}

	_v1_channel_type(){
		return this
		.belongsTo('App/Models/ChannelType')
	}

	_v1_channel_menus(){
		return this
		.hasMany('App/Models/ChannelMenu')
	}

	_v1_channel_subscriber(){
		return this
			.hasOne('App/Models/ChannelSubscriber')
	}

	_v1_channel_banners(){
		return this
		.hasMany('App/Models/ChannelBanner')
		.where('is_archived', 0)
		.where('is_active', 1)
		.orderBy('position', 'asc')
	}
}

module.exports = Channel