'use strict'

const Model = use('Model')

class ChannelJobCharacteristic extends Model {
	static castDates (key, value) {
		return value.toISOString()
	}

    _channel_job(){
		return this
			.belongsTo('App/Models/ChannelJob', 'channel_job_id', 'id')
	}

    _speciaist(){
		return this
			.belongsTo('App/Models/Specialist', 'ref_id', 'id')
	}

}

module.exports = ChannelJobCharacteristic