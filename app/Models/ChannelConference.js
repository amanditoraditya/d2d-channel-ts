'use strict'

const GueBaseModel = use('App/Models/GueBaseModel')

class ChannelConference extends GueBaseModel {
	//cms
	subscribers() {
		return this
			.belongsToMany('App/Models/ChannelSubscriber', 'conference_id')
			.pivotModel('App/Models/ChannelConferenceAttendee')
	}
	channels() {
		return this
			.belongsTo('App/Models/Channel')
	}

	//api
	_v1_channel_conference_attendees() {
		return this
			.hasOne('App/Models/ChannelConferenceAttendee', 'id', 'conference_id')
	}

}

module.exports = ChannelConference