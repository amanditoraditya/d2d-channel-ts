'use strict'

const Logger = use('Logger')
const Database = use('Database')

class GlobalParam {
	async handle({ view }, next) {
		let settings = await Database.select('value').from('settings')
		view.share({
			globalParam: settings
		})
		
		await next()
	}
}

module.exports = GlobalParam