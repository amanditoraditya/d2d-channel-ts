'use strict'
//MODELS
const ApiService = use('App/Models/ApiService')
// MODULES
const crypto = require("crypto-js")
const admin = require('firebase-admin')
const jwt = require('jsonwebtoken')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
//STATICS
const Dictionary = require('../../resources/statics/dictionary')

class ApiAuthenticator {
	async handle ({ params, request, response }, next) {
		let success = false
		let options = {
			type : 'ERR_500',
			response_type : 'obj',
			reason : Dictionary.TR000008,
			exec_time : new Date().getTime()
		}
		let apidetail = null
		let decoded = null
		try {
			const auth = request.header('Authorization')
			const tokenarr = auth.split(" ")
			const token = tokenarr[1]
			const clientkey = request.header('X-DCHANNEL-KEY')
			let api_service = null
			let gtoken = await admin.auth().verifyIdToken(token)
			//HARCODED TOKEN FOR TEST ONLY
			/*
			let gtoken = {
				"name": "bonpay",
				"iss": "https://securetoken.google.com/gue-login",
				"aud": "gue-login",
				"auth_time": 1614322052,
				"user_id": "z5m7gDK7CnRQ6f66ERD4bK7IWjx2",
				"sub": "z5m7gDK7CnRQ6f66ERD4bK7IWjx2",
				"iat": 1614576712,
				"exp": 1614580312,
				"email": "bonpay14@yopmail.com",
				"email_verified": true,
				"firebase": {
				  "identities": {
					"email": [
					  "bonpay14@yopmail.com"
					]
				  },
				  "sign_in_provider": "password"
				}
			}
			*/
			if(gtoken && clientkey) {
				api_service = await ApiService.query()
				.select('client_secret')
				.where('client_key',  clientkey)
				.where('status',  1)
				.where('permissions',  'user')
				.first()
			}
			if(api_service){
				params.usersession = {
					uid : gtoken.user_id,
					sec : api_service.client_secret,
					token : token
				}
				success = true
			}
			if(success){
				await next()
			} else {
				options.type = 'ERR_AUTH_AUTH'
				options.reason  = await MicroH.reason('TR000008', '')
				const rspdata = await ApiResponseHelper.classify(options)
				response.status(rspdata.responseheader.code).json(rspdata.content)
			}
		} catch(err) {
			console.log(err)
			options.type = 'ERR_403'
			options.reason  = await MicroH.reason('TR000001', err.message)
			const rspdata = await ApiResponseHelper.classify(options)
			response.status(rspdata.responseheader.code).json(rspdata.content)
		}
	}
}

module.exports = ApiAuthenticator
