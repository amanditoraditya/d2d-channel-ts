'use strict'
// HELPERS
const ESerrviceH = use('App/Controllers/Http/Helper/ExtendedServiceHelper')

class UserSynchronization {
	async handle ({ params, request, response }, next) {
		ESerrviceH.generateuser(params.usersession)
		await next()
	}
}

module.exports = UserSynchronization
