'use strict'

const Env = use('Env')
// MODULES
const uuid = require('uuid')
const _ = require('lodash')
const Logger = use('Logger')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
//STATICS
const Dictionary = require('../../resources/statics/dictionary')

class ApiLoging {
	async handle ({ params, request, response }, next) {
		let eloging = false
		if(Env.get('CONSOLE_LOGING')){
			if(Env.get('CONSOLE_LOGING') == 'true'){
				eloging = true
			}
		}
		if(eloging){
			const ruid = uuid.v1()
			try {
				const log_data = {
					request_uid : ruid,
					type : "REQUEST",
					endpoint : request.originalUrl(),
					method : request.method(),
					request : {
						headers : request.headers(),
						qs : request.get(),
						body : request.post()
					}
				}
				const _options = {
					request_uid : ruid
	
				}
				if(params.options){
					params.options = _.merge(params.options, Object.assign({}, _options))
				} else{
					params.options = Object.assign({}, _options)
				}
				Logger.info('API log: ' + ruid + ' %j', log_data)
				await next()
			} catch(err) {
				console.log(err)
				options.type = 'ERR_403'
				options.reason  = await MicroH.reason('TR000001', err.message)
				const rspdata = await ApiResponseHelper.classify(options)
				response.status(rspdata.responseheader.code).json(rspdata.content)
			}
		} else {
			await next()
		}
	}
}

module.exports = ApiLoging
