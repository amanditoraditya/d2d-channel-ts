'use strict'
// MODULES
const _ = require('lodash')
//STATICS
const Dictionary = require('../../resources/statics/dictionary')

class ApiPreparation {
	async handle ({ params }, next) {
		const _options = {
			type : 'ERR_500',
			response_type : 'obj',
			reason : Dictionary.TR000000,
			exec_time : new Date().getTime()
		}
		if(params.options){
			params.options = _.merge(params.options, Object.assign({}, _options))
		} else{
			params.options = Object.assign({}, _options)
		}
		await next()
	}
}

module.exports = ApiPreparation
