class Datatable {
    constructor(models, parameter) {
        this.result = {
            draw: parameter.draw,
            data: [],
            recordsFiltered: 0,
            recordsTotal: 0
        }
        this.searchKey = parameter.search.value
        this.columns = parameter.columns
        this.start = parseInt(parameter.start)
        this.limitPerPage = parseInt(parameter['length'])
        this.models = models
        const { order } = parameter
        if (typeof order !== 'undefined') {
            this.orderedParam = order
        } else {
            this.orderedParam = null
        }
        this.customFilter = null
        this.objectAdditionalColumn = null
    }
    async setCustomFilter(whereClause) {
        this.customFilter = whereClause
        return this
    }
    async setAdditionalColumn(callbackAdditionalColumn) {
        this.objectAdditionalColumn = callbackAdditionalColumn
        return this
    }
    async make() {
        if (typeof this.searchKey != 'undefined' && this.searchKey !== '') {
            // Search Section
            this.columns.map((item) => {
                if (typeof item.searchable == 'undefined' || (typeof item.searchable !== 'undefined' && item.searchable == 'true')) {
                    this.models.orWhere(typeof item.name != 'undefined' && item.name != '' ? item.name : item.data, 'like', `%${this.searchKey}%`)
                }
            })
        }
        if (this.customFilter !== null) {
            this.customFilter(this.models, this.searchKey)
        }
        if(this.limitPerPage == -1){
            this.limitPerPage = await this.models.getCount()
        }
        this.result.recordsFiltered = await this.models.getCount()
        this.result.recordsTotal = this.result.recordsFiltered

        // order
        if (this.orderedParam !== null) {
            let fieldNameOrder = ''
            this.orderedParam.map((orderArr) => {
                fieldNameOrder = typeof this.columns[orderArr.column].name != 'undefined' && this.columns[orderArr.column].name != '' ? this.columns[orderArr.column].name : this.columns[orderArr.column].data
                this.models.orderBy(fieldNameOrder, orderArr.dir)
            })
        }
        this.result.data = await this.models.offset(this.start).limit(this.limitPerPage).fetch()
        if (this.objectAdditionalColumn !== null) {
            Object.keys(this.objectAdditionalColumn).map((itemKey) => {
                this.result.data.rows.map(async (itemRecord, indexOfArray) => {
                    this.setKeyByString(itemRecord, itemKey, await this.objectAdditionalColumn[itemKey](itemRecord));
                })
            })
        }
        return this.result
    }

    setKeyByString(obj, path, value) {
        const arrayPath = path.split('.')

        for (var i = 0, tmp = obj; i < arrayPath.length - 1; i++) {
            tmp = tmp[arrayPath[i]] = {}
        }
        tmp[arrayPath[i]] = value
    }
}
module.exports = Datatable