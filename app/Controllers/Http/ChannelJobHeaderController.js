'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const MyHash 		= require('./Helper/Hash.js')
const { v4: uuidv4 } = require('uuid');

const ChannelJob = use('App/Models/ChannelJob')
const ChannelJobHeaderAttribute = use('App/Models/ChannelJobHeaderAttribute')
const JobHeaderAttribute = use('App/Models/JobHeaderAttribute')
const QueryBuilder 		 = require('./Helper/DatatableBuilder.js')
const CheckAuth 		 = require('./Helper/CheckAuth.js')
const Datatable         = use('App/Helpers/Datatable')

const { validate, validateAll } = use('Validator')

class ChannelJobHeaderController {
	async datatable({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('ChannelJobHeaderAttribute', 'read', auth, true)

		let fdata 			= [];
		let recordsTotal 	= 0;
		let recordsFiltered = [];
		const formData 		= request.post()
		
		let data			= {
			draw: formData.draw,
			recordsTotal: 0,
			recordsFiltered: [],
			data:[]
		};

		let jobheaderId

		if(formData.id == null)
		{
			jobheaderId = '""'
		}
		else
		{
			jobheaderId = await MyHash.decrypt(formData.id);
		}

		let fields = [
			'cjha.id', 'cjha.value', `jha.name`, 'jha.icon'
		];
		let condition = []
		let joins = "channel_job_header_attributes cjha \
		LEFT JOIN channel_jobs cj ON cj.id = cjha.channel_job_id \
		LEFT JOIN job_header_attributes jha ON jha.id = cjha.job_header_attribute_id"
		
		condition = [`cjha.is_archived = 0 AND cj.is_archived = 0 AND jha.is_archived = 0 AND cj.id = ${jobheaderId}`]

		let tableDefinition = {
			sTableName		: 'channel_job_header_attributes cjha',
			sSelectSql		: fields,
			aSearchColumns	: ['value', 'name'],
			sWhereAndSql	: condition,
			sFromSql 		: joins
		}

		let queryBuilder = new QueryBuilder(tableDefinition)
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		recordsTotal = await Database.raw(queries.recordsTotal)
		recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let no = 0
		for(let row of select[0]) {
			let id = row['id']
			let icon = ''
			if (row['icon'] != null || row['icon'] != '') {
				icon = `<img class="pull-left img-responsive img-thumbnail" src="${row['icon']}" style="width:50px;height:50px;"/>`
			}
			let content = ''
				content  = [
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				row['id'],
				`<div style="overflow:hidden;text-overflow:ellipsis;max-width:250px">${row['value']}</div>`,
				`<div style="overflow:hidden;text-overflow:ellipsis;max-width:250px">${row['name']}</div>`,
				icon,
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"]
			fdata.push(content)
			no++
		}
		
		data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data;
	}

	async datatableDetail({request, response, auth, session}) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('ChannelJobHeaderAttribute', 'read', auth, admin_page)
		
		const formData = request.post()
		let jobheaderId = await MyHash.decrypt(formData.id);

		const queryModel = ChannelJobHeaderAttribute.query()
			.innerJoin('channel_jobs', 'channel_jobs.id', 'channel_job_header_attributes.channel_job_id')
			.innerJoin('job_header_attributes', 'job_header_attributes.id', 'channel_job_header_attributes.job_header_attribute_id')
			.select(
				'channel_job_header_attributes.id',
				'channel_job_header_attributes.value',
				'job_header_attributes.name',
				'job_header_attributes.icon',
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.where('channel_jobs.is_archived', 0)
				.where('channel_job_header_attributes.is_archived', 0)
				.where('job_header_attributes.is_archived', 0)
				.where('channel_jobs.id', jobheaderId)
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}

	async create({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJobHeaderAttribute', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data)

		let channeljobid = await MyHash.decrypt(params.id)
	
		let xid = uuidv4()
		let formData = {
			xid,
			channel_job_id: channeljobid,
			job_header_attribute_id: form.job_header_attribute_id ,
			value: form.value,
			updated_at: currentTime,
			created_at: currentTime
		}

		let rules = {
			value: 'required',
			job_header_attribute_id: 'required'
		}

		let channeljob = await ChannelJobHeaderAttribute.query().
		where('channel_job_id', await MyHash.decrypt(params.id)).
		andWhere('job_header_attribute_id', form.job_header_attribute_id ).
		andWhere('is_archived', 0).
		first()

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				if(_lo.isEmpty(channeljob)){
					await ChannelJobHeaderAttribute.create(formData)

					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '2000',
						message: 'Job Header added',
						data: params.id
					}
					return response.send(data)
				}
				else{
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Each Header Attribute can only be selected once',
						data: []
					}
					return response.send(data)
				}
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Job Header cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelJobHeaderAttribute', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let channeljobheader = await ChannelJobHeaderAttribute.find(await MyHash.decrypt(formData.id))
		if (channeljobheader){
			try {
				// Soft Delete Channel Job Header
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				channeljobheader.updated_by = auth.user.id,
				channeljobheader.updated_at = currentTime,
				channeljobheader.job_header_attribute_id = null,
				channeljobheader.is_archived = 1
				await channeljobheader.save()

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Job Header deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job Header cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job Header to delete',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelJobHeaderAttribute', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					// Soft Delete Channel Job Header
					let channeljobheader = await ChannelJobHeaderAttribute.find(await MyHash.decrypt(dataitem[i]))
					let jobheaderattribute = await JobHeaderAttribute.find(await channeljobheader.job_header_attribute_id)
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					jobheaderattribute.updated_at = currentTime,
					jobheaderattribute.is_archived = 1
					await jobheaderattribute.save()
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					Message: 'Channel Job Header deleted',
					data: []
				}
			return response.send(data)
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job Header cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job Header to delete',
				data: []
			}
			return response.send(data)
		}
	}

	async submitData({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJobHeaderAttribute', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let channeljobid = await MyHash.decrypt(params.id)

		let formData = {
			is_archived: 0,
			updated_at: currentTime,
			created_at: currentTime
		}

		try {
			await ChannelJob.query().where('id', channeljobid).update(formData)

			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel Job added',
				data: []
			}
			return response.send(data)
		} catch (e) {
			console.log(e)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Job cannot be added',
				data: []
			}
			return response.send(data)
		}
		
	}

	async getChannelJobHeaderAttribute({ request, response, auth, session }) {
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelJobHeaderAttribute', 'read', auth, admin_page)
		let req = request.get()
		let datas

		datas = await JobHeaderAttribute.query()
		.select('job_header_attributes.id as value', 'job_header_attributes.name as label', 'job_header_attributes.input_type as type')
		.where('is_archived', 0)
		.limit(20).fetch()

		const result = datas.toJSON()

		return response.send(result)
	}
}

module.exports = ChannelJobHeaderController