'use strict'

//library
const oss = require('ali-oss');
const Database = use('Database')
const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const _lo = use('lodash')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment');
const Hash = use('Hash')
const { v4: uuidv4 } = require('uuid');
const { validate, validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')

//custom module
const MyHash = require('./Helper/Hash.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const FileService = require('./Helper/FileService.js');

//models
const JobHeaderAttribute = use('App/Models/JobHeaderAttribute')

const content_type = 'header-master'

class HeaderMasterController {
	async datatable({ request, response, auth, session }) {
		let checkAuthAdmin = await CheckAuth.get('HeaderMaster', 'read', auth, true)

		const queryModel = JobHeaderAttribute.query()
			.select(
				'id','name','type','icon','input_type','position','is_archived','created_at'
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.orderBy('is_archived', 'asc')
				.orderBy('type', 'asc')
				.orderBy('position', 'asc')
				// .where('job_header_attributes.is_archived', 0)
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}

	async create({ request, response, auth, session }) {
		try {
			let checkAuth = await CheckAuth.get('HeaderMaster', 'create', auth)
			if (!checkAuth) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '4003',
					message: 'User cannot access this module',
					data: []
				}
				return response.send(returnData)
			}

			let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
			//get data form
			let dataRequest = JSON.parse(request.post().data);
			let data = dataRequest.header;

			let response_data = {
				code: '2000',
				message: 'Header added',
				data,
			}

			if (_lo.isEmpty(data) == false) {
				let response_message = [];

				let last_position = (await JobHeaderAttribute.query()
				.select(Database.raw('COUNT(id) as total_id'))
				.where('type', data.type)
				.first()).total_id

				let status = !data.is_archived;

				let headerForm = {
					name: data.name,
					position: (last_position+1),
					type : data.type,
					input_type: data.input_type,
					is_archived: status,
					created_at: currentTime,
					updated_at: currentTime,
				};

				const imageFile = request.file('icon', {
					type: 'image',
					subtype: ['image/jpeg', 'image/png'],
					size: '2mb',
					extnames: ['jpg', 'jpeg', 'png']
				})

				if (imageFile != null) {
					headerForm.icon = true
				}

				let rules = {
					name: 'required',
					type: 'required',
					input_type: 'required',
					icon: 'required'
				}

				const validation = await validateAll(headerForm, rules)
				if (validation.fails()) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let returnData = {
						code: '4004',
						message: 'Please fill required marker field!',
						data: validation
					}
					return response.send(returnData)
				} else {
					if (imageFile != null) {
						let path_tmp = `${Helpers.tmpPath('uploads')}/${imageFile.clientName}`
						fs.stat(path_tmp, function (err, stats) {
							if (err) {
								return console.error("Error on delete file | HeaderMasterController:update", err)
							}
							
							fs.unlink(path_tmp,function(err){
									if(err) return console.log(err);
							});  
						});
						// put temporary file to trigger validation
						await imageFile.move(Helpers.tmpPath('uploads'), {
							name: imageFile.clientName,
							overwrite: true
						})
						// error validation file
						if (!imageFile.moved()) {
							response.header('Content-type', 'application/json')
							response.type('application/json')
							let returnData = {
								code: '4004',
								message: `Invalid File Upload! - ${imageFile.error().message}`,
								data: imageFile.error()
							}
							return response.send(returnData)
						} else {
							//upload image
							let upload_setting = {
								content: content_type,
								tempPath: path_tmp,
							}
							let upload_image = await FileService.upload(imageFile, upload_setting, true)

							headerForm.icon = upload_image.url;
							// delete temporary file
							fs.unlinkSync(path_tmp)
						}
					}

					let newHeader = await JobHeaderAttribute.create(headerForm);
					
					response_message.push('Header Saved')
				}
			} else {
				response_data = {
					code: '4004',
					message: 'Header cannot be added',
					data: []
				}
			}
			response.header('Content-type', 'application/json')
			response.type('application/json')
			return response.send(response_data)
		} catch (e) {
			console.log(e)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Header cannot be added, System encounter an error.',
				data: []
			}
			return response.send(returnData)
		}
	}

	async edit({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('HeaderMaster', 'update', auth, false)

		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(returnData)
		}

		let headerId = await MyHash.decrypt(params.id)

		let responseData = {
			header :{}
		}
		let result = {}; 

		result = await JobHeaderAttribute.query()
		.select('name','icon','type','input_type','position','is_archived')
		.where('id', headerId)
		.first()

		if(result) {
			result.is_archived = !result.is_archived;
			responseData.header = result

			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '2000',
				message: 'Header found',
				data: responseData
			}
			return response.send(returnData)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Header cannot be found',
				data: []
			}
			return response.send(returnData)
		}
	}

	async update({ params, request, response, auth, session }) {
		const trx = await Database.beginTransaction()
		try {
			let checkAuth = await CheckAuth.get('HeaderData', 'update', auth)
			if (!checkAuth) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '4003',
					message: 'User cannot access this module',
					data: []
				}
				return response.send(returnData)
			}

			let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

			let headerId = await MyHash.decrypt(params.id)
			let headerData = await JobHeaderAttribute.find(headerId)
			let dataRequest = JSON.parse(request.post().data);
			let data = dataRequest.header;

			let status = !data.is_archived;

			if (_lo.isEmpty(headerData) == false) {
				let headerForm = {
					name: data.name,
					input_type: data.input_type,
					position: data.position,
					type : data.type,
					is_archived: status,
					updated_at: currentTime
				}

				const imageFile = request.file('icon', {
					type: 'image',
					subtype: ['image/jpeg', 'image/png'],
					size: '2mb',
					extnames: ['jpg', 'jpeg', 'png']
				})

				let rules = {
					name: 'required',
					input_type: 'required',
					type: 'required',
				}

				const validation = await validateAll(headerForm, rules)
				if (validation.fails()) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let returnData = {
						code: '4004',
						message: 'Please fill required marker field!',
						data: validation
					}
					return response.send(returnData)
				} else {
					if (headerData) {
						if (headerData.icon != '' && headerData.icon != null && (data.remove_image == true)) {
							let parts = headerData.icon.split('/')
							let url = parts.slice(3, parts.length)
							url = url.join('/')
							await FileService.delete(url);
							headerForm.icon = ''
						}
						//#region upload image
						if (imageFile != null) {
							// put temporary file to trigger validation
							let path_tmp = `${Helpers.tmpPath('uploads')}/${imageFile.clientName}`
							fs.stat(path_tmp, function (err, stats) {
								if (err) {
									return console.error("Error on delete file | HeaderMasterController:update", err)
								}
							 
								fs.unlink(path_tmp,function(err){
									 if(err) return console.log(err);
								});  
							 });
							
							await imageFile.move(Helpers.tmpPath('uploads'), {
								name: imageFile.clientName,
								overwrite: true
							})
							
							// error validation file
							if (!imageFile.moved()) {
								response.header('Content-type', 'application/json')
								response.type('application/json')
								let returnData = {
									code: '4004',
									message: `Invalid File Upload! - ${imageFile.error().message}`,
									data: imageFile.error()
								}
								return response.send(returnData)
							} else {
								//upload image
								let upload_setting = {
									content: content_type,
									tempPath: path_tmp,
								}
								let upload_image = await FileService.upload(imageFile, upload_setting, true)

								headerForm.icon = upload_image.url;
							}
						}
						//#endregion

						Object.assign(headerData.$attributes, headerForm)
						headerData.save()

						trx.commit()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let returnData = {
							code: '2000',
							message: 'Header Data updated',
							data: params.id
						}
						return response.send(returnData)
					} else {
						trx.rollback()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let returnData = {
							code: '4004',
							message: 'Header Data cannot be found',
							data: []
						}
						return response.send(returnData)
					}
				}
			} else {
				trx.rollback()
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '4004',
					message: 'Header not found.',
					data: []
				}
				return response.send(returnData)
			}
		} catch (e) {
			trx.rollback()
			console.log(e)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Header cannot be added, System encounter an error.',
				data: []
			}
			return response.send(returnData)
		}
	}

	async delete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('HeaderData', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(returnData)
		}

		const headerForm = request.post()
		let HeaderData = await JobHeaderAttribute.find(await MyHash.decrypt(headerForm.id))
		if (HeaderData) {
			try {
				// Soft Delete Header
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				// HeaderData.updated_by = auth.user.id,
				HeaderData.updated_at = currentTime,
				HeaderData.is_archived = 1
				await HeaderData.save()

				//#region delete logo
				if (!_lo.isEmpty(HeaderData.image)) {
					//remove
					let parts = HeaderData.image.split('/')
					let url = parts.slice(3, parts.length)
					url = url.join('/')
					await FileService.delete(url);
				}
				//#endregion


				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '2000',
					message: 'Header deleted',
					data: []
				}
				return response.send(returnData)
			} catch (e) {
				console.error(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '4004',
					message: 'Header cannot be deleted',
					data: []
				}
				return response.send(returnData)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Select Header to delete',
				data: []
			}
			return response.send(returnData)
		}
	}

	async multidelete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('HeaderData', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(returnData)
		}

		const headerForm = request.post()
		if (headerForm.totaldata != '0') {
			let dataitem = JSON.parse(headerForm.item)
			try {
				for (let i in dataitem) {
					// Soft Delete Header
					let HeaderData = await JobHeaderAttribute.find(await MyHash.decrypt(dataitem[i]))
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					// HeaderData.updated_by = auth.user.id,
					HeaderData.updated_at = currentTime,
					HeaderData.is_archived = 1
					await HeaderData.save()

					//#region delete logo
					if (!_lo.isEmpty(HeaderData.image)) {
						//remove
						let parts = HeaderData.image.split('/')
						let url = parts.slice(3, parts.length)
						url = url.join('/')
						await FileService.delete(url);
					}
					//#endregion
				}
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '2000',
					message: 'Header deleted',
					data: []
				}
				return response.send(returnData)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let returnData = {
					code: '4004',
					message: 'Header cannot be deleted',
					data: []
				}
				return response.send(returnData)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Select Header to delete',
				data: []
			}
			return response.send(returnData)
		}
	}

	async updateStatus({ request, response, auth }) {
		const { id } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await JobHeaderAttribute.find(idDecrypted)
		if (record != null) {
			Object.assign(record.$attributes, { is_archived: !record.is_archived })
			record.save()
			return response.defaultResponseJson(200, 'Success to update status header')
		} else {
			return response.defaultResponseJson(400, 'header not found')
		}
	}
}

module.exports = HeaderMasterController