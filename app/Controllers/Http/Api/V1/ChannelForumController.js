'use strict'
// MODULES
const _ = require('lodash')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
const ChannelForumMicro =  use('App/Controllers/Http/Api/Micro/V1/ChannelForumMicro')


class ChannelForumController {
	async get({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelForumMicro.get(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async action({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'action'])
		data = _.merge(data, request.post())
		options = await ChannelForumMicro.action(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async comment({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelForumMicro.comment(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async commentaction({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'action'])
		data = _.merge(data, request.post())
		options = await ChannelForumMicro.commentaction(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async getcomment({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelForumMicro.getcomment(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	
	async getreply({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelForumMicro.getreply(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async getcommentdetail({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelForumMicro.getcommentdetail(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

}

module.exports = ChannelForumController