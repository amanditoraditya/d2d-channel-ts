'use strict'
// MODULES
const _ = require('lodash')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
const UserMicro =  use('App/Controllers/Http/Api/Micro/V1/UserMicro')


class UserContoller {
	async triggerUpdate({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, [])
		data = _.merge(data, request.get())
		options = await UserMicro.triggerUpdate(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}
}

module.exports = UserContoller