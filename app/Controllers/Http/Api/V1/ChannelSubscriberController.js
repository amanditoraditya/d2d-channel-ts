'use strict'
// MODULES
const _ = require('lodash')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
const ChannelSubscriberMicro =  use('App/Controllers/Http/Api/Micro/V1/ChannelSubscriberMicro')

class ChannelSubscriberController {
	async subscribe({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelSubscriberMicro.subscribe(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async unsubscribe({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelSubscriberMicro.unsubscribe(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}
}

module.exports = ChannelSubscriberController