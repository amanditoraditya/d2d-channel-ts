'use strict'
// MODULES
const _ = require('lodash')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
const ChannelConferenceMicro =  use('App/Controllers/Http/Api/Micro/V1/ChannelConferenceMicro')

class ChannelConferenceController {
	async calendar({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['tz'])
		data = _.merge(data, request.get())
		options = await ChannelConferenceMicro.calendar(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async register({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelConferenceMicro.register(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async notify({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelConferenceMicro.notify(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async join({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelConferenceMicro.join(data, params.usersession, options, request)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}
}

module.exports = ChannelConferenceController