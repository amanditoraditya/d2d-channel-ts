'use strict'
// MODULES
const _ = require('lodash')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ApiResponseHelper = use('App/Controllers/Http/Helper/ApiResponse')
const ChannelJobMicro =  use('App/Controllers/Http/Api/Micro/V1/ChannelJobMicro')


class ChannelJobController {
	async get({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelJobMicro.get(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async gettype({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, [])
		data = _.merge(data, request.get())
		options = await ChannelJobMicro.gettype(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async apply({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid'])
		data = _.merge(data, request.post())
		options = await ChannelJobMicro.apply(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

	async getpublic({ params, request, response, auth }) {
		const usersession = params.usersession
		let options = params.options
		let data = _.pick(params, ['xid', 'tz'])
		data = _.merge(data, request.get())
		options = await ChannelJobMicro.getpublic(data, params.usersession, options)
		const rspdata = await ApiResponseHelper.classify(options)
		response.status(rspdata.responseheader.code).json(rspdata.content)
	}

}

module.exports = ChannelJobController