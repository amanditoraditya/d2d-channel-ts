'use strict'
const Database = use('Database')
//MODELS
const ChannelJob = use('App/Models/ChannelJob')
const ChannelJobApplicant = use('App/Models/ChannelJobApplicant')
const ChannelJobApplicantStatus = use('App/Models/ChannelJobApplicantStatus')
const ChannelJobType = use('App/Models/ChannelJobType')
// MODULES
const Validation = require('joi')
const Config = use('Config')
const _ = require('lodash')
const jwt = require('jsonwebtoken')
const uuid = require('uuid')
const moment = require('moment')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelJobMicro {
    //get
    async get(data, usersession, opt){
        let options = opt == undefined ? {} : opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            if(data.xid){
                const auth_schema = Validation.object({
                    xid : Validation.string().guid().required(),
                    tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required()
                })
                let validationprc = await MicroH.validation(data, auth_schema)
                if(validationprc.success){
                    let q_result = await ChannelJob
                    .query()
                    .select('id', 'xid', 'title', 'description', 'image', 'apply_button' ,'job_type_id','registration_url','registration_button_caption',
                        Database.raw('CONVERT_TZ(created_at, "+07:00",?) created_at', [validationprc.data.tz]),
                        Database.raw('CONVERT_TZ(due_date, gmt_tz,?) due_date', [validationprc.data.tz]),
                    )
                    .with('_v1_channel_job_type')
                    .with('_v1_channel_job_characteristic', (builder) => {
                        builder
                        .select('xid', 'channel_job_id', 'ref_id', 'ref_type', 'value', 'created_at', 'updated_at')
                        .where('is_archived', 0)
                    })
                    .with('_v1_channel_job_specs', (builder) => {
                        builder
                        .where('is_archived', 0)
                    })
                    .with('_v1_channel_job_header_attributes', (builder) => {
                        builder
                        .where('is_archived', 0)
                    })
                    .with('_v1_channel_job_applicant', (builder) => {
                        builder.where('channel_subscriber_id', [usersession.id])
                    })
                    .where('channel_id', usersession.channel_id)
                    .where('xid', data.xid)
                    .where('is_archived', 0)
                    .where('is_published', 1)
                    .first()
                    if(q_result){
                        q_result = q_result.toJSON()
                        q_result.type = null
                        if(q_result._v1_channel_job_type){
                            const _data = _.pick(q_result._v1_channel_job_type, ['name'])
                            q_result.type = _data
                        }
                        q_result.spec = []
                        if(q_result._v1_channel_job_specs){
                            for(const key in q_result._v1_channel_job_specs){
                                let r_data = q_result._v1_channel_job_specs[key]
                                const _pick = _.pick(r_data, ['xid', 'title', 'icon', 'is_doc_needed', 'description'])
                                q_result.spec.push(_pick)
                            }
                        }
                        q_result.header_attributes = []
                        if(q_result._v1_channel_job_header_attributes){
                            let valueJobHeader = ''
                            let dateTimeFormat = ''
                            for(const key in q_result._v1_channel_job_header_attributes){
                                dateTimeFormat = ''
                                let r_data = q_result._v1_channel_job_header_attributes[key]
                                switch (r_data._v1_job_header_attribute.input_type) {
                                    case 'date':
                                        valueJobHeader = moment(r_data.value).utcOffset(validationprc.data.tz)
                                        dateTimeFormat = 'DD-MMMM-YYYY'
                                        break;
                                    case 'datetime':
                                        valueJobHeader = moment(r_data.value).utcOffset(validationprc.data.tz)
                                        dateTimeFormat = 'DD-MMMM-YYYY hh:mm'
                                        break;
                                    case 'time':
                                        valueJobHeader = moment(r_data.value).utcOffset(validationprc.data.tz)
                                        dateTimeFormat = 'HH:mm'
                                        break;
                                    default:
                                        valueJobHeader = r_data.value
                                        break;
                                }
                                if (dateTimeFormat != '') {
                                    if (valueJobHeader.isValid()) {
                                        valueJobHeader = valueJobHeader.format(dateTimeFormat)
                                    } else {
                                        valueJobHeader = r_data.value
                                    }
                                }
                                const header_attributes = {
                                    icon : r_data._v1_job_header_attribute.icon,
                                    value : valueJobHeader
                                }
                                q_result.header_attributes.push(header_attributes)
                            }
                        }
                        q_result.applied = false
                        if(q_result._v1_channel_job_applicant){
                            q_result.applied = true
                            q_result.apply_button = "ANDA SUDAH DAFTAR"
                        }

                        dataset = _.pick(q_result, ['xid', 'title', 'spec', 'registration_url', 'registration_button_caption', 'description', 'image', 'apply_button', 'type', 'header_attributes', 'applied', 'created_at', 'due_date'])
                        options.reason = Dictionary.TR000004
                    }
                } else{
                    options.type = 'ERR_412'
                    options.reason  = await MicroH.reason('TR000003', validationprc.message)
                }
            } else {
                const auth_schema = Validation.object({
                    tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                    sort_by: Validation.string().min(4).max(50).optional().valid('oldest', 'added', 'alphabetic', 'reversealphabetic' ,'default'),
                    specialists : Validation.string().regex(/^[0-9\,]*$/).optional(),
                    types : Validation.string().regex(/^[0-9\,]*$/).optional(),
                    terms: Validation.string().min(3).max(50).optional(),
                    paginate : Validation.string().regex(/^\d+$/).optional(),
                    limit : Validation.string().regex(/^\d+$/).optional(),
                })
                let validationprc = await MicroH.validation(data, auth_schema)
                if(validationprc.success){
                    if(!validationprc.data.paginate) {
                        validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                    }
                    if(!validationprc.data.limit) {
                        validationprc.data.limit = Config.get('variables.default_values.page_limit')
                    }
                    if(!validationprc.data.type){
                        validationprc.data.type = "default"
                    }
                    let result_1 = ChannelJob
                    .query()
                    .select('id', 'xid', 'title', 'description', 'job_type_id', 'apply_button','registration_url','registration_button_caption',
                        Database.raw('CONVERT_TZ(due_date, gmt_tz,?) due_date', [validationprc.data.tz]),
                        Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                    )
                    .with('_v1_channel_job_type')
                    .where('channel_id', usersession.channel_id)
                    .where('is_archived', 0)
                    .where('is_published', 1)
                    .whereRaw('DATE(CONVERT_TZ(due_date , gmt_tz, "+07:00")) >= DATE(CONVERT_TZ(now(), gmt_tz, "+07:00"))')
                    
                    // JOB TYPE
                    if(validationprc.data.types){
                        const _types = validationprc.data.types
                        const _types_arr = _types.split(",")
                        result_1.whereHas('_v1_channel_job_type', (builder) => {
                            builder
                            .whereIn('id', _types_arr)
                        })
                    }
                    // SPECIALIS
                    if(validationprc.data.specialists){
                        const _specialists = validationprc.data.specialists
                        const _specialists_arr = _specialists.split(",")
                        result_1.whereHas('_v1_channel_job_characteristic', (builder) => {
                            builder
                            .select('xid', 'channel_job_id', 'ref_id')
                            .where('is_archived', 0)
    
                            if (_specialists_arr.length > 1) {
                                builder.whereIn('ref_id', _specialists_arr)
                            } else {
                                builder.where('ref_id', _specialists_arr[0])
                            }
                        })
                    }
                    // SORT BY
                    switch (validationprc.data.sort_by) {
                        case 'added':
                            result_1
                            .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) DESC', [validationprc.data.tz])
                            break
                        case 'alphabetic':
                            result_1
                            .orderBy('title', 'asc')
                            break
                        case 'reversealphabetic':
                            result_1
                            .orderBy('title', 'desc')
                            break
                        case 'oldest':
                            result_1
                            .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) ASC', [validationprc.data.tz])
                            break
                        default:
                            result_1
                            .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) ASC', [validationprc.data.tz])
                    }
                    
                    if(validationprc.data.terms){
                        result_1.where((builder) => {
                            builder.where('title', 'LIKE', `%${validationprc.data.terms}%`)
                            .orWhere('description', 'LIKE', `%${validationprc.data.terms}%`)
                        })
                    }
                    result_1 = await result_1
                    .paginate(validationprc.data.paginate, validationprc.data.limit)
                    if(result_1){
                        result_1 = result_1.toJSON()
                        if(result_1.data.length > 0){
                            let _r_data = []
                            for(const key in result_1.data){
                                let r_data = result_1.data[key]
                                r_data.type = null
                                if(r_data._v1_channel_job_type){
                                    r_data.type = _.pick(r_data._v1_channel_job_type, ['name'])
                                }
                                if(r_data._v1_channel_job_characteristic){
                                    r_data.characteristics = r_data._v1_channel_job_characteristic.map(function(row) {
                                        return {
                                            xid: row.xid,
                                            ref_id: row.ref_id,
                                            ref_type: row.ref_type,
                                            value: row.value
                                        }
                                    })
                                }
                                r_data.registration_button_caption = _.isEmpty(r_data.registration_button_caption) ? 'Daftar' : null
                                
                                const _pick = _.pick(r_data, ['xid', 'title', 'description', 'registration_url', 'registration_button_caption', 'image', 'type', 'characteristics', 'created_at', 'due_date'])
                                _r_data.push(_pick)
                            }
                            result_1.data = _r_data
                            options.reason = Dictionary.TR000004
                        } 
                        dataset = result_1
                    }
                } else {
                    options.type = 'ERR_412'
                    options.reason  = await MicroH.reason('TR000003', validationprc.message)
                } 
            }
            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    //get
    async gettype(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            let result_1 = await ChannelJobType
            .query()
            .select('id', 'name')
            .where('is_archived', 0)
            .fetch()
            if(result_1){
                result_1 = result_1.toJSON()
                if(result_1.length > 0){
                    options.reason = Dictionary.TR000004
                } 
                dataset = result_1
            }

            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    //apply
    async apply(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema) 
            if(validationprc.success){
                let channeljob = await ChannelJob
                .query()
                .with('_v1_channel_job_type')
                .with('_v1_channel_job_specs')
                .with('_v1_channel_job_applicant', (builder) => {
                    builder
                    .where('channel_subscriber_id', usersession.id)
                })
                .where('channel_id', usersession.channel_id)
                .where('xid', data.xid)
                .where('is_archived', 0)
                .where('is_published', 1)
                .first()
                if(channeljob){
                    channeljob = channeljob.toJSON()
                    if(!channeljob._v1_channel_job_applicant){
                        let exec_1 = null
                        let exec_2 = null
                        let record_1 = {
                            xid : uuid.v1(),
                            channel_job_id : channeljob.id,
                            channel_subscriber_id : usersession.id
                        }
                        let record_2 = {
                            xid : uuid.v1(),
                            status_step : 1,
                            is_final : 0,
                            status : "submitted"
                        }
                        const trx = await Database.beginTransaction()
                        try{
                            exec_1 = await ChannelJobApplicant.create(record_1, trx)
                            record_2.channel_job_applicant_id = exec_1.id
                            exec_2 = await ChannelJobApplicantStatus.create(record_2, trx)
                            dataset = {
                                xid : exec_1.xid
                            }
                            trx.commit()
                        }catch(e){
                            console.error(e)
                            trx.rollback()
                        }
                    } else {
                        options.type = 'ERR_409'
                        options.reason = Dictionary.TR000010
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    //get public
    async getpublic(data, usersession, opt){
        let options = opt == undefined ? {} : opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                sort_by: Validation.string().min(4).max(50).optional().valid('oldest', 'added', 'alphabetic', 'reversealphabetic' ,'default'),
                specialists : Validation.string().regex(/^[0-9\,]*$/).optional(),
                types : Validation.string().regex(/^[0-9\,]*$/).optional(),
                terms: Validation.string().min(3).max(50).optional(),
                paginate : Validation.string().regex(/^\d+$/).optional(),
                limit : Validation.string().regex(/^\d+$/).optional(),
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                if(!validationprc.data.paginate) {
                    validationprc.data.paginate = Config.get('variables.default_values.page_paginate')
                }
                if(!validationprc.data.limit) {
                    validationprc.data.limit = Config.get('variables.default_values.page_limit')
                }
                if(!validationprc.data.type){
                    validationprc.data.type = "default"
                }
                let result_1 = ChannelJob
                .query()
                .select('id', 'xid', 'title', 'description', 'job_type_id', 'apply_button', 'channel_id','registration_url','registration_button_caption',
                    Database.raw('CONVERT_TZ(due_date, gmt_tz,?) due_date', [validationprc.data.tz]),
                    Database.raw('CONVERT_TZ(created_at,"+07:00",?) created_at', [validationprc.data.tz]),
                )
                .with('_v1_channel_job_type')
                .with('_v1_channel_job_characteristic', (builder) => {
                    builder
                    .select('xid', 'channel_job_id', 'ref_id', 'ref_type', 'value', 'created_at', 'updated_at')
                    .where('is_archived', 0)
                })
                .with('_v1_channel', (builder) => {
                    builder
                    .with('_v1_channel_subscriber', (builder) => {
                        builder
                        .where('uid', usersession.uid)
                        .where('is_unsubscribed', 0)
                    })
                })
                .where('is_archived', 0)
                .where('is_published', 1)
                .whereRaw('DATE(CONVERT_TZ(due_date , gmt_tz, "+07:00")) >= DATE(CONVERT_TZ(now(), gmt_tz, "+07:00"))')
                
                // JOB TYPE
                if(validationprc.data.types){
                    const _types = validationprc.data.types
                    const _types_arr = _types.split(",")
                    result_1.whereHas('_v1_channel_job_type', (builder) => {
                        builder
                        .whereIn('id', _types_arr)
                    })
                }
                // SPECIALIS
                if(validationprc.data.specialists){
                    const _specialists = validationprc.data.specialists
                    const _specialists_arr = _specialists.split(",")
                    result_1.whereHas('_v1_channel_job_characteristic', (builder) => {
                        builder
                        .select('xid', 'channel_job_id', 'ref_id')
                        .where('is_archived', 0)

                        if (_specialists_arr.length > 1) {
                            builder.whereIn('ref_id', _specialists_arr)
                        } else {
                            builder.where('ref_id', _specialists_arr[0])
                        }
                    })
                }
                // SORT BY
                switch (validationprc.data.sort_by) {
                    case 'added':
                        result_1
                        .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) DESC', [validationprc.data.tz])
                        break
                    case 'alphabetic':
                        result_1
                        .orderBy('title', 'asc')
                        break
                    case 'reversealphabetic':
                        result_1
                        .orderBy('title', 'desc')
                        break
                    case 'oldest':
                        result_1
                        .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) ASC', [validationprc.data.tz])
                        break
                    default:
                        result_1
                        .orderByRaw('CONVERT_TZ(due_date, gmt_tz,?) ASC', [validationprc.data.tz])
                }
                
                if(validationprc.data.terms){
                    result_1.where((builder) => {
                        builder.where('title', 'LIKE', `%${validationprc.data.terms}%`)
                        .orWhere('description', 'LIKE', `%${validationprc.data.terms}%`)
                    })
                }
                result_1 = await result_1
                .paginate(validationprc.data.paginate, validationprc.data.limit)

                if(result_1){
                    result_1 = result_1.toJSON()
                    if(result_1.data.length > 0){
                        let _r_data = []
                        for(const key in result_1.data){
                            let r_data = result_1.data[key]
                            r_data.type = null
                            if(r_data._v1_channel_job_type){
                                r_data.type = _.pick(r_data._v1_channel_job_type, ['name'])
                            }
                            if(r_data._v1_channel){
                                r_data._v1_channel.subscribed = false
                                if(r_data._v1_channel._v1_channel_subscriber){
                                    r_data._v1_channel.subscribed = true
                                }
                                r_data.channel = _.pick(r_data._v1_channel, ['xid', 'country_code', 'channel_name', 'description', 'logo', 'subscribed'])
                            }
                            if(r_data._v1_channel_job_characteristic){
                                r_data.characteristics = r_data._v1_channel_job_characteristic.map(function(row) {
                                    return {
                                        xid: row.xid,
                                        ref_id: row.ref_id,
                                        ref_type: row.ref_type,
                                        value: row.value
                                    }
                                })
                            }
                            r_data.registration_button_caption = _.isEmpty(r_data.registration_button_caption) ? 'Daftar' : null
                            const _pick = _.pick(r_data, ['xid', 'title', 'description', 'registration_url', 'registration_button_caption', 'image', 'type', 'characteristics', 'created_at', 'channel', 'due_date'])
                            _r_data.push(_pick)
                        }
                        result_1.data = _r_data
                        options.reason = Dictionary.TR000004
                    } 
                    dataset = result_1
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
    
}

module.exports = new ChannelJobMicro

