'use strict'

//MODELS
const Channel = use('App/Models/Channel')
const ChannelSubscriber = use('App/Models/ChannelSubscriber')
const UserChannel = use('App/Models/UserChannel')
// MODULES
const Validation = require('joi')
const Config = use('Config')
const _ = require('lodash')
const uuid = require('uuid')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelSubscriberMicro {
    async subscribe(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                source: Validation.string().min(5).max(50).required().valid('mobile', 'direct', 'event'),
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required(),
                referral_type : Validation.string(),
                referral_admin_id : Validation.string()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channel = await Channel
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('is_admin', 0)
                .where('status', 1)
                .where('is_archived', 0)
                .first()
                if(channel){
                    let subscribe = await ChannelSubscriber
                    .query()
                    .where('uid', validationprc.data.uid)
                    .where('channel_id', channel.id)
                    .first()
                    if(subscribe){
                        let exec_1 = null
                        subscribe = subscribe.toJSON()
                        if(subscribe.is_unsubscribed){
                            const referralObj = {}
                            // checking data referral_admin_id and referral_type
                            if (typeof data.referral_admin_id != 'undefined' && typeof data.referral_type != 'undefined' && data.referral_admin_id != '' && data.referral_type != '') {
                                let payloadReferral = {}
                                // first checking referral type
                                const validReferralType = ['qr', 'link']
                                if (validReferralType.indexOf(data.referral_type) >= 0) {
                                    payloadReferral.referral_type = data.referral_type
                                }
                                // get to user channel
                                const adminRecord = await UserChannel.query().select('id').where('xid', data.referral_admin_id).first()
                                if (adminRecord != null) {
                                    payloadReferral.referral_admin_id = adminRecord.id
                                }
                                if (typeof payloadReferral.referral_type != 'undefined' && typeof payloadReferral.referral_admin_id != 'undefined') {
                                    Object.assign(referralObj, payloadReferral)
                                }
                            }
                            exec_1 = await ChannelSubscriber.query()
                            .where('id',  subscribe.id)
                            .update({ 
                                is_unsubscribed : 0,
                                ...referralObj
                            })
                            if(exec_1){
                                dataset = {
                                    xid : subscribe.xid
                                }
                            } 
                        } else {
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }
                    } else {
                        let exec_1 = null
                        const record_1 = {
                            xid : uuid.v1(),
                            uid : validationprc.data.uid,
                            channel_id : channel.id,
                            source : validationprc.data.source
                        }
                        exec_1 = await ChannelSubscriber.create(record_1)
                        if(exec_1){
                            dataset = {
                                xid : exec_1.xid
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async unsubscribe(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            data.uid = usersession.uid
            const auth_schema = Validation.object({
                uid : Validation.string().alphanum().min(25).max(30).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channel = await Channel
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('is_admin', 0)
                .where('status', 1)
                .where('is_archived', 0)
                .first()
                if(channel){
                    let subscribe = await ChannelSubscriber
                    .query()
                    .where('uid', validationprc.data.uid)
                    .where('channel_id', channel.id)
                    .first()
                    if(subscribe){
                        let exec_1 = null
                        subscribe = subscribe.toJSON()
                        if(!subscribe.is_unsubscribed){
                            exec_1 = await ChannelSubscriber.query()
                            .where('id',  subscribe.id)
                            .update({ 
                                is_unsubscribed : 1
                            })
                            if(exec_1){
                                dataset = {
                                    xid : subscribe.xid
                                }
                            } 
                        } else {
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }
                    } 
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
}

module.exports = new ChannelSubscriberMicro

