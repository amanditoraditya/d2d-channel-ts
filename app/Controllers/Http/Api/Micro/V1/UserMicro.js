'use strict'
const Env = use('Env')
const Database = use('Database')
//MODELS
const TemporaryMember = use('App/Models/TemporaryMember')
// MODULES
const _ = require('lodash')
// HELPERS
const ApiService = use('App/Controllers/Http/Helper/ApiService')
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')
const ESerrviceH = use('App/Controllers/Http/Helper/ExtendedServiceHelper')
const { D2D_DOMAIN, D2D_API_DOMAIN } = use('App/Controllers/Http/Helper/Enum')

let d2ddomain = D2D_DOMAIN.STAGING 
let d2ddomainApi = D2D_API_DOMAIN.STAGING
if(Env.get('NODE_ENV') == 'production'){
    d2ddomain = D2D_DOMAIN.PRODUCTION 
    d2ddomainApi = D2D_API_DOMAIN.PRODUCTION 
}

class UserMicro {
    async triggerUpdate(data, usersession, opt){

        let options = opt
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            if(_.isEmpty(usersession.uid) == false){
                let action = null
                let member_id = null
                const member = await TemporaryMember
                .query()
                .select('id', 'uid')
                .where('uid', usersession.uid)
                .first()

                if(_.isEmpty(member) == false){
                    action = "UPDATE" 
                    member_id = member.id

                    //get data from profile api d2d
                    let apiurl = d2ddomainApi + "v3.1/profile"
                    const reqOptions = {
                        headers: {
                            authorization: usersession.token
                        }
                    }
                    const reqparam = {}
                    const datauser = await ApiService.getData(apiurl, 'GET', reqparam, reqOptions)

                    if(datauser.acknowledge){
                        const udata = datauser.result
                        await ESerrviceH.temp_user(member_id, udata, action)
                        options.reason = Dictionary.TR000004
                    }
                }else{
                    action = "ADD" 

                    //get data from profile api d2d
                    let apiurl = d2ddomainApi + "v3.1/profile"
                    const reqOptions = {
                        headers: {
                            authorization: usersession.token
                        }
                    }
                    const reqparam = {}
                    const datauser = await ApiService.getData(apiurl, 'GET', reqparam, reqOptions)

                    if(datauser.acknowledge){
                        const udata = datauser.result
                        await ESerrviceH.temp_user(member_id, udata, action)
                        options.reason = Dictionary.TR000004
                    }
                }    
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
}

module.exports = new UserMicro

