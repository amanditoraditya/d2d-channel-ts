'use strict'
const Env = use('Env')
const Database = use('Database')
//MODELS
const ChannelConference = use('App/Models/ChannelConference')
const ChannelConferenceAttendee = use('App/Models/ChannelConferenceAttendee')
const ChannelConferenceActivity = use('App/Models/ChannelConferenceActivity')
const TemporaryMember = use('App/Models/TemporaryMember')
// MODULES
const Validation = require('joi').extend(require('@joi/date'))
const Config = use('Config')
const _ = require('lodash')
const uuid = require('uuid')
const crypto = require("crypto-js")
const moment = require('moment')
// HELPERS
const MicroH = use('App/Controllers/Http/Helper/MicroHelper')
const ExtendedServiceH = use('App/Controllers/Http/Helper/ExtendedServiceHelper')
const Dictionary = require('../../../../../../resources/statics/dictionary')

class ChannelConferenceMicro {
    //get
    async calendar(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'SUC_200'
            options.reason = Dictionary.TR000005
            const auth_schema = Validation.object({
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                by : Validation.string().min(5).max(50).optional().valid('month', 'date', 'default')
            }).when('.by', {  /* <-- prefix with . */
                switch : [
                    { 
                        is: 'month', then: 
                        Validation.object({ 
                            month : Validation.number().integer().min(1).max(12).required(),
                            year : Validation.number().integer().min(1945).max(9999).required()
                        }) 
                    },
                    { 
                        is: 'date', then: 
                        Validation.object({ 
                            date : Validation.date().format(['YYYY/MM/DD', 'DD-MM-YYYY']).required()
                        }) 
                    },
                ]
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                if(!validationprc.data.by){
                    validationprc.data.by = 'default'
                }
                let conference = ChannelConference
                .query()
                .select('id', 'xid', 'title', 'description', 'max_attendees',
                    Database.raw(
                        `
                        CONVERT_TZ(start_date, gmt_tz,?) start_at,
                        CONVERT_TZ(end_date, gmt_tz,?) end_at,
                        TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) start_sec,
                        TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(end_date, gmt_tz,'+07:00'), now())) end_sec,
                        TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) end_reg
                        `, [validationprc.data.tz, validationprc.data.tz]
                    ),
                    Database.raw('(SELECT COUNT(id) attendees from channel_conference_attendees cca WHERE cca.conference_id = channel_conferences.id) current_attendees')
                )
                .with('_v1_channel_conference_attendees', (builder) => {
                    builder
                    .where('channel_subscriber_id', usersession.id)
                    .where('blocked', 0)
                    .where('is_removed', 0)
                })
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereNotNull('registration_until')
                switch (validationprc.data.by) {
                    case 'month':
                        conference
                        .whereRaw('YEAR(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = ?', [validationprc.data.tz, validationprc.data.year])
                        .whereRaw('MONTH(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = ?', [validationprc.data.tz, validationprc.data.month])
                        break
                    case 'date':
                        conference
                        .whereRaw('DATE(CONVERT_TZ(start_date, gmt_tz, ?)) = ?', [validationprc.data.tz, validationprc.data.date])
                        break
                    default:
                        conference
                        .whereRaw('YEAR(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = YEAR(DATE(CONVERT_TZ(now(), ?, ?)))', [validationprc.data.tz, '+07:00',validationprc.data.tz])
                        .whereRaw('MONTH(DATE(CONVERT_TZ(start_date, gmt_tz, ?))) = MONTH(DATE(CONVERT_TZ(now(), ?, ?)))', [validationprc.data.tz, '+07:00',validationprc.data.tz])
                }
                conference
                .orderByRaw('CONVERT_TZ(start_date, gmt_tz,?) ASC', [validationprc.data.tz])

                conference = await conference.fetch()
                if(conference){
                    conference = conference.toJSON()
                    if(validationprc.data.by == 'month'){
                        let filtered = []
                        let filtered_current = conference.filter(function(row) {
                            return row.start_sec < 0 && row.end_sec > 0;
                        });
                        let filtered_upcoming = conference.filter(function(row) {
                            return row.start_sec > 0;
                        });
                        let filtered_expired = conference.filter(function(row) {
                            return row.end_sec < 0;
                        });

                        //sort expired desc
                        filtered_expired = filtered_expired.sort(function(a, b){
                            let keyA = a.start_sec,
                                keyB = b.start_sec;
                            if(keyA < keyB) return 1;
                            if(keyA > keyB) return -1;
                            return 0;
                        });

                        //combine filtered
                        filtered = [...filtered_current, ...filtered_upcoming, ...filtered_expired]
                        
                        conference = filtered;
                    }

                    let _conference = []
                    for(const key in conference){
                        const ckey = conference[key]
                        ckey.current = 'AKAN DATANG'
                        ckey.hex = '#0096db' 
                        ckey.action = {
                            register : false,
                            join : false
                        }
                        if(ckey._v1_channel_conference_attendees){
                            ckey.current = 'MASUK'
                            ckey.hex = '#7dc9a4'
                            ckey.action.join = true
                            if(ckey.end_sec < 0){
                                ckey.current = 'TELAH BERAKHIR'
                                ckey.hex = '#7dc9a4'
                                ckey.action.join = false
                            }
                            if(ckey.start_sec > 0){
                                ckey.current = 'BELUM DIMULAI'
                                ckey.hex = '#7dc9a4'
                                ckey.action.join = false
                                if(ckey.start_sec < 3600){
                                    //ckey.current = 'SEGERA DIMULAI'
                                    ckey.current = 'BELUM DIMULAI'
                                    ckey.hex = '#7dc9a4'
                                    ckey.action.join = false
                                }
                            }
                        } else {
                            ckey.current = 'DAFTAR'
                            ckey.hex = '#7dc9a4'
                            ckey.action.register = true
                            if(ckey.end_reg < 0){
                                ckey.current = 'TELAH DITUTUP'
                                ckey.action.register = false
                            } 
                            if(ckey.current_attendees >= ckey.max_attendees){
                                ckey.current = 'PENUH'
                                ckey.action.register = false
                            } 
                        }
                        const _data = _.pick(ckey, ['xid', 'title', 'description', 'start_at', 'start_sec', 'end_at', 'current', 'hex', 'action'])
                        _conference.push(_data)
                    }

                    let timeline = []
                    if(_conference.length > 0){
                        for(const key in _conference){
                            const date = moment(_conference[key].start_at).format("YYYY-MM-DD")
                            let timeline_item = timeline.find(o => o.date === date)
                            if(!timeline_item){
                                timeline.push(
                                    {
                                        date : date,
                                        data : [_conference[key]]
                                    }
                                )
                            } else {
                                timeline_item['data'].push(_conference[key])
                            }
                        }
                        options.reason = Dictionary.TR000004
                    }

                    dataset = timeline
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 
            if(dataset){
                options.type = 'SUC_200'
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async register(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                source: Validation.string().min(5).max(50).required().valid('mobile', 'direct', 'event'),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channelconference = await ChannelConference
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(end_date, gmt_tz,'+07:00'), now())) > 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()
                    if(attend){
                        let exec_1 = null
                        attend = attend.toJSON()
                        options.type = 'ERR_409'
                        options.reason = Dictionary.TR000010
                    } else {
                        let exec_1 = null
                        const record_1 = {
                            xid : uuid.v1(),
                            conference_id : channelconference.id,
                            channel_subscriber_id : usersession.id,
                            registered_from : validationprc.data.source
                        }
                        exec_1 = await ChannelConferenceAttendee.create(record_1)
                        if(exec_1){
                            dataset = {
                                xid : exec_1.xid
                            }
                        } 
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async notify(data, usersession, opt){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            
            if(validationprc.success){
                const channelconference = await ChannelConference
                .query()
                .select('id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) > 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(registration_until, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()
                    if(attend){
                        if(!attend.is_notify){
                            let exec_1 = null
                            exec_1 = await ChannelConferenceAttendee.query()
                            .where('id',  attend.id)
                            .update({ 
                                is_notify : 1
                            })
                            if(exec_1){
                                dataset = {
                                    xid : attend.xid
                                }
                            } 
                        } else {
                            let exec_1 = null
                            attend = attend.toJSON()
                            options.type = 'ERR_409'
                            options.reason = Dictionary.TR000010
                        }               
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }

    async join(data, usersession, opt, request){
        let options = opt
        let dataset = null
        try{
            options.type = 'ERR_400'
            options.reason = Dictionary.TR000006
            const auth_schema = Validation.object({
                tz : Validation.string().regex(/^(?:Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$/).required(),
                xid : Validation.string().guid().required()
            })
            let validationprc = await MicroH.validation(data, auth_schema)
            if(validationprc.success){
                let channelconference = await ChannelConference
                .query()
                .select('id', 'xid', 'title', 'url', 'meet_key', 'meet_id')
                .where('xid', validationprc.data.xid)
                .where('channel_id', usersession.channel_id)
                .where('is_published', 1)
                .where('is_archived', 0)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(start_date, gmt_tz,'+07:00'), now())) < 0`)
                .whereRaw(`TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(end_date, gmt_tz,'+07:00'), now())) > 0`)
                .first()
                if(channelconference){
                    let attend = await ChannelConferenceAttendee
                    .query()
                    .where('channel_subscriber_id', usersession.id)
                    .where('conference_id', channelconference.id)
                    .first()

                    let members = await TemporaryMember
                    .query()
                    .where('uid', usersession.uid)
                    .first()

                    if(attend && members){
                        channelconference = channelconference.toJSON()
                        const meet = await ExtendedServiceH.generatmeet(channelconference.url, channelconference.meet_id, channelconference.title, members.email, members.name, 'member')
                        if(meet){
                            dataset = meet
                        }           
                    }
                }
            } else {
                options.type = 'ERR_412'
                options.reason  = await MicroH.reason('TR000003', validationprc.message)
            } 

            if(dataset){
                options.type = 'SUC_200'
                options.reason = Dictionary.TR000004
                options.dataset = dataset
            }
        } catch(e){
            console.log(e)
            options.type = 'ERR_400'
            options.reason  = await MicroH.reason('TR000001', e.message)
        }
        return options
    }
    
    async activities(channelconference, usersession, type, gmt_tz, request){
        try{
            if(channelconference && request){
                let exec_1 = null
                let record_1 = {
                    conference_id : channelconference.id,
                    channel_subscriber_id : usersession.id,
                    type : type,
                    gmt_tz : gmt_tz
                }
                const agent = await MicroH.detectagent(request)
                record_1 = _.merge(record_1, agent)
                exec_1 = await ChannelConferenceActivity.create(record_1)
            }
        } catch(e){
            console.log(e)
        }
    }
}

module.exports = new ChannelConferenceMicro

