'use strict'

const ChannelMenu = use('App/Models/ChannelMenu')
const ChannelRoleMenu = use('App/Models/ChannelRoleMenu')
const Channel = use('App/Models/Channel')
const ChannelRoles = use('App/Models/ChannelRole')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const _lo = use('lodash')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig = require('../../../next/config.js')
const { validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')

class ChannelRolesController {

	async datatable({ request, response, auth, session }) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('ChannelRoles', 'read', auth, admin_page)

		const queryModel = ChannelRoles.query()
			// .innerJoin('channel_role_menus', 'channel_roles.id', 'channel_role_menus.channel_role_id')
			.innerJoin('channels', 'channel_roles.channel_id', 'channels.id')
			// .innerJoin('channel_menus', 'channel_menus.id', 'channel_role_menus.channel_menu_id')
			// .innerJoin('menus', 'menus.id', 'channel_menus.menu_id')
			.select(
				'channel_roles.id',
				'channel_roles.name AS role_name',
				'channel_roles.permissions',
				'channel_roles.status as role_status',
				'channels.channel_name',
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.where('channel_roles.is_archived', 0)

			if (!checkAuthAdmin) {
				model
					.innerJoin('user_channels', 'channels.id', 'user_channels.channel_id')
					.where('user_channels.xid', auth.user.xid)
			}
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}

	async create({ request, response, auth, session }) {
		let datas2
		let channelid
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelRoles', 'create', auth, admin_page)

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data)

		if (checkAuth) {
			try {
				channelid = await MyHash.decrypt(form.cid)
			} catch (e) {
				channelid = form.cid
			}
		}
		else {
			channelid = auth.user.channel_id
		}

		let tempxid = uuidv4()
		let formData = {
			xid: tempxid,
			name: form.name,
			channel_id: channelid,
			status: form.status,
			permissions: typeof form.permissions != 'undefined' ? JSON.stringify(form.permissions) : null,
			type: form.type == null || undefined ? '' : form.type.value,
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}

		let rules = {
			name: 'required',
			channel_id: 'required',
			status: 'required',
		}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: validation
			}
			return response.send(data)
		} else {
			try {
				await ChannelRoles.create(formData)

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Roles added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Roles cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async edit({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelRoles', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()

		let channelroles = await ChannelRoles.query()
			.select(
				'channels.id as channel_id',
				'channel_roles.name as channel_roles_name',
				'channel_roles.permissions',
				'channel_roles.status',
				'channel_roles.id as channel_role_id',
				'channel_roles.type',
				'channels.channel_name',
			)
			.leftJoin('channels', 'channels.id', 'channel_roles.channel_id')
			.where('channel_roles.id', await MyHash.decrypt(formData.id))
			.first()

		if (channelroles) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel Roles found',
				data: channelroles
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Roles cannot be found',
				data: []
			}
			return response.send(data)
		}
	}

	async update({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelRoles', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-	DD kk:mm:ss');
		let form = JSON.parse(request.post().data)

		let channelroles2 = await ChannelRoles.find(await form.channel_role_id)
		let formData = {
			name: form.name,
			channel_id: form.cid,
			status: form.status,
			type: form.type == null || undefined ? '' : form.type.value,
			permissions: typeof form.permissions != 'undefined' ? JSON.stringify(form.permissions) : null,
			updated_by: auth.user.id,
			updated_at: currentTime
		}

		let rules = {
			name: 'required',
			channel_id: 'required',
			status: 'required',
		}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: validation
			}
			return response.send(data)
		} else {
			Object.assign(channelroles2.$attributes, formData)
			channelroles2.save()
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Roles updated',
				data: []
			}
			return response.send(data)
		}
	}

	async delete({ request, response, auth, session }) {
		let channelroledata
		let channelrole
		let checkAuth = await CheckAuth.get('ChannelRoles', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		const formData = request.post()
		let channelrolesmenu = await ChannelRoles.find(await MyHash.decrypt(formData.id))
		if (channelrolesmenu) {
			try {
				// channelroledata = await ChannelRoleMenu.query().select('channel_role_id').where('id', await MyHash.decrypt(formData.id)).limit(20).fetch()
				// await ChannelRoleMenu.query().where('id', await MyHash.decrypt(formData.id)).update({
				// 	is_archived: 1,
				// 	updated_by: auth.user.id,
				// 	updated_at: currentTime
				// })

				// channelrole = channelroledata.toJSON()

				await ChannelRoles.query().where('id', await MyHash.decrypt(formData.id)).update({
					is_archived: 1,
					updated_by: auth.user.id,
					updated_at: currentTime
				})

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Roles Menu deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				// console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Roles Menu cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Roles Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async multidelete({ request, response, auth, session }) {
		let channelroledata
		let channelrole
		let checkAuth = await CheckAuth.get('ChannelRoles', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				const ids = []
				for (let i in dataitem) {
					ids.push(await MyHash.decrypt(dataitem[i]))
				}

				await ChannelRoles.query().whereIn('id', ids).update({
					is_archived: 1,
					updated_by: auth.user.id,
					updated_at: currentTime
				})

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Roles Menu deleted',
					data: []
				}
				return response.send(data)

			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Roles Menu cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Roles Menu to delete',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannel({ request, response, auth, session }) {
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelRoles', 'read', auth, admin_page)
		let req = request.get()
		let models = Channel.query().select('id as value', 'channel_name as label')

		if (!checkAuth) {
			models = models.where('is_admin', '=', false)
		}

		if (req.phrase != '') {
			models = models.where('channel_name', 'like', `%${req.phrase}%`)
		}
		const results = await models.limit(100).fetch()

		return response.send(results.toJSON())
	}

	async getChannelMenu({ request, response, auth, session }) {
		let req = request.all()
		let datas
		let checkAuth = await CheckAuth.get('ChannelRoles', 'read', auth, true)

		if (!checkAuth) {
			datas = await ChannelMenu.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').where('channel_menus.is_archived', 'LIKE', '%0%').andWhere('channel_menus.channel_id', 'LIKE', auth.user.channel_id).limit(100).fetch()
		}
		else {
			if (req.phrase != '') {
				datas = await ChannelMenu.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').where('channel_menus.is_archived', 'LIKE', '%0%').andWhere('channel_menus.channel_id', 'LIKE', req.channel_id).limit(100).fetch()
			} else {
				datas = await ChannelMenu.query().select('channel_menus.id', 'menus.name').leftJoin('menus', 'menus.id', 'channel_menus.menu_id').limit(100).fetch()
			}
		}

		let data = datas.toJSON()

		let result = []
		for (let i in data) {
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}

		return response.send(result)
	}

	async updateStatus({ request, response, auth }) {
        const { id } = request.post()
        const idDecrypted = await MyHash.decrypt(id)
        const record = await ChannelRoles.find(idDecrypted)
        if (record != null) {
			Object.assign(record.$attributes, { status: !record.status })
            record.save()
            return response.defaultResponseJson(200, 'Success to update status channel')
        } else {
            return response.defaultResponseJson(400, 'Banner not found')
        }
    }
}

module.exports = ChannelRolesController