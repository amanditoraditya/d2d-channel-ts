'use strict'

const MyHash 		      = require('./Helper/Hash.js')
const CheckAuth 		  = require('./Helper/CheckAuth.js')
const ChannelJobApplicant = use('App/Models/ChannelJobApplicant')
const Datatable = use('App/Helpers/Datatable')

class ChannelJobApplicantController {
	async datatableDetail({request, response, auth, session}) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('ChannelJobApplicant', 'read', auth, admin_page)
		
		const formData = request.post()
		let jobapplicantId = await MyHash.decrypt(formData.id);

		const queryModel = ChannelJobApplicant.query()
			.innerJoin('channel_jobs', 'channel_jobs.id', 'channel_job_applicants.channel_job_id')
			.innerJoin('channel_job_applicant_statuses', 'channel_job_applicant_statuses.channel_job_applicant_id', 'channel_job_applicants.id')
			.innerJoin('channels', 'channels.id', 'channel_jobs.channel_id')
			.innerJoin('channel_subscribers ', 'channel_job_applicants.channel_subscriber_id', 'channel_subscribers.id')
			.innerJoin('temporary_members', 'channel_subscribers.uid', 'temporary_members.uid')
			.select(
				'channel_job_applicants.id',
				'temporary_members.name',
				'temporary_members.email',
				'temporary_members.phone',
				'temporary_members.home_location',
				'temporary_members.spesialization',
				'temporary_members.nonspesialization',
				'temporary_members.display_picture',
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.where('channel_job_applicant_statuses.status', 'submitted')
				.where('channel_subscribers.blocked', 0)
				.where('channel_subscribers.is_unsubscribed', 0)
				.where('channels.is_archived', 0)
				.where('channel_jobs.id', jobapplicantId)
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}
}

module.exports = ChannelJobApplicantController