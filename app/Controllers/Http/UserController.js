'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const RoleUser = use('App/Models/RoleUser')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const MyHash = require('./Helper/Hash.js')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Env = use('Env')
const Logger = use('Logger')

class UserController {
	async datatable({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('User', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		
		let tableDefinition
		// let get_roles = await auth.user.roles().fetch()
		// let users_roles_json = get_roles.toJSON()
		// let users_roles = []
		// for(let i in users_roles_json){
		// 	users_roles.push(users_roles_json[i].role_slug)
		// }
		
		// if (users_roles.includes('superadmin')) {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["users.id", "users.username", "users.email", "users.fullname"],
				aSearchColumns: ["users.username", "users.email", "users.fullname"]
			}
		// } else {
		// 	tableDefinition = {
		// 		sTableName: 'users',
		// 		sSelectSql: ["users.id", "users.username", "users.email", "users.fullname"],
		// 		aSearchColumns: ["users.username", "users.email", "users.fullname"],
		// 		sWhereAndSql: 'users.id = "'+ auth.user.id +'"'
		// 	}
		// }
		
		Logger.info('tableDefinition is %j', tableDefinition)
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			let id = select[0][x]['id']
			let check = ''
			let action = ''
			if (select[0][x]['id'] == auth.user.id) {
				check = ""
				action = "<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/users/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/users/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
					</div>\
				</div>\n"
			} else {
				check = "<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n"
				action = "<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' data-href='/module/users/edit?id="+ await MyHash.encrypt(id.toString()) +"' data-as='/users/edit/"+ await MyHash.encrypt(id.toString()) +"' class='btn btn-sm btn-primary btn-edit' title='Edit'><i class='fas fa-pencil-alt'></i></a>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"
			}
			fdata.push([
				check,
				select[0][x]['id'],
				select[0][x]['username'],
				select[0][x]['fullname'],
				select[0][x]['email'],
				action
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('User', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const { fullname, username, email, password, user_type, user_role } = request.only(['fullname', 'username', 'email', 'password','user_type','user_role'])
		
		let formData = {
			username: username,
			email: email,
			fullname: fullname,
			password: password,
			user_type: user_type,
			user_role: user_role,
			social_token: '',
			activation_key: await Hash.make(password),
			block: 'N',
			forget_key: null,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			username: 'required|unique:users,username',
			email: 'required|email|unique:users,email',
			password: 'required',
			user_type: 'required',
			user_role: 'required'
		}
		
		const validation = await validateAll(formData, rules)	
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				let newuser = await User.create(formData)
				
				await RoleUser.create({
					role_id: user_role,
					user_id: newuser.id,
					created_by: auth.user.id,
					updated_by: auth.user.id
				})
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let user
		let sessionUser = session.get('users_roles')
		const formData = request.post()
		
		// if (auth.user.id == '1') {
			user = await User.query().select('users.*', 'roles.role_title').leftJoin('roles', 'users.user_role', 'roles.id').where('users.id', await MyHash.decrypt(formData.id)).first()
		// } else {
		// 	if (auth.user.id == await MyHash.decrypt(formData.id)) {
		// 		user = await User.query().select('users.*', 'roles.role_title').leftJoin('roles', 'users.user_role', 'roles.id').where('users.id', await MyHash.decrypt(formData.id)).first()
		// 	} else {
		// 		if (sessionUser[0] == 'superadmin') {
		// 			user = await User.query().select('users.*', 'roles.role_title').leftJoin('roles', 'users.user_role', 'roles.id').where('users.id', await MyHash.decrypt(formData.id)).first()
		// 		} else {
		// 			user = false
		// 		}
		// 	}
		// }
		
		if (user) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User found',
				data: user
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const { fullname, username, email, password, user_type, user_role } = request.only(['fullname', 'username', 'email', 'password', 'user_type','user_role'])

		let user
		let sessionUser = session.get('users_roles')
		if (auth.user.id == '1') {
			user = await User.find(await MyHash.decrypt(params.id))
		} else {
			if (auth.user.id == await MyHash.decrypt(params.id)) {
				user = await User.find(await MyHash.decrypt(params.id))
			} else {
				if (sessionUser[0] == 'superadmin') {
					user = await User.find(await MyHash.decrypt(params.id))
				} else {
					user = false
				}
			}
		}
		
		const userData = {
			username: username,
			email: email,
			user_type: user_type,
			user_role: user_role
		}
		
		let rules = {
			username: `required|unique:users,username,id,${user.id}`,
			email: `required|unique:users,email,id,${user.id}`
		}

		const validation = await validateAll(userData, rules)	
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (user) {
				if (password == '' || password == null) {
					await User.query().where('id', await MyHash.decrypt(params.id)).update({
						username: username,
						fullname: fullname,
						email: email,
						user_type: user_type,
						user_role: user_role,
						updated_by: auth.user.id
					})
				} else {
					await User.query().where('id', await MyHash.decrypt(params.id)).update({
						username: username,
						fullname: fullname,
						email: email,
						password: await Hash.make(password),
						user_type: user_type,
						user_role: user_role,						
						updated_by: auth.user.id
					})
				}
				
				await RoleUser.query().where('user_id', await MyHash.decrypt(params.id)).update({
					role_id: user_role,
					user_id: await MyHash.decrypt(params.id),
					updated_by: auth.user.id
				})
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		if (await MyHash.decrypt(formData.id) != auth.user.id) {
			let user
			let sessionUser = session.get('users_roles')
			if (auth.user.id == '1') {
				user = await User.find(await MyHash.decrypt(formData.id))
			} else {
				if (auth.user.id == await MyHash.decrypt(formData.id)) {
					user = await User.find(await MyHash.decrypt(formData.id))
				} else {
					if (sessionUser[0] == 'superadmin') {
						user = await User.find(await MyHash.decrypt(formData.id))
					} else {
						user = false
					}
				}
			}
			if (user){      
				try {
					await user.delete()
					
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '2000',
						message: 'User success deleted',
						data: []
					}
					return response.send(data)
				} catch (e) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'User cannot be deleted',
						data: []
					}
					return response.send(data)
				}
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				if (await MyHash.decrypt(dataitem[i]) != auth.user.id) {
					let user
					let sessionUser = session.get('users_roles')
					if (auth.user.id == '1') {
						user = await User.find(await MyHash.decrypt(dataitem[i]))
						try {
							await user.delete()
						} catch (e) {}
					} else {
						if (auth.user.id == await MyHash.decrypt(dataitem[i])) {
							user = await User.find(await MyHash.decrypt(dataitem[i]))
							try {
								await user.delete()
							} catch (e) {}
						} else {
							if (sessionUser[0] == 'superadmin') {
								user = await User.find(await MyHash.decrypt(dataitem[i]))
								try {
									await user.delete()
								} catch (e) {}
							} else {
								user = false
							}
						}
					}
				}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async getUser({request, response, auth, session}) {
		let req = request.get()
		let datas
		if (req.phrase != '') {
			datas = await User.query().select('users.id', 'users.fullname', 'users.email').where(function() {
				this.where('users.fullname', 'LIKE', '%' + req.phrase + '%')
				.orWhere('users.email', 'LIKE', '%' + req.phrase + '%')
			}).limit(20).orderBy('users.id', 'DESC').fetch()
		} else {
			datas = await User.query().select('users.id', 'users.fullname', 'users.email').limit(20).orderBy('users.id', 'DESC').fetch()
		}
		let data = datas.toJSON()
		
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['fullname'] + ' (' + data[i]['email'] + ')'
			})
		}
		
		return response.send(result)
	}
}

module.exports = UserController