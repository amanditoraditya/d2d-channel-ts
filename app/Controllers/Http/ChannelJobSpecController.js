'use strict'

const oss = require('ali-oss');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV;
const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const MyHash 		= require('./Helper/Hash.js')
const { v4: uuidv4 } = require('uuid');

const ChannelJobSpec   	= use('App/Models/ChannelJobSpec')
const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')
const Datatable         = use('App/Helpers/Datatable')

const { validate, validateAll } = use('Validator')

class ChannelJobSpecController {
	async datatable({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('ChannelJobSpec', 'read', auth, true)

		let fdata 			= [];
		let recordsTotal 	= 0;
		let recordsFiltered = [];
		const formData 		= request.post()
		
		let data			= {
			draw: formData.draw,
			recordsTotal: 0,
			recordsFiltered: [],
			data:[]
		};

		let jobspecId

		if(formData.id == null)
		{
			jobspecId = '""'
		}
		else
		{
			jobspecId = await MyHash.decrypt(formData.id);
		}

		let fields = [
			'cjs.id', 'cjs.title AS spec_title', 'cjs.description AS spec_desc'
		];
		let condition = []
		let joins = "channel_job_specs cjs \
		LEFT JOIN channel_jobs cj ON cj.id = cjs.channel_job_id"
		
		condition = [`cjs.is_archived = 0 AND cj.is_archived = 0 AND cj.id = ${jobspecId}`]

		let tableDefinition = {
			sTableName		: 'channel_job_specs cjs',
			sSelectSql		: fields,
			aSearchColumns	: ['cjs.title', 'cjs.description'],
			sWhereAndSql	: condition,
			sFromSql 		: joins
		}

		let queryBuilder = new QueryBuilder(tableDefinition)
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		recordsTotal = await Database.raw(queries.recordsTotal)
		recordsFiltered = await Database.raw(queries.recordsFiltered)

		let no = 0
		for(let row of select[0]) {
			let id = row['id']
			let content = ''
				content  = [
				"<div class='text-center'>\
					<label class='checkbox checkbox-lg checkbox-inline'>\
						<input type='checkbox' id='titleCheckdel' />\
						<span></span>\
					</label>\
					<input type='hidden' class='deldata' name='item[]' value='"+ await MyHash.encrypt(id.toString()) +"' disabled />\
				</div>\n",
				row['id'],
				`<div style="overflow:hidden;text-overflow:ellipsis;max-width:250px">${row['spec_title']}</div>`,
				`<div style="overflow:hidden;text-overflow:ellipsis;max-width:750px">${row['spec_desc']}</div>`,
				"<div class='text-center'>\
					<div class='btn-group btn-group-sm'>\
						<a href='javascript:void(0);' class='btn btn-sm btn-danger alertdel' id='"+ await MyHash.encrypt(id.toString()) +"' title='Delete'><i class='far fa-trash-alt'></i></a>\
					</div>\
				</div>\n"]
			fdata.push(content)
			no++
		}
		
		data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data;
	}

	async datatableDetail({request, response, auth, session}) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('ChannelJobSpec', 'read', auth, admin_page)
		
		const formData = request.post()
		let jobspecId = await MyHash.decrypt(formData.id);

		const queryModel = ChannelJobSpec.query()
			.innerJoin('channel_jobs', 'channel_jobs.id', 'channel_job_specs.channel_job_id')
			.select(
				'channel_job_specs.id',
				'channel_job_specs.title AS spec_title',
				'channel_job_specs.description AS spec_desc',
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.where('channel_jobs.is_archived', 0)
				.where('channel_job_specs.is_archived', 0)
				.where('channel_jobs.id', jobspecId)
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}

	async create({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJobSpec', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data)

		let channeljobid = await MyHash.decrypt(params.id)
	
		let xid = uuidv4()
		let formData = {
			xid,
			channel_job_id: channeljobid,
			title: form.title,
			description: form.description,
			is_doc_needed: null,
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}

		let rules = {
			title: 'required',
			description: 'required'
		}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {

				await ChannelJobSpec.create(formData)

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Job Spec added',
					data: params.id
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Job Spec cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelJobSpec', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let channeljobspec = await ChannelJobSpec.find(await MyHash.decrypt(formData.id))
		if (channeljobspec){
			try {
				// Soft Delete Channel Job Spec
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				channeljobspec.updated_by = auth.user.id,
				channeljobspec.updated_at = currentTime,
				channeljobspec.is_archived = 1
				await channeljobspec.save()

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Job Spec deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job Spec cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job Spec to delete',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('ChannelJobSpec', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					// Soft Delete Channel Job Spec
					let channeljobspec = await ChannelJobSpec.find(await MyHash.decrypt(dataitem[i]))
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					channeljobspec.updated_by  = auth.user.id,
					channeljobspec.updated_at  = currentTime,
					channeljobspec.is_archived = 1
					await channeljobspec.save()
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					Message: 'Channel Job Spec deleted',
					data: []
				}
			return response.send(data)
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job Spec cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job Spec to delete',
				data: []
			}
			return response.send(data)
		}
	}
}

module.exports = ChannelJobSpecController