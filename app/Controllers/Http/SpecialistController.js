'use strict'

const Database = use('Database')
const _lo = use('lodash')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment');

const Specialist = use('App/Models/Specialist')
const ContentSVC = require('../../Services/content-service')

const { validate, validateAll } = use('Validator')

class SpecialistController {
    async list({ request, response, auth, session }) {
        let req = request.all()
        let page = -1
        let limit = 20
        let offset = Number(req.offset)

        page = (offset / limit) + 1
        
        let data = await ContentSVC.specialist.getAll([], page, limit)
        let result = []
        let docs = _lo.isEmpty(data.docs) ? [] : data.docs
        for (let row of docs) {
            result.push({
                value: row.id,
                label: `${row.title} - ${row.description}`
            })
        }

        let res = {}
        res.result = result
        res.has_more = _lo.isEmpty(result) ? false : true

        return response.send(res)
    }
}

module.exports = SpecialistController