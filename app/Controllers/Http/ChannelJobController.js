'use strict'

//library
const { data } = require('jquery');
const oss = require('ali-oss');
const Database = use('Database')
const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const _lo = use('lodash')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment');
const Hash = use('Hash')
const { v4: uuidv4 } = require('uuid');
const { validate, validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')

//custom module
const goaConfig = require('../../../next/config.js')
const MyHash = require('./Helper/Hash.js')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const FileService = require('./Helper/FileService.js')

//models
const Channel = use('App/Models/Channel')
const ChannelJob = use('App/Models/ChannelJob')
const ChannelJobType = use('App/Models/ChannelJobType')
const ChannelJobCharacteristic = use('App/Models/ChannelJobCharacteristic')
const ChannelJobSpec = use('App/Models/ChannelJobSpec')
const ChannelJobHeaderAttribute = use('App/Models/ChannelJobHeaderAttribute')

const environment = goaConfig.NODE_ENV;
const content_type = 'job'

class ChannelJobController {
	async datatable({ request, response, auth, session }) {
		let checkAuthAdmin = await CheckAuth.get('ChannelJob', 'read', auth, true)

		const queryModel = ChannelJob.query()
			.innerJoin('channels', 'channel_jobs.channel_id', 'channels.id')
			.innerJoin('channel_job_types', 'channel_jobs.job_type_id', 'channel_job_types.id')
			.select(
				'channel_jobs.id',
				'channels.channel_name',
				'channel_job_types.name as channel_job_type_name',
				'channel_jobs.title',
				'channel_jobs.is_published',
				'channel_jobs.created_at',
				'channel_jobs.due_date'
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model
				.where('channel_jobs.is_archived', 0)

			if (!checkAuthAdmin) {
				model
					.innerJoin('user_channels', 'channels.id', 'user_channels.channel_id')
					.where('user_channels.xid', auth.user.xid)
			}
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}
	async create({ request, response, auth, session }) {
		try {
			let checkAuth = await CheckAuth.get('ChannelJob', 'create', auth)
			if (!checkAuth) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: 'User cannot access this module',
					data: []
				}
				return response.send(data)
			}

			let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
			//get data form
			let data = JSON.parse(request.post().data)

			let response_data = {
				code: '2000',
				message: 'Channel Job added',
				data,
			}

			if (_lo.isEmpty(data.job) == false) {
				let response_message = [];

				let apply_button = null;
				if (data.job.job_type_id_value == 1) {
					apply_button = "Daftar Sebagai Speaker"
				}
				else {
					apply_button = "Daftar Sebagai Moderator"
				}

				let xid = uuidv4();
				let job_form = {
					xid: xid,
					channel_id: data.job.channel_id_value,
					title: data.job.title,
					registration_url: data.job.registration_url,
					registration_button_caption: data.job.registration_button_caption,
					job_type_id: data.job.job_type_id_value,
					triggered_by: auth.user.id,
					image: '',
					apply_button: apply_button,
					is_published: data.job.is_published,
					due_date: data.job.due_date,
					is_archived: 0,
					created_by: auth.user.id,
					updated_by: auth.user.id,
					created_at: currentTime,
					updated_at: currentTime,
				};

				const image_file = request.file('image', {
					type: 'image',
					subtype: ['image/jpeg', 'image/png'],
					size: '2mb',
					extnames: ['jpg', 'jpeg', 'png']
				})

				if (image_file != null) {
					job_form.image = true
				} else if (data.job.image_copy != null){
					job_form.image = true
				}

				let rules = {
					title: 'required',
					channel_id: 'required',
					job_type_id: 'required',
					is_published: 'required',
					due_date: 'required',
					image: 'required'
				}

				const validation = await validateAll(job_form, rules)
				if (validation.fails()) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Please fill required marker field!',
						data: validation
					}
					return response.send(data)
				} else {
					if (image_file != null) {

						let path_tmp = `${Helpers.tmpPath('uploads')}/${image_file.clientName}`
						fs.stat(path_tmp, function (err, stats) {
							if (err) {
								return console.error("Error on delete file | ChannelJobController:update", err)
							}

							fs.unlink(path_tmp, function (err) {
								if (err) return console.log(err);
							});
						});
						// put temporary file to trigger validation
						await image_file.move(Helpers.tmpPath('uploads'), {
							name: image_file.clientName,
							overwrite: true
						})
						// error validation file
						if (!image_file.moved()) {
							response.header('Content-type', 'application/json')
							response.type('application/json')
							let data = {
								code: '4004',
								message: `Invalid File Upload! - ${image_file.error().message}`,
								data: image_file.error()
							}
							return response.send(data)
						} else {
							//upload image
							let upload_setting = {
								xid: xid,
								content: content_type,
								tempPath: path_tmp,
							}
							let upload_image = await FileService.upload(image_file, upload_setting, true)

							job_form.image = upload_image.url;
							// delete temporary file
							fs.unlinkSync(path_tmp)
						}
					} else if(image_file == null && data.job.image_copy != null) {
						//#region copy image
						if (!_lo.isEmpty(data.job.image_copy)) {
							let parts = data.job.image_copy.split('/')
							let url = parts.slice(3, parts.length)
							url = url.join('/')
							let fullnewUrl = data.job.image_copy.replace(/[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/, xid)
 							let newUrl = url.replace(/[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/, xid)
							
							await FileService.copy(newUrl,url);
							job_form.image = fullnewUrl
						}
						//#endregion
					}

					let newJob = await ChannelJob.create(job_form);
					response_message.push('Channel Job Saved')

					if (_lo.isEmpty(newJob) == true) {
						newJob = await ChannelJob.query()
							.where('channel_id', channelid)
							.where('xid', xid)
							.first();
					}

					if (_lo.isEmpty(data.spec) == false) {
						let job_specs = Array.isArray(data.spec) ? data.spec : [data.spec];

						for (let spek in job_specs) {
							let valid = _lo.isEmpty(job_specs[spek].title) == false && _lo.isEmpty(job_specs[spek].description) == false;
							if (valid == true) {
								xid = uuidv4();
								let spec_form = {
									xid: xid,
									channel_job_id: newJob.id,
									title: job_specs[spek].title,
									description: job_specs[spek].description,
									is_doc_needed: null,
									updated_by: auth.user.id,
									updated_at: currentTime,
									created_by: auth.user.id,
									created_at: currentTime
								}

								await ChannelJobSpec.create(spec_form)
							}
						}
						response_message.push('Channel Job Spec Saved')
					}

					if (_lo.isEmpty(data.header) == false) {
						let job_headers = Array.isArray(data.header) ? data.header : [data.header]

						for (let header in job_headers) {
							if (typeof job_headers[header].job_header_attribute_id != 'undefined' && job_headers[header].job_header_attribute_id != null && typeof job_headers[header].detail != 'undefined' && job_headers[header].detail != null) {
								xid = uuidv4();
								let header_form = {
									xid: xid,
									channel_job_id: newJob.id,
									job_header_attribute_id: job_headers[header].job_header_attribute_id,
									value: job_headers[header].detail,
									updated_at: currentTime,
									created_at: currentTime,
									updated_by: auth.user.id,
									created_by: auth.user.id,
								}

								await ChannelJobHeaderAttribute.create(header_form)
							}
						}
						response_message.push('Channel Job Header Saved')
					}

					if (_lo.isEmpty(data.job.specialist) == false) {
						let specialistTemp = _lo.isArray(data.job.specialist) == false ? [data.job.specialist] : data.job.specialist

						for (let spe of specialistTemp) {
							xid = uuidv4();
							let jobSpecialistData = {
								xid: xid,
								channel_job_id: newJob.id,
								ref_id: spe.value,
								ref_type: 'specialist',
								value: spe.label,
								is_archived: 0,
								updated_by: auth.user.id,
								updated_at: currentTime,
								created_by: auth.user.id,
								created_at: currentTime
							}
							await ChannelJobCharacteristic.create(jobSpecialistData)
						}
					}

					response_data = {
						code: '2000',
						message: 'Channel Job added',
						data: response_message
					}
				}
			} else {
				response_data = {
					code: '4004',
					message: 'Channel Job cannot be added',
					data: []
				}
			}
			response.header('Content-type', 'application/json')
			response.type('application/json')
			return response.send(response_data)
		} catch (e) {
			console.log(e)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Job cannot be added, System encounter an error.',
				data: []
			}
			return response.send(data)
		}
	}

	async edit({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJob', 'update', auth, false)

		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let channelJobId = await MyHash.decrypt(params.id)
		let responseData = {
			job: '',
			spec: [],
			header: [],
		}

		let channel_job = await ChannelJob.query()
			.select('channel_jobs.id', 'channel_jobs.xid', 'channel_jobs.title', 'channel_jobs.description', 'channel_jobs.image',
				'channel_jobs.is_published', 'channel_jobs.registration_url', 'channel_jobs.registration_button_caption', 'channel_jobs.due_date', 'channel_job_types.name as job_type_name',
				'channel_job_types.id as job_type_id', 'channels.channel_name', 'channels.id as channel_id')
			.leftJoin('channels', 'channels.id', 'channel_jobs.channel_id')
			.leftJoin('channel_job_types', 'channel_job_types.id', 'channel_jobs.job_type_id')
			.where('channel_jobs.id', channelJobId)
			.first()

		if (channel_job) {
			channel_job.registration_url = channel_job.registration_url == null ? '' : channel_job.registration_url
			channel_job.registration_button_caption = channel_job.registration_button_caption == null ? '' : channel_job.registration_button_caption
			//#region get job specialist
			let getSpecialist = await ChannelJobCharacteristic.query()
				.select('ref_id as id', 'value as name')
				.where('channel_job_id', '=', channelJobId)
				.andWhere('is_archived', '0')
				.fetch()

			let jobSpecialistJson = getSpecialist.toJSON()
			let specialist = []
			for (let i in jobSpecialistJson) {
				specialist.push({
					id: jobSpecialistJson[i]['id'],
					title: jobSpecialistJson[i]['name']
				})
			}
			channel_job.specialist = specialist

			responseData.job = channel_job;
			//#endregion

			//#region get job specification
			let getJobSpec = await ChannelJobSpec.query()
				.select('id', 'title', 'description')
				.where('channel_job_id', '=', channelJobId)
				.andWhere('is_archived', '0')
				.fetch()

			let jobSpecJson = getJobSpec.toJSON()
			let jobSpec = []
			for (let i in jobSpecJson) {
				jobSpec.push({
					id: jobSpecJson[i]['id'],
					title: jobSpecJson[i]['title'],
					description: jobSpecJson[i]['description']
				})
			}

			if (!_lo.isEmpty(jobSpec)) {
				responseData.spec = jobSpec
			}
			//#endregion

			//#region get job header
			let getJobHeader = await ChannelJobHeaderAttribute.query()
				.select('channel_job_header_attributes.id', 'channel_job_header_attributes.job_header_attribute_id', 'value as detail', 'job_header_attributes.name', 'job_header_attributes.input_type as type')
				.leftJoin('job_header_attributes', 'job_header_attributes.id', 'channel_job_header_attributes.job_header_attribute_id')
				.where('channel_job_header_attributes.channel_job_id', '=', channelJobId)
				.andWhere('channel_job_header_attributes.is_archived', '0')
				.fetch()

			let jobHeader = getJobHeader.toJSON()
			jobHeader.map((header) => {
				let date
				if (header.type == 'datetime') {
					date = moment(header.detail, "YYYY-MM-DD HH:mm")
					header.detail = date.isValid() ? header.detail : null

				} else if (header.type == 'date') {
					date = moment(header.detail, "YYYY-MM-DD")
					header.detail = date.isValid() ? header.detail : null
				} else if (header.type == 'time') {
					date = moment(header.detail, "HH:mm")
					header.detail = date.isValid() ? header.detail : null
				}
			})

			if (!_lo.isEmpty(jobHeader)) {
				responseData.header = jobHeader
			}
			//#endregion

			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Channel Job found',
				data: responseData
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Job cannot be found',
				data: []
			}
			return response.send(data)
		}
	}

	async update({ params, request, response, auth, session }) {
		const trx = await Database.beginTransaction()
		try {
			let checkAuth = await CheckAuth.get('ChannelJob', 'update', auth)
			if (!checkAuth) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: 'User cannot access this module',
					data: []
				}
				return response.send(data)
			}

			let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

			let channelJobId = await MyHash.decrypt(params.id)
			let channel_job = await ChannelJob.find(channelJobId)
			let payload = JSON.parse(request.post().data)

			if (_lo.isEmpty(channel_job) == false) {
				let apply_button
				if (payload.job.job_type_id_value == 1) {
					apply_button = "Daftar Sebagai Speaker"
				} else {
					apply_button = "Daftar Sebagai Moderator"
				}
				let xid = channel_job.xid
				let formData = {
					xid: xid,
					channel_id: payload.job.channel_id_value,
					title: payload.job.title,
					registration_url: payload.job.registration_url,
					registration_button_caption: payload.job.registration_button_caption,
					job_type_id: payload.job.job_type_id_value,
					triggered_by: auth.user.id,
					apply_button,
					is_published: payload.job.is_published,
					due_date: payload.job.due_date,
					is_archived: 0,
					updated_by: auth.user.id,
					updated_at: currentTime
				}

				const image_file = request.file('image', {
					type: 'image',
					subtype: ['image/jpeg', 'image/png'],
					size: '2mb',
					extnames: ['jpg', 'jpeg', 'png']
				})

				let rules = {
					title: 'required',
					channel_id: 'required',
					job_type_id: 'required',
					is_published: 'required',
					due_date: 'required',
				}

				const validation = await validateAll(formData, rules)
				if (validation.fails()) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Please fill required marker field!',
						data: validation
					}
					return response.send(data)
				} else {
					if (channel_job) {
						if (channel_job.image != '' && channel_job.image != null && payload.job.remove_image) {
							let parts = channel_job.image.split('/')
							let url = parts.slice(3, parts.length)
							url = url.join('/')
							await FileService.delete(url);
							formData.image = ''
						}
						//#region upload image
						if (image_file != null) {
							// put temporary file to trigger validation
							let path_tmp = `${Helpers.tmpPath('uploads')}/${image_file.clientName}`
							fs.stat(path_tmp, function (err, stats) {
								if (err) {
									return console.error("Error on delete file | ChannelJobController:update", err)
								}

								fs.unlink(path_tmp, function (err) {
									if (err) return console.log(err);
								});
							});

							await image_file.move(Helpers.tmpPath('uploads'), {
								name: image_file.clientName,
								overwrite: true
							})

							// error validation file
							if (!image_file.moved()) {
								response.header('Content-type', 'application/json')
								response.type('application/json')
								let data = {
									code: '4004',
									message: `Invalid File Upload! - ${image_file.error().message}`,
									data: image_file.error()
								}
								return response.send(data)
							} else {
								//upload image
								let upload_setting = {
									xid: uuidv4(),
									content: content_type,
									tempPath: path_tmp,
								}
								let upload_image = await FileService.upload(image_file, upload_setting, true)

								formData.image = upload_image.url;
							}
						}
						//#endregion

						Object.assign(channel_job.$attributes, formData)
						channel_job.save()

						//=======================================
						//#region update data job specification
						if (_lo.isEmpty(payload.spec) == false) {
							let job_specs = _lo.isArray(payload.spec) ? payload.spec : [payload.spec];
							const editedSpec = []
							const objEditedSpec = {}
							const newestSpec = []
							job_specs.map((spec) => {
								if (typeof spec.id != 'undefined' && spec.id != '' && spec.id != null) {
									editedSpec.push(spec.id)
									objEditedSpec[spec.id] = spec
								} else {
									newestSpec.push({
										xid: uuidv4(),
										channel_job_id: channelJobId,
										title: spec.title,
										description: spec.description,
										is_doc_needed: null,
										updated_by: auth.user.id,
										updated_at: currentTime,
										created_by: auth.user.id,
										created_at: currentTime
									})
								}
							})
							await ChannelJobSpec.query()
								.where('channel_job_id', channelJobId)
								.whereNotIn('id', editedSpec)
								.update({
									is_archived: 1,
									updated_by: auth.user.id,
									updated_at: currentTime
								})
							if (newestSpec.length > 0) {
								await ChannelJobSpec.createMany(newestSpec)
							}

							// update spec by id
							editedSpec.map(async (specPayload) => {
								// console.log('specPayload', specPayload)
								if (typeof specPayload.id != 'undefined') {
									await ChannelJobSpec.query()
										.where('channel_job_id', channelJobId)
										.where('id', specPayload.id)
										.update({
											title: specPayload.title,
											description: specPayload.description,
											updated_by: auth.user.id,
											updated_at: currentTime
										})
								}
							})
						} else {
							await ChannelJobSpec.query()
								.where('channel_job_id', channelJobId)
								.update({
									is_archived: 1,
									updated_by: auth.user.id,
									updated_at: currentTime
								})
						}
						//#endregion

						//=======================================
						//#region update data job Header
						if (_lo.isEmpty(payload.header) == false) {
							const editedHeader = []
							const objEditedHeader = {}
							let newestHeader = []
							const newestHeaderObj = {}
							const indexNewestHeader = {}
							let indexHeader = 0
							let job_headers = Array.isArray(payload.header) ? payload.header : [payload.header];
							job_headers.map((header) => {
								if (typeof header.id != 'undefined' && header.id != '' && header.id != null) {
									editedHeader.push(header)
									objEditedHeader[header.id] = header
								} else if (typeof header.job_header_attribute_id != 'undefined' && header.job_header_attribute_id != null && typeof header.detail != 'undefined' && header.detail != null) {
									newestHeader.push({
										xid: uuidv4(),
										channel_job_id: channelJobId,
										job_header_attribute_id: header.job_header_attribute_id,
										value: header.detail,
										updated_at: currentTime,
										created_at: currentTime,
										updated_by: auth.user.id,
										created_by: auth.user.id,
									})
									newestHeaderObj[header.job_header_attribute_id] = header.detail
									indexNewestHeader[header.job_header_attribute_id] = indexHeader
									indexHeader += 1
								}
							})
							if (newestHeader.length > 0) {
								await ChannelJobHeaderAttribute.createMany(newestHeader)
							}
							// update spec by id
							editedHeader.map(async (headerPayload) => {
								await ChannelJobHeaderAttribute.query()
									.where('channel_job_id', channelJobId)
									.where('id', headerPayload.id)
									.update({
										job_header_attribute_id: headerPayload.job_header_attribute_id,
										value: headerPayload.detail,
										updated_by: auth.user.id,
										updated_at: currentTime
									})
							})
						} else {
							await ChannelJobHeaderAttribute.query()
								.where('channel_job_id', channelJobId)
								.update({
									is_archived: 1,
									updated_by: auth.user.id,
									updated_at: currentTime
								})
						}
						//#endregion

						//=======================================
						//update data job specialist
						let specialist = payload.job.specialist
						if (specialist.length > 0) {
							// first mapping all data specialist based on id specialist and channel job id
							const refIds = []
							specialist.map(({ value }) => {
								refIds.push(value)
							})
							// get existing specialist
							const querySpecialist = await ChannelJobCharacteristic.query()
								.select('id', 'ref_id')
								.where('ref_type', 'specialist')
								.where('channel_job_id', channelJobId)
								.whereIn('ref_id', refIds)
								.fetch()
							const existingSpecialist = querySpecialist.toJSON()
							const existingSpecialistIds = []
							const existingRefIds = []
							const newestSpecialist = []
							existingSpecialist.map((specExist) => {
								existingSpecialistIds.push(specExist.id)
								existingRefIds.push(specExist.ref_id)
							})
							// mapping data
							specialist.map(async (allSpec) => {
								if (existingRefIds.indexOf(allSpec.value) < 0) {
									newestSpecialist.push({
										xid: uuidv4(),
										channel_job_id: channelJobId,
										ref_id: allSpec.value,
										ref_type: 'specialist',
										value: allSpec.label,
										is_archived: 0,
										updated_by: auth.user.id,
										updated_at: currentTime,
										created_by: auth.user.id,
										created_at: currentTime
									})
								} else {
									await ChannelJobCharacteristic.query()
										.where('channel_job_id', channelJobId)
										.where('ref_id', allSpec.value)
										.where('is_archived', 1)
										.update({
											is_archived: 0,
											updated_by: auth.user.id,
											updated_at: currentTime
										})
								}
							})
							await ChannelJobCharacteristic.query()
								.where('channel_job_id', channelJobId)
								.whereNotIn('id', existingSpecialistIds)
								.update({
									is_archived: 1,
									updated_by: auth.user.id,
									updated_at: currentTime
								})
							await ChannelJobCharacteristic.createMany(newestSpecialist)
						}

						trx.commit()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '2000',
							message: 'Channel Job updated',
							data: params.id
						}
						return response.send(data)
					} else {
						trx.rollback()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: 'Channel Job cannot be found',
							data: []
						}
						return response.send(data)
					}
				}
			} else {
				trx.rollback()
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel not found.',
					data: []
				}
				return response.send(data)
			}
		} catch (e) {
			trx.rollback()
			console.log(e)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Channel Job cannot be added, System encounter an error.',
				data: []
			}
			return response.send(data)
		}
	}

	async delete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJob', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let channeljob = await ChannelJob.find(await MyHash.decrypt(formData.id))
		if (channeljob) {
			try {
				// Soft Delete Channel Job
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				channeljob.updated_by = auth.user.id,
					channeljob.updated_at = currentTime,
					channeljob.is_archived = 1
				await channeljob.save()

				//#region delete logo
				if (!_lo.isEmpty(channeljob.image)) {
					//remove
					let parts = channeljob.image.split('/')
					let url = parts.slice(3, parts.length)
					url = url.join('/')
					await FileService.delete(url);
				}
				//#endregion


				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Job deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.error(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job to delete',
				data: []
			}
			return response.send(data)
		}
	}

	async multidelete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('ChannelJob', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					// Soft Delete Channel Job
					let channeljob = await ChannelJob.find(await MyHash.decrypt(dataitem[i]))
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					channeljob.updated_by = auth.user.id,
						channeljob.updated_at = currentTime,
						channeljob.is_archived = 1
					await channeljob.save()

					//#region delete logo
					if (!_lo.isEmpty(channeljob.image)) {
						//remove
						let parts = channeljob.image.split('/')
						let url = parts.slice(3, parts.length)
						url = url.join('/')
						await FileService.delete(url);
					}
					//#endregion
				}
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Channel Job deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Channel Job cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Select Channel Job to delete',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannel({ request, response, auth, session }) {
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelJob', 'read', auth, admin_page)
		let req = request.get()
		let datas

		if (req.phrase != '') {
			if (!checkAuth) {
				datas = await Channel.query().where('is_admin', 'LIKE', '%0%').limit(100).fetch()
			} else {
				datas = await Channel.query().limit(20).fetch()
			}
		}
		else {
			if (!checkAuth) {
				datas = await Channel.query().select('channels.id', 'channels.channel_name').leftJoin('user_channels', 'channels.id', 'user_channels.channel_id').where('channels.is_archived', 'LIKE', '%0%').andWhere('channels.is_admin', 'LIKE', '%0%').andWhere('user_channels.xid', 'LIKE', `${auth.user.xid}`).limit(100).fetch()
			} else {
				datas = await Channel.query().where('channels.is_archived', 'LIKE', '%0%').limit(100).fetch()
			}
		}

		let data = datas.toJSON()

		let result = []
		for (let i in data) {
			result.push({
				value: data[i]['id'],
				label: data[i]['channel_name']
			})
		}
		return response.send(result)
	}

	async getChannelJobType({ request, response, auth, session }) {
		let admin_page = true
		let checkAuth = await CheckAuth.get('ChannelJob', 'read', auth, admin_page)
		let req = request.get()
		let datas

		if (req.phrase != '') {
			datas = await ChannelJobType.query().select('channel_job_types.id', 'channel_job_types.name').where('channel_job_types.is_archived', 'LIKE', '%0%').limit(20).fetch()
		} else {
			datas = await ChannelJobType.query().select('channel_job_types.id', 'channel_job_types.name').limit(20).fetch()
		}

		let data = datas.toJSON()

		let result = []
		for (let i in data) {
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}
		return response.send(result)
	}

	async updateStatus({ request, response, auth }) {
		const { id } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await ChannelJob.find(idDecrypted)
		if (record != null) {
			Object.assign(record.$attributes, { is_published: !record.is_published })
			record.save()
			return response.defaultResponseJson(200, 'Success to update status channel')
		} else {
			return response.defaultResponseJson(400, 'Job not found')
		}
	}
}

module.exports = ChannelJobController