'use strict'
const Datatable = use('App/Helpers/Datatable')
const RecordLog = use('App/Models/RecordLog')
const Database = use('Database')

class HistoryRecordController {
    /**
     * This function will retrieve datatable
     * @return {json}
     * @author : Tsani Nashrullah (tsani.nashrullah@ptgue.com)
     */
    async datatable({ request, response }) {
        const queryModel = RecordLog.query()
            .innerJoin('user_channels', 'user_channels.id', 'record_logs.user_channel_id')
            .select(
                Database.raw('CONCAT(user_channels.first_name, " ", user_channels.last_name) AS full_name'),
                'user_channels.email',
                'record_logs.table_name',
                'record_logs.foreign_id',
                'record_logs.transaction_type',
                'record_logs.created_at',
                'record_logs.last_payload',
                'record_logs.latest_payload',
            )
        const datatable = new Datatable(queryModel, request.all())
        return response.json(await datatable.make())
    }
}

module.exports = HistoryRecordController
