'use strict'

const Conference = use('App/Models/ChannelConference')
const ConferenceAttendee = use('App/Models/ChannelConferenceAttendee')
const Subscriber = use('App/Models/ChannelSubscriber')
const Member = use('App/Models/TemporaryMember')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const _lo = use('lodash')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Env = use('Env')
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV
const meet_url = Env.get('MEET_D2D_URL', 'https://meet.d2d.co.id')
const crypto = require('crypto')
const { validate, validateAll } = use('Validator')
const ServiceHelper = require('./Helper/ExtendedServiceHelper')
const Datatable = use('App/Helpers/Datatable')

class ChannelConferenceController {

    async datatable({ request, response, auth }) {
        let admin_page = true
        let checkAuthAdmin = await CheckAuth.get('Conference', 'read', auth, admin_page)

        const queryModel = Conference.query()
            .innerJoin('channels', 'channel_conferences.channel_id', 'channels.id')
            .select(
                'channel_conferences.id',
                'channel_conferences.title',
                'channel_conferences.url',
                'channels.channel_name',
                'channel_conferences.gmt_tz',
                'channel_conferences.start_date',
                'channel_conferences.end_date',
                'channel_conferences.is_archived',
                'channel_conferences.created_at',
                'channel_conferences.is_published',
            )

        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model.where(`channel_conferences.is_archived`, '0')

            if (!checkAuthAdmin) {
                model.where('channel_conferences.channel_id', auth.user.channel_id)
            }
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async attendeeDatatable({ request, response, auth }) {

        const formData = request.post()
        let conference_id = await MyHash.decrypt(formData.id);
        const queryModel = ConferenceAttendee.query()
            .select(
                'channel_conference_attendees.id',
                'temporary_members.name',
                'temporary_members.email',
                'temporary_members.spesialization',
                'temporary_members.nonspesialization',
                'temporary_members.display_picture'
            )
            .leftJoin('channel_subscribers', 'channel_conference_attendees.channel_subscriber_id', 'channel_subscribers.id')
            .leftJoin('temporary_members', 'temporary_members.uid', 'channel_subscribers.uid')

        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model
                .where(`channel_subscribers.blocked`, '0')
                .where(`channel_subscribers.is_unsubscribed`, '0')
                .where(`channel_conference_attendees.conference_id`, conference_id)
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async create({ request, response, auth, session }) {
        let checkIsAdmin = await CheckAuth.get('Conference', 'create', auth, true)

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

        const {
            cid,
            title,
            //description,
            is_published,
            max_attendees,
            start_date,
            end_date,
            registration_until,
            gmt_tz,
            channel_subscribers
        } = request.only([
            'cid',
            'title',
            //'description',
            'is_published',
            'max_attendees',
            'start_date',
            'end_date',
            'registration_until',
            'gmt_tz',
            'channel_subscribers'
        ])

        let formData = {
            xid: uuidv4(),
            channel_id: cid,
            title,
            description: null,
            is_published,
            is_instant: 0,
            url: meet_url,
            meet_key: uuidv4(),
            meet_id: uuidv4(),
            max_attendees,
            start_date,
            end_date,
            gmt_tz,
            registration_until,
            is_archived: 0,
            updated_by: auth.user.id,
            updated_at: currentTime,
            created_by: auth.user.id,
            created_at: currentTime
        }
        let rules = {
            start_date: 'required',
            end_date: 'required',
            channel_id: 'required',
            registration_until: 'required',
            xid: 'required',
            title: 'required',
            gmt_tz: 'required'
        }

        const validation = await validateAll(formData, rules)

        if (validation.fails()) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            try {
                let newConference = await Conference.create(formData)

                let cs = _lo.isEmpty(channel_subscribers) ? [] : channel_subscribers
                //#region menu bertambah
                for (let row of cs) {
                    let xid = uuidv4()
                    let channel_subs_data = {
                        xid,
                        conference_id: newConference.id,
                        channel_subscriber_id: row.value,
                        registered_from: 'CMS',
                        is_notify: 0,
                        blocked: 0,
                        updated_by: auth.user.id,
                        updated_at: currentTime,
                        created_by: auth.user.id,
                        created_at: currentTime
                    }
                    await ConferenceAttendee.create(channel_subs_data)
                }
                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Conference added',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                console.log(e)
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Conference cannot be added',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async viewConference({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }
        const formData = request.post()
        let channel_conference = []
        channel_conference = await Conference.find(await MyHash.decrypt(formData.id))
        let url = await ServiceHelper.generatmeet(channel_conference.url, channel_conference.meet_id, channel_conference.title, auth.user.email, `${auth.user.first_name} ${auth.user.last_name}`, 'moderator')

        if (channel_conference) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Conference found',
                data: url
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be found',
                data: []
            }
            return response.send(data)
        }
    }

    async edit({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let channel_conference = []
        channel_conference = await Conference.find(await MyHash.decrypt(formData.id))

        let get_channels = await channel_conference.channels().where('channels.is_archived', '0').fetch()

        let channels_json = []
        if (!_lo.isEmpty(get_channels)) {
            channels_json = get_channels.toJSON()
            if (!_lo.isEmpty(channels_json)) {
                channel_conference.channels = {
                    value: channels_json['id'],
                    label: channels_json['channel_name']
                }
            }
        } else {
            channel_conference.channels = {
                value: '',
                label: 'No Data'
            }
        }

        let get_subscribers = await channel_conference.subscribers().where('channel_subscribers.blocked', '0').fetch()
        let subs_json = []

        if (!_lo.isEmpty(get_subscribers)) {
            subs_json = get_subscribers.toJSON()
            let channel_subscribers = []

            for (let i in subs_json) {
                let member = await Member
                    .query()
                    .select('id', 'uid', 'email')
                    .andWhere('uid', subs_json[i]['uid'])
                    .limit(1)
                    .fetch()
                member = !_lo.isEmpty(member) ? member.toJSON()[0]['email'] : ''
                channel_subscribers.push({
                    value: subs_json[i]['id'],
                    label: member
                })
            }

            if (!_lo.isEmpty(channel_subscribers)) {
                channel_conference.channel_subscribers = channel_subscribers
            }
        }
        if (channel_conference) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Conference found',
                data: channel_conference
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be found',
                data: []
            }
            return response.send(data)
        }
    }

    async update({ params, request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

        const {
            cid,
            title,
            //description,
            is_published,
            url,
            meet_key,
            meet_id,
            max_attendees,
            start_date,
            end_date,
            registration_until,
            gmt_tz
        } = request.only([
            'cid',
            'title',
            //'description',
            'is_published',
            'url',
            'meet_key',
            'meet_id',
            'max_attendees',
            'start_date',
            'end_date',
            'gmt_tz',
            'registration_until',
        ])
        let { channel_subscribers } = request.only(['channel_subscribers'])
        let conference = await Conference.find(await MyHash.decrypt(params.id))

        let xid = conference.xid
        let formData = {
            xid,
            channel_id: cid,
            title,
            description: null,
            is_published,
            is_instant: 0,
            url,
            meet_key,
            meet_id,
            max_attendees,
            start_date,
            end_date,
            gmt_tz,
            registration_until,
            is_archived: 0,
            updated_by: auth.user.id,
            updated_at: currentTime
        }
        let rules = {
            start_date: 'required',
            end_date: 'required',
            channel_id: 'required',
            registration_until: 'required',
            xid: 'required',
            title: 'required',
            gmt_tz: 'required'
        }

        const validation = await validateAll(formData, rules)
        if (validation.fails()) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            if (conference) {

                let id = await MyHash.decrypt(params.id)
                // conference.updatedPayload = formData
                Object.assign(conference.$attributes, formData)
                conference.save()
                // await Conference.query().where('id', id).update(formData)

                //#region  soft delete old conference_attendee

                //get attendee existing
                let ca = await ConferenceAttendee.query()
                    .where('conference_id', id)
                    .fetch()
                ca = _lo.isEmpty(ca) ? [] : ca.toJSON()
                channel_subscribers = _lo.isEmpty(channel_subscribers) ? [] : channel_subscribers
                for (let row of ca) {
                    // if menu berkurang

                    // check kalau ada menu dari form dengan menu existing di table
                    if (!channel_subscribers.some(item => item.value == row.channel_subscriber_id)) {
                        // kalau ada kita hanya update archived-nya
                        await ConferenceAttendee.query()
                            .where('conference_id', id)
                            .andWhere('channel_subscriber_id', row.channel_subscriber_id)
                            .update({
                                updated_by: auth.user.id,
                                updated_at: currentTime,
                                is_removed: 1
                            })

                        // mengurangi menu yang didapat dari form yang akan diiterasikan untuk ditambah ke table
                        channel_subscribers = channel_subscribers.filter(function (obj) {
                            return obj.value != row.channel_subscriber_id;
                        });
                    }
                }
                // menu bertambah
                for (let row of channel_subscribers) {

                    // check kalau ada menu dari form dengan menu existing di table
                    // kalau ada tidak ditambahkan lagi
                    var cm_filtered = ca.filter(function (obj) {
                        return obj.channel_subscriber_id == row.value;
                    });

                    // kalau menu sudah ada di table hanya update archive
                    if (!_lo.isEmpty(cm_filtered)) {
                        if (cm_filtered[0].is_archived == 1) {
                            await ConferenceAttendee.query()
                                .where('conference_id', id)
                                .andWhere('channel_subscriber_id', row.value)
                                .update({
                                    is_removed: 0,
                                    updated_by: auth.user.id,
                                    updated_at: currentTime
                                })
                        }
                    } else {
                        // kalau menu belum ada di table di insert
                        let xid = uuidv4()
                        let channel_subs_data = {
                            xid,
                            conference_id: id,
                            channel_subscriber_id: row.value,
                            registered_from: 'CMS',
                            is_notify: 0,
                            blocked: 0,
                            updated_by: auth.user.id,
                            updated_at: currentTime,
                            created_by: auth.user.id,
                            created_at: currentTime
                        }
                        await ConferenceAttendee.create(channel_subs_data)
                    }
                }
                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Conference updated',
                    data: []
                }
                return response.send(data)
            } else {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Conference cannot be found',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async delete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let channel = await Conference.find(await MyHash.decrypt(formData.id))
        if (channel) {
            try {
                console.log(channel)
                let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                channel.updated_by = auth.user.id
                channel.updated_at = currentTime
                channel.is_archived = 1
                await channel.save()

                // await Conference.query().where('id', await MyHash.decrypt(formData.id)).update({is_archived:1})
                // await channel.delete()

                // delete old conference_attendee
                let cm = await ConferenceAttendee.query()
                    .where('conference_id', await MyHash.decrypt(formData.id))
                    .update({ is_removed: 1 })

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Conference success deleted',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Conference cannot be deleted',
                    data: []
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async multidelete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        if (formData.totaldata != '0') {
            let dataitem = JSON.parse(formData.item)
            for (let i in dataitem) {
                let channel = await Conference.find(await MyHash.decrypt(dataitem[i]))
                try {
                    let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                    channel.updated_by = auth.user.id,
                        channel.updated_at = currentTime,
                        channel.is_archived = 1
                    await channel.save()
                    let cm = await ConferenceAttendee.query()
                        .where('conference_id', await MyHash.decrypt(dataitem[i]))
                        .update({ is_removed: 1 })

                    // await channel.delete()
                } catch (e) { }
            }

            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Conference success deleted',
                data: []
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async getConferenceSubscribers({ request, response, auth, session }) {
        let req = request.all()
        let datas
        let checkAuth = await CheckAuth.get('Conference', 'read', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }
        let channel_id = req.channel_id == undefined ? '-1' : req.channel_id
        datas = await Subscriber
            .query()
            .select('channel_subscribers.id', 'channel_subscribers.uid', 'temporary_members.email')
            .leftJoin('temporary_members', 'temporary_members.uid', 'channel_subscribers.uid')
            .andWhere('channel_subscribers.blocked', '0')
            .andWhere('channel_subscribers.channel_id', channel_id)
            .limit(20)
            .fetch()
        let data = datas.toJSON()

        let result = []
        for (let row of data) {
            result.push({
                value: row['id'],
                label: row['email'] == null ? 'Invalid Email' : row['email']
            })
        }

        return response.send(result)
    }

    async updateStatus({ request, response, auth }) {
		const { id } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await Conference.find(idDecrypted)
		if (record != null) {
			Object.assign(record.$attributes, { is_published: !record.is_published })
			record.save()
			return response.defaultResponseJson(200, 'Success to update status channel')
		} else {
			return response.defaultResponseJson(400, 'Conference not found')
		}
	}
}

module.exports = ChannelConferenceController