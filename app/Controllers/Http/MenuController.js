'use strict'

const Menu = use('App/Models/Menu')
const MyHash = require('./Helper/Hash.js')
const oss = require('ali-oss');
const _lo = use('lodash')
const CheckAuth = require('./Helper/CheckAuth.js')
const moment = require('moment');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV
const { v4: uuidv4 } = require('uuid');
const { validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')
const Helpers = use('Helpers')
const fs = require('fs')
const FileService = require('./Helper/FileService.js')
const content_type = 'menu'

class MenuController {
	async datatable({ request, response, auth }) {
		let admin_page = true
		let checkAuthAdmin = await CheckAuth.get('Menu', 'read', auth, admin_page)

		if (!checkAuthAdmin) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const queryModel = Menu.query()
			.select(
				'menus.id',
				'menus.name',
				'menus.icon',
				'menus.description',
				'menus.status',
				'menus.is_published',
				'menus.created_at'
			)

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model.where(`menus.is_archived`, '0')
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}


	async create({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('Menu', 'create', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

		let form = JSON.parse(request.post().data)
		const icon_file = request.file('icon', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})

		let xid = uuidv4()
		let formData = {
			xid,
			name: form.name,
			description: form.description,
			link: form.link,
			target: form.target,
			status: form.status,
			is_published: form.is_published,
			parent_id: form.parent_id,
			position: form.position,
			is_archived: 0,
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}

		let rules = {
			xid: 'required',
			name: 'required',
			link: 'required',
			target: 'required'
		}

		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			console.log(validation.fails())
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: validation
			}
			return response.send(data)
		} else {
			try {
				//#region upload icon
				if (icon_file != null) {

					// put temporary file to trigger validation
					await icon_file.move(Helpers.tmpPath('uploads'), {
						name: icon_file.clientName,
						overwrite: true
					})
					let path_tmp = `${Helpers.tmpPath('uploads')}/${icon_file.clientName}`
					// error validation file
					if (!icon_file.moved()) {
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: `Invalid File Upload! - ${icon_file.error().message}`,
							data: icon_file.error()
						}
						return response.send(data)
					} else {
						let upload_setting = {
							xid: uuidv4(),
							content: content_type,
							tempPath: path_tmp,
						}
						let upload_result = await FileService.upload(icon_file, upload_setting, true)
						formData.icon = upload_result.url

						// delete temporary file
						fs.unlinkSync(path_tmp)
					}
				}
				//#endregion
				await Menu.create(formData)

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				console.log(e)
				let data = {
					code: '4004',
					message: 'Menu cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async edit({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('Menu', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let menu = await Menu.find(await MyHash.decrypt(formData.id))

		if (menu) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Menu found',
				data: menu
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be found',
				data: []
			}
			return response.send(data)
		}
	}

	async update({ params, request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('Menu', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

		let form = JSON.parse(request.post().data)

		const icon_file = request.file('icon', {
			type: 'image',
			subtype: ['image/jpeg', 'image/png'],
			size: '2mb',
			extnames: ['jpg', 'jpeg', 'png']
		})

		let menu = await Menu.find(await MyHash.decrypt(params.id))

		let xid = uuidv4()
		let formData = {
			xid,
			name: form.name,
			description: form.description,
			link: form.link,
			target: form.target,
			status: form.status,
			is_published: form.is_published,
			parent_id: form.parent_id,
			position: form.position,
			is_archived: 0,
			updated_by: auth.user.id,
			updated_at: currentTime
		}

		let rules = {
			xid: 'required',
			name: 'required',
			link: 'required',
			target: 'required'
		}

		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			console.log(validation)
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (menu) {
				if (form.remove_image) {
					//remove old
					let parts = menu.icon.split('/')
					let url = parts.slice(3, parts.length)
					url = url.join('/')
					await FileService.delete(url);
					formData.icon = ''
				}

				//#region upload icon
				if (icon_file != null) {

					//remove old
					let parts = menu.icon.split('/')
					let url = parts.slice(3, parts.length)
					url = url.join('/')
					await FileService.delete(url);

					// put temporary file to trigger validation
					await icon_file.move(Helpers.tmpPath('uploads'), {
						name: icon_file.clientName,
						overwrite: true
					})
					let path_tmp = `${Helpers.tmpPath('uploads')}/${icon_file.clientName}`
					// error validation file
					if (!icon_file.moved()) {
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: `Invalid File Upload! - ${icon_file.error().message}`,
							data: icon_file.error()
						}
						return response.send(data)
					} else {
						let upload_setting = {
							xid: uuidv4(),
							content: content_type,
							tempPath: path_tmp,
						}
						let upload_result = await FileService.upload(icon_file, upload_setting, true)
						formData.icon = upload_result.url

						// delete temporary file
						fs.unlinkSync(path_tmp)
					}
				}
				//#endregion

				Object.assign(menu.$attributes, formData)
				menu.save()

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Menu cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}

	async delete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('Menu', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let menu = await Menu.find(await MyHash.decrypt(formData.id))
		if (menu) {
			try {
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				menu.updated_by = auth.user.id,
					menu.updated_at = currentTime,
					menu.is_archived = 1
				await menu.save()
				// await Menu.query().where('id', await MyHash.decrypt(formData.id)).update({is_archived:1})
				// await menu.delete()

				//#region delete icon
				if (!_lo.isEmpty(menu.icon)) {
					let parts = menu.icon.split('/')
					let url = parts.slice(3, parts.length)
					url = url.join('/')
					await FileService.delete(url);
				}
				//#endregion

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Menu success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Menu cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async multidelete({ request, response, auth, session }) {
		let checkAuth = await CheckAuth.get('Menu', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			for (let i in dataitem) {
				let menu = await Menu.find(await MyHash.decrypt(dataitem[i]))
				try {
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					menu.updated_by = auth.user.id,
						menu.updated_at = currentTime,
						menu.is_archived = 1
					await menu.save()
					// await menu.delete()

					//#region delete icon
					if (!_lo.isEmpty(menu.icon)) {
						let parts = menu.icon.split('/')
						let url = parts.slice(3, parts.length)
						url = url.join('/')
						await FileService.delete(url);
					}
					//#endregion
				} catch (e) { }
			}

			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Menu success deleted',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Menu cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async getMenu({ request, response, auth, session }) {
		let req = request.get()
		let datas

		let checkAuth = await CheckAuth.get('Channel', 'read', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		if (req.phrase != '') {
			datas = await Menu.query().where('name', 'LIKE', '%' + req.phrase + '%').andWhere('is_archived', '0').limit(20).fetch()
		} else {
			datas = await Menu.query().where('is_archived', '0').limit(20).fetch()
		}

		let data = datas.toJSON()

		let result = []
		for (let i in data) {
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}

		return response.send(result)
	}
    
	async updateStatus({ request, response, auth }) {
		const { id, type } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await Menu.find(idDecrypted)
		if (record != null) {
            if (type == 'status') {
                Object.assign(record.$attributes, { status: !record.status })
            } else if (type == 'is_published') {
                Object.assign(record.$attributes, { is_published: !record.is_published })
            }
			record.save()
			return response.defaultResponseJson(200, 'Success to update status channel')
		} else {
			return response.defaultResponseJson(400, 'Channel not found')
		}
	}
}

module.exports = MenuController