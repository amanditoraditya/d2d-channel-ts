'use strict'

//library
const Database = use('Database')
const fs = require('fs')
const Helpers = use('Helpers')
const _lo = use('lodash')
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const { validateAll } = use('Validator')

//custom module
const Datatable = use('App/Helpers/Datatable')
const MyHash = require('./Helper/Hash.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const FileService = require('./Helper/FileService.js');

//models
const Brochure = use('App/Models/Brochure')
const content_type = 'brochure'

class ChannelBrochureController {

    async datatable({ request, response, auth }) {
        let checkAuthAdmin = await CheckAuth.get('Brochure', 'read', auth, true)

        const queryModel = Brochure.query()
            .innerJoin('channels', 'brochures.channel_id', 'channels.id')
            .select(
                'brochures.id',
                'channels.channel_name',
                'brochures.title',
                'brochures.description',
                'brochures.cover',
                'brochures.is_published',
                'brochures.is_archived',
            )

        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model
                .orderBy('is_archived', 'asc')
            if (!checkAuthAdmin) {
                model.where('brochures.channel_id', auth.user.channel_id)
            }
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async create({ request, response, auth, session }) {
        try {
            let checkAuth = await CheckAuth.get('Brochure', 'create', auth)
            if (!checkAuth) {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4003',
                    message: 'User cannot access this module',
                    data: []
                }
                return response.send(data)
            }

            let currentTime = moment()
            let form = JSON.parse(request.post().data)

            let response_data = {
				code: '2000',
				message: 'Brochure added',
				form,
			}

            if (_lo.isEmpty(form.brochurePayLoad) == false) {   
                const cover_image = request.file('cover', {
                    type: 'image',
                    subtype: ['image/jpeg', 'image/png'],
                    size: '2mb',
                    extnames: ['jpg', 'jpeg', 'png', 'JPG']
                })
    
                const brochure_file = request.file('filename', {
                    type: 'pdf',
                    size: '2mb',
                    extnames: ['pdf']
                })
    
                let xid = uuidv4()
                let formData = {
                    xid,
                    channel_id: form.brochurePayLoad.channel_id_value,
                    title: form.brochurePayLoad.title,
                    description: form.brochurePayLoad.description,
                    is_published: form.brochurePayLoad.is_published,
                    is_archived: 0,
                    updated_at: currentTime,
                    created_at: currentTime
                }
    
                if (cover_image != null) {
                    formData.cover = true
                }
    
                if (brochure_file != null) {
                    formData.filename = true
                }
    
                let rules = {
                    xid: 'required',
                    title: 'required',
                    description: 'required',
                    channel_id: 'required',
                    cover: 'required',
                    filename: 'required'
                }
    
                const validation = await validateAll(formData, rules)
                if (validation.fails()) {
                    response.header('Content-type', 'application/json')
                    response.type('application/json')
                    let data = {
                        code: '4004',
                        message: 'Please fill required marker field!',
                        data: validation
                    }
                    return response.send(data)
                } else {
                    //#region upload image
                    formData.cover = ''
                    formData.filename = ''

                    let path_tmp = `${Helpers.tmpPath('uploads')}/${cover_image.clientName}`
                    // put temporary file to trigger validation
                    await cover_image.move(Helpers.tmpPath('uploads'), {
                        name: cover_image.clientName,
                        overwrite: true
                    })
                    // error validation file
                    if (!cover_image.moved()) {
                        response.header('Content-type', 'application/json')
                        response.type('application/json')
                        let data = {
                            code: '4004',
                            message: `Invalid File Upload! - ${cover_image.error().message}`,
                            data: cover_image.error()
                        }
                        return response.send(data)
                    } else {
                        //upload image
                        let upload_setting = {
                            xid: xid,
                            content: content_type,
                            tempPath: path_tmp,
                        }
                        let upload_image = await FileService.upload(cover_image, upload_setting, true)

                        formData.cover = upload_image.url;
                        // delete temporary file
                        fs.unlinkSync(path_tmp)
                    }

                    path_tmp = `${Helpers.tmpPath('uploads')}/${brochure_file.clientName}`
                    // put temporary file to trigger validation
                    await brochure_file.move(Helpers.tmpPath('uploads'), {
                        name: brochure_file.clientName,
                        overwrite: true
                    })
                    // error validation file
                    if (!brochure_file.moved()) {
                        response.header('Content-type', 'application/json')
                        response.type('application/json')
                        let data = {
                            code: '4004',
                            message: `Invalid File Upload! - ${brochure_file.error().message}`,
                            data: brochure_file.error()
                        }
                        return response.send(data)
                    } else {
                        //upload image
                        let upload_setting = {
                            xid: xid,
                            content: content_type,
                            tempPath: path_tmp,
                        }
                        let upload_file = await FileService.upload(brochure_file, upload_setting, true)

                        formData.filename = upload_file.url;
                        // delete temporary file
                        fs.unlinkSync(path_tmp)
                    }

                    await Brochure.create(formData) 

                    response_data = {
                        code: '2000',
                        message: 'Brochure added',
                        data: []
                    }
                }
            } else {
                response_data = {
					code: '4004',
					message: 'Brochure cannot be added',
					data: []
				}
            }
            response.header('Content-type', 'application/json')
			response.type('application/json')
			return response.send(response_data)
        } catch (e) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Brochure cannot be added, System encounter an error.',
				data: []
			}
			return response.send(data)
		}
    }

    async edit({ params, request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Brochure', 'update', auth, false)

		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(returnData)
		}

		let brochureId = await MyHash.decrypt(params.id)
		let responseData = {}

		responseData = await Brochure.query()
		.select('channel_id', 'title', 'brochures.description as description', 'is_published', 'cover', 'filename', 'channels.channel_name',  'channels.id as channel_id')
		.leftJoin('channels', 'channels.id', 'brochures.channel_id')
        .where('brochures.id', brochureId)
		.first()

		if(responseData) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '2000',
				message: 'Brochure found',
				data: responseData
			}
			return response.send(returnData)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let returnData = {
				code: '4004',
				message: 'Brochure cannot be found',
				data: []
			}
			return response.send(returnData)
		}
	}

	async update({ params, request, response, auth, session }) {
        const trx = await Database.beginTransaction()
		try {
			let checkAuth = await CheckAuth.get('Brochure', 'update', auth)
			if (!checkAuth) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4003',
					message: 'User cannot access this module',
					data: []
				}
				return response.send(data)
			}

			let currentTime = moment()

			let brochureId = await MyHash.decrypt(params.id)
			let brochure = await Brochure.find(brochureId)
			let form = JSON.parse(request.post().data)

			if (_lo.isEmpty(brochure) == false) {
				let formData = {
					channel_id: form.brochurePayLoad.channel_id_value,
                    title: form.brochurePayLoad.title,
                    description: form.brochurePayLoad.description,
                    is_published: form.brochurePayLoad.is_published,
					is_archived: 0,
					updated_at: currentTime
				}

                const cover_image = request.file('cover', {
                    type: 'image',
                    subtype: ['image/jpeg', 'image/png'],
                    size: '2mb',
                    extnames: ['jpg', 'jpeg', 'png', 'JPG']
                })
    
                const brochure_file = request.file('filename', {
                    type: 'pdf',
                    size: '2mb',
                    extnames: ['pdf']
                })

				let rules = {
                    title: 'required',
                    description: 'required',
                    channel_id: 'required',
                }

				const validation = await validateAll(formData, rules)
				if (validation.fails()) {
					response.header('Content-type', 'application/json')
					response.type('application/json')
					let data = {
						code: '4004',
						message: 'Please fill required marker field!',
						data: validation
					}
					return response.send(data)
				} else {
					if (brochure) {

						if (brochure.cover != '' && brochure.cover != null && form.brochurePayLoad.remove_cover) {
							let parts = brochure.cover.split('/')
							let url = parts.slice(3, parts.length)
							url = url.join('/')
							await FileService.delete(url);
							formData.cover = ''
						}

                        if (brochure.filename != '' && brochure.filename != null && form.brochurePayLoad.remove_file) {
							let parts = brochure.filename.split('/')
							let url = parts.slice(3, parts.length)
							url = url.join('/')
							await FileService.delete(url);
							formData.filename = ''
						}

						//#region upload cover 
						if (cover_image != null) {
                            let path_tmp = `${Helpers.tmpPath('uploads')}/${cover_image.clientName}`
                            fs.stat(path_tmp, function (err, stats) {
                                if (err) {
                                    return console.error("Error on delete file | ChannelBrochureController:update", err)
                                }
    
                                fs.unlink(path_tmp, function (err) {
                                    if (err) return console.log(err);
                                });
                            });
                            
                            // put temporary file to trigger validation
                            await cover_image.move(Helpers.tmpPath('uploads'), {
                                name: cover_image.clientName,
                                overwrite: true
                            })
                            // error validation file
                            if (!cover_image.moved()) {
                                response.header('Content-type', 'application/json')
                                response.type('application/json')
                                let data = {
                                    code: '4004',
                                    message: `Invalid File Upload! - ${cover_image.error().message}`,
                                    data: cover_image.error()
                                }
                                return response.send(data)
                            } else {
                                //upload image
                                let upload_setting = {
                                    content: content_type,
                                    tempPath: path_tmp,
                                }
                                let upload_image = await FileService.upload(cover_image, upload_setting, true)
    
                                formData.cover = upload_image.url;
                                // delete temporary file
                                fs.unlinkSync(path_tmp)
                            }
						}
						//#endregion

                        //#region upload file 
                        if (brochure_file != null) {
                            let path_tmp = `${Helpers.tmpPath('uploads')}/${brochure_file.clientName}`
                            fs.stat(path_tmp, function (err, stats) {
                                if (err) {
                                    return console.error("Error on delete file | ChannelBrochureController:update", err)
                                }
    
                                fs.unlink(path_tmp, function (err) {
                                    if (err) return console.log(err);
                                });
                            });
                            
                            // put temporary file to trigger validation
                            await brochure_file.move(Helpers.tmpPath('uploads'), {
                                name: brochure_file.clientName,
                                overwrite: true
                            })
                            // error validation file
                            if (!brochure_file.moved()) {
                                response.header('Content-type', 'application/json')
                                response.type('application/json')
                                let data = {
                                    code: '4004',
                                    message: `Invalid File Upload! - ${brochure_file.error().message}`,
                                    data: brochure_file.error()
                                }
                                return response.send(data)
                            } else {
                                //upload image
                                let upload_setting = {
                                    content: content_type,
                                    tempPath: path_tmp,
                                }
                                let upload_file = await FileService.upload(brochure_file, upload_setting, true)
    
                                formData.filename = upload_file.url;
                                // delete temporary file
                                fs.unlinkSync(path_tmp)
                            }
                        }
                        //#endregion

						Object.assign(brochure.$attributes, formData)
						brochure.save()

						trx.commit()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '2000',
							message: 'Brochure updated',
							data: params.id
						}
						return response.send(data)
					} else {
						trx.rollback()
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: 'Brochure  cannot be found',
							data: []
						}
						return response.send(data)
					}
				}
			} else {
				trx.rollback()
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Brochure cannot be added',
					data: []
				}
				return response.send(data)
			}
		} catch (e) {
			trx.rollback()
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Brochure cannot be added, System encounter an error.',
				data: []
			}
			return response.send(data)
		}
	}

    async delete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Brochure', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let brochure = await Brochure.find(await MyHash.decrypt(formData.id))
        if (brochure) {
            try {

                let currentTime = moment()
				brochure.updated_at = currentTime,
                brochure.cover = ''
                brochure.filename = ''
				brochure.is_archived = 1
				await brochure.save()

                //#region delete image
                if (!_lo.isEmpty(brochure.cover)) {
                    //remove
                    let parts = brochure.cover.split('/')
                    let url = parts.slice(3, parts.length)
                    url = url.join('/')
                    await FileService.delete(url);
                }
                //#endregion

                //#region delete file
                if (!_lo.isEmpty(brochure.filename)) {
                    //remove
                    let parts = brochure.filename.split('/')
                    let url = parts.slice(3, parts.length)
                    url = url.join('/')
                    await FileService.delete(url);
                }
                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Brochure success deleted',
                    data: []
                }
                return response.send(data)

            } catch (e) {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Brochure cannot be deleted',
                    data: []
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Brochure cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async multidelete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Brochure', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        if (formData.totaldata != '0') {
            let dataitem = JSON.parse(formData.item)
            try {
                for (let i in dataitem) {
                    let brochure = await Brochure.find(await MyHash.decrypt(dataitem[i]))
                    let currentTime = moment()
                    brochure.updated_at = currentTime,
                    brochure.cover = ''
                    brochure.filename = ''
                    brochure.is_archived = 1
                    await brochure.save()

                    //#region delete image
                    if (!_lo.isEmpty(brochure.cover)) {
                        //remove
                        let parts = brochure.cover.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);
                    }
                    //#endregion

                    //#region delete file
                    if (!_lo.isEmpty(brochure.filename)) {
                        //remove
                        let parts = brochure.filename.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);
                    }
                    //#endregion
                }

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Brochure success deleted',
                    data: []
                }
                return response.send(data)
                
            } catch (e) { 
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Brochure cannot be deleted',
                    data: []
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Select Brochure to delete',
                data: []
            }
            return response.send(data)
        }
    }

    async updateStatus({ request, response, auth  }) {
        const { id } = request.post()
        const idDecrypted = await MyHash.decrypt(id)
        const record = await Brochure.find(idDecrypted)
        if (record != null) {
            Object.assign(record.$attributes, { is_published: !record.is_published })
            record.save()
            return response.defaultResponseJson(200, 'Success to update Brochure status')
        } else {
            return response.defaultResponseJson(400, 'Brochure not found')
        }
    }
    
    async restore({ request, response, auth  }) {
        const { id } = request.post()
        const idDecrypted = await MyHash.decrypt(id)
        const record = await Brochure.find(idDecrypted)
        if (record != null) {
            Object.assign(record.$attributes, { is_archived: !record.is_archived })
            record.save()
            return response.defaultResponseJson(200, 'Success to restore Brochure')
        } else {
            return response.defaultResponseJson(400, 'Brochure not found')
        }
    }
}

module.exports = ChannelBrochureController