'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const oss 			 = require('ali-oss');
const { v4: uuidv4 } = require('uuid');

const Channel 			= use('App/Models/Channel')
const ForumPost 		= use('App/Models/ChannelForumPost')
const ForumLike  		= use('App/Models/ChannelForumLike')
const ForumAttachment  	= use('App/Models/ChannelForumAttachment')
const ForumContentType  = use('App/Models/ChannelForumContentType')
const ForumComment   	= use('App/Models/ChannelForumComment')

const MyHash 			= require('./Helper/Hash.js')
const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')
const Datatable 		= use('App/Helpers/Datatable')
const FileService 		= require('./Helper/FileService.js')

const goaConfig 	 = require('../../../next/config.js')
const environment 	 = goaConfig.NODE_ENV

const { validate, validateAll } = use('Validator')
const content_type = 'forum'

class ForumController {
	async datatable({request, response, auth, session}) {
		let admin_page = true
		let checkIsAdmin = await CheckAuth.get('Forum', 'read', auth, admin_page)

		const queryModel = ForumPost.query()
			.select(
				'channel_forum_posts.id', 
				'channel_forum_posts.xid', 
				'channel_forum_posts.channel_id', 
				'channel_forum_posts.title',
				'channel_forum_posts.created_at', 
				'channel_forum_posts.is_published', 
				Database.raw(`concat(user_channels.first_name,' ', user_channels.last_name) as username`), 
				'user_channels.email',
				'channels.channel_name'
			)
			.leftJoin('channels', 'channels.id', 'channel_forum_posts.channel_id' )
			.leftJoin('user_channels', 'user_channels.id', 'channel_forum_posts.created_by')

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model.where(`channel_forum_posts.is_archived`, '0')

			if (checkIsAdmin == false) {
				model.where('channel_forum_posts.channel_id', auth.user.channel_id)
			}
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}
	
	async create({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('Forum', 'create', auth, true)
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		let xid = uuidv4()
		let channel_id

		if (checkIsAdmin) {
			try{
				channel_id = await MyHash.decrypt(form.channel_id_value)
			}catch(e){
				channel_id = form.channel_id_value
			}
		} else {
			channel_id = auth.user.channel_id
		}

		let formData = {
			xid,
			channel_id,
			title			: form.title,
			short_description : form.short_description,
			description		: form.description,
			is_published	: form.is_published,
			is_archived		: '0',
			updated_by		: auth.user.id,
			updated_at		: currentTime,
			created_by		: auth.user.id,
			created_at		: currentTime
		}
		
		let rules = {
			title		 : 'required',
			description	 : 'required',
		}
		
		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				let newForumPost = await ForumPost.create(formData);

				const attachment_file = request.file('attachment',{
					type : ['application','image'],
					size : '2mb',
					extnames: ['jpg', 'png', 'jpeg','doc','docx','pdf','xls','xlsx']
				});

				if (attachment_file != null) {

					// put temporary file to trigger validation
					await attachment_file.move(Helpers.tmpPath('uploads'), {
						name: attachment_file.clientName,
						overwrite: true
					})
					let path_tmp = `${Helpers.tmpPath('uploads')}/${attachment_file.clientName}`
					// error validation file
					if (!attachment_file.moved()) {
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: `Invalid File Upload! - ${attachment_file.error().message}`,
							data: attachment_file.error()
						}
						return response.send(data)
					} else {
						let xid = uuidv4();
						let upload_setting = {
							xid: xid,
							content: content_type,
							tempPath: path_tmp,
						}
						let upload_result = await FileService.upload(attachment_file, upload_setting, true)	
						let contentType = await ForumContentType.query()
						.select('id')
						.where('name', '=', 'channel_forum_posts')
						.andWhere('is_archived','=', 0)
						.first();

						if (_lo.isEmpty(upload_result.url) == false && _lo.isEmpty(contentType) == false) {	
							let form_attachment = {
								xid	: xid,
								ref_id : newForumPost.id,
								channel_forum_content_type_id : contentType.id,
								attachment_name : upload_result.filename,
								attachment_type : upload_result.filetype,
								attachment_url  : upload_result.url,
								is_archived		: '0',
								updated_by		: auth.user.id,
								updated_at		: currentTime,
								created_by		: auth.user.id,
								created_at		: currentTime
							};

							await ForumAttachment.create(form_attachment);
						}
						fs.unlinkSync(path_tmp)
					}
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth 	 = await CheckAuth.get('Forum', 'update', auth, false)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let postId = await MyHash.decrypt(formData.id);

		let forumPost = await ForumPost.query()
		.select('channel_forum_posts.id','channel_forum_posts.channel_id','channel_forum_posts.title',
		'channel_forum_posts.short_description','channel_forum_posts.description','channel_forum_posts.is_published')
		.where('channel_forum_posts.id', postId)
		.andWhere('is_archived','=',0)
		.first();

		if(_lo.isEmpty(forumPost) == false){
			forumPost.is_published = forumPost.is_published == 1? true: false;

			//get attachment
			let attachment = await ForumAttachment.query()
			.select('attachment_name as name','attachment_url as url')
			.where('ref_id', '=', postId)
			.andWhere('is_archived','=', 0)
			.first();
	
			if(_lo.isEmpty(attachment) == false){
				forumPost.attachment = {
					name: attachment.name,
					url: attachment.url
				}
			}
	
			// get channels
			let get_channels = await forumPost._v1_channel().where('channels.is_archived', '0').fetch()
			let channels_json = []
	
			if (!_lo.isEmpty(get_channels)) {
				channels_json = get_channels.toJSON()
				if (!_lo.isEmpty(channels_json)) {
					forumPost.channels = {
						value: channels_json['id'],
						label: channels_json['channel_name']
					}
				}
			} else {
				forumPost.channels = {
					value: '',
					label: 'No Data'
				}
			}
		}

		if (forumPost) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Discussion found',
				data: forumPost
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		let postId = await MyHash.decrypt(params.id);
		let forumPost = await ForumPost.find(postId)

		let formData = {
			channel_id		: form.channel_id_value,
			title			: form.title,
			short_description : form.short_description,
			description		: form.description,
			is_published	: form.is_published,
			is_archived		: '0',
			updated_by		: auth.user.id,
			updated_at		: currentTime
		}

		if(_lo.isEmpty(forumPost) == false){
			formData.xid = forumPost.xid;
		}
		
		let rules = {
			title: 'required',
			description: 'required',
			channel_id: 'required',
			is_published: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (forumPost) {
				if (form.remove_Attachment == true) {
					// delete old forum attachment
					let fm = await ForumAttachment.query()
					.where('ref_id', postId)
					.update({ is_archived: 1 })
				}

				Object.assign(forumPost.$attributes, formData)
				forumPost.save()	

				const attachment_file = request.file('attachment',{
					type : ['application','image'],
					size : '2mb',
					extnames: ['jpg', 'png', 'jpeg','doc','docx','pdf','xls','xlsx']
				});

				if (attachment_file != null) {
					// put temporary file to trigger validation
					await attachment_file.move(Helpers.tmpPath('uploads'), {
						name: attachment_file.clientName,
						overwrite: true
					})
					let path_tmp = `${Helpers.tmpPath('uploads')}/${attachment_file.clientName}`

					// error validation file
					if (!attachment_file.moved()) {
						response.header('Content-type', 'application/json')
						response.type('application/json')
						let data = {
							code: '4004',
							message: `Invalid File Upload! - ${attachment_file.error().message}`,
							data: attachment_file.error()
						}
						return response.send(data)

					} else {
						let xid = uuidv4();
						let upload_setting = {
							xid: xid,
							content: content_type,
							tempPath: path_tmp,
						}
						let upload_result = await FileService.upload(attachment_file, upload_setting, true)					
						let contentType = await ForumContentType.query()
						.select('id')
						.where('name', '=', 'channel_forum_posts')
						.andWhere('is_archived','=', 0)
						.first();

						if (_lo.isEmpty(upload_result.url) == false && _lo.isEmpty(contentType) == false) {	

							let form_attachment = {
								xid				: xid,
								ref_id 			: postId,
								channel_forum_content_type_id : contentType.id,
								attachment_name : upload_result.filename,
								attachment_type : upload_result.filetype,
								attachment_url 	: upload_result.url,
								is_archived		: '0',
								updated_by		: auth.user.id,
								updated_at		: currentTime,
								created_by		: auth.user.id,
								created_at		: currentTime
							};
							await ForumAttachment.create(form_attachment);
						}
						fs.unlinkSync(path_tmp)
					}
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion updated',
					data: []
				}
				return response.send(data)
			} else {
				console.log('err')
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Forum', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let forumPost = await ForumPost.find(await MyHash.decrypt(formData.id))
		if (forumPost){
			try {
				//soft delete => change is_archived to true
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				forumPost.updated_by  = auth.user.id,
				forumPost.updated_at  = currentTime,
				forumPost.is_archived = 1
				await forumPost.save()

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Forum', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					let forumPost = await ForumPost.find(await MyHash.decrypt(dataitem[i]))

					//soft delete => change is_archived to true
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					forumPost.updated_by  = auth.user.id,
					forumPost.updated_at  = currentTime,
					forumPost.is_archived = 1
					await forumPost.save()

				} 
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be deleted',
					data: []
				}
				return response.send(data)
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Discussion deleted',
				data: []
			}
			return response.send(data)
			
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async updateStatus({ request, response, auth }) {
		const { id } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await ForumPost.find(idDecrypted)
		if (record != null) {
			Object.assign(record.$attributes, { is_published: !record.is_published })
			record.save()
			return response.defaultResponseJson(200, 'Success to update status channel')
		} else {
			return response.defaultResponseJson(400, 'Banner not found')
		}
	}
}

module.exports = ForumController