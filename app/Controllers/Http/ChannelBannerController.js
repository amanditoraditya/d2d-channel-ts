'use strict'

const Banner = use('App/Models/ChannelBanner')
const MyHash = require('./Helper/Hash.js')
const oss = require('ali-oss');
const _lo = use('lodash')
const CheckAuth = require('./Helper/CheckAuth.js')
const moment = require('moment');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV
const { v4: uuidv4 } = require('uuid');
const { validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')
const Helpers = use('Helpers')
const fs = require('fs')
const FileService = require('./Helper/FileService.js')
const content_type = 'banner'

class ChannelBannerController {

    async datatable({ request, response, auth }) {
        let admin_page = true
        let checkAuthAdmin = await CheckAuth.get('Banner', 'read', auth, admin_page)

        const queryModel = Banner.query()
            .innerJoin('channels', 'channel_banners.channel_id', 'channels.id')
            .select(
                'channel_banners.id',
                'channels.channel_name',
                'channel_banners.name',
                'channel_banners.image',
                'channel_banners.is_active',
                'channel_banners.created_at'
            )

        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model.where(`channel_banners.is_archived`, '0')
            if (!checkAuthAdmin) {
                model.where('channel_banners.channel_id', auth.user.channel_id)
            }
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async create({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Banner', 'create', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

        let form = JSON.parse(request.post().data)
        const image_file = request.file('image', {
            type: 'image',
            subtype: ['image/jpeg', 'image/png'],
            size: '2mb',
            extnames: ['jpg', 'jpeg', 'png']
        })
        let xid = uuidv4()
        let formData = {
            xid,
            channel_id: form.cid,
            position: form.position,
            name: form.name,
            target: null,
            link: form.link,
            is_active: form.is_active,
            action: null,
            is_archived: 0,
            updated_by: auth.user.id,
            updated_at: currentTime,
            created_by: auth.user.id,
            created_at: currentTime
        }
        if (image_file != null) {
            formData.image = true
        }

        let rules = {
            xid: 'required',
            name: 'required',
            channel_id: 'required',
            is_active: 'required',
            image: 'required'
        }

        const validation = await validateAll(formData, rules)

        if (validation.fails()) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            try {
                //#region upload image
                if (image_file != null) {

                    // put temporary file to trigger validation
                    await image_file.move(Helpers.tmpPath('uploads'), {
                        name: image_file.clientName,
                        overwrite: true
                    })
                    let path_tmp = `${Helpers.tmpPath('uploads')}/${image_file.clientName}`
                    // error validation file
                    if (!image_file.moved()) {
                        response.header('Content-type', 'application/json')
                        response.type('application/json')
                        let data = {
                            code: '4004',
                            message: `Invalid File Upload! - ${image_file.error().message}`,
                            data: image_file.error()
                        }
                        return response.send(data)
                    } else {
                        // upload file to oss
                        let upload_setting = {
                            xid: uuidv4(),
                            content: content_type,
                            tempPath: path_tmp,
                        }
                        let upload_result = await FileService.upload(image_file, upload_setting, true)
                        formData.image = upload_result.url
                        // delete temporary file
                        fs.unlinkSync(path_tmp)
                    }
                }
                //#endregion
                await Banner.create(formData)

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Banner added',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                console.log(e)
                let data = {
                    code: '4004',
                    message: 'Banner cannot be added',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async edit({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Banner', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let banner = await Banner.find(await MyHash.decrypt(formData.id))
        let get_channels = await banner.channels().where('channels.is_archived', '0').fetch()

        let channels_json = []

        if (!_lo.isEmpty(get_channels)) {
            channels_json = get_channels.toJSON()
            if (!_lo.isEmpty(channels_json)) {
                banner.channels = {
                    value: channels_json['id'],
                    label: channels_json['channel_name']
                }
            }
        } else {
            banner.channels = {
                value: '',
                label: 'No Data'
            }
        }

        if (banner) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Banner found',
                data: banner
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Banner cannot be found',
                data: []
            }
            return response.send(data)
        }
    }

    async update({ params, request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Banner', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
        let form = JSON.parse(request.post().data)

        const image_file = request.file('image', {
            type: 'image',
            subtype: ['image/jpeg', 'image/png'],
            size: '2mb',
            extnames: ['jpg', 'jpeg', 'png']
        })

        let banner = await Banner.find(await MyHash.decrypt(params.id))

        let xid = banner.xid
        let formData = {
            xid,
            channel_id: form.cid,
            position: form.position,
            name: form.name,
            target: null,
            link: form.link,
            is_active: form.is_active,
            action: null,
            is_archived: 0,
            updated_by: auth.user.id,
            updated_at: currentTime
        }

        if (!form.remove_image || image_file != null) {
            formData.image_flag = true
        }

        let rules = {
            xid: 'required',
            name: 'required',
            channel_id: 'required',
            is_active: 'required',
            image_flag: 'required'
        }

        const validation = await validateAll(formData, rules)
        if (validation.fails()) {
            console.log(validation)
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            if (banner) {
                try {

                    //delete flag
                    delete formData.image_flag

                    if (form.remove_image) {
                        //remove
                        let parts = banner.image.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);
                        formData.image = null
                    }
                    //#region upload image
                    if (image_file != null) {

                        //remove old
                        let parts = banner.image.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);

                        // put temporary file to trigger validation
                        await image_file.move(Helpers.tmpPath('uploads'), {
                            name: image_file.clientName,
                            overwrite: true
                        })
                        let path_tmp = `${Helpers.tmpPath('uploads')}/${image_file.clientName}`
                        // error validation file
                        if (!image_file.moved()) {
                            response.header('Content-type', 'application/json')
                            response.type('application/json')
                            let data = {
                                code: '4004',
                                message: `Invalid File Upload! - ${image_file.error().message}`,
                                data: image_file.error()
                            }
                            return response.send(data)
                        } else {
                            // upload file to oss
                            let upload_setting = {
                                xid: uuidv4(),
                                content: content_type,
                                tempPath: path_tmp,
                            }
                            let upload_result = await FileService.upload(image_file, upload_setting, true)
                            formData.image = upload_result.url
                            // delete temporary file
                            fs.unlinkSync(path_tmp)
                        }
                    }
                    //#endregion

                    Object.assign(banner.$attributes, formData)
                    banner.save()

                    response.header('Content-type', 'application/json')
                    response.type('application/json')
                    let data = {
                        code: '2000',
                        message: 'Banner updated',
                        data: []
                    }
                    return response.send(data)
                } catch (error) {
                    response.header('Content-type', 'application/json')
                    response.type('application/json')
                    let data = {
                        code: '4004',
                        message: 'Banner cannot be updated',
                        data: error
                    }
                    return response.send(data)
                }
            } else {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Banner cannot be found',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async delete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Banner', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let banner = await Banner.find(await MyHash.decrypt(formData.id))
        if (banner) {
            try {
                // let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                // banner.updated_by = auth.user.id
                // banner.updated_at = currentTime
                // banner.is_archived = 1
                // await banner.save()
                // await Banner.query().where('id', await MyHash.decrypt(formData.id)).update({is_archived:1})
                await banner.delete()

                //#region delete image
                if (!_lo.isEmpty(banner.image)) {
                	//remove
                	let parts = banner.image.split('/')
                	let url = parts.slice(3, parts.length)
                	url = url.join('/')
                	await FileService.delete(url);
                }
                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Banner success deleted',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                // Logger.info('e is %j', e)
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Banner cannot be deleted',
                    data: []
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Banner cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async multidelete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Banner', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        if (formData.totaldata != '0') {
            let dataitem = JSON.parse(formData.item)
            for (let i in dataitem) {
                let banner = await Banner.find(await MyHash.decrypt(dataitem[i]))
                try {
                    // let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                    // banner.updated_by = auth.user.id
                    // banner.updated_at = currentTime
                    // banner.is_archived = 1
                    // await banner.save()
                    await banner.delete()

                    //#region delete image
                    if (!_lo.isEmpty(banner.image)) {
                        //remove
                        let parts = banner.image.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);
                    }
                    //#endregion
                } catch (e) { }
            }

            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Banner success deleted',
                data: []
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Banner cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async getMenu({ request, response, auth, session }) {
        let req = request.get()
        let datas

        let checkAuth = await CheckAuth.get('Channel', 'read', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        if (req.phrase != '') {
            datas = await Banner.query().where('name', 'LIKE', '%' + req.phrase + '%').andWhere('is_archived', '0').limit(20).fetch()
        } else {
            datas = await Banner.query().limit(20).fetch()
        }

        let data = datas.toJSON()

        let result = []
        for (let i in data) {
            result.push({
                value: data[i]['id'],
                label: data[i]['name']
            })
        }

        return response.send(result)
    }

    async updateStatus({ request, response, auth }) {
        const { id } = request.post()
        const idDecrypted = await MyHash.decrypt(id)
        const record = await Banner.find(idDecrypted)
        if (record != null) {
            Object.assign(record.$attributes, { is_active: !record.is_active })
            record.save()
            return response.defaultResponseJson(200, 'Success to update status channel')
        } else {
            return response.defaultResponseJson(400, 'Banner not found')
        }
    }
}

module.exports = ChannelBannerController