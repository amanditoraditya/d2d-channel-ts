'use strict'

const Env = use('Env')
const Database = use('Database')

// MODULES
const crypto = require("crypto-js")
const moment = require('moment')

// HELPERS
const ApiService = use('App/Controllers/Http/Helper/ApiService')
// MODELS
const TemporaryMember = use('App/Models/TemporaryMember')

class ExtendedServiceHelper {
	async generateuser(usersession) {
		try{
			let action = null
			let member_id = null
			const member = await TemporaryMember
			.query()
			.select('id', Database.raw('HOUR(TIMEDIFF(updated_at, now())) updated_hour'))
			.where('uid', usersession.uid)
			.first()
			if(member){
				if(member.updated_hour > 24){ 
					action = "UPDATE" 
					member_id = member.id
				}
			} else {
				action = "ADD"
			}
			if(action){
				let apiurl = "https://staging-api.d2d.co.id/v3.1/profile"
				if(Env.get('NODE_ENV') == 'production'){
					apiurl = "https://api.d2d.co.id/v3.1/profile"
				}
				const options = {
					headers: {
						authorization: usersession.token
					}
				}
				const reqparam = {}
				const datauser = await ApiService.getData(apiurl, 'GET', reqparam, options)
				if(datauser.acknowledge){
					const udata = datauser.result
					await this.temp_user(member_id, udata, action)
				}
			}
		} catch(e){ console.log(e) }
	}

	async temp_user(id, userdata, action){
		try{
			let spesialization = ""
			let nonspesialization = ""
			if(userdata.spesialization[0]){
				spesialization = userdata.spesialization[0].description
			}
			if(userdata.nonspesialization[0]){
				nonspesialization = userdata.nonspesialization[0].description
			}
			let display_picture = "https://images.vexels.com/media/users/3/151709/isolated/preview/098c4aad185294e67a3f695b3e64a2ec-doctor-avatar-icon-by-vexels.png"
			if(userdata.photo){
				display_picture = userdata.photo
			}
			let drecord = {
				email : userdata.email,
				name : userdata.name,
				spesialization : spesialization,
				nonspesialization : nonspesialization,
				display_picture : display_picture,
                phone : userdata.phone,
                home_location : userdata.home_location
			}
			if(action == "ADD"){
				drecord.uid = userdata.uid
				await TemporaryMember.create(drecord)
			} else if (action == "UPDATE"){
				await TemporaryMember.query()
				.where('id',  id)
				.update(drecord)
			}
		} catch(e){console.log(e)}
    }

	async generatmeet(domain, meet_id, title, email, display_name, type) {
		let dataset = null
		console.log("title", title)
		try{
			const exp = moment().add(2, 'minutes')
			const knock_key = Math.random().toString(36).substr(2, 5) + ''
			let meet_cred = {
				domain : domain,
				option : {
					roomName: meet_id,
					subject : title,
					userInfo: {
						email: email,
						displayName: display_name
					},
					disableInviteFunctions: true,
					configOverwrite: { 
						disableDeepLinking : true
					}
				},
				others : {}
			}

			if(type != "moderator"){
				meet_cred.option.configOverwrite.startAudioOnly = true
				meet_cred.option.configOverwrite.remoteVideoMenu = {
					disableKick: true
				}
				meet_cred.option.interfaceConfigOverwrite = {
					TOOLBAR_BUTTONS: ['microphone', 'camera', 'tileview', 'filmstrip', 'raisehand', 'chat'],
					DISPLAY_WELCOME_FOOTER: false
				}
			}

			const encrypted = crypto.AES.encrypt(JSON.stringify(meet_cred), knock_key).toString()
			const _encrypted = knock_key + encrypted
			let buff = Buffer.from(_encrypted) 
			let base64data = buff.toString('base64')
			let _channelconference = Env.get('NEXT_BASE_URL') + 'meet' + '?knock=' + base64data
			if(_channelconference){
				dataset = {
					dmeet : _channelconference
				}
			}   
		} catch(e){ console.log(e) }
		return dataset
	}
}

module.exports = new ExtendedServiceHelper