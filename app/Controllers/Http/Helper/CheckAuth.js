'use strict'

const Channel = use('App/Models/Channel')
const Logger = use('Logger')
const _lo = use('lodash')

class CheckAuth {
	async get(mod, permit, auth, admin_page) {
		let valid = false
		let user = auth.user
		
		admin_page = admin_page == true ? true : false

		let userRoles = await Channel.query()
			.where('user_channels.xid', user.xid)
			.leftJoin('user_channels', 'channels.id', 'user_channels.channel_id')
			.first()
		
		let is_admin = false
		if (!_lo.isEmpty(userRoles)) {
			is_admin = userRoles.is_admin == 1 ? true : false
		}

		if (admin_page) {
			if (is_admin) {
				valid = true
			} else {
				valid = false
			}
		} else {
			valid = true
		}
		
		return valid
	}
}

module.exports = new CheckAuth