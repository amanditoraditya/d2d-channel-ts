'use strict'
const _ = require('lodash')
const Env = use('Env');

const SYSTEM_INFO = {
    ENVIRONMENT : Env.get('NODE_ENV')
}

const D2D_DOMAIN = {
    STAGING : _.isEmpty(Env.get('D2D_DOMAIN_STAGING')) ? 'https://staging.d2d.co.id/' : Env.get('D2D_DOMAIN_STAGING'),
    PRODUCTION : _.isEmpty(Env.get('D2D_DOMAIN_PRODUCTION')) ? 'https://d2d.co.id/' : Env.get('D2D_DOMAIN_PRODUCTION'),
}

const D2D_API_DOMAIN = {
    STAGING :  _.isEmpty(Env.get('D2D_API_DOMAIN_STAGING')) ?'https://staging-api.d2d.co.id/' : Env.get('D2D_API_DOMAIN_STAGING'),
    PRODUCTION :  _.isEmpty(Env.get('D2D_API_DOMAIN_PRODUCTION')) ? 'https://staging-api.d2d.co.id/' : Env.get('D2D_API_DOMAIN_PRODUCTION'),
}

const OSS_SETTING = {
    REGION : Env.get('OSS_REGION'),
    ACCESSKEYID : Env.get('OSS_ACCESSKEYID'),
    ACCESSKEYSECRET : Env.get('OSS_ACCESSKEYSECRET'),
    BUCKET : Env.get('OSS_BUCKET'),
}

module.exports = { SYSTEM_INFO, D2D_DOMAIN, D2D_API_DOMAIN, OSS_SETTING }