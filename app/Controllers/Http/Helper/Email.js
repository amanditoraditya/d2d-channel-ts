'use strict'

const Mail = use('Mail')
const Env = use('Env')

class Email {
	async send(to, subject, template, data) {
		await Mail.send(template, data, (message) => {
			message
				.to(to)
				.from(Env.get('MAIL_USERNAME'), 'GPOS')
				.subject(subject)
		})
	}
}

module.exports = new Email
