'use strict'

const SystemLog = use('App/Models/SystemLog')
const Logger = use('Logger')
const _lo = use('lodash')
const parser = require('ua-parser-js')
const Env = use('Env')

class Log {
	async set(data) {
		if (Env.get('LOG_SYS') == 'false') {
		} else {
			let ua = parser(data.user_agent)
			
			if (data.url.indexOf('activity-log') == 1 || data.url.indexOf('import') == 1) {
			} else {
				await SystemLog.create({
					user_id: (data.user_id || ''),
					access: (data.access || ''),
					ip: (data.ip || ''),
					user_agent: (data.user_agent || ''),
					browser: 'Name: ' + (ua.browser.name || '') + ', Version: ' + (ua.browser.version || '') + '.' + (ua.browser.major || ''),
					cpu: 'Arch: ' + (ua.cpu.architecture || ''),
					device: 'Vendor: ' + (ua.device.vendor || '') + ', Model: ' + (ua.device.model || '') + ', Type: ' + (ua.device.type || ''),
					engine: 'Name: ' + (ua.engine.name || '') + ', Version: ' + (ua.engine.version || ''),
					os: 'Name: ' + (ua.os.name || '') + ', Version: ' + (ua.os.version || ''),
					url: (data.url || ''),
					method: (data.method || ''),
					param: (data.params || ''),
					body: JSON.stringify(data.body),
					response: JSON.stringify(data.response)
				})
			}
		}
		
		return true
	}
}

module.exports = new Log