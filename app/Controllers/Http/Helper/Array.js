'use strict'

class Array {
    async compareUniqueElement(currentArray, otherArray, currentKey, otherKey) {
        currentArray = currentArray.filter(
            function (row) {
                let res = null
                if (currentKey != undefined && otherKey != undefined) {
                    res = otherArray.findIndex(function (obj) {
                        return obj[otherKey] == row[currentKey];
                    });
                } else {
                    res = otherArray.findIndex(function (obj) {
                        return obj == row;
                    });
                }

                res = res < 0;

                return res
            }
        )
        return currentArray
    }
}

module.exports = new Array