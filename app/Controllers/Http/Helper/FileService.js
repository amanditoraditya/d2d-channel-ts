'use strict'

const Env = use('Env')
const { v4: uuidv4 } = require('uuid');
const _lo = use('lodash')
const oss = require('ali-oss');

const { OSS_SETTING, SYSTEM_INFO } = use('App/Controllers/Http/Helper/Enum')
// const goaConfig 	 = require('../../../next/config.js')

// const environment 	 = goaConfig.NODE_ENV
const environment = SYSTEM_INFO.ENVIRONMENT;
const client = new oss({
    region: OSS_SETTING.REGION,
    accessKeyId: OSS_SETTING.ACCESSKEYID,
    accessKeySecret: OSS_SETTING.ACCESSKEYSECRET,
    bucket: OSS_SETTING.BUCKET
})

class FileService {
    /*upload file to oss
    > file => Attachment File
    > setting :
    -> content : this attchement content, ex forum
    -> xid : unique guid
    -> tempPath: temp path attachment (if file upload has been moved)
    -> type : allowed type
    -> size : max size 
    */
    async upload(file, setting = { xid: '', content: '', tempPath:'' }, setHttps = true) {
        let upload_result = {
            url: '',
            filename: '',
            filetype: ''
        };
        try {
            if (_lo.isEmpty(file) == false) {
                let name = file.clientName;
                let type = file.type;
                let env = environment == 'production' ? '' : 'dev/'
                let xid = _lo.isEmpty(setting.xid) ? uuidv4() : setting.xid;
                let content = _lo.isEmpty(setting.content) ? 'general' : setting.content;
                let path = _lo.isEmpty(setting.tempPath) ? file.tmpPath : setting.tempPath;

                upload_result = await client.put(`${env}${type}/${content}/${xid}.${file.subtype}`, path);

                upload_result.filename = name;
                upload_result.filetype = type;

                if (setHttps == true) {
                    upload_result.url = upload_result.url.replace("http://", "https://");
                }
            }
        } catch (e) {
            console.log(e)
            upload_result = {
                url: '',
                filename: '',
                filetype: ''
            };
        }

        return upload_result
    }

    async delete(url) {
        let deleted = null
        try {
            deleted = await client.delete(url);
        } catch (error) {
            console.log(error)
        }
        return deleted;
    }

    async copy(newUrl,url) {
        let copy = null
        try {
            copy = await client.copy(newUrl,url);
        } catch (error) {
            console.log(error)
        }
        return copy;
    }
}

module.exports = new FileService