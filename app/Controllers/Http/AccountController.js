'use strict'

const User 		 = use('App/Models/UserChannel')
const Role 		 = use('App/Models/ChannelRole')
const Channel	 = use('App/Models/Channel')
const Helpers 	 = use('Helpers')
const Hash 		 = use('Hash')
const Logger 	 = use('Logger')
const Env 		 = use('Env')
const ms 		 = require('ms')
const delay 	 = require('delay')
const moment 	 = require('moment')
const CheckAuth  = require('./Helper/CheckAuth.js')
const ApiService = require('./Helper/ApiService.js')

class AccountController {
	
	async clearToken({ request, response, auth, view, session }) {
		response.clearCookie('XSRF-TOKEN')
		response.clearCookie('engine-session')
		response.clearCookie('engine-session-values')

		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '4004',
			message: 'Cookie Cleared',
			data: []
		}
		return response.send(data)
	}
	
	async login({ request, response, auth, view, session }) {
		const { email, password } = request.all()
		
	 	try {
			let checkifblock = await User.query().select('id').where('email', email).where('block', '0').first()
			// now logged
	 		let setLogin
	 		if(checkifblock) {
				setLogin = await auth.attempt(email, password)
			} else {
				setLogin = false
			}
			
			if (setLogin) {
				const user = await auth.getUser()
				// console.log(user.xid)
				let currentToken = moment(new Date()).format('YYYYMMDDHHmmss')
				let channelAssigned = await Channel.query()
					.where('user_channels.xid', user.xid)
					.leftJoin('user_channels', 'channels.id', 'user_channels.channel_id')
					.first()
				// get all roles due to permissions
				const roleAssigned = await user.roles().select('permissions', 'name', 'type').where('user_channel_roles.is_archived', '=', '0').fetch()
				const permissionAssigned = {}
                let isMedicalRep = false
				let permissionRole = {}
				if (roleAssigned.rows.length > 0) {
					roleAssigned.rows.map((roleRecord) => {
						permissionRole = JSON.parse(roleRecord.permissions)
                        permissionRole = permissionRole == null ? [] : permissionRole

						isMedicalRep = roleRecord.type == 'medical-representative' ? true : false

                        permissionRole.map((permission) => {
							if (typeof permissionAssigned[permission.menu_id] == 'undefined') {
								permissionAssigned[permission.menu_id] = {}
							}
							Object.keys(permission).map((keyPermission) => {
								if (['create', 'read', 'update', 'delete'].indexOf(keyPermission) >= 0 && (typeof permissionAssigned[permission.menu_id][keyPermission] == 'undefined' || (typeof permissionAssigned[permission.menu_id][keyPermission] != 'undefined' && !permissionAssigned[permission.menu_id][keyPermission]))) {
									permissionAssigned[permission.menu_id][keyPermission] = permission[keyPermission]
								}
							})
						})
					})
				}

				let user_data = {
					user: setLogin,
					is_admin: channelAssigned.is_admin,
					permissions: permissionAssigned,
                    is_medical_rep: isMedicalRep,
					current_token: currentToken
				}
				
				session.put('user', user_data)
				response.cookie('gpos-rememberme', {
					email: email,
					password: password
				}, {
					httpOnly: true,
					sameSite: true,
					path: '/',
					expires: new Date(Date.now() + ms('7d'))
				})
				
				await User.query().where('id', user.id).update({
					current_token: currentToken,
					updated_by: user.id
				})
				
				// session.flash({ users_roles: users_roles })
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User logged',
					data: user_data
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Cannot verify user',
					data: []
				}
				return response.send(data)
			}
		} catch (e) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: e.message,
				data: []
			}
			return response.send(data)
		}
	}
	
	async checklogin({ request, response, auth, session }) {
		let user = session.get('user')
		let req = request.post()

		if (user) {
			let checkUser = await User.query().where('id', user.user.id).first()
			let currentToken = false
			if (checkUser) {
				if (checkUser.current_token == user.current_token) {
					currentToken = true
				}
			}
			
			if (currentToken) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User logged',
					data: user
				}
				return response.send(data)
			} else {
				await auth.logout()
				session.forget('user')
				response.clearCookie('gpos-rememberme')
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User not logged',
				data: []
			}
			return response.send(data)
		}
	}

	async logout({ request, response, auth, session }) {
		await auth.logout()
		session.forget('user')
		response.clearCookie('gpos-rememberme')
		
		await delay(1000)
		return response.redirect('/')
	}
	
	async checkauth({ request, response, auth, session }) {
		let req = request.post()
		
		let checkAuth = await CheckAuth.get(req.module, req.permission, auth, req.admin)
		if (checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User can access this module',
				data: []
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
	}
	
	async checkpass({ request, response, auth, session }) {
		let req = request.post()
		
		let role = await Role.query().whereIn('role_slug', ['superadmin', 'administrator']).pluck('id')
		let user = await User.query().where('username', req.username).where('block', 'N').whereIn('user_role', role).first()
		if (user) {
			if (await Hash.verify(req.password, user.password) == true) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User verify',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Cannot verify user',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Cannot verify user',
				data: []
			}
			return response.send(data)
		}
	}
}

module.exports = AccountController