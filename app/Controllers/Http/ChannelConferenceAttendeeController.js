'use strict'

const Conference = use('App/Models/ChannelConference')
const ConferenceAttendee = use('App/Models/ChannelConferenceAttendee')
const Subscriber = use('App/Models/ChannelSubscriber')
const Member = use('App/Models/TemporaryMember')
const MyHash = require('./Helper/Hash.js')
const Database = use('Database')
const _lo = use('lodash')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const Env = use('Env')
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV
const meet_url = Env.get('MEET_D2D_URL', 'https://meet.d2d.co.id')
const crypto = require('crypto')
const { validate, validateAll } = use('Validator')
const ServiceHelper = require('./Helper/ExtendedServiceHelper')
const Datatable = use('App/Helpers/Datatable')

class ChannelConferenceAttendeeController {

    async datatable({ request, response, auth }) {

        const formData = request.post()
        let conference_id = await MyHash.decrypt(formData.id);
        const queryModel = ConferenceAttendee.query()
            .select(
                'channel_conference_attendees.id',
                'temporary_members.name',
                'temporary_members.email',
                'temporary_members.spesialization',
                'temporary_members.nonspesialization',
                'temporary_members.display_picture',
                'channel_conference_attendees.created_at'
            )
            .leftJoin('channel_subscribers', 'channel_conference_attendees.channel_subscriber_id', 'channel_subscribers.id')
            .leftJoin('temporary_members', 'temporary_members.uid', 'channel_subscribers.uid')

        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model
                .where(`channel_subscribers.blocked`, '0')
                .where(`channel_subscribers.is_unsubscribed`, '0')
                .where(`channel_conference_attendees.is_removed`, '0')
                .where(`channel_conference_attendees.conference_id`, conference_id)
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async update({ params, request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

        let { channel_subscribers } = request.only(['channel_subscribers'])
        let conference = await Conference.find(await MyHash.decrypt(params.id))
        if (conference) {
            let id = await MyHash.decrypt(params.id)

            //#region  soft delete old conference_attendee

            //get attendee existing
            let ca = await ConferenceAttendee.query()
                .where('conference_id', id)
                .fetch()
            ca = _lo.isEmpty(ca) ? [] : ca.toJSON()
            channel_subscribers = _lo.isEmpty(channel_subscribers) ? [] : channel_subscribers
            console.log('channel_subscribers', channel_subscribers)
            console.log('ca', ca)
            for (let row of ca) {
                // if menu berkurang

                // check kalau ada menu dari form dengan menu existing di table
                if (!channel_subscribers.some(item => item.value == row.channel_subscriber_id)) {
                    // kalau ada kita hanya update archived-nya
                    await ConferenceAttendee.query()
                        .where('conference_id', id)
                        .andWhere('channel_subscriber_id', row.channel_subscriber_id)
                        .update({
                            updated_by: auth.user.id,
                            updated_at: currentTime,
                            is_removed: 1
                        })

                    // mengurangi menu yang didapat dari form yang akan diiterasikan untuk ditambah ke table
                    channel_subscribers = channel_subscribers.filter(function (obj) {
                        return obj.value != row.channel_subscriber_id;
                    });
                }
            }
            // menu bertambah
            for (let row of channel_subscribers) {

                // check kalau ada menu dari form dengan menu existing di table
                // kalau ada tidak ditambahkan lagi
                var cm_filtered = ca.filter(function (obj) {
                    return obj.channel_subscriber_id == row.value;
                });

                // kalau menu sudah ada di table hanya update archive
                if (!_lo.isEmpty(cm_filtered)) {
                    if (cm_filtered[0].is_removed == 1) {
                        await ConferenceAttendee.query()
                            .where('conference_id', id)
                            .andWhere('channel_subscriber_id', row.value)
                            .update({
                                is_removed: 0,
                                updated_by: auth.user.id,
                                updated_at: currentTime
                            })
                    }
                } else {
                    // kalau menu belum ada di table di insert
                    let xid = uuidv4()
                    let channel_subs_data = {
                        xid,
                        conference_id: id,
                        channel_subscriber_id: row.value,
                        registered_from: 'CMS',
                        is_notify: 0,
                        blocked: 0,
                        updated_by: auth.user.id,
                        updated_at: currentTime,
                        created_by: auth.user.id,
                        created_at: currentTime
                    }
                    await ConferenceAttendee.create(channel_subs_data)
                }
            }
            //#endregion

            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Conference updated',
                data: []
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be found',
                data: []
            }
            return response.send(data)
        }
    }

    async delete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let id = await MyHash.decrypt(formData.id)
        console.log('formData.id', formData.id)
        console.log('id', id)
        let attendee = await ConferenceAttendee.find(id)
        if (attendee) {
            try {
                console.log(attendee)
                let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                attendee.updated_by = auth.user.id
                attendee.updated_at = currentTime
                attendee.is_removed = 1
                await attendee.save()

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Conference success deleted',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Conference cannot be deleted',
                    data: e
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async multidelete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Conference', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        if (formData.totaldata != '0') {
            let dataitem = JSON.parse(formData.item)
            for (let i in dataitem) {
                let attendee = await ConferenceAttendee.find(await MyHash.decrypt(dataitem[i]))
                try {
                    let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                    attendee.updated_by = auth.user.id,
                        attendee.updated_at = currentTime,
                        attendee.is_removed = 1
                    await attendee.save()

                    // await channel.delete()
                } catch (e) { }
            }

            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Conference success deleted',
                data: []
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Conference cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }
}

module.exports = ChannelConferenceAttendeeController