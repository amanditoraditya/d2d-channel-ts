'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const MyHash 		= require('./Helper/Hash.js')
const { v4: uuidv4 } = require('uuid');

const Channel 			= use('App/Models/Channel')
const ForumPost 		= use('App/Models/ChannelForumPost')
const ForumLike  		= use('App/Models/ChannelForumLike')
const ForumAttachment  	= use('App/Models/ChannelForumAttachment')
const ForumComment   	= use('App/Models/ChannelForumComment')

const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')
const Datatable 		= use('App/Helpers/Datatable')

const { validate, validateAll } = use('Validator')

class ForumCommentController {
	async datatable({request, response, auth, session}) {
		let page_admin = true
		let checkIsAdmin = await CheckAuth.get('Forum', 'read', auth, page_admin)
		
		const formData  = request.post()
		let forumPostId = await MyHash.decrypt(formData.forumpostid);
		
		const queryModel = ForumComment.query()
			.select(
				'channel_forum_comments.id', 
				'channel_forum_comments.xid', 
				Database.raw('IF(channel_forum_comments.reply_to = 0, "comment", "replies") AS type'),
				Database.raw('IF(channel_forum_comments.reply_to = 0, "post", channel_forum_comments.reply_to) AS reply_to'),
				'channel_forum_comments.comment', 
				`temporary_members.name`, 
				'temporary_members.email'
			)
			.leftJoin('channel_subscribers', 'channel_subscribers.id', 'channel_forum_comments.channel_subscriber_id' )
			.leftJoin('temporary_members', 'temporary_members.uid', 'channel_subscribers.uid' )

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model.where(`channel_forum_comments.is_archived`, '0')
			model.where(`channel_forum_comments.channel_forum_post_id`, '=', forumPostId)
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			}
		})

		return response.json(await datatable.make())
	}
	
	async create({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('Forum', 'create', auth, true)
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		let xid = uuidv4()
		let channel_id

		if (checkIsAdmin) {
			try{
				channel_id = await MyHash.decrypt(form.channel_id)
			}catch(e){
				channel_id = form.channel_id
			}
		} else {
			channel_id = auth.user.channel_id
		}

		let formData = {
			xid,
			channel_id,
			title			: form.title,
			description		: form.description,
			is_published	: form.is_published,
			is_archived		: '0',
			updated_by		: auth.user.id,
			updated_at		: currentTime,
			created_by		: auth.user.id,
			created_at		: currentTime
		}
		
		let rules = {
			title		 : 'required',
			description	 : 'required',
		}
		
		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			try {
				let newForumPost = await ForumPost.create(formData)
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				console.log(e)
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth 	 = await CheckAuth.get('Forum', 'update', auth, false)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let forumPost = await ForumPost.query()
									.select('channel_forum_posts.*')
									.where('channel_forum_posts.id', await MyHash.decrypt(formData.id))
									.first()

		let get_channels = await forumPost._v1_channel().where('channels.is_archived', '0').fetch()
		let channels_json = []

		if (!_lo.isEmpty(get_channels)) {
			channels_json = get_channels.toJSON()
			if (!_lo.isEmpty(channels_json)) {
				forumPost.channels = {
					value: channels_json['id'],
					label: channels_json['channel_name']
				}
			}
		} else {
			forumPost.channels = {
				value: '',
				label: 'No Data'
			}
		}

		if (forumPost) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Discussion found',
				data: forumPost
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		let forumPost = await ForumPost.find(await MyHash.decrypt(params.id))

		let formData = {
			xid				: form.xid,
			channel_id		: form.channel_id,
			title			: form.title,
			description		: form.description,
			is_published	: form.is_published,
			is_archived		: '0',
			updated_by		: auth.user.id,
			updated_at		: currentTime,
			created_by		: auth.user.id,
			created_at		: currentTime
		}
		
		let rules = {
			title: 'required',
			description: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: []
			}
			return response.send(data)
		} else {
			if (forumPost) {
				let id = await MyHash.decrypt(params.id)
				let updateForumPost = await ForumPost.query().where('id', id).update(formData)

				console.log('sampai akhir')
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion updated',
					data: []
				}
				return response.send(data)
			} else {
				console.log('err')
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Forum', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let forumPost = await ForumPost.find(await MyHash.decrypt(formData.id))
		if (forumPost){
			try {

				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				forumPost.updated_by  = auth.user.id,
				forumPost.updated_at  = currentTime,
				forumPost.is_archived = 1
				await forumPost.save()

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'Discussion deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Forum', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					let forumPost = await ForumPost.find(await MyHash.decrypt(dataitem[i]))

					//soft delete => change is_archived to true
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					forumPost.updated_by  = auth.user.id,
					forumPost.updated_at  = currentTime,
					forumPost.is_archived = 1
					await forumPost.save()
				} 
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'Discussion cannot be deleted',
					data: []
				}
				return response.send(data)
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'Discussion deleted',
				data: []
			}
			return response.send(data)
			
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Discussion cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
}

module.exports = ForumCommentController