'use strict'

const Channel = use('App/Models/Channel')
const ChannelType = use('App/Models/ChannelType')
const ChannelMenu = use('App/Models/ChannelMenu')
const ChannelBanner = use('App/Models/ChannelBanner')
const MyHash = require('./Helper/Hash.js')
const oss = require('ali-oss');
const _lo = use('lodash')
const CheckAuth = require('./Helper/CheckAuth.js')
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const goaConfig = require('../../../next/config.js')
const environment = goaConfig.NODE_ENV
const { validateAll } = use('Validator')
const Datatable = use('App/Helpers/Datatable')
const FileService = require('./Helper/FileService.js')
const ArrayHelper = require('./Helper/Array.js')
const content_type = 'channel'
const Helpers = use('Helpers')
const Database = use('Database')
const fs = require('fs')

class ChannelController {

    async datatable({ request, response, auth }) {
        let admin_page = true
        let checkAuthAdmin = await CheckAuth.get('Channel', 'read', auth, admin_page)

        const queryModel = Channel.query()
            .select(
                'channels.id',
                'channels.country_code',
                'channels.channel_name',
                'channels.channel_type_id',
                'channels.status',
                'channel_types.name as channel_type_name',
                'channels.created_at'
            ).leftJoin('channel_types', 'channels.channel_type_id', 'channel_types.id')


        const datatable = new Datatable(queryModel, request.all())

        datatable.setCustomFilter((model, searchKey) => {
            model
                .where(`channels.is_archived`, '0')
                .where(`channels.is_admin`, '0')

            if (!checkAuthAdmin) {
                model
                    .leftJoin('user_channels', 'channels.id', 'user_channels.channel_id')
                    .where('user_channels.xid', auth.user.xid)
            }
        })

        datatable.setAdditionalColumn({
            'encrypted': async (data) => {
                return await MyHash.encrypt(data.id.toString())
            }
        })

        return response.json(await datatable.make())
    }

    async create({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Channel', 'create', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

        let form = JSON.parse(request.post().data)

        const logo_file = request.file('logo', {
            type: 'image',
            subtype: ['image/jpeg', 'image/png'],
            size: '2mb',
            extnames: ['jpg', 'jpeg', 'png']
        })

        let xid = uuidv4()
        let formData = {
            xid,
            channel_type_id: form.ctid,
            country_code: form.country_code,
            channel_name: form.channel_name,
            description: form.description,
            is_admin: 0,
            is_archived: 0,
            status: form.status,
            updated_by: auth.user.id,
            updated_at: currentTime,
            created_by: auth.user.id,
            created_at: currentTime
        }

        let rules = {
            xid: 'required',
            country_code: 'required',
            channel_type_id: 'required',
            channel_name: 'required'
        }

        const validation = await validateAll(formData, rules)

        if (validation.fails()) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            try {
                //#region upload logo
                if (logo_file != null) {

                    // put temporary file to trigger validation
                    await logo_file.move(Helpers.tmpPath('uploads'), {
                        name: logo_file.clientName,
                        overwrite: true
                    })
                    let path_tmp = `${Helpers.tmpPath('uploads')}/${logo_file.clientName}`
                    // error validation file
                    if (!logo_file.moved()) {
                        response.header('Content-type', 'application/json')
                        response.type('application/json')
                        let data = {
                            code: '4004',
                            message: `Invalid File Upload! - ${logo_file.error().message}`,
                            data: logo_file.error()
                        }
                        return response.send(data)
                    } else {
                        let upload_setting = {
                            xid: uuidv4(),
                            content: content_type,
                            tempPath: path_tmp,
                        }
                        let upload_result = await FileService.upload(logo_file, upload_setting, true)
                        formData.logo = upload_result.url

                        fs.unlinkSync(path_tmp)
                    }
                }
                //#endregion

                let newChannel = await Channel.create(formData)
                let channel_menus = form.channel_menus
                channel_menus = _lo.isEmpty(channel_menus) ? [] : channel_menus
                //#region menu bertambah
                
                let channel_menu_to_insert = []

                for (let row of channel_menus) {
                    let xid = uuidv4()
                    let channel_menu_data = {
                        xid,
                        channel_id: newChannel.id,
                        menu_id: row.value,
                        position: 0,
                        is_archived: 0,
                        updated_by: auth.user.id,
                        updated_at: currentTime,
                        created_by: auth.user.id,
                        created_at: currentTime
                    }
                    channel_menu_to_insert.push(channel_menu_data)
                }
                
                await ChannelMenu.createMany(channel_menu_to_insert)
                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Channel added',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                console.log(e)
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Channel cannot be added',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async edit({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Channel', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        // let channel	= await Channel.find(await MyHash.decrypt(formData.id))
        let channel = await Channel.query()
            .select('channels.*', 'channel_types.name as channel_type_name')
            .leftJoin('channel_types', 'channels.channel_type_id', 'channel_types.id')
            .where('channels.id', await MyHash.decrypt(formData.id))
            .first()

        let get_menus = await channel.menus().where('channel_menus.is_archived', '0').fetch()
        let channel_menus_json = get_menus.toJSON()
        let channel_menus = []
        for (let i in channel_menus_json) {
            channel_menus.push({
                value: channel_menus_json[i]['id'],
                label: channel_menus_json[i]['name']
            })
        }

        if (!_lo.isEmpty(channel_menus)) {
            channel.channel_menus = channel_menus
        }

        if (channel) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Channel found',
                data: channel
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Channel cannot be found',
                data: []
            }
            return response.send(data)
        }
    }

    async update({ params, request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Channel', 'update', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
        let form = JSON.parse(request.post().data)

        const logo_file = request.file('logo', {
            type: 'image',
            subtype: ['image/jpeg', 'image/png'],
            size: '2mb',
            extnames: ['jpg', 'jpeg', 'png']
        })

        let channel = await Channel.find(await MyHash.decrypt(params.id))
        let channel_menus = form.channel_menus

        let xid = channel.xid
        let formData = {
            xid,
            channel_type_id: form.ctid,
            country_code: form.country_code,
            channel_name: form.channel_name,
            description: form.description,
            is_admin: 0,
            is_archived: 0,
            status: form.status,
            updated_by: auth.user.id,
            updated_at: currentTime
        }

        if (!form.remove_image || logo_file != null) {
            formData.logo_flag = true
        }

        let rules = {
            xid: 'required',
            country_code: 'required',
            channel_type_id: 'required',
            channel_name: 'required',
        }

        const validation = await validateAll(formData, rules)
        if (validation.fails()) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Please fill required marker field!',
                data: validation
            }
            return response.send(data)
        } else {
            if (channel) {
                try {
                    //delete flag
                    delete formData.logo_flag
                    
                    //#region upload image
                    if (logo_file != null) {
                        // put temporary file to trigger validation
                        await logo_file.move(Helpers.tmpPath('uploads'), {
                            name: logo_file.clientName,
                            overwrite: true
                        })
                        
                        let path_tmp = `${Helpers.tmpPath('uploads')}/${logo_file.clientName}`
                        // error validation file
                        if (!logo_file.moved()) {
                            response.header('Content-type', 'application/json')
                            response.type('application/json')
                            let data = {
                                code: '4004',
                                message: `Invalid File Upload! - ${logo_file.error().message}`,
                                data: logo_file.error()
                            }
                            return response.send(data)
                        } else {
                            // upload file to oss
                            let upload_setting = {
                                xid: uuidv4(),
                                content: content_type,
                                tempPath: path_tmp,
                            }
                            let upload_result = await FileService.upload(logo_file, upload_setting, true)
                            formData.logo = upload_result.url
                            // delete temporary file
                            fs.unlinkSync(path_tmp)
                        }
                    }
                    //#endregion

                    Object.assign(channel.$attributes, formData)
                    await channel.save()

                    //#region channel menu update
                    let menus_existing = await channel.channel_menus().fetch()
                    let menus_existing_json = menus_existing.toJSON()
                    channel_menus = _lo.isEmpty(channel_menus) ? [] : channel_menus

                    let id_only_existing = []
                    let id_only_form = []

                    for (let row of menus_existing_json) {
                        id_only_existing.push(row.menu_id)
                    }
                    for (let row of channel_menus) {
                        id_only_form.push(row.value)
                    }

                    var onlyInExisting = await ArrayHelper.compareUniqueElement(id_only_existing, id_only_form);
                    var onlyInForm = await ArrayHelper.compareUniqueElement(id_only_form, id_only_existing);

                    // menu berkurang
                    await channel
                        .channel_menus()
                        .where('menu_id', 'in', onlyInExisting)
                        .delete()

                    // menu bertambah
                    let menu_to_insert = []

                    for (let row of onlyInForm) {
                        let update_row = await channel
                            .channel_menus()
                            .onlyTrashed()
                            .where('menu_id', row)
                            .first()

                        if (update_row) {
                            update_row.restore()
                        } else {
                            menu_to_insert.push(row)
                        }
                    }

                    let channel_menu_data = {
                        xid: '',
                        channel_id: channel.id,
                        menu_id: '',
                        position: 0,
                        is_archived: 0,
                        updated_by: auth.user.id,
                        updated_at: currentTime,
                        created_by: auth.user.id,
                        created_at: currentTime
                    }
                    let channel_menu_to_insert = []

                    for (let row of menu_to_insert) {
                        channel_menu_data.xid = uuidv4()
                        channel_menu_data.menu_id = row
                        channel_menu_to_insert.push(channel_menu_data)
                    }

                    if (channel_menu_to_insert.length) {
                        await ChannelMenu.createMany(channel_menu_to_insert)
                    }
                    //#endregion

                    // await trx.commit()
                    response.header('Content-type', 'application/json')
                    response.type('application/json')
                    let data = {
                        code: '2000',
                        message: 'Channel updated',
                        data: []
                    }
                    return response.send(data)
                } catch (error) {
                    // await trx.rollback()
                    console.log(error)
                    response.header('Content-type', 'application/json')
                    response.type('application/json')
                    let data = {
                        code: '4004',
                        message: 'Error',
                        data: error
                    }
                    return response.send(data)
                }
            } else {
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Channel cannot be found',
                    data: []
                }
                return response.send(data)
            }
        }
    }

    async delete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        let channel = await Channel.find(await MyHash.decrypt(formData.id))

        if (channel) {
            try {
                // let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                // channel.updated_by = auth.user.id,
                //     channel.updated_at = currentTime,
                //     channel.is_archived = 1
                // await channel.save()

                await channel.delete()

                // delete old channel_menu
                let cm = await ChannelMenu.query()
                    .where('channel_id', await MyHash.decrypt(formData.id))
                    .delete()

                //#region delete logo				
                if (!_lo.isEmpty(channel.logo)) {
                    let parts = channel.logo.split('/')
                    let url = parts.slice(3, parts.length)
                    url = url.join('/')

                    await FileService.delete(url);
                }

                //#endregion

                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '2000',
                    message: 'Channel success deleted',
                    data: []
                }
                return response.send(data)
            } catch (e) {
                console.log(e)
                response.header('Content-type', 'application/json')
                response.type('application/json')
                let data = {
                    code: '4004',
                    message: 'Channel cannot be deleted',
                    data: []
                }
                return response.send(data)
            }
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Channel cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async multidelete({ request, response, auth, session }) {
        let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        const formData = request.post()
        if (formData.totaldata != '0') {
            let dataitem = JSON.parse(formData.item)
            for (let i in dataitem) {
                let channel = await Channel.find(await MyHash.decrypt(dataitem[i]))
                try {
                    // let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
                    // channel.updated_by = auth.user.id,
                    //     channel.updated_at = currentTime,
                    //     channel.is_archived = 1
                    // await channel.save()
                    await channel.delete()
                    let cm = await ChannelMenu.query()
                        .where('channel_id', await MyHash.decrypt(dataitem[i]))
                        .delete()

                    //#region delete logo
                    if (!_lo.isEmpty(channel.logo)) {
                        let parts = channel.logo.split('/')
                        let url = parts.slice(3, parts.length)
                        url = url.join('/')
                        await FileService.delete(url);
                    }
                    //#endregion

                    // await channel.delete()
                } catch (e) { }
            }

            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '2000',
                message: 'Channel success deleted',
                data: []
            }
            return response.send(data)
        } else {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4004',
                message: 'Channel cannot be deleted',
                data: []
            }
            return response.send(data)
        }
    }

    async getChannelType({ request, response, auth, session }) {
        let req = request.get()
        let datas

        let checkAuth = await CheckAuth.get('Channel', 'read', auth)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        if (req.phrase != '') {
            datas = await ChannelType.query().select('id', 'name').where('name', 'LIKE', '%' + req.phrase + '%').andWhere('is_archived', '0').limit(20).fetch()
        } else {
            datas = await ChannelType.query().limit(20).fetch()
        }

        let data = datas.toJSON()

        let result = []
        for (let i in data) {
            result.push({
                value: data[i]['id'],
                label: data[i]['name']
            })
        }

        return response.send(result)
    }

    async getChannels({ request, response, auth, session }) {
        let datas
        let req = request.all()
        let checkAuth = await CheckAuth.get('Channel', 'read', auth, true)
        if (!checkAuth) {
            response.header('Content-type', 'application/json')
            response.type('application/json')
            let data = {
                code: '4003',
                message: 'User cannot access this module',
                data: []
            }
            return response.send(data)
        }

        // console.log(Number(req.offset))
        datas = Channel.query()
            .select('id', 'channel_name')
            .where('is_archived', '=', 0)
            .limit(20)
            .offset(Number(req.offset))

        if (req.phrase != '' && !_lo.isEmpty(req.phrase)) {
            datas.where('channel_name', 'LIKE', '%' + req.phrase + '%')
        }

        datas = await datas.fetch()
        let data = datas.toJSON()
        // console.log(data)

        let result = []
        for (let i in data) {
            result.push({
                value: data[i]['id'],
                label: data[i]['channel_name']
            })
        }
        let res = {}
        res.result = result
        res.has_more = _lo.isEmpty(result) ? false : true
        return response.send(res)
    }

    async updateStatus({ request, response, auth }) {
        const { id } = request.post()
        const idDecrypted = await MyHash.decrypt(id)
        const record = await Channel.find(idDecrypted)
        if (record != null) {
            Object.assign(record.$attributes, { status: !record.status })
            record.save()
            return response.defaultResponseJson(200, 'Success to update status channel')
        } else {
            return response.defaultResponseJson(400, 'Channel not found')
        }
    }

    async channelBanner({ params, response }) {
        const channelId = await MyHash.decrypt(params.id)
        const banners = await ChannelBanner.query().select('xid', 'image').where('channel_id', channelId).where('is_archived', '=', 0).orderBy('position', 'asc').fetch()
        return response.defaultResponseJson(200, 'Success to fetch banners', banners)
    }

    async updateBanner({ request, params, response }) {
        const channelId = await MyHash.decrypt(params.id)
        const { banners } = request.post()
        if (typeof banners != 'undefined' && banners.length > 0) {
            banners.map(async (banner, indexBanner) => {
                await ChannelBanner.query()
                    .where('xid', banner.xid)
                    .where('channel_id', channelId)
                    .update({
                        position: indexBanner + 1
                    })
            })
            return response.defaultResponseJson(200, 'Success to update banners')
        } else {
            return response.defaultResponseJson(400, 'Banners is required.')
        }
    }

    async channelQr({ params, response }) {
        let channelId = null
        try {
            channelId = await MyHash.decrypt(params.id)
        } catch (error) {
            channelId = params.id
        }

        const qr = await Channel.query()
            .select('xid','channel_name')
            .where('id', channelId)
            .where('is_archived', '=', 0)
            .first()
        return response.defaultResponseJson(200, 'Success to fetch QR', qr)
    }
}

module.exports = ChannelController