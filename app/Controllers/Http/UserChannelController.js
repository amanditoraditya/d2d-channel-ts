'use strict'

const Database 		= use('Database')
const fs 			= require('fs')
const path 			= require('path')
const Helpers 		= use('Helpers')
const _lo 			= use('lodash')
const Env 			= use('Env')
const Logger 		= use('Logger')
const moment		= require('moment');
const Hash 			= use('Hash')
const { v4: uuidv4 } = require('uuid');

const Channel 			= use('App/Models/Channel')
const UserChannel   	= use('App/Models/UserChannel')
const ChannelRoles   	= use('App/Models/ChannelRoles')
const UserChannelRole   = use('App/Models/UserChannelRole')

const MyHash 			= require('./Helper/Hash.js')
const Datatable 		= use('App/Helpers/Datatable')
const QueryBuilder 		= require('./Helper/DatatableBuilder.js')
const CheckAuth 		= require('./Helper/CheckAuth.js')

const { validate, validateAll } = use('Validator')

class UserChannelController {
	async datatable({request, response, auth, session}) {
		let admin_page = true
		let checkIsAdmin = await CheckAuth.get('UserChannel', 'read', auth, admin_page)

		const queryModel = UserChannel.query()
			.select(
				'user_channels.id', 
				'user_channels.xid', 
				'user_channels.channel_id',
				Database.raw(`concat(user_channels.first_name,' ', user_channels.last_name) as name`), 
				'user_channels.email', 
				'user_channels.block',
				'channels.channel_name',
				'user_channels.created_at'
			)
			.leftJoin('channels', 'channels.id', 'user_channels.channel_id' )

		const datatable = new Datatable(queryModel, request.all())

		datatable.setCustomFilter((model, searchKey) => {
			model.where('user_channels.is_archived', '0')
			if (checkIsAdmin == false) {
				model.where('user_channels.channel_id', auth.user.channel_id)
			}
		})

		datatable.setAdditionalColumn({
			'encrypted': async (data) => {
				return await MyHash.encrypt(data.id.toString())
			},
		})

		return response.json(await datatable.make())
	}
	
	async create({request, response, auth, session}) {
		let checkIsAdmin = await CheckAuth.get('UserChannel', 'create', auth, true)
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');

		let form = JSON.parse(request.post().data);
		let xid = uuidv4()
		let status = !form.block;
		
		let formData = {
			xid,
			channel_id: form.cid,
			email: form.email,
			password: form.password,
			first_name: form.first_name,
			last_name: form.last_name,
			block: status,
			is_archived: '0',
			updated_by: auth.user.id,
			updated_at: currentTime,
			created_by: auth.user.id,
			created_at: currentTime
		}
		
		let rules = {
			channel_id: 'required',
			first_name: 'required',
			last_name: 'required',
			email: 'required|email',
			password: 'required',
		}
		
		const validation = await validateAll(formData, rules)

		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: validation
			}
			return response.send(data)
		} else {
			try {
				let newUserChannel = await UserChannel.create(formData)
				let channel_roles = form.channel_role_id
				
				for (let row of channel_roles) {
					let xid = uuidv4()
					let channel_role_data = {
						xid				: xid,
						user_channel_id	: newUserChannel.id,
						channel_role_id	: row.value,
						status			: 1,
						is_archived		: 0,
						updated_by		: auth.user.id,
						updated_at		: currentTime,
						created_by		: auth.user.id,
						created_at		: currentTime
					}
					await UserChannelRole.create(channel_role_data)
				}
				
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User Channel added',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be added',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async edit({request, response, auth, session}) {
		let checkAuth 	 = await CheckAuth.get('UserChannel', 'update', auth, false)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		const formData = request.post()
		let userchannel = await UserChannel.query()
									.select('user_channels.*')
									.where('user_channels.id', await MyHash.decrypt(formData.id))
									.first()
		let get_roles = await userchannel.roles().where('user_channel_roles.is_archived', '0').fetch()
		let channel_roles_json = get_roles.toJSON()
		let channel_roles = []
		for(let i in channel_roles_json){
			channel_roles.push({
				value: channel_roles_json[i]['id'],
				label: channel_roles_json[i]['name']
			})
		}

		if (!_lo.isEmpty(channel_roles)) {
			userchannel.channel_roles = channel_roles
		}

		let get_channels = await userchannel.channels().where('channels.is_archived', '0').fetch()
		let channels_json = []

		if (!_lo.isEmpty(get_channels)) {
			channels_json = get_channels.toJSON()
			if (!_lo.isEmpty(channels_json)) {
				userchannel.channels = {
					value: channels_json['id'],
					label: channels_json['channel_name']
				}
			}
		} else {
			userchannel.channels = {
				value: '',
				label: 'No Data'
			}
		}
		
		if (userchannel) {
			userchannel.block = !userchannel.block;
			
			response.header('Content-type', 'application/json')
			response.header('Cache-Control', 'no-cache');
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User Channel found',
				data: userchannel
			}
			return response.send(data)
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be found',
				data: []
			}
			return response.send(data)
		}
	}
	
	async update({params, request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'update', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}
		
		let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
		let form = JSON.parse(request.post().data);
		let userChannel = await UserChannel.find(await MyHash.decrypt(params.id))
		let status = !form.block;

        let formData = {
			xid : form.xid,
			channel_id: form.cid,
			first_name: form.first_name,
			last_name: form.last_name,
			email: form.email,
			is_archived: form.is_archived,
			block: status,			
			updated_by: auth.user.id,
			updated_at: currentTime
		}
		
		let rules = {
			channel_id: 'required',
			first_name: 'required',
			last_name: 'required',
			email: 'required|email',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'Please fill required marker field!',
				data: validation
			}
			return response.send(data)
		} else {
			if (userChannel) {

				//if password empty keep existing password
				let password =  !_lo.isEmpty(form.password) ? form.password : userChannel.password
				formData.password = password

				let id = await MyHash.decrypt(params.id)
				Object.assign(userChannel.$attributes, formData)
				userChannel.save()

				let channel_roles = form.channel_roles;
				// delete old channel_menu
				let cr = await UserChannelRole.query()
							.where('user_channel_id', id)
							.fetch()
				cr = _lo.isEmpty(cr) ? [] : cr.toJSON()
				channel_roles = _lo.isEmpty(channel_roles) ? [] : channel_roles;

				for (let row of cr) {
					//role berkurang
					if (!channel_roles.some(item => item.value == row.channel_role_id)) {
						await UserChannelRole.query()
							.where('user_channel_id', id)
							.andWhere('channel_role_id', row.channel_role_id)
							.update({
								updated_by: auth.user.id,
								updated_at: currentTime,
								is_archived: 1
							})

						channel_roles = channel_roles.filter(function( obj ) {
							return obj.value != row.channel_role_id;
						});
					} 
				}
				// menu bertambah
				for (let row of channel_roles) {
					var cr_filtered = cr.filter(function(obj) {
						return obj.channel_role_id == row.value;
					});

					if (!_lo.isEmpty(cr_filtered)) {
						if (cr_filtered[0].is_archived == 1) {
							await UserChannelRole.query()
								.where('user_channel_id', id)
								.andWhere('channel_role_id', row.value)
								.update({
									is_archived		:0, 
									updated_by		: auth.user.id,
									updated_at		: currentTime
								})
						} 
					} else {
						let xid = uuidv4()
						let channel_role_data = {
							xid,
							user_channel_id	: id,
							channel_role_id	: row.value,
							is_archived		: 0,
							status			: 1,
							updated_by		: auth.user.id,
							updated_at		: currentTime,
							created_by		: auth.user.id,
							created_at		: currentTime
						}
						await UserChannelRole.create(channel_role_data)
					}
				}
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User Channel updated',
					data: []
				}
				return response.send(data)
			} else {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be found',
					data: []
				}
				return response.send(data)
			}
		}
	}
	
	async delete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		let userchannel = await UserChannel.find(await MyHash.decrypt(formData.id))
		if (userchannel){
			try {
				//soft delete => change is_archived to true
				let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
				userchannel.updated_by  = auth.user.id,
				userchannel.updated_at  = currentTime,
				userchannel.is_archived = 1
				await userchannel.save()

				// delete old user channel role
				let cm = await UserChannelRole.query()
							.where('user_channel_id', await MyHash.decrypt(formData.id))
							.update({ is_archived: 1 })

				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '2000',
					message: 'User Channel success deleted',
					data: []
				}
				return response.send(data)
			} catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be deleted',
					data: []
				}
				return response.send(data)
			}
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}
	
	async multidelete({request, response, auth, session}) {
		let checkAuth = await CheckAuth.get('Channel', 'delete', auth)
		if (!checkAuth) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4003',
				message: 'User cannot access this module',
				data: []
			}
			return response.send(data)
		}

		const formData = request.post()
		if (formData.totaldata != '0') {
			let dataitem = JSON.parse(formData.item)
			try {
				for (let i in dataitem) {
					let userchannel = await UserChannel.find(await MyHash.decrypt(dataitem[i]))
					//soft delete => change is_archived to true
					let currentTime = moment().format('YYYY-MM-DD kk:mm:ss');
					userchannel.updated_by  = auth.user.id,
					userchannel.updated_at  = currentTime,
					userchannel.is_archived = 1
					await userchannel.save()

					// delete old user channel role
					await UserChannelRole.query()
							.where('user_channel_id', await MyHash.decrypt(dataitem[i]))
							.update({ is_archived: 1 })
					} 
			}catch (e) {
				response.header('Content-type', 'application/json')
				response.type('application/json')
				let data = {
					code: '4004',
					message: 'User Channel cannot be deleted',
					data: []
				}
				return response.send(data)
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '2000',
				message: 'User Channel success deleted',
				data: []
			}
			return response.send(data)
			
		} else {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '4004',
				message: 'User Channel cannot be deleted',
				data: []
			}
			return response.send(data)
		}
	}

	async getChannelRoleList({request, response, auth, session}) {
		let req = request.all()
		let datas = [];

		if(_lo.isEmpty(req.channel_id) == false){
			if (req.phrase != '' && !_lo.isEmpty(req.phrase)) {
				datas = await ChannelRoles.query()
				.select('id', 'name')
				.where('name', 'LIKE', '%' + req.phrase + '%')
				.andWhere('channel_id', '=', req.channel_id)
				.andWhere('is_archived','=', 0)
				.andWhere('status', '=', 1)
				.limit(20).fetch()
			} else {
				datas = await ChannelRoles.query()
				.select('id', 'name')
				.where('channel_id', '=', req.channel_id)
				.andWhere('is_archived', '=', 0)
				.andWhere('status', '=', 1)
				.limit(20).fetch()
			}
		}
		
		let data = datas.toJSON()
		let result = []
		for(let i in data){
			result.push({
				value: data[i]['id'],
				label: data[i]['name']
			})
		}

		return response.send(result)
	}

	async updateStatus({ request, response, auth }) {
		const { id } = request.post()
		const idDecrypted = await MyHash.decrypt(id)
		const record = await UserChannel.find(idDecrypted)
		if (record != null) {
			Object.assign(record.$attributes, { block: !record.block })
			record.save()
			return response.defaultResponseJson(200, 'Success to update status channel')
		} else {
			return response.defaultResponseJson(400, 'Banner not found')
		}
	}
}

module.exports = UserChannelController