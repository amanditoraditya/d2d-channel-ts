const _ = require('lodash');
const API_REQUEST = require('./utils/api-request');
const Env = use('Env')
const CONTENTSVC_URL = Env.get('CONTENTSVC_URL')
const CONTENTSVC_VERSION = Env.get('CONTENTSVC_VERSION')
const BASE_URL_V2 = `${CONTENTSVC_URL}/${CONTENTSVC_VERSION}`;
// const SERVICE = 'Content';
// const jwt = require('jsonwebtoken')

const ContentService = {};

generateToken = (platform = 'android') => {
    // const payload = { keyAccess: 'f553eae2cad992cc501723b39143d96e' };
    // const token = jwt.sign(payload, '089B6BC6EDDBB1B7666133926B895B3A', { expiresIn: 360 });

    // return token;
    let result = ''
    const keyIos = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJmNTUzZWFlMmNhZDk5MmNjNTAxNzIzYjM5MTQzZDk2ZSIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuaW9zIiwicGxhdGZvcm0iOiJpb3MiLCJpYXQiOjE1MjIwNjgzOTR9.5EDnPOK-HB7H0TCyA3AcztC8aZc_VpkXr7dRxlL0IwQ';
    const keyAndroid = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBfaWQiOiJkNmM4OGUwZGE3MWFjNzFiNjU3MTg5NTI4MTZlZTMwMCIsInBhY2thZ2VfbmFtZSI6ImNvbS5kMmQuYW5kcm9pZCIsInBsYXRmb3JtIjoiYW5kcm9pZCIsImlhdCI6MTUyNDExMTQ3MX0.59_NSziZw1zm_QiqgpokdLIK8pOeOGz13OcRO9NQLxQ';

    result = platform == 'android' ? keyAndroid : keyIos;

    return result
}

ContentService.specialist = {
    /* GET ALL SPECIALIST
     ** uid          : String from firebase
     ** filter       : Array object with format --> [{key: 'skp', value: 'true'}]
     ** page         : Integer
     ** limit        : Integer
     ** headres      : Array object headers
     */
    getAll: async (filter = [], page = 1, limit = 20, countrycode = 'ID', headers = { platform: 'android' }) => {
        let result = [];

        try {            
            let TOKEN = generateToken();
            let URL = `specialist?page=${page}&limit=${limit}`;

            if (filter.length > 0) {
                for (let val of filter) {
                    URL += `&${val.key}=${val.value}`;
                }
            }

            _.assign(headers, { countrycode });
            console.log("--------- INTEGRATION TO CONTENT || SPECIALIST -------------")
            console.log("URL API : ", `${BASE_URL_V2}/${URL}`)
            console.log("HEADERS : ", headers)
            console.log("TOKEN : ", TOKEN)

            let record = await API_REQUEST(BASE_URL_V2, TOKEN, headers).get(URL);
            console.log("RESPONSE : ", record.data)
            console.log("--------- END INTEGRATION TO CONTENT || SPECIALIST -------------")
            if (record.data.acknowledge == false) {
                console.log('Error: ', record);
            }
            if (record.data.message == 'No data found!') {
                console.log('Error: ', record);
            }

            result = record.data.message == 'No data found!' ? [] : record.data.result;
        } catch (error) {
            result = error;
        }

        return result;
    },
}

module.exports = ContentService;
