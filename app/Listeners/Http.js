'use strict'

const Http = exports = module.exports = {}

Http.onStart = async () => {
	const Server = use('Adonis/Src/Server')
	Server.getInstance().timeout = 300000
	
	const View = use('View')
	View.global('hasRole', function (user_roles, role) {
		return ((user_roles) && (user_roles.indexOf(role) >= 0))
	})
}
