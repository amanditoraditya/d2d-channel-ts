'use strict'

const { ServiceProvider } = require('@adonisjs/fold')

class ResponseMacroProvider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    //
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    const Response = use('Adonis/Src/Response')
    Response.macro('defaultResponseJson', function (status, message, data = {}, codeMeta = null) {
      this.status(status).json({
        code: codeMeta != null ? codeMeta : status,
        message,
        data
      })
    })
  }
}

module.exports = ResponseMacroProvider
