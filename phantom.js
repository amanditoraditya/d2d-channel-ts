"use strict";

var page = require('webpage').create(),
	system = require('system'),
	count, address, output, size, pageWidth, pageHeight, renderTimeout;

if (system.args.length < 3 || system.args.length > 5) {
	phantom.exit();
} else {
	count = 0
	address = system.args[1];
	output = system.args[2];
	page.viewportSize = { width: 1280, height: 1024 };
	if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
		size = system.args[3].split('*');
		page.paperSize = size.length === 2 ? { width: size[0], height: size[1], margin: '0px' } : { format: system.args[3], orientation: 'portrait', margin: '1cm' };
	} else if (system.args.length > 3 && system.args[3].substr(-2) === "px") {
		size = system.args[3].split('*');
		if (size.length === 2) {
			pageWidth = parseInt(size[0], 10);
			pageHeight = parseInt(size[1], 10);
			page.viewportSize = { width: pageWidth, height: pageHeight };
			page.clipRect = { top: 0, left: 0, width: pageWidth, height: pageHeight };
		} else {
			pageWidth = parseInt(system.args[3], 10);
			pageHeight = parseInt(pageWidth * 3/4, 10);
			page.viewportSize = { width: pageWidth, height: pageHeight };
		}
	}
	if (system.args.length > 4) {
		page.zoomFactor = system.args[4];
	}
	
	page.onResourceRequested = function (req) {
		count += 1;
		clearTimeout(renderTimeout);
	};
	
	page.onResourceReceived = function (res) {
		if (!res.stage || res.stage === 'end') {
			count -= 1;
			if (count === 0) {
				renderTimeout = setTimeout(doRender, resourceWait);
			}
		}
	};

	page.open(address, function (status) {
		if (status !== 'success') {
			phantom.exit();
		} else {
			setTimeout(function () {
				page.render(output);
				phantom.exit();
			}, 5000);
		}
	});
}